//
//  notificationInfo.swift
//  Bean2Go
//
//  Created by AKS on 02/02/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import Foundation
class notificationInfo: NSObject {
    var notificationId = String()
    var notiUserType = String()
    var notificationMsg = String()
    var notificationOrderId = Int()
    var notificationOrderNumber = String()
    var notificationTimeAgo = String()
    var notificationTimeStamp = String()
    var notificationIsRead: Bool = false
}
