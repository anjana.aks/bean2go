//
//  recentOrdersInfo.swift
//  Bean2Go
//
//  Created by AKS on 02/02/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import Foundation

class RecentOrdersInfo: NSObject {
    var orderId = Int()
    var orderNumber = String()
    var orderAmount = String()
    var currency = String()
    var buyerName = String()
    var totalProducts = Int()
    var buyerContactNumber = String()
    var orderStatus = String()
    var orderDateTime = String()
    var orderTimeAgo = String()
    var orderAmountWithCurrency = String()
    var timestamp = String()
}
