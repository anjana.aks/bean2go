//
//  MyOrderInfo.swift
//  Bean2Go
//
//  Created by Anjana Aks on 11/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import Foundation

class orderInfo: NSObject {
    var id: Int = 0
    var orderNumber: String?
    var orderAmount: String?
    var currency: String = "R"
    var storeName: String?
    var storeStatus: String?
    var storeDateTime: String?
    var storePriceWithCurrency: String?
    var orderTax: String?
    var storeLat: String?
    var storeLng: String?
    var buyerName: String?
    var subProductList = [IngredientInfo]()
}

// Seller Detail
class SubProductsInfo: NSObject {
    var productName: String = ""
    var ammount: String = ""
    var quantityOfProduct: Int = 0
    var productComment: String = ""
    var productImage: String = ""
    var subProductList = [IngredientInfo]()
}

class ProductSellerDetailInfo: NSObject {
    var id: Int = 0
    var orderNumber: String?
    var orderAmount: String?
    var currency: String = "R"
    var storeName: String?
    var storeStatus: String?
    var storeDateTime: String?
    var storePriceWithCurrency: String?
    var orderTax: String?
    var storeLat: String?
    var storeLng: String?
    var buyerName: String?
    var orderStatusId: Int = 0
    var orderPin: String?
    var eachProductInfo = [SubProductsInfo]()
    var timestamp: String?
    var productComment: String?
    var rejectReason: String?
    var cancelReason: String?
    var scheduleForDate: String?
    var isSchedule: Bool = false
    var buyerLocationLat: String?
    var buyerLocationLng: String?
}
