//
//  CartCellData.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 09/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class CartCellData: NSObject {
    var cartId = Int()
    var productName = String()
    var productImage = String()
    var productQuantity = Int()
    var productCurrency = String()
    var productPrice = String()
    var optionsTotal = String()
    var productTotalAmount = String()
    var productTax = String()
    var productTotalAmountWithTax = String()
    var options = [IngredientInfo]()
    var productComment = String()
}
