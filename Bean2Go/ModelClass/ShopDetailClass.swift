//
//  ShopDetailClass.swift
//  Bean2Go
//
//  Created by Aks on 06/12/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation

class ShopDetailClass: NSObject {
    var storeId: Int?
    var storeContactInfo: String?
    var storeAddress: String?
    var vehicleNumber: String?
    var parkingAvailable: Bool = false
    var storeOpenAt: String?
    var storeCloseAt: String?
    var storeRating: String?
    var storeDescription: String = ""
    var storeShopName: String?
    var storeIsFav: Bool = false
    var lastOrderCompleted: Bool = true
    var cartStatus: Int = 0   // (0 for empty cart,1 for same store, 2 for different store)
    var storeImagesArray: [StoreImagesClass] = []
    var storeReviewArray: [StoreReviewClass] = []
    var storeCoverImg: String?
    var storeShortDescription: String = ""
    var isRestaurantOpen: Bool = true
}

class StoreReviewClass: NSObject {
    var reviewUserName: String?
    var reviewRating: String?
    var reviewComment: String?
    var reviewUserImageUrl: String?
}

class StoreImagesClass: NSObject {
    var storeImageUrl: String?
    var storeThumbImageUrl: String?
}
