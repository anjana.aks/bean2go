//
//  UserProfile.swift
//  Bean2Go
//
//  Created by AKS on 15/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation
import UIKit



class UserDetails: NSObject {
    var userName: String?
    var userEmail: String?
    var userPassword: String?
    var userOldPassword: String?
    var userNewPassword: String?
    var userConfirmPassword: String?
    var userProfileImage: String?
    var userContactNo: String?
    var userID: Int?
    var userType: String?
    var userComment: String?
    var otp: String?
    var isRemember: Bool = false
    var countryCode:String = "+91"
}
