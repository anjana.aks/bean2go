//
//  IngredientInfo.swift
//  Bean2Go
//
//  Created by AKS on 11/12/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation

class ProductInfo: NSObject {
    var optionType: String = ""
    var options = [IngredientInfo]()
}

class IngredientInfo: NSObject {
    var option_name = String()
    var option_price = String()
    var optionType = String()
    var product_option_id = Int()
    var optionImage = String()
    var optionalQuantity = Int()
    var optionComment = String()
    var isSelected: Bool = false
}

class SellerProductInfo: NSObject {
    
}
