//
//  FavInfoClass.swift
//  Bean2Go
//
//  Created by AKS on 11/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import Foundation
class FavInfoClass: NSObject{
    var store_id = Int()
    var store_name = String()
    var store_address = String()
    var available = String()
    var ratings = String()
    var store_cover = String()
}
