//
//  categoryInfo.swift
//  Bean2Go
//
//  Created by AKS on 11/12/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation

class categoryInfo: NSObject{
    var product_id = String()
    var store_product_id = String()
    var product_name = String()
    var product_description = String()
    var product_price = String()
    var product_price_display = String()
    var product_image = String()
}
