//
//  LocationInfo.swift
//  Bean2Go
//
//  Created by Aks on 06/12/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation

class PlaceInfo: NSObject {
    
    var storeId: Int?
    var storeName: String?
    var storeContactNumber: String?
    var storeAddress: String?
    var storeDistance: String?
    var storeVehiNumber: String?
    var storeParkingAvailable: Bool = false
    var lat: String?
    var lon: String?
    var isSelected: Bool = false
    var notificationCount: Int?
    var isRestaurantOpen: Bool = true
}
