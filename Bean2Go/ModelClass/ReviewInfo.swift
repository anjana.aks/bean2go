//
//  ReviewInfo.swift
//  Bean2Go
//
//  Created by AKS on 24/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation
import UIKit

class ReviewInfo: NSObject {
    var reviewUserProfileImage: String?
    var reviewUserId: String?
    var reviewRating: String?
    var reviewRatingInFloat: CGFloat = 0.0
    var reviewDesc: String?
}
