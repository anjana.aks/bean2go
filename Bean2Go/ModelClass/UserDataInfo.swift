//
//  UserDataInfo.swift
//  Bean2Go
//
//  Created by AKS on 21/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class BCUserInfo: NSObject {
    
    var username: String?
    var name = String()
    var distributor_type = String()
    var userID = String()
    var email = String()
    var message = String()
    var phoneNumber = String()
    
    var landLineNumber = String()
    var password = String()
    var confirmPassword = String()
    var profileImage = UIImage()
    var profileImageURL = String()
    var role = String()
    var profileImageData = Data()
    var noData = Data()
    var currentAvailablePoints = String()
    var eligibleForRedemption = Bool()
    var distributorFirstLoginComplete = Bool()
    var totalSellInPoints = String()
    var totalSellOutPoints = String()
    var totalPointsEarned = String()
    var totalPointsRedeemed = String()
    var totalPointsAvailable = String()
    var distributorId = String()
}

