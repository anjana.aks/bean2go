//
//  orderDetailsViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 05/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import GoogleMaps

class orderDetailsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var ResturantNameLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var paymentStatusLbl: UILabel!
    @IBOutlet weak var paymentModeLbl: UILabel!
    @IBOutlet weak var totalAmountPaidLbl: UILabel!
    @IBOutlet weak var billingView: UIView!
    @IBOutlet weak var confirm: UIButton!
    
    @IBOutlet weak var ingredeintView: UIView!
    @IBOutlet weak var viewDetailView: UIView!
    @IBOutlet weak var superCommentStackView: UIStackView!
    @IBOutlet weak var ingredientViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var usercommentTextview: UITextView!
    @IBOutlet weak var usercommentHeightconstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelOrderView: UIView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var viewDetailBackBtn: UIButton!
    
    // cancelOrder Outlet
    @IBOutlet weak var reasonToCancelOrderTextView: UITextView!
    @IBOutlet weak var reasonToCancelView: UIView!
    @IBOutlet weak var cancelOrderParentView: UIView!
    
    var orderNumber: String?
    var orderID: Int = 0
    var notificationId = String()
    
    var productDetail = ProductSellerDetailInfo()
    var optionDetail = [IngredientInfo]()
    var subtotal = 0
    var userType:String?
    let minHeight: CGFloat = 50.0
    let maxHeight: CGFloat = 150.0
    var timer: Timer = Timer()
    var remarkMaxheight:CGFloat = 100.0
    var remarkMinheight:CGFloat = 30.0
    var remarkHeight:CGFloat = 30.0
    
    var locationManager = CLLocationManager()
    var coordinates = CLLocation()
    var isComingFromNotification: Bool = false
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        userType = USER_DEFAULTS.value(forKey: USER_TYPE) as? String
        
        self.usercommentTextview.layer.addborder(width: 2, color: UIColor.lightGray)
        self.usercommentTextview.layer.cornerRadius = 5
        self.usercommentTextview.setContentOffset(CGPoint.zero, animated: false)
        usercommentTextview.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        self.reasonToCancelOrderTextView.sizeToFit()
        self.reasonToCancelView.layer.addborder(width: 2, color: UIColor.lightGray)
        self.reasonToCancelView.layer.cornerRadius = 5
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshScreen(notification:)), name: NSNotification.Name(rawValue: "refreshBuyerOrderDetail"), object: nil)
//
        callAPItoGetOrderDetail()
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.bottomView.shadowApply(shadowRadius: 10)
        self.bottomView.topShadow(shadowRadius: 2, color: .lightGray)
        
        
        self.getUserCurrentLocation()
        
    }
    
    @objc func refreshScreen(notification: NSNotification) {
      
//        let dict = notification.object as? [String: Any]
//        self.orderNumber = dict?["orderNumber"] as? String ?? ""
//        self.orderID = dict?["orderID"] as? Int ?? 0
//        self.notificationId = dict?["notiId"] as? String ?? ""
//        
//        print(notification.userInfo)
        isComingFromNotification = true
        callAPItoGetOrderDetail()
      
    }
    
    // MARK:- Set up Initial Value
    fileprivate func setUpInitailView() {
    }
    
    //MARK:- Calculate the total amount of product with tax
    fileprivate func setTotalAmountOfProduct() {
        
        let orderAmount = (self.productDetail.orderAmount! as NSString).floatValue
        let percentageOfProduct = (self.productDetail.orderTax! as NSString).floatValue
        let percentageAmoutOfProduct = (orderAmount ?? 0) * ((percentageOfProduct ?? 1) / 100)
        
        let percentage2DigitAfterDecimal = String(format: "%.2f", percentageAmoutOfProduct)
        
        let productAmountWithTax = percentageAmoutOfProduct + orderAmount
        
        subTotalLbl.text = self.productDetail.currency + " \(self.productDetail.orderAmount!)"
        taxLbl.text = self.productDetail.currency + " \(percentage2DigitAfterDecimal)"
        totalAmountPaidLbl.text = self.productDetail.currency + " \(productAmountWithTax)"
        
        
    }
    
    
    //MARK:- Button Action
    
    @IBAction func cancelOrderBtnDidTapped(_ sender: UIButton) {
        self.cancelOrderParentView.isHidden = false
    }
    
    @IBAction func hideCancelView(_ sender: UIButton) {
        self.cancelOrderParentView.isHidden = true
    }
    
    @IBAction func submitCancelOrderBtn(_ sender: UIButton) {
        if self.reasonToCancelOrderTextView.text == "Reason" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please give a reason", buttonTitle: "OK"), animated: true, completion: nil)
        } else {
            self.cancelOrderAPI()
        }
    }
    
    @IBAction func backButtonDidTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func locationButtonDidTapped(_ sender: UIButton) {
        let nVC = UIStoryboard(name:"Main" , bundle:nil).instantiateViewController(withIdentifier: "navigationViewController") as! navigationViewController
        
        nVC.destinationLat = Double(self.productDetail.storeLat!)!
        nVC.destinationLng = Double(self.productDetail.storeLng!)!
        
        nVC.originLng = coordinates.coordinate.longitude
        nVC.originLat = coordinates.coordinate.latitude
        
        nVC.roomName = self.orderNumber
        
        nVC.destinationName = self.productDetail.storeName ?? "Cafe coffee day."
        
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @IBAction func runBtnDidTapped(_ sender: UIButton) {
        //        if userType == "User"{
        //            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        //
        //            self.navigationController?.pushViewController(nVC, animated: true)
        //        }
        
    }
    @IBAction func acceptBtnTapped(_ sender: UIButton) {
        //        if userType == "User"{
        //            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        //
        //            self.navigationController?.pushViewController(nVC, animated: true)
        //        }else{
        //            self.navigationController?.popViewController(animated: true)
        //        }
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func viewDetailButtonTapped(_ sender: UIButton) {
        self.viewDetailView.isHidden = false;
        self.viewDetailBackBtn.tag = sender.tag
        setIngredientData(atIndex: sender.tag)
    }
    
    @IBAction func viewDetailBackButtonDidTapped(_ sender: UIButton) {
        self.viewDetailView.isHidden = true;
        self.removeIngredientData(atIndex: sender.tag)
    }
    
    fileprivate func removeIngredientData(atIndex: Int) {
        
        let allViews = self.ingredeintView.subviews
        if allViews.count != 0 {
            for view in allViews {
                if view.isKind(of: cartItemDetailRowClass.self) {
                    view.removeFromSuperview()
                }
            }
        }
        
        superCommentStackView.isHidden = true
        usercommentTextview.layoutIfNeeded()
        
    }
    
    //MARK:- Set ingredient data on view
    fileprivate func setIngredientData(atIndex: Int){
        
        var optionDetailList = productDetail.eachProductInfo[atIndex].subProductList
        
        self.productNameLabel.text = productDetail.eachProductInfo[atIndex].productName
        
        for i in 0...optionDetailList.count - 1 {
            let ingredientViewObj = cartItemDetailRowClass()
            let ingredintInfo = optionDetailList[i] as IngredientInfo
            
            ingredientViewObj.frame = CGRect(x: 0, y: 25 * i, width: Int(self.ingredeintView.bounds.width), height: 25)
            
            ingredientViewObj.addValue(heading: "\(ingredintInfo.optionType)", value: "\(ingredintInfo.option_name) ")
            
            self.ingredeintView.addSubview(ingredientViewObj)
            
        }
        
        let comment = productDetail.eachProductInfo[atIndex].productComment
        if comment != "" {
            superCommentStackView.isHidden = false
            usercommentTextview.text = comment
            usercommentTextview.isEditable = false
            
            if usercommentTextview.contentSize.height >= maxHeight || usercommentTextview.contentSize.height > minHeight {
                //self.usercommentTextview.isScrollEnabled = true
                self.usercommentHeightconstraint.constant = maxHeight
            }
            else{
                
                self.usercommentHeightconstraint.constant = minHeight
            }
            usercommentTextview.layoutIfNeeded()
        }
        
        // needs to be put in condition
        
        self.ingredientViewHeightConstraint.constant = CGFloat(25*(optionDetailList.count))
        //self.ingredientViewHeightConstraint.constant = CGFloat(25*productOptions.options.count)
        
    }
    
    //MARK:-  timer Functions
    
    func startTimer(){
        //let currentTimestamp = currentDate.toMillis()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(sellerOrderDetailVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    
    func calculateTime() -> String {
        
        let dateFormatter = DateFormatter()
        
        let userCalendar = Calendar.current
        
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startTime = Date()
        
        //let startDateTime = startTime.addingTimeInterval(TimeInterval(5.0 * 60.0))
        
        guard let dateInString = self.productDetail.timestamp else {
            return "Invalidate"
        }
        
        if dateInString == "" {
            return "Invalidate"
        }
        
        let endTime = dateFormatter.date(from: dateInString)
        //        let endDateTime = endTime!.addingTimeInterval(TimeInterval(15.5 * 60.0))
        
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime, to: endTime!)
        
        //        var diffrece = "\(timeDifference.month) Months \(timeDifference.day) Days \(timeDifference.minute) Minutes \(timeDifference.second) Seconds"
        
        // if seconds goes to negative then return 0:0
        if ((timeDifference.second)!) < 0 {
            return "0:0"
        }
        // if seconds less than 10 then add 0 before seconds
        if ((timeDifference.second)!) < 10 {
            let stringToShowInMinTime = "\((timeDifference.minute)!):0\((timeDifference.second)!)"
            return stringToShowInMinTime
        }
        
        let stringToShowInMinTime = "\((timeDifference.minute)!):\((timeDifference.second)!)"
        return stringToShowInMinTime
    }
    
    @objc func updateTimer() {
        self.collectionView.reloadData()
    }
}


extension orderDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productDetail.eachProductInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderDetailsCell", for: indexPath) as! orderDetailsCell
        
        let orderInfo: SubProductsInfo = productDetail.eachProductInfo[indexPath.item]
        
        cell.shadow(UIColor.lightGray)
        //cell.cardView.layer.cornerRadius = 2
        cell.itemImgView.layer.cornerRadius = 10
        cell.itemImgView.clipsToBounds = true
        
        cell.viewDetailButton.tag = indexPath.row
        cell.itemNameLbl.text = orderInfo.productName
        cell.amountLbl.text = productDetail.currency + " " + orderInfo.ammount
        cell.quantityLbl.text = "\(orderInfo.quantityOfProduct)"
        cell.itemImgView.af_setImage(withURL: URL(string: orderInfo.productImage)!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 10, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView: orderDetailHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HedderView", for: indexPath) as! orderDetailHeaderView
            headerView.shopName.text = productDetail.storeName
            headerView.timeLbl.text = productDetail.storeDateTime
            headerView.statusLbl.text = productDetail.storeStatus
            headerView.amountLbl.text = productDetail.storePriceWithCurrency
            
            let orderStatusId = productDetail.orderStatusId
            
            switch orderStatusId {
            case 1: // Order Create
                headerView.navigationButton.isHidden = true
                self.cancelOrderView.isHidden = true
                
                let timeString = self.calculateTime()
                if timeString == "0:0" {
                    invalidateTimeAndCallAutoRejectAPI()
                    //                    headerView.timmerLbl.text = timeString
                    
                } else if timeString == "Invalidate" {
                    
                } else {
                    startTimer()
                }
                
                
                
                break
                
            case 2:// Payment Processing
                self.cancelOrderView.isHidden = true
                break
                
            case 3:// Payment Failed
                self.cancelOrderView.isHidden = true
                break
                
            case 4:// Payment Completed
                //                headerView.timmerView.isHidden = true
                headerView.navigationButton.isHidden = true
                self.cancelOrderView.isHidden = true
                
                let timeString = self.calculateTime()
                if timeString == "0:0" {
                    invalidateTimeAndCallAutoRejectAPI()
                    //                    headerView.timmerLbl.text = timeString
                    
                } else if timeString == "Invalidate" {
                    
                } else {
                    startTimer()
                }
                
                break
            case 5:// Order Reject
                //                headerView.timmerView.isHidden = true
                headerView.navigationButton.isHidden = true
                headerView.remarkStackview.isHidden = false
                headerView.remarkTxtView.text = self.productDetail.rejectReason
                
                if headerView.remarkTxtView.contentSize.height > maxHeight {
                    remarkHeight = remarkMaxheight
                    headerView.remarkTxtViewHeightConstraint.constant = remarkMaxheight
                    headerView.remarkTxtView.isUserInteractionEnabled = true
                } else {
                    remarkHeight = remarkMinheight
                    headerView.remarkTxtViewHeightConstraint.constant = remarkMinheight
                    headerView.remarkTxtView.isUserInteractionEnabled = false   // isscrollenable moves the textview content a bit toward bottom
                }
                self.collectionView.collectionViewLayout.invalidateLayout()
                self.cancelOrderView.isHidden = true
                break
            case 6:// Order Accept
                //                headerView.timmerView.isHidden = true
                headerView.navigationButton.isHidden = false
                self.cancelOrderView.isHidden = false
                headerView.otpLbl.text = productDetail.orderPin
                headerView.otpView.isHidden = false
                headerView.otpView.layer.borderColor = (UIColor.white).cgColor
                headerView.otpView.layer.borderWidth = 2
                headerView.otpView.layer.cornerRadius = 5
                
                
                break
            case 7: // Auto Declined
                //                headerView.timmerView.isHidden = true
                headerView.navigationButton.isHidden = true
                self.cancelOrderView.isHidden = true
                
                break
            case 8:// Cancelled
                //                headerView.timmerView.isHidden = true
                headerView.navigationButton.isHidden = true
                self.cancelOrderView.isHidden = true
                break
            case 9:// recieved
                //                headerView.timmerView.isHidden = true
                headerView.navigationButton.isHidden = true
                self.cancelOrderView.isHidden = true
                
                if isComingFromNotification {
                    if let navController = self.navigationController {
                        for controller in navController.viewControllers {
                            if controller is DashBoardViewController { // Change to suit your menu view controller subclass
                                navController.popToViewController(controller, animated:true)
                                break
                            }
                        }
                    }
                }
                
                break
            default:
                break
                
            }
            
            return headerView
            
        case UICollectionView.elementKindSectionFooter:
            return UICollectionReusableView()
            
        default:
            
            assert(false, "Unexpected element kind")
            return UICollectionReusableView()
        }
        
     }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let orderStatusId = productDetail.orderStatusId
        if orderStatusId == 5 {
            if self.productDetail.rejectReason != ""{
                return CGSize(width: collectionView.frame.size.width, height: CGFloat(130 + remarkHeight))
            }
        }
        // ofSize should be the same size of the headerView's label size:
        return CGSize(width: collectionView.frame.size.width, height: 130)
    }
    
    
}

//MARK:- CALL WEB API
extension orderDetailsViewController {
    // MARK:- Get order detail
    func callAPItoGetOrderDetail() {
        
        let paraDict = NSMutableDictionary()
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "buyer/orders/details/\(self.orderNumber!)/\(self.orderID)?notiId=\(notificationId)") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    self.collectionView.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                self.collectionView.isHidden = true
                return
            } else {
                
                guard let productInfo = response?["data"] as? NSDictionary else {
                    return
                }
                
                self.productDetail.id = productInfo["id"] as? Int ?? 0
                self.productDetail.orderNumber = productInfo["order_number"] as? String ?? ""
                self.productDetail.orderAmount = productInfo["order_amount"] as? String ?? ""
                self.productDetail.orderTax = productInfo["tax"] as? String ?? ""
                self.productDetail.currency = productInfo["currency"] as? String ?? ""
                self.productDetail.storeName = productInfo["store_name"] as? String ?? ""
                self.productDetail.storeStatus = productInfo["status"] as? String ?? "Waiting for approval."
                self.productDetail.storeDateTime = productInfo["date_time"] as? String ?? "0.0"
                self.productDetail.orderStatusId = productInfo["status_id"] as? Int ?? 0
                self.productDetail.orderPin = productInfo["otp"] as? String ?? ""
                self.productDetail.rejectReason = productInfo["reject_reason"] as? String ?? ""
                self.productDetail.timestamp = productInfo["timestamp_available_upto"] as? String ?? ""
                
                if let orderAmount = self.productDetail.orderAmount {
                    self.productDetail.storePriceWithCurrency = "\(self.productDetail.currency) \(orderAmount)"
                }
                
                guard let location = productInfo["location"] as? NSDictionary else {
                    return
                }
                
                SocketManagerClass.shared.eventsHandleInSocket(roomName: self.productDetail.orderNumber, id: self.productDetail.orderStatusId)
                
                self.productDetail.storeLat = location["lat"] as? String ?? "0.0"
                self.productDetail.storeLng = location["lng"] as? String ?? "0.0"
                let subProductArray = productInfo["order_products"] as? NSArray ?? []
                //self.productDetail?.eachProductInfo = []
                
                if subProductArray.count != 0 {
                    
                    for order in subProductArray {
                        let dict = order as? NSDictionary
                        let orderDetail = SubProductsInfo()
                        
                        orderDetail.productName = dict?["product_name"] as? String ?? ""
                        orderDetail.ammount = dict?["ammount"] as? String ?? ""
                        orderDetail.quantityOfProduct = dict?["qty"] as? Int ?? 0
                        orderDetail.productComment = dict?["comment"] as? String ?? ""
                        orderDetail.productImage = dict?["product_image"] as? String ?? ""
                        let optionsArray = dict?["options"] as? NSArray ?? []
                        
                        if optionsArray.count != 0 {
                            for option in optionsArray {
                                let optionDict = option as? NSDictionary
                                let optionDetail = IngredientInfo()
                                optionDetail.option_name = optionDict?["option_name"] as? String ?? ""
                                optionDetail.option_price = optionDict?["option_price"] as? String ?? ""
                                optionDetail.optionType = optionDict?["option_type"] as? String ?? ""
                                orderDetail.subProductList.append(optionDetail)
                            }
                        }
                        
                        self.productDetail.eachProductInfo.append(orderDetail)
                    }
                    self.collectionView.reloadData()
                    
                } else {
                    let alertController = UIAlertController(title: "", message: "No data found.", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        //self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                self.totalAmountPaidLbl.text = self.productDetail.currency + " \(self.productDetail.orderAmount!)"
                
                //self.setTotalAmountOfProduct()
                //self.collectionView.isHidden = true
            }
        }
    }
    
    // MARK:- Auto reject API
    fileprivate func autoRejectAPI() {
        
        let paraDict = NSMutableDictionary()
        paraDict["order_number"] = self.productDetail.orderNumber
        paraDict["order_id"] = self.productDetail.id
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/orders/autoReject") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    self.collectionView.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                self.collectionView.isHidden = true
                return
            } else {
                
                guard let dataInfo = response?["data"] as? NSDictionary else {
                    return
                }
                
                guard let msg = dataInfo["message"] as? String else {
                    return
                }
                
                SocketManagerClass.shared.eventsHandleInSocket(roomName: self.orderNumber, id: AUTO_DECLINED_ID)
                
                let alertController = UIAlertController(title: "Time Up !!", message: msg, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                    self.collectionView.reloadData()
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }
    
    //MARK:- Cancel Order
    fileprivate func cancelOrderAPI() {
        
        let paraDict = NSMutableDictionary()
        paraDict["order_number"] = self.productDetail.orderNumber
        paraDict["order_id"] = self.productDetail.id
        paraDict["reason"] = productDetail.productComment
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/orders/cancelMyOrder") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.cancelOrderParentView.isHidden = true
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                return
            } else {
                
                guard let dataInfo = response?["data"] as? NSDictionary else {
                    return
                }
                
                guard let msg = dataInfo["message"] as? String else {
                    return
                }
                
                
                SocketManagerClass.shared.eventsHandleInSocket(roomName: self.orderNumber, id: CANCELLED_ID)
                
                
                let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
//                    self.cancelOrderParentView.isHidden = true
//                    self.collectionView.reloadData()
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }
    
    // MARK:- Auto Reject
    func invalidateTimeAndCallAutoRejectAPI() {
        timer.invalidate()
        //seconds = 0
        //self.autoRejectAPI()
    }
}

extension orderDetailsViewController {
    
    //MARK:- SET MAp Initial
    func getUserCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self as! CLLocationManagerDelegate
        
    }
}

extension orderDetailsViewController: CLLocationManagerDelegate {
    //MARK:- GET current location of user
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        coordinates = location
    }
    
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
        //googleMapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

extension orderDetailsViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Reason" {
            textView.text = ""
            textView.textColor = .black
        } else {
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = .gray
            textView.text = "Reason"
        }
    }
    
    //To resign first responder on done click
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (range.length == 0) {
            
            productDetail.productComment = trimWhiteSpace(str: textView.text)
            
            if text == "\n" {
                textView.resignFirstResponder()
                return false;
            }
        }
        return true;
    }
}

extension orderDetailsViewController: refreshOrderDetailScreen {
    
    func refreshOrderDetailScreenSocket() {
        self.callAPItoGetOrderDetail()
    }
}
