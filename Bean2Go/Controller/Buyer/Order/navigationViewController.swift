//
//  navigationViewController.swift
//  Bean2Go
//
//  Created by AKS on 15/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import GoogleMaps

class navigationViewController: UIViewController, ARCarMovementDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    
    var moveMent: ARCarMovement!
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    var gmsMarkerArray = [MarkerView]()
    var currentLocation: CLLocation?
    var oldLocation: CLLocation?
    var zoomLevel: Float = 15.0
    var placesArray = [PlaceInfo]()
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    var coordinates = CLLocation()
    
    var destinationLat: Double = 0.0
    var destinationLng: Double = 0.0
    
    var originLat: Double = 0.0
    var originLng: Double = 0.0
    
    var destinationMarker = GMSMarker()
    var new_driverMarker = GMSMarker()
    var oldCoordinate = CLLocationCoordinate2D()
    var newCoordinate = CLLocationCoordinate2D()
    var roomName: String?
    
    
    var selectedRoute: Dictionary<String, AnyObject> = Dictionary()
    var overviewPolyline: Dictionary<String, AnyObject> = Dictionary()
    
    var path: GMSPath = GMSPath()
    var oldPolylineArr = [GMSPolyline]()
    var arrayOfPath = [GMSPath]()
    
    var destinationName = String()
    
    var iTemp:Int = 0
    var timer = Timer()
    
    var fraction: Float = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpMapViewInitial()
        oldCoordinate = CLLocationCoordinate2D(latitude: self.originLat, longitude: self.originLng)

        self.bottomView.shadow(UIColor.lightGray)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
        let destinationLocation = CLLocationCoordinate2D(latitude: self.destinationLat, longitude: self.destinationLng)
        

        destinationMarker.position = destinationLocation

        destinationMarker.map = googleMapView
        
        destinationMarker.title = destinationName
        
        
        self.getDirections(origin: "\(self.originLat),\(self.originLng)", destination:
        "\(destinationLat),\(destinationLng)", waypoints: nil, travelMode: "driving" as AnyObject) { [unowned self](status, success, distance) -> Bool in
            if success! {
                

                return false
            }
            else {
                //isReturn = true
                return true
                // print(status!)
            }
        }
    }
    
    // MARK:- TEST
    fileprivate func readLatLongFromFile() {
        if let path = Bundle.main.path(forResource: "LocationData", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let person = jsonResult["Locations"] as? [Any] {
                    // do stuff
                }
            } catch {
                // handle error
            }
        }
    }
    
    
    // Mark:- Button Click
    
    @IBAction func backButtonDidTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension navigationViewController {
    //MARK:- SET MAp Initial
    func setUpMapViewInitial() {
        
        moveMent = ARCarMovement()
        moveMent.delegate = self

        
        self.googleMapView.mapType = .normal

        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.activityType = .automotiveNavigation
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
      
    }

    
    // MARK:- Direction API
    func getDirections(origin: String!, destination: String!, waypoints: Array<String>!, travelMode: AnyObject!, completionHandler: @escaping ((_ status: String?, _ success: Bool?, _ distance: String?) -> Bool)) {
        if origin != nil {
            if destination != nil {
                // var directionsURLString = baseURLDirections + "origin=" + origin + "&destination=" + destination
                //"&mode=walking"
                
                let directionsURLString = "\(baseURLDirections)origin=\(origin!)&destination=\(destination!)&mode=driving&sensor=false&mode=car&key=AIzaSyD71EOMcYPgpUlI6qOe7LtWm6PC3hc_Rac" //&units=metric
                
                //print(directionsURLString)
                directionsURLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                
                if let directionsURL = URL(string: directionsURLString){
                    
                    if Thread.isMainThread {
                        
                        calculateDistance(directionsURL, completionHandler)
                    } else {
                        DispatchQueue.main.sync {
                            calculateDistance(directionsURL, completionHandler)
                        }
                    }
                }
                
            }
            else {
                _ = completionHandler("Destination is nil.", false, "")
            }
        }
        else {
            _ = completionHandler("Origin is nil", false,"")
        }
    }
    
    //MARK:- getDirections()
    fileprivate func calculateDistance(_ directionsURL: URL, _ completionHandler: ((String?, Bool?, String?) -> Bool)) {
        if let directionsData = NSData(contentsOf: directionsURL){
            do {
                let dictionary = try JSONSerialization.jsonObject(with: directionsData as Data, options: JSONSerialization.ReadingOptions.mutableContainers)
                let status = (dictionary as! Dictionary<String, AnyObject>)["status"] as! String
                
                if status == "OK" {
                    self.selectedRoute = ((dictionary as! Dictionary<String, AnyObject>)["routes"] as! Array<Dictionary<NSObject, AnyObject>>)[0] as! Dictionary<String, AnyObject>
                    self.overviewPolyline = (self.selectedRoute)["overview_polyline"] as! Dictionary<NSObject, AnyObject> as! Dictionary<String, AnyObject>
                    
                    let legs = (selectedRoute)["legs"] as! Array<Dictionary<String, AnyObject>>
                    
                    var totalDistanceInMeters: UInt = 0
                    var totalDurationInSeconds: UInt = 0
                    
                    for leg in legs {
                        totalDistanceInMeters += (leg["distance"] as! Dictionary<String, AnyObject>)["value"] as! UInt
                        totalDurationInSeconds += (leg["duration"] as! Dictionary<String, AnyObject>)["value"] as! UInt
                    }
                    
                    let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
                    let totalDistance = "\(distanceInKilometers) Km"
                    
                    let mins = totalDurationInSeconds / 60
                    let totalDuration = "\(mins) min"
                   
                    distanceLbl.text = totalDistance
                    timeLbl.text = totalDuration

                    _ = completionHandler(status, true, totalDistance)
                }
                else {
                    _ = completionHandler(status, false, "")
                }
            }
            catch {
                print(error)
                _ = completionHandler("", false, "")
            }
        }
    }
}

// MARK:- CLLocation Manger Delegate
extension navigationViewController: CLLocationManagerDelegate {
    
    //MARK:- GET current location of user
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
       // locationManager.startUpdatingLocation()
        
        //5
        //googleMapView.isMyLocationEnabled = true
//        googleMapView.settings.myLocationButton = true
    }
    
    //MARK:- clearRoute()
    func clearRoute() {
        if oldPolylineArr.count != 0 {
            for p in (0 ..< oldPolylineArr.count) {
                oldPolylineArr[p].map = nil
            }
        }
        
        if self.arrayOfPath.count != 0 {
            arrayOfPath = []
        }
        
    }
    
    func drawRoute() {
        //showToast(message: "new path found")
        
        let route = self.overviewPolyline["points"] as! String
        self.path = GMSPath(fromEncodedPath: route)!
        
        let path = GMSPath(fromEncodedPath: route)!
        let routePolyline = GMSPolyline(path: path)
        routePolyline.map = self.googleMapView
        routePolyline.strokeWidth = 5
        routePolyline.strokeColor = UIColor.darkGray
        routePolyline.geodesic = true
        self.arrayOfPath.append(path)
        self.oldPolylineArr.append(routePolyline)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else {
            return
        }
        
        let newCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(coordinates.coordinate.latitude, coordinates.coordinate.longitude)
        
        coordinates = location
        
        if oldLocation != nil {
            // if oldLocation?.coordinate.latitude != location.coordinate.latitude {
            
            oldCoordinate = CLLocationCoordinate2DMake(coordinates.coordinate.latitude, coordinates.coordinate.longitude)
          
            // Create a GMSCameraPosition that tells the map to display the marker
            
            let camera = GMSCameraPosition.camera(withLatitude: coordinates.coordinate.latitude, longitude: coordinates.coordinate.longitude, zoom: Float(zoomLevel), bearing: 0, viewingAngle: 0)
            googleMapView.camera = camera
            //            mapView.isMyLocationEnabled = true
            googleMapView.delegate = self
            
            if oldLocation != location {
                oldLocation = location
                let camera = GMSCameraUpdate.setTarget(location.coordinate, zoom: self.zoomLevel)
                self.googleMapView.moveCamera(camera)
                self.googleMapView.animate(with: camera)
                SocketManagerClass.shared.eventsHandleInSocket(roomName: self.roomName, id: 6)
                self.moveMent.arCarMovement(self.new_driverMarker, withOldCoordinate: self.oldCoordinate, andNewCoordinate: newCoordinate, inMapview: self.googleMapView, withBearing: 0)
                
            }
        } else {
            oldLocation = location
            let camera = GMSCameraUpdate.setTarget(location.coordinate, zoom: self.zoomLevel)
            self.googleMapView.moveCamera(camera)
            self.googleMapView.animate(with: camera)
            
//            if (self.new_driverMarker) {
//                self.new_driverMarker.map = nil;
//            }
            self.new_driverMarker = GMSMarker()
            self.new_driverMarker.position = oldCoordinate
            self.new_driverMarker.icon = UIImage(named: "tracking_icon_down")
            self.new_driverMarker.map = googleMapView
            
            //self.moveMent.arCarMovement(marker: self.driverMarker, oldCoordinate: self.oldCoordinate, newCoordinate: newCoordinate, mapView: self.googleMapView, bearing: 0)
            
            self.moveMent.arCarMovement(self.new_driverMarker, withOldCoordinate: self.oldCoordinate, andNewCoordinate: newCoordinate, inMapview: self.googleMapView, withBearing: 0)
            
        }
        
        let distanceInMeters = oldLocation?.distance(from: location) // result is in meters
        
        
        //=========>> Redraw polyline if location is changed(Start) // Rerouting <<===================
        // if distanceInMeters == 4 {
        //showToast(message: "equal to 4")
        if GMSGeometryIsLocationOnPathTolerance(newCoordinate, self.arrayOfPath.last ?? GMSPath(), false, 4) {
            //showToast(message: "same path")
            //            KSToastView.ks_showToast("YES: you are in this polyline with newCoordinate:- \(newCoordinate)", duration: 3.0)
        } else {
            
            // DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { // Change `2.0` to the desired number of seconds.
            // Code you want to be delayed
            
            self.getDirections(origin: "\(self.coordinates.coordinate.latitude),\(self.coordinates.coordinate.longitude)", destination:
                "\(self.destinationLat),\(self.destinationLng)",
                waypoints: nil, travelMode: "" as AnyObject, completionHandler: { (status, success, distance) -> Bool in
                    if success! {
                        self.clearRoute()
                        self.drawRoute()
                        return false
                    
                    }
                    else {
                        print(status!)
                        //KSToastView.ks_showToast("Message by Google : \(status!)", duration: 4.0)
                        if status! == "ZERO_RESULTS" {
                            //                                    KSToastView.ks_showToast("Google Returning ZERO RESULTS", duration: 2.0)
                        }
                        else if status! == "OVER_QUERY_LIMIT" {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
                                self.getDirections(origin: "\(self.coordinates.coordinate.latitude),\(self.coordinates.coordinate.longitude)", destination:
                                    "\(self.destinationLat),\(self.destinationLng)",
                                    waypoints: nil, travelMode: "" as AnyObject, completionHandler: { (status, success, distance) -> Bool in
                                        if success! {
                                            //self.drawRoute()
                                            //                                            print("\(self.totalDistance + "\n" + self.totalDuration)")
                                            return false
                                        }
                                        return true
                                })
                            }
                            //                                    KSToastView.ks_showToast("Message by Google : \(status!)\nThat's why path is not drawn", duration: 2.0)
                        }
                        return true
                    }
            })
            // }
            
            
            //            KSToastView.ks_showToast("YES: you are out of this polyline:- \(coordinates.coordinate.latitude),\(coordinates.coordinate.longitude)", duration: 3.0)
        }
        //  }
        
        //===============>> Redraw polyline if location is changed(End) <<===================
        
        
        
        
    }
    
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
        //googleMapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        //print("Error: \(error)")
    }
    
}

/*extension navigationViewController: MapPathViewModelDelegate {
 func isSucessReadJson()  {
 drawPathOnMap()
 }
 
 //fail json read delegate method
 func isFailReadJson(msg : String)  {
 let alert = UIAlertController(title: "Map Alert", message: msg, preferredStyle: .alert)
 let actionOK : UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
 alert.addAction(actionOK)
 self.present(alert, animated: true, completion: nil)
 }
 
 
 //path create
 func drawPathOnMap()  {
 let path = GMSMutablePath()
 let marker = GMSMarker()
 
 let inialLat:Double = objMapModel.arrayMapPath[0].lat!
 let inialLong:Double = objMapModel.arrayMapPath[0].lon!
 
 for mapPath in objMapModel.arrayMapPath
 {
 path.add(CLLocationCoordinate2DMake(mapPath.lat!, mapPath.lon!))
 }
 //set poly line on mapview
 let rectangle = GMSPolyline(path: path)
 rectangle.strokeWidth = 5.0
 marker.map = googleMapView
 rectangle.map = googleMapView
 
 //Zoom map with path area
 let loc : CLLocation = CLLocation(latitude: inialLat, longitude: inialLong)
 
 let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoomLevel)
 self.googleMapView.animate(to: camera)
 
 }
 }
 
 extension navigationViewController: getDataArrayOfLatLng {
 func getLatLngFromSocket(latLongArray: [Any]) {
 for location in latLongArray {
 let currentLocation = location as! NSDictionary
 
 var latitude = ""
 var longitude = ""
 var angle = ""
 for i in 0...currentLocation.allValues.count - 1 {
 let dict = currentLocation.allValues[i] as? NSDictionary
 
 latitude = dict?["lat"]  as? String ?? ""
 longitude = dict?["lng"]  as? String ?? ""
 angle = dict?["angle"]  as? String ?? ""
 
 print("laitude \(latitude)")
 print("longitude \(longitude)")
 print("angle \(angle)")
 }
 
 //            let latitude = currentLocation["lat"] as? String ?? ""
 //            let longitude = currentLocation["lng"] as? String ?? ""
 
 if latitude == "" {
 return
 }
 
 
 let MomentaryLatitude = (latitude as NSString).doubleValue
 let MomentaryLongitude = (longitude as NSString).doubleValue
 
 //            self.oldCoordinate = CLLocationCoordinate2DMake(MomentaryLatitude,MomentaryLongitude)
 //
 //            self.driverMarker.position = self.oldCoordinate
 //            self.driverMarker.icon = UIImage(named: "car")
 //            self.driverMarker.map = self.googleMapView
 
 
 
 self.oldCoordinate = CLLocationCoordinate2DMake(MomentaryLatitude,MomentaryLongitude)
 // Creates a marker in the center of the map.
 
 self.driverMarker.position = self.oldCoordinate
 self.driverMarker.icon = UIImage(named: "car")
 
 //                        let camera = GMSCameraPosition.camera(withTarget: self.marker.position, zoom: self.zoomLevel)
 //                        self.mapView.camera = camera
 
 //Rotation changes
 //                      self.driverMarker.rotation = DegreeBearing(self.fromPoint, B: self.toPoint)
 self.driverMarker.map = self.googleMapView
 
 let head = self.locationManager.location?.course ?? 0
 self.driverMarker.rotation = (angle as NSString).doubleValue
 
 let updatedCamera = GMSCameraUpdate.setTarget(self.driverMarker.position, zoom: self.zoomLevel) //self.mapView.camera.zoom
 self.googleMapView.moveCamera(updatedCamera)
 
 // DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
 // Code you want to be delayed
 
 //  }
 
 
 UIView.animate(withDuration: 2.0) {
 self.googleMapView.animate(with: updatedCamera)
 }
 
 
 //self.bottomMapConstraint.constant = 0
 
 }
 }
 }
 
 extension navigationViewController {
 func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
 func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
 
 func playCar()
 {
 if iTemp <= (objMapModel.arrayMapPath.count - 1 )
 {
 let iTempMapPath = objMapModel.arrayMapPath[iTemp]
 
 let loc : CLLocation = CLLocation(latitude: iTempMapPath.lat!, longitude: iTempMapPath.lon!)
 //let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoomLevel)
 
 let lat:String = String(format:"%f", loc.coordinate.latitude)
 let lng:String = String(format:"%f", loc.coordinate.longitude)
 let angle:String = String(format:"%f", iTempMapPath.angle!)
 //var angle: Float = angle(fromCoordinate: oldLocation, toCoordinate: newLocation)
 //            self.latitude.text = lat
 //
 //            self.longitude.text = lng
 
 //            let dLon = lon2 - lon1
 //
 //            let y = sin(dLon) * cos(lat2)
 //            let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
 //            let radiansBearing = atan2(y, x)
 //
 //
 //
 //            SocketManagerClass.shared.emitTheUserLocation(latitude: lat, longitude: lng, angle: angle, key: "tracking")
 //
 
 //
 //            self.googleMapView.animate(to: camera)
 //            marker.position = CLLocationCoordinate2DMake(iTempMapPath.lat!, iTempMapPath.lon!)
 //
 //            marker.rotation = iTempMapPath.angle!
 //
 //            marker.icon = UIImage(named: "car.png")
 //            marker.map = googleMapView
 
 // Timer close
 if iTemp == (objMapModel.arrayMapPath.count - 1)
 {
 // timer close
 timer.invalidate()
 //buttonPlay.isEnabled = true
 iTemp = 0
 }
 iTemp += 1
 }
 }
 
 func angle(fromCoordinate first: CLLocationCoordinate2D, toCoordinate second: CLLocationCoordinate2D) -> Float {
 
 let deltaLongitude = Float(second.longitude - first.longitude)
 let deltaLatitude = Float(second.latitude - first.latitude)
 let angle: Float = (.pi * 0.5) - atan(deltaLatitude / deltaLongitude)
 
 if deltaLongitude > 0 {
 return angle
 } else if deltaLongitude < 0 {
 return angle + .pi
 } else if deltaLatitude < 0 {
 return .pi
 }
 
 return 0.0
 }
 
 }*/

private extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}

private extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}
