//
//  GalleryViewController.swift
//  Bean2Go
//
//  Created by Aks on 07/12/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import AlamofireImage

class GalleryViewController: UIViewController {

    @IBOutlet weak var loaderView: UIActivityIndicatorView!
    var galleryImagesArray = [StoreImagesClass]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Button Action
    
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension GalleryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryImagesArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath)
        
        let imageView = cell.viewWithTag(1010) as! UIImageView
        let loaderIndicator = cell.viewWithTag(1111) as! UIActivityIndicatorView
        
        loaderIndicator.startAnimating()
        if galleryImagesArray[indexPath.item].storeThumbImageUrl != nil || galleryImagesArray[indexPath.item].storeThumbImageUrl != "" {
            
            loaderIndicator.stopAnimating()
            loaderIndicator.hidesWhenStopped = true
            imageView.af_setImage(withURL: URL(string: galleryImagesArray[indexPath.item].storeThumbImageUrl ?? "")!)
            
            
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ZoomViewController") as? ZoomViewController
        vc?.galleryImagesArray = self.galleryImagesArray
        vc?.imageIndexFullView = indexPath.item
        vc?.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc!, animated: true, completion: nil)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let itemsPerRow:CGFloat = 2
        let totalSpacing:CGFloat = 3
        let cellWidth:CGFloat = (collectionView.frame.size.width - totalSpacing) / itemsPerRow
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
}

