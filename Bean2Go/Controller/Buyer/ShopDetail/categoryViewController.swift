//
//  categoryViewController.swift
//  Bean2Go
//
//  Created by AKS on 30/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import AlamofireImage

class categoryViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var refreshController = UIRefreshControl()
    
    var categoryArray = NSMutableArray()
    var storeId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchCategoryDataAPI()
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.collectionView.refreshControl = refreshController
    }
    
    //MARK:- Button Action
    @IBAction func backBtnTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Refresh Action
    @objc func refresh(sender:AnyObject) {
        fetchCategoryDataAPI()
        refreshController.endRefreshing()
    }
    
}

extension categoryViewController:UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCollectionViewCell", for: indexPath) as! categoryCollectionViewCell
        
        let data = self.categoryArray[indexPath.row] as! categoryInfo
        
        cell.coffeeNameLbl.text = data.product_name
        cell.coffeeImgView.af_setImage(withURL: URL(string: data.product_image)!)
        
        cell.contentView.layer.addborder(width: 2, color: UIColor.init(red: 230/255, green: 230/255, blue: 230/255, alpha: 0.6))
        cell.contentView.layer.cornerRadius = 4
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / 2 ) - 10, height: (collectionView.frame.size.width / 2 ) + 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let nVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "FeatureScrollingViewController") as! FeatureScrollingViewController
        
        let data = self.categoryArray[indexPath.row] as! categoryInfo
        nVC.storeId = String(describing: storeId!)
        nVC.store_product_id = data.store_product_id
        nVC.basePriceOfProduct = data.product_price
        nVC.productName = data.product_name
        self.navigationController?.pushViewController(nVC, animated: true)
    }
}

//MARK: Web APi
extension categoryViewController{
    
    func fetchCategoryDataAPI(){
        let paraDict = NSMutableDictionary()
        
        paraDict["store_id"] = String(describing: storeId!)
        
        categoryArray = []
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "getData/getStoreProductsForBuyer") { (response, error, message, statusCode) in
            
            if message != nil{
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
            } else {
                let JSON = response as! NSDictionary
                let productArray = JSON["products"] as! NSArray
                
                if productArray.count == 0 {
                    let alertController = UIAlertController(title: "", message: "No products available", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    for product in productArray {
                        let productDict = product as! NSDictionary
                        
                        let productData = categoryInfo()
                        productData.product_id = String(productDict["product_id"] as? Int ?? 0)
                        productData.store_product_id = String(productDict["store_product_id"] as? Int ?? 0)
                        productData.product_name = productDict["product_name"] as? String ?? ""
                        productData.product_description = productDict["product_dexcription"] as? String ?? ""
                        productData.product_price = productDict["product_price"] as? String ?? ""
                        productData.product_price_display = productDict["product_price_display"] as? String ?? ""
                        productData.product_image = productDict["product_image"] as? String ?? ""
                        
                        self.categoryArray.add(productData)
                    }
                }
                
            }
            self.collectionView.reloadData()
            
        }

    }
}
