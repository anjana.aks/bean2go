//
//  FeatureScrollingViewController.swift
//  Expandable Table View
//
//  Created by Aks on 15/12/18.
//  Copyright © 2018 Aks. All rights reserved.
//

import UIKit

class FeatureScrollingViewController: UIViewController {
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var titleOfView: UINavigationItem!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var arrowViewConstraints: NSLayoutConstraint!
    
    var productArray = [ProductInfo]()
    var storeId : String?
    var productName : String?
    var store_product_id : String?
    var currency: String = "R"
    var comment: String?
    var basePriceOfProduct: String = ""
    var refreshController = UIRefreshControl()
    
    var selectedProductIdArray = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchIngrediatDataAPI()
        titleOfView.title = productName
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushPaymentController(notification:)), name: NSNotification.Name(rawValue: "openMyCartViewController"), object: nil)
        
    }
   
    //MARK:- Refresh Action
    @objc func refresh(sender:AnyObject) {
        self.fetchIngrediatDataAPI()
        refreshController.endRefreshing()
    }
    
    @objc func pushPaymentController(notification: NSNotification) {
        let nVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "cartScheduledViewController") as! cartScheduledViewController
        nVC.isFailureTransaction = true
        nVC.isFromScreen = FEATURE_CONTROLLER
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    
    //MARK:- Button Click
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func myCartButtonDidTapped(_ sender: UIBarButtonItem) {
        self.openCartViewController()
    }
    
    fileprivate func openCartViewController() {
        let nVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "cartScheduledViewController") as! cartScheduledViewController
        nVC.isFromScreen = FEATURE_CONTROLLER
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @IBAction func addToCartBtnDidTapped(_ sender: UIButton) {
        selectedProductIdArray = []
        for product in productArray {
            let subProductArray = product.options
            let subProductName = product.optionType
            var isSelectedProduct:Bool = false
            var selectedProductId: Int = -1
            
            
            for subProductInfo in subProductArray {
                if subProductInfo.isSelected == true {
                   isSelectedProduct = subProductInfo.isSelected
                    selectedProductId = subProductInfo.product_option_id
                }
            }
            
            if isSelectedProduct == false {
                self.present(UIAlertController.alertWithTitle(title: "", message: "Please select \(subProductName)" , buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            if selectedProductId != -1 {
                selectedProductIdArray.append(selectedProductId)
            }
            
        }
        
        if selectedProductIdArray.count != 0 {
            self.callWebServiceToAddCard()
        }
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openMyCartViewController"), object: nil)
        
    }
}


//MARK:- CALL WEB SERVICE
extension FeatureScrollingViewController {
    
    //MARK:- fecth all ingredients
    func fetchIngrediatDataAPI() {
        let paraDict = NSMutableDictionary()
        
        paraDict["store_id"] = storeId
        paraDict["store_product_id"] = store_product_id
        
        productArray = []
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "getData/getStoreProductOptionsForBuyer") { (response, error , message, statusCode) in
            
            if message != nil{
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let JSON = response as! NSDictionary
                let data =  JSON["data"] as! NSDictionary
                self.currency = data["currency"] as? String ?? ""
                let productOptions = data["productOptions"] as! NSArray
                
//                if currentCurrency != self.currency {
//                    let priceText = "\(self.currency) \(Float(self.basePriceOfProduct)!)"
//                    if self.priceLbl.text == priceText {
                        self.priceLbl.text = "\(self.currency) \(Float(self.basePriceOfProduct)!)"
//                    }
//                }

                if productOptions.count != 0 {
                    for productOption in productOptions {
                        let dict = productOption as? NSDictionary
                        let productInfo = ProductInfo()
                        productInfo.optionType = dict?["option_type"] as? String ?? ""
                        let array = dict?["options"] as? NSArray ?? []
                        //productInfo.options = array as? [IngredientInfo] ?? []
                        
                        if array.count != 0 {
                            for proInfo in array {
                                let dict = proInfo as? NSDictionary
                                let ingredientInfo = IngredientInfo()
                                ingredientInfo.option_name = dict?["option_name"] as? String ?? ""
                                ingredientInfo.option_price = dict?["option_price"] as? String ?? ""
                                ingredientInfo.product_option_id = dict?["product_option_id"] as? Int ?? -1
                                productInfo.options.append(ingredientInfo)
                            }
                        }
                        self.productArray.append(productInfo)
                        self.mainCollectionView.reloadData()
                    }
                } else {
                    let alertController = UIAlertController(title: "", message: "No data found", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                }
                
                //print("New product Array \(self.productArray)")
                
            }
        }
        
       
    }
    
    
    //MARK:- call web service to add cart
    
    func callWebServiceToAddCard() {
        let paraDict = NSMutableDictionary()
        
        paraDict["store_product_id"] = store_product_id
        paraDict["qty"] = "1"
        paraDict["options"] = selectedProductIdArray
        paraDict["comment"] = comment
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/cart/addToCart") { (response, error , message, statusCode) in
            
            if message != nil{
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                //data to be parsed
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                let msg = data["message"] as? String
                
                let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.openCartViewController()
                }
                
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
        
    }
}

//MARK:- Collection view datasource and delegate
extension FeatureScrollingViewController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
       return CGSize(width: self.mainCollectionView.frame.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeadderCell", for: indexPath)
            
//            let cell = collectionView.cellForItem(at: indexPath)
//            let label = cell?.viewWithTag(1010) as! UILabel
//            label.text = productArray[indexPath.item].optionType.uppercased()
            
            return headerView
            
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footerCommentView", for: indexPath) as! MySupplementaryView
            
            footerView.commentTextView.delegate = self as UITextViewDelegate
            
            return footerView
            
        default:
            
            assert(false, "Unexpected element kind")
            return UICollectionReusableView()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewCell", for: indexPath) as UICollectionViewCell
        //
        //        let label = cell?.viewWithTag(1234) as! UILabel
        //        label.text = productArray[indexPath.item].optionType.uppercased()
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellId", for: indexPath) as! CategoryCell
        
        cell.categoryTitle.text = productArray[indexPath.item].optionType.uppercased()
        //cell.categoryTitle.tag = indexPath.item
        
        let subProductArray = productArray[indexPath.item].options
        
        cell.productInfoArray = subProductArray
        
        //        if subProductArray.count > 2 {
        //            cell.arrowViewConstraints.constant = 40
        //            cell.contentView.animateViewFromRightLeft()
        //        } else {
        cell.arrowViewConstraints.constant = 0
        //        }
        
        cell.selectedCellDelegate = self as SubCatagoryCellSelected
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.mainCollectionView.frame.width, height: 180)
    }
   
}



//MARK:- Textview delegate method
extension FeatureScrollingViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Comments" {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = .gray
            textView.text = "Comments"
        }
    }
    
    //To resign first responder on done click
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (range.length == 0) {
            
            comment = trimWhiteSpace(str: textView.text)
            
            if text == "\n" {
                textView.resignFirstResponder()
                
                
                
                return false;
            }
        }
        return true;
    }
}

// MARK:- Calculate product price

extension FeatureScrollingViewController: SubCatagoryCellSelected {
    
    func selectedCellPriceCalculate() {
        var totalPrice = Float(basePriceOfProduct)!
        
        for product in productArray {
            let subProductArray = product.options
            
            for subProductInfo in subProductArray {
                if subProductInfo.isSelected == true {
                    let optionPrice = Float(subProductInfo.option_price)!
                    
                    totalPrice = totalPrice + optionPrice
                    self.priceLbl.text = "\(self.currency)\(totalPrice)"
                }
            }
        }
    }
}

// MARK:- sub class of footerview
class MySupplementaryView: UICollectionReusableView {
    
    @IBOutlet weak var commentTextView: UITextView!
}


// MARK:- sub class of footerview
class CartFooterView: UICollectionReusableView {
    @IBOutlet weak var totalBasePriceLbl: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var percentageInRupeeLbl: UILabel!
    @IBOutlet weak var totalAmountWithTaxLbl: UILabel!
    @IBOutlet weak var shadowView: UIView!
}
