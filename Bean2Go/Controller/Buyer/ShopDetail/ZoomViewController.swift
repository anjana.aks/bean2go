//
//  ZoomViewController.swift
//  OnlineBearingsMarket
//
//  Created by AKS on 20/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class ZoomViewController: UIViewController, PagingScrollViewDelegate, PagingScrollViewDataSource {
    
    private let pagingControl:PagingScrollView = PagingScrollView()
    
    @IBOutlet weak var containerView: UIView!
    
    var samplePhotos = NSMutableArray()
    var imageArray = NSMutableArray()
    var imageIndexFullView = Int()
    
    var galleryImagesArray = [StoreImagesClass]()
    // MARK:- View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUp()
    }
    
    
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK:- Memory Warning Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func setUp() {
        pagingControl.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.width, height: self.containerView.frame.height)
        pagingControl.delegate   = self
        pagingControl.dataSource = self
        pagingControl.backgroundColor = UIColor.clear
        self.containerView.addSubview(pagingControl)
        pagingControl.center = CGPoint(x: self.containerView.frame.width/2, y: self.containerView.frame.height/2)
        self.view.layoutSubviews()
        
        pagingControl.reloadData()
    }
    
    func pagingScrollView(_ pagingScrollView:PagingScrollView, willChangedCurrentPage currentPageIndex:NSInteger) {
       // print("current page will be changed to \(currentPageIndex).")
    }
    
    func pagingScrollView(_ pagingScrollView:PagingScrollView, didChangedCurrentPage currentPageIndex:NSInteger) {
        //print("current page did changed to \(currentPageIndex).")
    }
    
    func pagingScrollView(_ pagingScrollView:PagingScrollView, layoutSubview view:UIView) {
        //print("paging control call layoutsubviews.")
    }
    
    func pagingScrollView(_ pagingScrollView:PagingScrollView, recycledView view:UIView?, viewForIndex index:NSInteger) -> UIView {
        guard view == nil else { return view! }
        
        let zoomingView = ZoomingScrollView(frame: self.containerView.bounds)
        //        zoomingView.backgroundColor = UIColor.orange
        zoomingView.singleTapEvent = {
           // print("single tapped...")
        }
        
        zoomingView.doubleTapEvent = {
           // print("double tapped...")
        }
        
        zoomingView.pinchTapEvent = {
            //print("pinched...")
        }
        
        return zoomingView
    }
    
    func pagingScrollView(_ pagingScrollView:PagingScrollView, prepareShowPageView view:UIView, viewForIndex index:NSInteger) {
        guard let zoomingView = view as? ZoomingScrollView else { return }
        guard let zoomContentView = zoomingView.targetView as? ZoomContentView else { return }
        //        zoomContentView.backgroundColor = UIColor.orange
        //        zoomContentView.setImageWith(URL(string: imageArray[index] as! String)!)
        if galleryImagesArray[index].storeThumbImageUrl != nil || galleryImagesArray[index].storeThumbImageUrl != "" {
            zoomContentView.af_setImage(withURL: URL(string: (galleryImagesArray[index]).storeThumbImageUrl ?? "")!)
        } 
        // just call this methods after set image for resizing.
        zoomingView.prepareAfterCompleted()
        zoomingView.setMaxMinZoomScalesForCurrentBounds()
    }
    
    func startIndexOfPageWith(pagingScrollView:PagingScrollView) -> NSInteger {
        return imageIndexFullView
    }
    
    func numberOfPageWith(pagingScrollView:PagingScrollView) -> NSInteger {
        //return imageArray.count
        return galleryImagesArray.count
    }
    
    // MARK:- Button Actions
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
