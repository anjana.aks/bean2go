//
//  ReviewViewController.swift
//  Bean2Go
//
//  Created by Aks on 07/12/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    
    //MARK:- OutLets
    
    @IBOutlet weak var mainTableView: UITableView!
    
    // MARK:- VARIABLES
    var reviewArray: [StoreReviewClass] = []
    
    

    // MARK:- Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ReviewViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewTableViewCell
        
        
        let storeReviewClass: StoreReviewClass = reviewArray[indexPath.row]
       
        cell.userNameLbl.text = storeReviewClass.reviewUserName
        cell.descriptionLbl.text = storeReviewClass.reviewComment
        cell.ratingLabel.text = storeReviewClass.reviewRating
        let ratingInDouble: String = storeReviewClass.reviewRating ?? "3.5"
        cell.userImgView.af_setImage(withURL: URL(string: storeReviewClass.reviewUserImageUrl ?? "")!)
        cell.reviewView.rating = Double(ratingInDouble) ?? 3.7
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}



