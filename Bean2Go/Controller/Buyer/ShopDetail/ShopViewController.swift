//
//  HomeViewController.swift
//  Bean2Go
//
//  Created by AKS on 24/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import Alamofire

var imageOfRest: UIImage?

class ShopViewController: UIViewController {
    
    @IBOutlet weak var reviewView: UIStackView!
    @IBOutlet weak var photoView: UIStackView!
    @IBOutlet weak var TimeTableStckView: UIStackView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var adressLbl: UILabel!
    @IBOutlet weak var daysLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var streachableHeadderView: StretchHeader!
    @IBOutlet weak var restaurentNavigationBarImg: UIImageView!
    @IBOutlet weak var telephoneNumberLabel: UILabel!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var seeMoreButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tilteLabel: UILabel!
    @IBOutlet weak var orderBtnView: UIView!
    
    @IBOutlet weak var bottomContentView: NSLayoutConstraint!
    
    var refreshController = UIRefreshControl()
    var frontRefreshController = UIRefreshControl()
    
    var timingViewElements = 1
    var restImage: UIImage?
    var imageView = UIImageView()
    let datePicker = UIDatePicker()
    
    var latitude: CGFloat?
    var longitude: CGFloat?
    var storeId: Int?
    var shopeDetail: ShopDetailClass?
    @IBOutlet weak var photoHeaderLbl: UILabel!
    
    @IBOutlet weak var reviewHeader: UILabel!
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 39, height: 39))
        //setUp()
        
        callToGetShopDetail()
        
        self.mainTableView.rowHeight = UITableView.automaticDimension
        self.mainTableView.estimatedRowHeight = 100
        
        
        
        
        
        //        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        //        frontRefreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        //        //self.mainTableView.refreshControl = frontRefreshController  :- dont need to refresh table if data is fetched(as per current requirment)
        //        self.backgroundScrollView.refreshControl = refreshController
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        self.timingViewHeightConstraint.constant = CGFloat(40 + timingViewElements * 30)
        //        self.scrollView.updateContentView()
        //        bottomContentView.constant = scrollView.subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? scrollView.contentSize.height
        self.view.layoutSubviews()
        
        //self.contentView.frame =
        restImage = self.buildImage(image: UIImage(named: "restaurants1")!)
    }
    
    //MARK:- HIDE BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    fileprivate func setDataOnView() {
        if shopeDetail != nil {
            streachableHeadderView.bookMarkButton.tag = shopeDetail?.storeId ?? 0
            streachableHeadderView.setData(resturantName: shopeDetail?.storeShopName ?? "Caffe coffee shop", rating: shopeDetail?.storeRating ?? "3.6", imageUrl: shopeDetail?.storeCoverImg ?? "")
            
            adressLbl.text = shopeDetail?.storeAddress
            telephoneNumberLabel.text = shopeDetail?.storeContactInfo
            
            if self.shopeDetail?.storeShortDescription != "" {
                let messageString = "\(self.shopeDetail?.storeShortDescription ?? "") See More"
                
                let messageAttributedString = NSMutableAttributedString(string:messageString )
                
                messageAttributedString.setAsColor(textToFind: "See More")
                self.descriptionLbl.attributedText = messageAttributedString
                
            }
            
            if self.shopeDetail?.storeDescription != "" {
                let messageString = "\(self.shopeDetail?.storeDescription ?? "") See Less"
                
                let messageAttributedString = NSMutableAttributedString(string:messageString )
                
                messageAttributedString.setAsColor(textToFind: "See Less")
                self.descriptionLbl.attributedText = messageAttributedString
            }
            
            if shopeDetail?.storeIsFav ?? false {
                streachableHeadderView.bookMarkButton.isSelected = true
            } else {
                streachableHeadderView.bookMarkButton.isSelected = false
            }
            
            self.tilteLabel.text = shopeDetail?.storeShopName ?? "Caffe coffee shop"
            
            timeLbl.text = "\(convertDateToDisplay( dateAsString :shopeDetail?.storeOpenAt ?? "10:00 am" )) - \(convertDateToDisplay( dateAsString :shopeDetail?.storeCloseAt ?? "11:00 pm" ))"
            
            self.orderBtnView.isHidden = false
            
            if self.shopeDetail?.isRestaurantOpen == false {
                self.orderBtnView.isHidden = true
            }
            
            if self.shopeDetail?.storeImagesArray.count != 0 {
                self.photoView.isHidden = false
                self.photoCollectionView.reloadData()
            } else {
                self.photoView.isHidden = true
            }
            
            //self.photoCollectionView.reloadData()
            self.mainTableView.reloadData()
            
            
        }
    }
    
    var documentInteractionController:UIDocumentInteractionController!
    
    fileprivate func shareImageAndTextOnWhatsApp(imageURL: UIImage) {
        let urlWhats = "whatsapp://app"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    let imgURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let fileName = imageURL
                    //let fileURL = imgURL.appendingPathComponent(fileName)
                    if let image:UIImage = imageURL {
                        if let imageData = image.jpegData(compressionQuality: 0.75) {
                            let tempFile = NSURL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/yourImageName.jpg")
                            do {
                                try imageData.write(to: tempFile!, options: .atomicWrite)
                                
                                self.documentInteractionController = UIDocumentInteractionController(url: tempFile!)
                                self.documentInteractionController.uti = "net.whatsapp.image"
                                
                                self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                            } catch {
                                //print(error)
                            }
                        }
                    }
                } else {
                    // Cannot open whatsapp
                }
            }
        } else {
            ImageHelper.shareRestaurentdetail(image: imageURL, controller: self, storeId: self.storeId!, description: "\(self.shopeDetail?.storeShortDescription ?? "")")
        }
    }
    
    fileprivate func shareImge(image: UIImage, description: String, storeId: String) {
        let activity = UIActivityViewController(activityItems: [image, "#\(ALBUM_NAME) - \(description)\ncom.bean2go://ShopViewController/\(storeId)"], applicationActivities: nil)
        
        //        let activity = UIActivityViewController(activityItems: ["#\(ALBUM_NAME)", "com.bean2go://ShopViewController/\(storeId)"], applicationActivities: nil)
        
        activity.popoverPresentationController?.sourceView = self.view
        self.present(activity, animated: true, completion: nil)
    }
    
    
    
    
    
    //MARK:- BUTTON ACTION
    
    /*@IBAction func whatsappShareWithImages(_ sender: AnyObject){
     
     let urlWhats = "whatsapp://app"
     if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed) {
     if let whatsappURL = URL(string: urlString) {
     
     if UIApplication.shared.canOpenURL(whatsappURL as URL) {
     
     if let image = UIImage(named: “whatsappIcon”) {
     if let imageData = UIImageJPEGRepresentation(image, 1.0) {
     let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent(“Documents/whatsAppTmp.wai”)
     do {
     try imageData.write(to: tempFile, options: .atomic)
     self.documentInteractionController = UIDocumentInteractionController(url: tempFile)
     self.documentInteractionController.uti = “net.whatsapp.image”
     self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
     
     } catch {
     print(error)
     }
     }
     }
     
     } else {
     // Cannot open whatsapp
     }
     }
     }
     
     }*/
    
    @IBAction func shareButtonDidTapped(_ sender: UIButton) {
        // Share APP
        
        //        let whatsappURL:NSURL? = NSURL(string: "whatsapp://send?text=Hello%2C%20World!")
        //        if (UIApplication.shared.canOpenURL(whatsappURL! as URL)) {
        //            UIApplication.shared.openURL(whatsappURL! as URL)
        //        }
        //        
        //        return
        
        
        
        if let imageString = shopeDetail?.storeCoverImg {
            
            
            Alamofire.request(imageString).responseImage { response in
                
                //print(response.result)
                
                if let image = response.result.value {
                    
                    //self.shareImageAndTextOnWhatsApp(imageURL: image)
                    
                    //                    ImageHelper.shareRestaurentdetail(image: image, controller: self, storeId: self.storeId!, description: "\(self.shopeDetail?.storeShortDescription ?? "")")
                    
                    ImageHelper.shareRestaurentdetail(image: image, controller: self, storeId: self.storeId!, description: "Hey check out this amazing cafe: ")
                    
                    
                    
                } else {
                    
                    //self.shareImageAndTextOnWhatsApp(imageURL: UIImage(named: "restaurants1")!)
                    
                    //                    ImageHelper.shareRestaurentdetail(image: UIImage(named: "restaurants1")!, controller: self, storeId: self.storeId!, description: "\(self.shopeDetail?.storeShortDescription ?? "")")
                    
                    ImageHelper.shareRestaurentdetail(image: UIImage(named: "restaurants1")!, controller: self, storeId: self.storeId!, description: "Hey check out this amazing cafe: ")
                    
                }
            }
        } else {
            //self.shareImageAndTextOnWhatsApp(imageURL: UIImage(named: "restaurants1")!)
            
            //ImageHelper.shareRestaurentdetail(image: UIImage(named: "restaurants1")!, controller: self, storeId: self.storeId!, description: "\(self.shopeDetail?.storeShortDescription ?? "")")
            
            ImageHelper.shareRestaurentdetail(image: UIImage(named: "restaurants1")!, controller: self, storeId: self.storeId!, description: "Hey check out this amazing cafe: ")
            
            
        }
        
    }
    
    
    @IBAction func callButtonDidTapped(_ sender: UIButton) {
        //(shopeDetail?.storeContactInfo!)!.makeAColl()
        
        //        guard let number = URL(string: "tel://" + (shopeDetail?.storeContactInfo!)!) else { return }
        //        UIApplication.shared.open(number)
    }
    
    @IBAction func moreReviewButtonDidTapped(_ sender: UIButton) {
        
        let nVC = UIStoryboard(name:"Main" , bundle: nil).instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
        nVC.reviewArray = shopeDetail?.storeReviewArray ?? []
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @IBAction func orderNowBtnDidTapped(_ sender: UIButton) {
        
        let isLastOrderComplete: Bool = self.shopeDetail?.lastOrderCompleted ?? true
        let checkCartStatus: Int = self.shopeDetail?.cartStatus ?? 0
        
        // restrict user if order is in processing
        if isLastOrderComplete == false {
            let alertController = UIAlertController(title: "Oops!", message: "Your order is in the process after completing it, you will be able to create new order.\n Thanks!!!", preferredStyle: .alert)
            let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                //self.navigationController?.popViewController(animated: true)
                return
            }
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        
        // restrict user if cart contains items
        switch checkCartStatus {
        case 1:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "categoryViewController") as! categoryViewController
            nVC.storeId = storeId
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case 2:
            let alertController = UIAlertController(title: "", message: "You have items saved in your cart.", preferredStyle: .alert)
            let action1 = UIAlertAction(title: "View Cart", style: .default) { (UIAlertAction) in
                let nVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "cartScheduledViewController") as! cartScheduledViewController
                self.navigationController?.pushViewController(nVC, animated: true)
            }
            
            let deleteBtn = UIAlertAction(title: "Erase", style: .default) { (UIAlertAction) in
                self.deleteItemsInCartAPi()
            }
            alertController.addAction(action1)
            alertController.addAction(deleteBtn)
            self.present(alertController, animated: true, completion: nil)
            break
        default:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "categoryViewController") as! categoryViewController
            nVC.storeId = storeId
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        }
        
    }
    
    @IBAction func backButtonDidSelect(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func seeLessAndSeeMore(_ sender: UITapGestureRecognizer) {
        guard let text = descriptionLbl.attributedText?.string else {
            return
        }
        
        if text.range(of:"See Less") != nil {
            if self.shopeDetail?.storeShortDescription != "" {
                let messageString = "\(self.shopeDetail?.storeShortDescription ?? "") See More"
                
                let messageAttributedString = NSMutableAttributedString(string:messageString )
                
                messageAttributedString.setAsColor(textToFind: "See More")
                self.descriptionLbl.attributedText = messageAttributedString
                
            }
        }
        
        if text.range(of:"See More") != nil {
            if self.shopeDetail?.storeDescription != "" {
                let messageString = "\(self.shopeDetail?.storeDescription ?? "") See Less"
                
                let messageAttributedString = NSMutableAttributedString(string:messageString )
                
                messageAttributedString.setAsColor(textToFind: "See Less")
                self.descriptionLbl.attributedText = messageAttributedString
            }
        }
    }
    
    
    @objc func refresh(sender:AnyObject) {
        callToGetShopDetail()
        refreshController.endRefreshing()
    }
    
}

extension UIScrollView {
    func updateContentView() {
        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
    }
}

extension ShopViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if tableView == timingTableView {
        //            return timingViewElements
        //        }
        //
        var totalRows = shopeDetail?.storeReviewArray.count ?? 0
        
        seeMoreButton.isHidden = true
        if totalRows > 2 {
            totalRows = 2
            seeMoreButton.isHidden = false
        }
        
        return totalRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        if tableView == timingTableView{
        //            let cell: shopsTimingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "shopsTimingTableViewCell", for: indexPath) as! shopsTimingTableViewCell
        //            return cell
        //        }
        let cell: ReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewTableViewCell
        
        
        let storeReviewClass: StoreReviewClass = shopeDetail?.storeReviewArray[indexPath.row] ?? StoreReviewClass()
        
        cell.userNameLbl.text = storeReviewClass.reviewUserName
        cell.descriptionLbl.text = storeReviewClass.reviewComment
        cell.ratingLabel.text = storeReviewClass.reviewRating
        let ratingInDouble: String = storeReviewClass.reviewRating ?? "3.5"
        // cell.reviewView.rating = Double(ratingInDouble) ?? 3.5
        cell.userImgView.af_setImage(withURL: URL(string: storeReviewClass.reviewUserImageUrl ?? "")!)
        cell.reviewView.rating = Double(ratingInDouble) ?? 3.5
        cell.descriptionLbl.sizeToFit()
        cell.descriptionLbl.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //        let cell: ReviewTableViewCell = tableView.cellForRow(at: indexPath) as! ReviewTableViewCell
        //        return (80 + (cell.descriptionLabel.bounds.size.height))
        //        if tableView == timingTableView{
        //            return 30
        //        }
        return UITableView.automaticDimension
    }
    
    // MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        streachableHeadderView.updateScrollViewOffset(scrollView)
        
        // NavigationHeader alpha update
        let offset : CGFloat = scrollView.contentOffset.y
        
        if (offset > 150) {
            let alpha : CGFloat = min(CGFloat(1), CGFloat(1) - (CGFloat(50) + 44 - offset) / 44)
            self.restaurentNavigationBarImg.alpha = alpha
            
        } else {
            self.restaurentNavigationBarImg.alpha = 0.0
            
        }
    }
    
    func buildImage(image: UIImage) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: streachableHeadderView.frame.size.width, height: 64), false, 1)
        //image.draw(in: CGRect(x: 0, y: 0, width: streachableHeadderView.frame.size.width, height: 100))
        image.draw(at: CGPoint(x: 0, y: 0))
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return finalImage!
    }
    
}

extension ShopViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var numberOfCells: Int = self.shopeDetail?.storeImagesArray.count ?? 0
        
        if numberOfCells > 4 {
            numberOfCells = 4
        }
        
        return numberOfCells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == photoCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath)
            
            let storeImageDict = self.shopeDetail?.storeImagesArray[indexPath.item]
            
            let imageView = cell.viewWithTag(1010) as! UIImageView
            
            
            
            if storeImageDict?.storeThumbImageUrl != nil {
                if storeImageDict?.storeThumbImageUrl != "" {
                    
                    imageView.af_setImage(withURL: URL(string: storeImageDict?.storeThumbImageUrl ?? "")!)
                }
            }
            
            let viewAllView = cell.viewWithTag(1111)
            
            viewAllView?.isHidden = indexPath.item == 3 ? false : true
            
            return cell
        } else {
            let cell: MenuCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCollectionViewCell
            
            cell.viewAllView.isHidden = indexPath.item == 3 ? false : true
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 3 {
            let nVC = UIStoryboard(name:"Main" , bundle: nil).instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            nVC.galleryImagesArray = self.shopeDetail?.storeImagesArray ?? []
            self.navigationController?.pushViewController(nVC, animated: true)
            
        } else {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ZoomViewController") as? ZoomViewController
            vc?.galleryImagesArray = (self.shopeDetail?.storeImagesArray)!
            vc?.imageIndexFullView = indexPath.item
            vc?.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc!, animated: false, completion: nil)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let itemsPerRow:CGFloat = 4
        let totalSpacing:CGFloat = 3
        let cellWidth:CGFloat = (collectionView.frame.size.width - totalSpacing) / itemsPerRow
        return CGSize(width: cellWidth, height: (collectionView.frame.size.height - totalSpacing))
    }
}


extension ShopViewController {
    
    func showShortDescriptionView() {
        
    }
    
    func showDetailDescriptionView() {
        
    }
    
    
    /* if let range = text.range(of:"See More") {
     self.showDetailDescriptionView()
     }
     else if let range = text.range(of: "Privacy"){
     print(range)
     UIApplication.shared.open(URL(string: "https://www.google.com/url?q=https://www.iubenda.com/privacy-policy/58446596&sa=D&source=hangouts&ust=1528787597335000&usg=AFQjCNEPkofPxSm7TDRMvxjOjCz5cio27w")!, options: [:])
     }*/
}

//MARK:- Web Api
extension ShopViewController {
    
    func callToGetShopDetail() {
        
        let paraDict = NSMutableDictionary()
        
        paraDict["store_id"] = storeId
        
        //print(paraDict)
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "getData/getStoreDetailsForBuyer") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                     let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                     if self.navigationController?.viewControllers.count ?? 0 > 0 {
                     
                     if ((self.navigationController?.viewControllers.last) != nil) {
                     self.navigationController?.popToRootViewController(animated: false)
                     } else {
                     self.navigationController?.viewControllers.removeAll()
                     }
                     }
                     let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                     self.navigationController?.viewControllers = [mainView]
                     let obj = AppDelegate()
                     obj.window?.rootViewController = self.navigationController
                     obj.window?.makeKeyAndVisible()
                     }
                     alertController.addAction(action1)
                     self.present(alertController, animated: true, completion: nil)
                     */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    self.mainTableView.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                //self.mainTableView.isHidden = true
                return
            }else {
                guard let data = response?["data"] as? NSDictionary else {
                    return
                }
                self.shopeDetail = ShopDetailClass()
                self.shopeDetail?.storeId = data["store_id"] as? Int
                self.shopeDetail?.storeContactInfo = data["store_contact_number"] as? String
                self.shopeDetail?.storeAddress = data["store_address"] as? String
                self.shopeDetail?.vehicleNumber = data["vehicle_number"] as? String
                self.shopeDetail?.storeDescription = data["description"] as? String ?? ""
                self.shopeDetail?.parkingAvailable = data["parking_available"] as? Int == 1 ? true : false
                self.shopeDetail?.storeOpenAt = data["opens_at"] as? String
                self.shopeDetail?.storeCloseAt = data["closes_at"] as? String
                self.shopeDetail?.storeRating = data["ratings"] as? String
                self.shopeDetail?.storeShopName = data["store_name"] as? String
                self.shopeDetail?.storeIsFav = data["userFav"] as? Int == 1 ? true : false
                self.shopeDetail?.lastOrderCompleted = data["lastOrderCompleted"] as? Int == 1 ? true : false
                self.shopeDetail?.cartStatus = data["cartStatus"] as? Int ?? 0
                self.shopeDetail?.storeCoverImg = data["store_cover"] as? String
                self.shopeDetail?.storeShortDescription = data["short_description"] as? String ?? ""
                let isResptaurntOpen = data["isRestaurantOpen"] as? Int ?? 1
                self.shopeDetail?.isRestaurantOpen = isResptaurntOpen == 1 ? true : false
                
                let storeImagesArray = data["store_images"] as? NSArray
                for storeImageDetail in storeImagesArray! {
                    
                    var storeImageDict = Dictionary<String, AnyObject>()
                    storeImageDict = storeImageDetail as! Dictionary<String, AnyObject>
                    let storeImageObj = StoreImagesClass()
                    storeImageObj.storeImageUrl = storeImageDict["thumbnail_url"] as? String
                    storeImageObj.storeThumbImageUrl = storeImageDict["thumbnail_url"] as? String
                    
                    if storeImageObj.storeImageUrl != nil && storeImageObj.storeThumbImageUrl != nil {
                        if storeImageObj.storeImageUrl != "" && storeImageObj.storeThumbImageUrl != "" {
                            self.shopeDetail?.storeImagesArray.append(storeImageObj)
                        }
                    }
                }
                
                
                
                let storeReviewArray = data["reviews"] as? NSArray
                
                for storeReviewDetail in storeReviewArray! {
                    
                    var storeReviewDict = Dictionary<String, AnyObject>()
                    storeReviewDict = storeReviewDetail as! Dictionary<String, AnyObject>
                    let storeReviewObj = StoreReviewClass()
                    storeReviewObj.reviewUserName = storeReviewDict["user_name"] as? String
                    storeReviewObj.reviewRating = storeReviewDict["ratings"] as? String
                    storeReviewObj.reviewComment = storeReviewDict["comment"] as? String
                    storeReviewObj.reviewUserImageUrl = storeReviewDict["user_thumbnail"] as? String ?? "usericon"
                    self.shopeDetail?.storeReviewArray.append(storeReviewObj)
                    
                }
                
                //print(self.shopeDetail as Any)
                if storeReviewArray?.count != 0 {
                    self.reviewView.isHidden = false
                    self.mainTableView.reloadData()
                } else {
                    self.reviewView.isHidden = true
                }
                
                self.setDataOnView()
                
            }
        }
    }
    
    // MARK:- Delete My Cart data
    fileprivate func deleteItemsInCartAPi() {
        
        let paraDict = NSMutableDictionary()
        
        paraDict["store_id"] = storeId
        
        self.view.isUserInteractionEnabled = false
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .delete, apiName: "buyer/cart/destroyCart") { (response, error,message,statusCode ) in
            
            self.view.isUserInteractionEnabled = true
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /* let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                     let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                     if self.navigationController?.viewControllers.count ?? 0 > 0 {
                     
                     if ((self.navigationController?.viewControllers.last) != nil) {
                     self.navigationController?.popToRootViewController(animated: false)
                     } else {
                     self.navigationController?.viewControllers.removeAll()
                     }
                     }
                     let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                     self.navigationController?.viewControllers = [mainView]
                     let obj = AppDelegate()
                     obj.window?.rootViewController = self.navigationController
                     obj.window?.makeKeyAndVisible()
                     }
                     alertController.addAction(action1)
                     self.present(alertController, animated: true, completion: nil)
                     */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                return
            } else {
                
                // cart status update
                self.shopeDetail?.cartStatus = 1
                let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "categoryViewController") as! categoryViewController
                nVC.storeId = self.storeId
                self.navigationController?.pushViewController(nVC, animated: true)
            }
        }
    }
}


extension String {
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeAColl() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}
