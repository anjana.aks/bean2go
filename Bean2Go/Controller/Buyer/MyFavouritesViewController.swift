//
//  MyFavouritesViewController.swift
//  Bean2Go
//
//  Created by Anjana Aks on 10/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit

class MyFavouritesViewController: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    
    var refreshController = UIRefreshControl()
    var favDataArray = [FavInfoClass]()
    var currentPageCount: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.mainTableView.refreshControl = refreshController
        
        favDataArray = []
        callToGetMyFavList()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Refresh Action
    @objc func refresh(sender:AnyObject) {
        currentPageCount = 1
        favDataArray = []
        self.mainTableView.reloadData()
        
        callToGetMyFavList()
        refreshController.endRefreshing()
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func bookmarkbuttonAction(_ sender: UIButton) {
        
        addToFavAPICall(storeId: favDataArray[sender.tag].store_id)
    }
    
}

//MARK:- CollectionView Datasource and Delegate

extension MyFavouritesViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favDataArray.count
        //return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewTableViewCell
        cell.tag = indexPath.row
        
        cell.bookMarkButton.tag = indexPath.row
        cell.cellBgView.shadow(UIColor.lightGray)
        
        let storeReviewClass: FavInfoClass = favDataArray[indexPath.row]
        
        cell.userNameLbl.text = storeReviewClass.store_name
        cell.descriptionLbl.text = storeReviewClass.store_address
        cell.ratingLabel.text = storeReviewClass.ratings
        let ratingInDouble: String = storeReviewClass.ratings
        cell.reviewView.rating = Double(ratingInDouble) ?? 0
        cell.userImgView.af_setImage(withURL: URL(string: storeReviewClass.store_cover )!)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ShopViewController") as! ShopViewController
        nVC.storeId = favDataArray[indexPath.row].store_id
        self.navigationController?.pushViewController(nVC, animated: false)
    }
}

//MARK:- Web API
extension MyFavouritesViewController{
    
    
    // Mark:- Get favourite list
    fileprivate func callToGetMyFavList(){
        
        let paraDict = NSMutableDictionary()
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "buyer/favStores/getMyFavStores") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                self.clearAllData()
            } else {
                
                self.clearAllData()
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSArray else {
                    return
                }
                
                if data.count != 0 {
                    for item in data{
                        let favInfo = FavInfoClass()
                        let dataDict = item as! NSDictionary
                        
                        favInfo.store_id = dataDict["store_id"] as? Int ?? 0
                        favInfo.store_name = dataDict["store_name"] as? String ?? ""
                        favInfo.store_cover = dataDict["store_cover"] as? String ?? ""
                        favInfo.store_address = dataDict["store_address"] as? String ?? ""
                        favInfo.available = dataDict["available"] as? String ?? ""
                        favInfo.ratings = dataDict["ratings"] as? String ?? ""
                        
                        self.favDataArray.append(favInfo)
                        LogInfo(favInfo)
                    }
                   
                } else {
                    let alertController = UIAlertController(title: "", message: "No data found", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        return
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                }
                 self.mainTableView.reloadData()
              
                
            }
        }
    }
    
    func addToFavAPICall(storeId: Int) {
        
        let paraDict = NSMutableDictionary()
        
        paraDict["store_id"] = storeId
        
        //print(paraDict)
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/favStores/addToFav") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                return
                
            } else if error != nil || response == nil {
                
            }else {
                self.callToGetMyFavList()
            }
        }
    }
    
    func clearAllData(){
        self.favDataArray.removeAll()
    }
}
