//
//  DashBoardViewController.swift
//  Bean2Go
//
//  Created by AKS on 25/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import Firebase

class DashBoardViewController: UIViewController {
    
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var filteredTableView: UITableView!
    @IBOutlet weak var notificationLbl: UILabel!
    @IBOutlet weak var myCartLbl: UILabel!
    @IBOutlet weak var searchTeaxt: UISearchBar!
    @IBOutlet weak var filterTableViewHeightConstant: NSLayoutConstraint!
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    var gmsMarkerArray = [MarkerView]()
    
    var currentLocation: CLLocation?
    var oldLocation: CLLocation?
    var zoomLevel: Float = 18.0
    var placesArray = [PlaceInfo]()
    var filteredArray = [PlaceInfo]()
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    var coordinates = CLLocation()
    var currentLocationMarker = GMSMarker()
    var originLat: CGFloat = 0.0
    var originLng: CGFloat = 0.0
    var refreshArrayOfRestOneTime: Bool = false      //refresh restaurant list only one time
    
    // MARK:- VIEW OVERIDE METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushShopViewController(notification:)), name: NSNotification.Name(rawValue: "openShopViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushMyCartViewController(notification:)), name: NSNotification.Name(rawValue: "openMyCartViewControllerFromDash"), object: nil)
        

        gmsMarkerArray = []
        filteredArray = []
        placesArray = []
        // self.setUpMapViewInitial()
        // filteredArray = placesArray
        //notificationButton.setTitlePositionAdjustment(.init(horizontal: 10, vertical: 10), for: .default)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        gmsMarkerArray = []
//        filteredArray = []
//        placesArray = []
        self.setUpMapViewInitial()
        
//        gmsMarkerArray = []
//        filteredArray = []
//        self.setUpMapViewInitial()
        self.callGetPlacesNearUserLocation(latitude: CGFloat(originLat), longitude: CGFloat(originLng))
    }
    
    @objc func pushShopViewController(notification: NSNotification) {
        
        let dict = notification.object as? [String: Int]
        
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ShopViewController") as! ShopViewController
        nVC.storeId = dict!["storeId"]
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @objc func pushMyCartViewController(notification: NSNotification) {
        let nVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "cartScheduledViewController") as! cartScheduledViewController
        nVC.isFailureTransaction = true
        nVC.isFromScreen = DASHBOARD
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @IBOutlet weak var searchStackView: UIStackView!
    
    // MARK:- BUTTON BAR ACTION
    
    @IBAction func searchButtonDidTappd(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
//        if sender.isSelected {
//            searchStackView.isHidden = false
//        } else {
//            searchStackView.isHidden = true
//        }
    }
    
    
    @IBAction func menuButtonDidTapped(_ sender: UIBarButtonItem) {
        
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
        nVC.openSelectedDelegate = self as openSelectedViewControllerDelegate
        nVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(nVC, animated: false, completion: nil)
    }
    
    @IBAction func myCartButtonDidTapped(_ sender: UIButton) {
       
        let nVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "cartScheduledViewController") as! cartScheduledViewController
        nVC.isFromScreen = DASHBOARD
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @IBAction func searchButtonDidTapped(_ sender: UIBarButtonItem) {
        
    }
    
    @IBAction func notificationButtonDidTapped(_ sender: UIButton) {
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(nVC, animated: false)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openShopViewController"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openMyCartViewControllerFromDash"), object: nil)
        
    }
}

// MARK:- INITIAL SETUP OF VIEW
extension DashBoardViewController {
    
    //MARK:- SET MAp Initial
    func setUpMapViewInitial() {
        
        self.googleMapView.mapType = .normal
        self.googleMapView.isMyLocationEnabled = true
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
 
    }
    
    // Mark:- Show marker on map
    fileprivate func showMarker(position: CLLocationCoordinate2D, index: Int){
        let marker = GMSMarker()
        marker.position = position
        marker.icon = UIImage(named: "location_icon")
        marker.accessibilityLabel = "\(index)" // get index from array when click on each marker to identify
        marker.map = googleMapView
    }
    
    // Mark:- Create Marker and set position
    fileprivate func setMarkerOnMap() {
        if placesArray.count != 0 {
            for i in 0...placesArray.count - 1 {
                let data = placesArray[i]
                
                guard let lat = data.lat else {
                    return
                }
                guard let lon = data.lon else {
                    return
                }
                
                //print("latitude  \(lat)  ******* longitude \(lon)")
                
                let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: Double(lat)!, longitude: Double(lon)!, zoom: zoomLevel)
//                self.googleMapView.camera = camera
                
//                let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(self.originLat),
//                                                      longitude: CLLocationDegrees(self.originLng),
//                                                      zoom: zoomLevel)
                //self.googleMapView.camera = camera
                showMarker(position: camera.target, index: i)
            }
        }
    }
}

// MARK:- GOOGLE MAP DELEGATE
extension DashBoardViewController: GMSMapViewDelegate {
    /* handles Info Window tap */
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        mapView.selectedMarker = nil
        let index:Int! = Int(marker.accessibilityLabel!)
 
        let storeInfo = placesArray[index]
        
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ShopViewController") as! ShopViewController
        nVC.storeId = storeInfo.storeId
        self.navigationController?.pushViewController(nVC, animated: false)
    }
    
    // MARK:- disappear marker info window when click on same maker// or marker work as toggle
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        var isReturn = true
        mapView.selectedMarker = nil
        let index:Int! = Int(marker.accessibilityLabel!)
        
        if self.placesArray.count != 0 {
            for i in 0...placesArray.count - 1 {
                
                if i == index {
                    let storeInfo = placesArray[i]
                    storeInfo.isSelected = !storeInfo.isSelected
                    placesArray[i] = storeInfo
                } else {
                    let storeInfo = placesArray[i]
                    storeInfo.isSelected = false
                    placesArray[i] = storeInfo
                }
            }
        } else {
            mapView.selectedMarker = nil
            isReturn = true
            return isReturn
        }
        
        let storeInfo = placesArray[index]
        
        let latitude =  Double(storeInfo.lat!)
        let longitude = Double(storeInfo.lon!)
        let distanceFromBackend = storeInfo.storeDistance
        //https://www.youtube.com/watch?v=f3xFpRWZEz8
        
        if storeInfo.isSelected == true {

            self.getDirections(origin: "\(self.coordinates.coordinate.latitude),\(self.coordinates.coordinate.longitude)", destination:
            "\(latitude!),\(longitude!)", waypoints: nil, travelMode: "driving" as AnyObject) { [unowned self](status, success, distance) -> Bool in
                if success! {
                    storeInfo.storeDistance = distance
                    self.placesArray[index] = storeInfo
                    marker.tracksInfoWindowChanges = true
                    isReturn = false
                    return isReturn
                }
                else {
                    storeInfo.storeDistance = distanceFromBackend
                    self.placesArray[index] = storeInfo
                    marker.tracksInfoWindowChanges = true
                    isReturn = false
                    return isReturn
                    // print(status!)
                }
            }
        } else {
            mapView.selectedMarker = nil
            isReturn = true
            return isReturn
        }
        
        return isReturn
//        if marker == mapView.selectedMarker {
//            mapView.selectedMarker = nil
//            return true
//        } else {
//            return false
//        }
    }
    
   
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
       // print("didLongPressInfoWindowOf")
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        marker.tracksInfoWindowChanges = true
        let index:Int! = Int(marker.accessibilityLabel!)
        let storeInfo = placesArray[index]
        
        let heightConstant: Int = index != 0 ? 140 : 80
        
        let view = MarkerView.init(frame: CGRect(x: 0, y: 20, width: 230, height: 140))
        
        //if index != 0 {
            view.restauAddressLabel.text = storeInfo.storeAddress
            view.distanceLabel.text = storeInfo.storeDistance
            view.restauNameLabel.text = storeInfo.storeName
            view.closedLbl.alpha = storeInfo.isRestaurantOpen == true ? 0 : 1
       // }
        
        
        gmsMarkerArray.append(view)
        
        return view
    }
    
    //MARK:- getDirections()
    fileprivate func calculateDistance(_ directionsURL: URL, _ completionHandler: ((String?, Bool?, String?) -> Bool)) {
        if let directionsData = NSData(contentsOf: directionsURL){
            do {
                let dictionary = try JSONSerialization.jsonObject(with: directionsData as Data, options: JSONSerialization.ReadingOptions.mutableContainers)
                let status = (dictionary as! Dictionary<String, AnyObject>)["status"] as! String
                
                if status == "OK" {
                    let selectedRoute = ((dictionary as! Dictionary<String, AnyObject>)["routes"] as! Array<Dictionary<NSObject, AnyObject>>)[0] as! Dictionary<String, AnyObject>
                    //self.overviewPolyline = (self.selectedRoute!)["overview_polyline"] as! Dictionary<NSObject, AnyObject> as! Dictionary<String, AnyObject>
                    
                    let legs = (selectedRoute)["legs"] as! Array<Dictionary<String, AnyObject>>
                    
                    var totalDistanceInMeters: UInt = 0
                    var totalDurationInSeconds: UInt = 0
                    
                    for leg in legs {
                        totalDistanceInMeters += (leg["distance"] as! Dictionary<String, AnyObject>)["value"] as! UInt
                        totalDurationInSeconds += (leg["duration"] as! Dictionary<String, AnyObject>)["value"] as! UInt
                    }
                    
                    var totalDistance: String = ""
                    
                    let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
                    
                    totalDistance = "\(distanceInKilometers) Km"
                    
                    //print("totalDistance   ******\(totalDistance)")
                    if distanceInKilometers == 0.0 {
                        totalDistance = ""
                        totalDistance = "\(totalDistanceInMeters) m"
                        //print("totalDistanceInMeters   ******\(totalDistance)")
                    }
                    
                    
                    
//                    let mins = totalDurationInSeconds / 60
//                    let hours = mins / 60
//                    let days = hours / 24
//                    let remainingHours = hours % 24
//                    let remainingMins = mins % 60
//                    let remainingSecs = totalDurationInSeconds % 60
//
//                    var totalDuration = "Duration: \(days) d, \(remainingHours) h, \(remainingMins) mins, \(remainingSecs) secs"
//
//                    print("totalDistance   ******\(totalDuration)")
                    
                  _ = completionHandler(status, true, totalDistance)
                }
                else {
                     _ = completionHandler(status, false, "")
                }
            }
            catch {
                print(error)
                 _ = completionHandler("", false, "")
            }
        }
    }
    
    func getDirections(origin: String!, destination: String!, waypoints: Array<String>!, travelMode: AnyObject!, completionHandler: @escaping ((_ status: String?, _ success: Bool?, _ distance: String?) -> Bool)) {
        if origin != nil {
            if destination != nil {
                // var directionsURLString = baseURLDirections + "origin=" + origin + "&destination=" + destination
                //"&mode=walking"
                
                let directionsURLString = "\(baseURLDirections)origin=\(origin!)&destination=\(destination!)&mode=driving&sensor=false&mode=car&key=AIzaSyD71EOMcYPgpUlI6qOe7LtWm6PC3hc_Rac"
                
//                &mode=driving&sensor=false&mode=car&key=AIzaSyD71EOMcYPgpUlI6qOe7LtWm6PC3hc_Rac
                
                //let directionsURLString = "\(baseURLDirections)origin=\(origin!)&destination=\(destination!)&mode=driving&sensor=false&mode=bus&key=AIzaSyD71EOMcYPgpUlI6qOe7LtWm6PC3hc_Rac" //&units=metric
                
                //print(directionsURLString)
                directionsURLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                
                if let directionsURL = URL(string: directionsURLString){
                    
                    if Thread.isMainThread {
                        
                        calculateDistance(directionsURL, completionHandler)
                    } else {
                        DispatchQueue.main.sync {
                        calculateDistance(directionsURL, completionHandler)
                        }
                    }
                }
               
            }
            else {
                 _ = completionHandler("Destination is nil.", false, "")
            }
        }
        else {
             _ = completionHandler("Origin is nil", false,"")
        }
    }
   
}

//MARK:- LOCATION DELEGATE METHOD
extension DashBoardViewController: CLLocationManagerDelegate {
    
    //MARK:- GET current location of user
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        googleMapView.isMyLocationEnabled = true
        
        googleMapView.settings.myLocationButton = true
      
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        coordinates = location
        if oldLocation != nil {
            // if oldLocation?.coordinate.latitude != location.coordinate.latitude {
            if oldLocation != location {
                oldLocation = location
                let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                      longitude: location.coordinate.longitude,
                                                      zoom: zoomLevel)
                
                //googleMapView.camera = camera
                
                self.googleMapView.animate(to: camera)
                
                self.originLat = CGFloat(location.coordinate.latitude)
                self.originLng = CGFloat(location.coordinate.longitude)

                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                    // your code here
                    self.callGetPlacesNearUserLocation(latitude: CGFloat(location.coordinate.latitude), longitude: CGFloat(location.coordinate.longitude))
                }

            }
        } else {
            oldLocation = location
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                  longitude: location.coordinate.longitude,
                                                  zoom: zoomLevel)
            
            //googleMapView.camera = camera
            self.googleMapView.animate(to: camera)
            
            self.originLat = CGFloat(location.coordinate.latitude)
            self.originLng = CGFloat(location.coordinate.longitude)
            
            self.callGetPlacesNearUserLocation(latitude: CGFloat(location.coordinate.latitude), longitude: CGFloat(location.coordinate.longitude))
          
        }

    }
    
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
        //googleMapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        //print("Error: \(error)")
    }
    
}

extension DashBoardViewController {
    
    // MARK:- get restaurant list from API
    func callGetPlacesNearUserLocation(latitude: CGFloat, longitude: CGFloat) {
        
        let paraDict = NSMutableDictionary()
        paraDict["lat"] = latitude //28.528232
        paraDict["lng"] = longitude //77.275734
        
        //print(paraDict)
        
        //indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "getData/getStoresNearMe") { (response, error,message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
//                ConveienceClass.networkAlert(controller: self, completionHandler: { (sucess) in
//                    if sucess==true {
//                        //self.navigationController?.popViewController(animated: true)
//                    }
//                })
                return
            }  else {
                guard let data = response?["data"] as? NSArray else {
                    return
                }
                
                guard let metaData = response?["meta"] as? NSDictionary else {
                    return
                }
                
                let notificationCount = metaData["notificationCount"] as? Int ?? 0
                
                let cartCount = metaData["cart_count"] as? Int ?? 0
                
                USER_DEFAULTS.set(notificationCount, forKey: NOTIFICATION_COUNT)
                
                let orderNumber = metaData["order_number"] as? String ?? ""
                
                if orderNumber != "" {
                    USER_DEFAULTS.set(orderNumber, forKey: ORDER_NUMBER)
                    SocketManagerClass.shared.eventsHandleInSocket(roomName: orderNumber, id: 6) // order accept
                }
                
                if notificationCount != 0 {
                    //self.notificationButton.setTitle("\(notificationCount)", for: .normal)
                    self.notificationLbl.text = "\(notificationCount)"
                } else {
                    self.notificationLbl.text = ""
                }
                
                UIApplication.shared.applicationIconBadgeNumber = Int(notificationCount)
                
                if cartCount != 0 {
                    //self.notificationButton.setTitle("\(notificationCount)", for: .normal)
                    self.myCartLbl.text = "\(cartCount)"
                } else {
                    self.myCartLbl.text = ""
                }
                
                let ratingDict = metaData["rating"] as? NSDictionary
                
//                let order_Id = ratingDict?["order_id"] as? Int ?? 0
//                
//                let store_Id = ratingDict?["store_id"] as? Int ?? 0
//                
//                    let ratingVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "ratingViewController") as! ratingViewController
//                    ratingVC.orderId = order_Id
//                    ratingVC.storeId = store_Id
//                    
//                    ratingVC.modalPresentationStyle = .overCurrentContext
//                    self.present(ratingVC, animated: true, completion: nil)
                
                
                if ratingDict != nil {
                    
                    let order_Id = ratingDict?["order_id"] as? Int ?? 0
                    
                    let store_Id = ratingDict?["store_id"] as? Int ?? 0
                    
                    if order_Id != 0 && store_Id != 0 {
                        let ratingVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "ratingViewController") as! ratingViewController
                        ratingVC.orderId = order_Id
                        ratingVC.storeId = store_Id
                        
                        ratingVC.modalPresentationStyle = .overCurrentContext
                        self.present(ratingVC, animated: true, completion: nil)
                    }
                }
                
                
               // if !self.refreshArrayOfRestOneTime {
                    self.refreshArrayOfRestOneTime = true
                    if data.count != 0 {
                        self.placesArray = []
                        for i in 0...data.count - 1 {
                            let storeInfo = data[i] as? NSDictionary
                            
                            let placeObj = PlaceInfo()
                            placeObj.lat = storeInfo?["lat"] as? String
                            placeObj.lon = storeInfo?["lng"] as? String
                            placeObj.storeId = storeInfo?["store_id"] as? Int
                            placeObj.storeName = storeInfo?["store_name"] as? String
                            placeObj.storeAddress = storeInfo?["store_address"] as? String
                            placeObj.storeDistance = storeInfo?["distance"] as? String
                            let isOpen = storeInfo?["isRestaurantOpen"] as? Int
                            placeObj.isRestaurantOpen = isOpen == 1 ? true : false
                            //placeObj.storeDistance = String(format:"%f km", (storeInfo?["distance"] as? Double ?? 0.9))
                            placeObj.storeContactNumber = storeInfo?["store_contact_number"] as? String
                            placeObj.storeVehiNumber = storeInfo?["store_vehicle_number"] as? String
                            placeObj.storeParkingAvailable = storeInfo?["store_parking_available"] as? Int == 1 ? true : false
                            
                            self.placesArray.append(placeObj)
                            
                        }
                        
                        self.setMarkerOnMap()
                    } else {
                        let alertController = UIAlertController(title: "", message: "No data found", preferredStyle: .alert)
                        let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                            
                        }
                        alertController.addAction(action1)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
               // }
            }
        }
        
        
//        let url = URL(string: "\(baseURL)getData/getStoresNearMe")
//
//        //self.indicatorViewObj.showIndicatorView(viewController: self)
//
//        Alamofire.request(url ?? "", method: .post, parameters: paraDict as? [String: Any], encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json"]).responseJSON { response in
//            print(response)
//
//            // self.indicatorViewObj.hideIndicatorView()
//
//
//            guard let result = response.result.value else{
//                return
//            }
//            let JSON = result as! NSDictionary
//
//            let error = response.error
//
//            if error != nil {
//                self.present(UIAlertController.alertWithTitle(title: "", message: "Network connection problem" , buttonTitle: "OK"), animated: true, completion: nil)
//                return
//            } else {
//                //to get status code
//                if let status = response.response?.statusCode {
//                    switch(status){
//                    case 200:
//                        //let data = JSON["data"] as? NSMutableArray
//
//                        guard let data = JSON["data"] as? NSArray else {
//                            return
//                        }
//
//                        if data.count != 0 {
//                            for i in 0...data.count - 1 {
//                                let storeInfo = data[i] as? NSDictionary
//
//                                let placeObj = PlaceInfo()
//                                placeObj.lat = storeInfo?["lat"] as? String
//                                placeObj.lon = storeInfo?["lng"] as? String
//                                placeObj.storeId = storeInfo?["store_id"] as? Int
//                                placeObj.storeName = storeInfo?["store_name"] as? String
//                                placeObj.storeAddress = storeInfo?["store_address"] as? String
//                                placeObj.storeDistance = String(format:"%f km", (storeInfo?["distance"] as? Double ?? 0.9))
//                                placeObj.storeContactNumber = storeInfo?["store_contact_number"] as? String
//                                placeObj.storeVehiNumber = storeInfo?["store_vehicle_number"] as? String
//                                placeObj.storeParkingAvailable = storeInfo?["store_parking_available"] as? Int == 1 ? true : false
//
//                                self.placesArray.append(placeObj)
//
//                            }
//
//                            self.setMarkerOnMap()
//                        }
//
//                    case 422:
//                        let message = JSON["message"] as? String ?? ""
//                        self.present(UIAlertController.alertWithTitle(title: "", message: message , buttonTitle: "OK"), animated: true, completion: nil)
//                    default:
//                        print("error with response status: \(status)")
//                    }
//                }
//            }
//
//        }
        
        
    }
}

// MARK:- Filtered Tableview Datasource and Delegate
extension DashBoardViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let label = cell.viewWithTag(1010) as! UILabel
        label.text = filteredArray[indexPath.row].storeName

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storeInfo = filteredArray[indexPath.row]
        
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ShopViewController") as! ShopViewController
        nVC.storeId = storeInfo.storeId
        self.navigationController?.pushViewController(nVC, animated: false)
    }
}

//MARK:- Search Bar Delegate

extension DashBoardViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            filteredArray = []
            filteredArray = placesArray
            filteredTableView.reloadData()
            filterTableViewHeightConstant.constant = filteredTableView.contentSize.height
            return
        }
        
        filteredArray = placesArray.filter({ (placeInfo) -> Bool in
            (placeInfo.storeName?.lowercased().contains(searchText.lowercased()))!
        })
        //filteredTableView.reloadData()
        //filterTableViewHeightConstant.constant = filteredTableView.contentSize.height

        
    }
}

//    var drawerList = ["Dashboard","Profile","My Account","Orders","Settings","Notifications","About us","Contact us","FAQ","Privacy Policy","Rate the app","Share","Logout"]
//MARK:- SIDE MENU
extension DashBoardViewController: openSelectedViewControllerDelegate {
    func openSelectedViewCOntroller(selectedView: String) {
        switch selectedView {
        case "Profile":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "My Account":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "myAccountViewController") as! myAccountViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Orders":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "MyOrderViewController") as! MyOrderViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "My Favourite":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "MyFavouritesViewController") as! MyFavouritesViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Payment":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
            nVC.isFromSideMenu = true
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Notifications":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "About us":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Contact us":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            
            break
        case "FAQ":
            break
        case "Privacy Policy":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Rate the app":
            break
        case "Share":
            break
        case "Logout":
            self.logoutButtonDidTapped()
            break
        default:
            break
        }
    }
    
    fileprivate func logoutButtonDidTapped() {
        
        let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            
            kAppDelegate.logoutAPI(viewController: self)
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                //print ("Signout")
            } catch let signOutError as NSError {
                //print ("Error signing out: %@", signOutError)
            }
           
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
        /*ConveienceClass.logoutAlert(controller: self) { (sucess) in
            if sucess ?? false {
                
                if self.navigationController?.viewControllers.count ?? 0 > 0 {
                    
                    if ((self.navigationController?.viewControllers.last) != nil) {
                        self.navigationController?.popToRootViewController(animated: false)
                    } else {
                        self.navigationController?.viewControllers.removeAll()
                    }
                }
                let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                self.navigationController?.viewControllers = [mainView]
                let obj = AppDelegate()
                obj.window?.rootViewController = self.navigationController
                obj.window?.makeKeyAndVisible()
                
            }
        }*/
    }
    
    fileprivate func logoutAPI() {
        let paraDict = NSMutableDictionary()
       
        paraDict["device_id"] = USER_DEFAULTS.value(forKey: DEVICE_TOKEN)
        paraDict["gcm_key"] = USER_DEFAULTS.value(forKey: FCM_TOKEN)
        
        //print(paraDict)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/logout") { (response, error,message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else if statusCode == "201" {
                    
                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as!SignUpViewController
                    self.navigationController?.pushViewController(nVC, animated: true)
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                
                return
            }else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                return
            } else {
                let sucess = ConveienceClass.clearAllDefaultValue()
                let room: String = USER_DEFAULTS.value(forKey: ORDER_NUMBER) as? String ?? ""
                if room != "" {
                    SocketManagerClass.shared.leaveRoom(key: room)
                }
                
                
                if sucess == true {
                    
                    if self.navigationController?.viewControllers.count ?? 0 > 0 {
                        
                        if ((self.navigationController?.viewControllers.last) != nil) {
                            self.navigationController?.popToRootViewController(animated: false)
                        } else {
                            self.navigationController?.viewControllers.removeAll()
                        }
                    }
                    let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                    self.navigationController?.viewControllers = [mainView]
                    let obj = AppDelegate()
                    obj.window?.rootViewController = self.navigationController
                    obj.window?.makeKeyAndVisible()
                    
                }
            }
        }
    }
    
}




//Json File data get
// func jsonDataRead() {
/*do {
 if let file = Bundle.main.url(forResource: "LocationData", withExtension: "json") {
 let data = try Data(contentsOf: file)
 let json = try JSONSerialization.jsonObject(with: data, options: [])
 if let object = json as? [String: Any] {
 parseJson(json: object)
 } else {
 print("JSON is invalid")
 //delegate?.isFailReadJson(msg: "JSON is invalid")
 }
 } else {
 print("no file")
 // delegate?.isFailReadJson(msg: "No File found")
 }
 } catch {
 print(error.localizedDescription)
 //delegate?.isFailReadJson(msg: error.localizedDescription)
 }*/
//}


//Pars json from array
/*func parseJson(json : [String: Any])  {
 //print(json)
 let pathArray = json["Locations"] as! NSArray
 for data in pathArray
 {
 let dic = data as! NSDictionary
 //print(dic)
 guard let lat = dic.value(forKey: "lat") as? String else {
 return
 }
 guard let lon:String = dic.value(forKey: "long") as? String else {
 return
 }
 //            guard let angle:String = dic.value(forKey: "angle") as? String else {
 //                return
 //            }
 
 let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: Double(lat)!, longitude: Double(lon)!, zoom: 10.0)
 self.googleMapView.camera = camera
 showMarker(position: camera.target,index: 0)
 
 //arrayMapPath.append(MapPath(lat: Double(lat), lon: Double(lon), angle: Double(angle)))
 }
 
 //        if arrayMapPath.count > 0
 //        {
 //            delegate?.isSucessReadJson()
 //        }
 }*/
