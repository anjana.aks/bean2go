//
//  PaygateCardDetailsViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 25/02/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import MessageUI


class CardDetails:NSObject{
    var cardHolderName: String?
    var cardNumber: String?
    var expireMonth: String?
    var expireYear: String?
    var cvvNumber: String?
    var expireDate: String?
    var isSelected: Bool = false
    var cardId: String = ""
}

class PaygateCardDetailsViewController: UIViewController {
    
    @IBOutlet var cardNumberTextFld: UITextField!
    @IBOutlet var cvvTxtFld: UITextField!
    @IBOutlet var cardHolderNameTxtField: UITextField!
    @IBOutlet var ProceedBtn: UIButton!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var totalTransactionAmountLbl: UILabel!
    @IBOutlet var countryCodeLbl: UILabel!
    @IBOutlet var expireMonthLbl: UILabel!
    @IBOutlet var expireYearLbl: UILabel!
    @IBOutlet var expireMonthView: UIView!
    @IBOutlet var expireYearView: UIView!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var expireMonthPicker: UIPickerView!
    @IBOutlet weak var expireYearPicker: UIPickerView!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var amountStackView: UIStackView!
    var isNewCard: Bool = true
    var isFromSideMenu: Bool = true
    let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
    
    
    var PassToDic = [String:String]()
    var cardUrl = "payhost/process.trans"
    var decoder = JSONDecoder()
    var merchantInfo = MerchantInfo()
    var cardInfo = CardDetails()
    var pickerMonthArray = [String]()
    var pickerYearArray = [String]()
    var mainStr = String()
    var isNextButtonTapped: Bool = false
    
    var orderNo = String()
    var orderId = Int()
    
//    var merchantId: String = "10011072130" // Testing Credentials
//    var merchantEncyptKey: String = "test"
    
    var monthArray = ["01","02","03","04","05","06","07","08","09","10","11","12"]
  
    var merchantId: String = "1026676100014" // Live Credentials
    var merchantEncyptKey: String = "Z7ws0GWf9EtcEHM0diQwHj9zHdo2S"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        setInitial()
        cardNumberTextFld.addToolBar()
        cvvTxtFld.addToolBar()
        
        for month in monthArray {
            self.pickerMonthArray.append(month)
        }
        
        self.expireMonthPicker.reloadAllComponents()
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        //print(formattedDate)
        
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: date)
        
        for i in currentYear...(currentYear + 30){
            self.pickerYearArray.append(String(i))
        }
        
        self.expireYearPicker.reloadAllComponents()
        
    }
    
    // MARK:- Set Initial Controller
    fileprivate func setInitial(){
        
        self.cardHolderNameTxtField.isUserInteractionEnabled = true
        self.cardNumberTextFld.isUserInteractionEnabled = true
        self.expireMonthLbl.isUserInteractionEnabled = true
        self.expireYearLbl.isUserInteractionEnabled = true
        self.cvvTxtFld.isUserInteractionEnabled = true
        
        if isFromSideMenu == true {
            self.amountStackView.isHidden = true
            if isNewCard == true {
                self.cardHolderNameTxtField.becomeFirstResponder() // make first responder
                self.nextButton.isHidden = false
                self.nextButton.setTitle("Add Card", for: .normal)
            } else {
                
                self.cardHolderNameTxtField.text = cardInfo.cardHolderName
                self.cardNumberTextFld.text = cardInfo.cardNumber
                self.expireMonthLbl.text = cardInfo.expireMonth
                self.expireYearLbl.text = "\(cardInfo.expireYear!)"
                self.cvvTxtFld.text = cardInfo.cvvNumber
                
                self.cardHolderNameTxtField.isUserInteractionEnabled = false
                self.cardNumberTextFld.isUserInteractionEnabled = false
                self.expireMonthLbl.isUserInteractionEnabled = false
                self.expireYearLbl.isUserInteractionEnabled = false
                self.cvvTxtFld.isUserInteractionEnabled = false
                
                //                self.cardHolderNameTxtField.text = cardInfo.cardHolderName
                //                self.cardNumberTextFld.text = cardInfo.cardNumber
                //                self.expireMonthLbl.text = cardInfo.expireMonth
                //                self.expireYearLbl.text = cardInfo.expireYear
                //                self.cvvTxtFld.text = cardInfo.cvvNumber
                self.nextButton.isHidden = true
            }
            
        } else {
            self.amountStackView.isHidden = false
            self.nextButton.setTitle("Next", for: .normal)
            if isNewCard == false {
               
                self.cardHolderNameTxtField.text = cardInfo.cardHolderName
                self.cardNumberTextFld.text = cardInfo.cardNumber
                self.expireMonthLbl.text = cardInfo.expireMonth
                self.expireYearLbl.text = "\(cardInfo.expireYear!)"
                self.cvvTxtFld.text = cardInfo.cvvNumber
            } else {
                self.cardHolderNameTxtField.becomeFirstResponder() // make first responder
            }
        }
    }
    
    // MARK:- Set layout
    fileprivate func setLayout(){
        self.nextButton.layer.addborder(width: 1, color: UIColor.gray)
        self.nextButton.layer.cornerRadius = 5
        
        self.expireMonthView.layer.addborder(width: 1, color: UIColor.gray)
        self.expireYearView.layer.addborder(width: 1, color: UIColor.gray)
        
        self.totalTransactionAmountLbl.text = self.merchantInfo.totalAmount
       
    }
    
    // mARK:- Hide Picker View
    fileprivate func hidePickerView() {
        self.expireYearPicker.isHidden = true
        self.expireMonthPicker.isHidden = true
        self.toolBar.isHidden = true
    }
    
    @IBAction func doneBtnDidTapped(_ sender: UIBarButtonItem) {
        self.hidePickerView()
    }
    
    //MARK:- Button Action
    @IBAction func backBtndidTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate func generateEncryptedStg() -> String {
        do {
            let sourceData = "AES256".data(using: .utf8)!
            let dic = ["CardHolderName": cardInfo.cardHolderName, "CardNumber": cardInfo.cardNumber, "ExpiryDate": "04/23","CVV":"999"]
            
            let cookieHeader = (dic.compactMap({ (key, value) -> String in
                return "\(key):\(value!)"
            }) as Array).joined(separator: ";")
            
            let password = cookieHeader
            let salt = AES256Crypter.randomSalt()
            let iv = AES256Crypter.randomIv()
            let key = try AES256Crypter.createKey(password: password.data(using: .utf8)!, salt: salt)
            let aes = try AES256Crypter(key: key, iv: iv)
            //print(aes)
            let encryptedData = try aes.encrypt(sourceData)
            let decryptedData = try aes.decrypt(encryptedData)
            let encodedString: String = String(decoding: encryptedData, as: UTF8.self)
            //print("Encrypted hex string: \(encodedString)")
            return encodedString
            //print("Decrypted hex string: \(decryptedData.hexString)")
        } catch {
//            print("Failed")
//            print(error)
             return "encodedString"
        }
    }
    
    fileprivate func generateEncryptedString() -> String {
        let rsaObj = RSAWrapper.RSASharedObj
        var encryptedString: String = ""
        let userId:Int = USER_DEFAULTS.value(forKey: USER_ID) as! Int
        let privateKey: String = "2009WED112201245" //YMDAYUIDHMS
        let success : Bool = (rsaObj.generateKeyPair(keySize: 2048, privateTag: privateKey, publicTag: "com.atarmkplant"))
        if (!success) {
            //print("Failed")
            return encryptedString
        }
        
        let dic = ["CardHolderName": cardInfo.cardHolderName, "CardNumber": cardInfo.cardNumber, "ExpiryDate": "04/23","CVV":"999"]
        
        let cookieHeader = (dic.compactMap({ (key, value) -> String in
            return "\(key):\(value!)"
        }) as Array).joined(separator: ";")
        
        let encryption = rsaObj.encryptBase64(text: cookieHeader)
        encryptedString = encryption
        
        return encryptedString
    }
    
    @IBAction func nextBtndidTapped(_ sender: UIButton) {
        
       // if !isNextButtonTapped {
            isNextButtonTapped = true
            if isAllVerified() {
                
                cardInfo.expireDate = "\(cardInfo.expireMonth!)\(cardInfo.expireYear!)"
                
                if isNewCard == true {
                    
                    // if self.isFromSideMenu {
                    let newDate = "\(self.cardInfo.expireMonth!)/\(self.cardInfo.expireYear!)"
                    
                    let dic = ["CardHolderName": self.cardInfo.cardHolderName, "CardNumber": self.cardInfo.cardNumber, "ExpiryDate": "\(newDate)","CVV":self.cardInfo.cvvNumber]
                    
                    let cookieHeader = (dic.compactMap({ (key, value) -> String in
                        return "\(key):\(value!)"
                    }) as Array).joined(separator: ";")
                    
                    let encryptString: String = cookieHeader
                    //let encryptString: String = self.generateEncryptedStg()
                    
                    if encryptString != "" {
                        self.callAPIToAddNewCard(encryptedKey: encryptString)
                    }
                    // return
                    //} else {
//                    if self.isFromSideMenu {
//                        self.callAPIToInitiatePaymentGateway(orderId: self.orderId, completionHandler: { (sucess) in
//
//                            self.isNextButtonTapped = false
//
//                            self.setMerchantData(orderId: self.orderId, orderNo: self.orderNo)
//                            self.mainStr = self.setWebRequsetParms()
//                            self.callCheckSumAPI()
//                        })
//                    }
                    // }
                } else {
                    self.callAPIToInitiatePaymentGateway(orderId: self.orderId, completionHandler: { (sucess) in
                        
                        self.isNextButtonTapped = false
                        
                        self.setMerchantData(orderId: self.orderId, orderNo: self.orderNo)
                        self.mainStr = self.setWebRequsetParms()
                        self.callCheckSumAPI()
                    })
                }
                
            } else {
                isNextButtonTapped = false
            }
       // }
    }
    
    @IBAction func pickExpireMonthDidTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.expireMonthPicker.isHidden = false
        self.expireYearPicker.isHidden = true
        toolBar.isHidden = false
    }
    
    @IBAction func pickExpireYearBtnDidTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.expireMonthPicker.isHidden = true
        self.expireYearPicker.isHidden = false
        toolBar.isHidden = false
    }
    
    // MARK:- TOUCH EVENT to hide keyboard
    @IBAction func viewdidtapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.hidePickerView()
    }
    
    //MARK:-  Validation
    
    func isAllVerified() -> Bool{
        
        var isAllVerified = false
        self.cardInfo.cardHolderName = trimWhiteSpace(str: self.cardHolderNameTxtField.text ?? "")
        self.cardInfo.cardNumber = trimWhiteSpace(str: self.cardNumberTextFld.text ?? "")
        self.cardInfo.expireMonth = trimWhiteSpace(str: self.expireMonthLbl.text ?? "")
        self.cardInfo.expireYear = trimWhiteSpace(str: self.expireYearLbl.text ?? "")
        self.cardInfo.cvvNumber = trimWhiteSpace(str: self.cvvTxtFld.text ?? "")
        
        if self.cardInfo.cardHolderName == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter name on card", buttonTitle: "OK"), animated: true, completion: nil)
            return isAllVerified
        } else if self.cardInfo.cardNumber == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your card number", buttonTitle: "OK"), animated: true, completion: nil)
            return isAllVerified
        }
        
//        else if self.cardInfo.cardNumber?.count == 0 || (self.cardInfo.cardNumber?.count)! < 16 {
//            present(UIAlertController.alertWithTitle(title: "", message: "The card number must be at least 16 digits", buttonTitle: "OK"), animated: true, completion: nil)
//            return isAllVerified
//        }
        
        else if self.cardInfo.expireMonth == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please select your expire month", buttonTitle: "OK"), animated: true, completion: nil)
            return isAllVerified
        } else if self.cardInfo.expireYear == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please select your expire year", buttonTitle: "OK"), animated: true, completion: nil)
            return isAllVerified
        } else if self.cardInfo.cvvNumber == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter CVV", buttonTitle: "OK"), animated: true, completion: nil)
            return isAllVerified
        } else if (self.cardInfo.cvvNumber?.count)! < 3 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter valid cvv number", buttonTitle: "OK"), animated: true, completion: nil)
            return isAllVerified
        } else {
            isAllVerified = true
        }
        return true
    }
}


// MARK:- UIText Delegate
extension PaygateCardDetailsViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == cvvTxtFld {
            var str : NSString = (textField.text as NSString?)!
            str = str.replacingCharacters(in: range, with: string) as NSString
            if trimWhiteSpace(str: str as String).count > 3 {
                return false
            }
        }
        
        if textField == cardNumberTextFld {
            var str : NSString = (textField.text as NSString?)!
            str = str.replacingCharacters(in: range, with: string) as NSString
            if trimWhiteSpace(str: str as String).count > 16 {
                return false
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        hidePickerView()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == cardHolderNameTxtField {
            self.cardNumberTextFld.becomeFirstResponder()
        }
//        else if textField == cardNumberTextFld {
//            self.cvvTxtFld.becomeFirstResponder()
//        }else{
            self.resignFirstResponder()
        //}
        return true
    }
}

extension PaygateCardDetailsViewController: UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == expireMonthPicker {
            return pickerMonthArray.count
        }
        
        if pickerView == expireYearPicker {
            return pickerYearArray.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == expireMonthPicker {
            return pickerMonthArray[row]
        }
        
        if pickerView == expireYearPicker {
            return pickerYearArray[row]
        }
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == expireMonthPicker {
            self.expireMonthLbl.text = pickerMonthArray[row]
        } else if pickerView == expireYearPicker {
            self.expireYearLbl.text = pickerYearArray[row]
        }
    }
}

//Mark:- PayGate Implementation
extension PaygateCardDetailsViewController: XMLParserDelegate {
    
    func setMerchantData(orderId : Int, orderNo: String){
        //        self.merchantInfo.payGateId = "10011072130"
        //        self.merchantInfo.encryptionKey = "test"
        
        self.merchantInfo.payGateId = self.merchantId
        self.merchantInfo.encryptionKey = self.merchantEncyptKey
        
        self.merchantInfo.buyerName = USER_DEFAULTS.value(forKey: USER_NAME) as? String ?? ""
        self.merchantInfo.buyerEmail = USER_DEFAULTS.value(forKey: EMAIL_ADDRESS) as? String ?? ""
        self.merchantInfo.buyerLastName = ""
        self.merchantInfo.orderID = orderId
        self.merchantInfo.orderNumber = orderNo
    }
    
    func paygateTotal() -> String {
                
        var returnString = self.merchantInfo.totalAmount
//        var returnString = "1.0"
        let range: Range<String.Index> = returnString.range(of: ".")!
        let index: Int = returnString.distance(from: returnString.startIndex, to: range.lowerBound)
        //print(returnString.count-index)
        if returnString.count-index < 3{
            for _ in 1...(returnString.count-index - 1) {
                returnString.append("0")
            }
        }
        returnString = returnString.replacingOccurrences(of: ".", with: "")
        
        return returnString
    }
    
    func callCheckSumAPI(){
        ServiceHelper.sharedInstance.callPaygateApi(parms: mainStr, Url: cardUrl, Success: {(responseObject,ResponseString) -> () in
            DispatchQueue.main.async {
                //self.req_textview.resignFirstResponder()
                if(ResponseString.contains("Complete")){
                    
                    self.ReadWebPaymnetMethod(Response: ResponseString, success: true)
                }else{
                    self.ReadWebPaymnetMethod(Response: ResponseString, success: false)
                    //LogInfo("Error...\(ResponseString)")
                }
            }
            
        }, Failure: {(error) -> () in
            //print(error!.description)
            self.present(UIAlertController.alertWithTitle(title: "", message: "Request Failed" , buttonTitle: "OK"), animated: true, completion: nil)
        },showLoader: true, hideLoader: true)
    }
    
    func ReadWebPaymnetMethod(Response:String, success: Bool){
        var paygateid = String()
        var pay_requ_id = String()
        var ReferenceId  = String()
        var cheakSumid  = String()
        var error = String()
        
        var xmlDictionary = try? XMLReader.dictionary(forXMLString: Response)
        let dic1 : NSDictionary = xmlDictionary!["SOAP-ENV:Envelope"] as! NSDictionary
        let dic2 : NSDictionary = dic1["SOAP-ENV:Body"] as! NSDictionary
        
        if success {
            
            let dic3 : NSDictionary = dic2["ns2:SinglePaymentResponse"] as! NSDictionary
            var dic4  = NSDictionary()
            
            dic4 = dic3["ns2:CardPaymentResponse"] as! NSDictionary
            
            if dic4["ns2:Redirect"] != nil {
                let dic5  = dic4["ns2:Redirect"] as! NSDictionary
                
                if(dic5["ns2:RedirectUrl"] != nil){
                    let dic6 = dic5["ns2:RedirectUrl"] as! NSDictionary
                    let redirectUrl = dic6["text"] as! String
                    let urlParms = dic5["ns2:UrlParams"] as! NSArray
                    
                    let dic2 = urlParms[0] as! NSDictionary
                    let payreqVal = dic2["ns2:value"] as! NSDictionary
                    pay_requ_id = payreqVal["text"] as! String
                    
                    let dic3 = urlParms[1] as! NSDictionary
                    let ReferenceDic = dic3["ns2:value"] as! NSDictionary
                    paygateid = ReferenceDic["text"] as! String
                    
                    let dic4 = urlParms[2] as! NSDictionary
                    let cheakSum = dic4["ns2:value"] as! NSDictionary
                    
                    cheakSumid = cheakSum["text"] as! String
                    
                    
                    var PassToDic = [String:String]()
                    PassToDic["url"] = redirectUrl
                    PassToDic["ref_id"] = ReferenceId
                    PassToDic["chaeksum_id"] = cheakSumid
                    PassToDic["payGate_id"] = paygateid
                    PassToDic["pay_requ_id"] = pay_requ_id
                    
//                    if isNewCard {
//                        let newDate = "\(self.cardInfo.expireMonth!)/\(self.cardInfo.expireYear!)"
//                        
//                        let dic = ["CardHolderName": self.cardInfo.cardHolderName, "CardNumber": self.cardInfo.cardNumber, "ExpiryDate": "\(newDate)","CVV":self.cardInfo.cvvNumber]
//                        
//                        let cookieHeader = (dic.compactMap({ (key, value) -> String in
//                            return "\(key):\(value!)"
//                        }) as Array).joined(separator: ";")
//                        
//                        let encryptString: String = cookieHeader
//                        //let encryptString: String = self.generateEncryptedStg()
//                        
//                        if encryptString != "" {
//                            self.callAPIToAddNewCard(encryptedKey: encryptString)
//                        }
//                    }
                    
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                    vc.PassToDic = PassToDic
                    //vc.orderId = self.merchantInfo.orderID
                    self.navigationController?.pushViewController(vc, animated:true)
                }
                
                
            } else if dic4["ns2:Status"] != nil{
                let dic5  = dic4["ns2:Status"] as! NSDictionary
                let resultDesc = dic5["ns2:ResultDescription"] as! NSDictionary
                let resultDescValue = resultDesc["text"] as! String
                
                self.present(UIAlertController.alertWithTitle(title: "", message: resultDescValue , buttonTitle: "OK"), animated: true, completion: nil)
            }else{
                
                
                
                self.present(UIAlertController.alertWithTitle(title: "", message: "Request Failed" , buttonTitle: "OK"), animated: true, completion: nil)
            }
        } else {
            
            
            if dic2["SOAP-ENV:Fault"] != nil {
                let dic3 : NSDictionary = dic2["SOAP-ENV:Fault"] as! NSDictionary
                let dic4 : NSDictionary = dic3["detail"] as! NSDictionary
                let paygateError = dic4["payhost:error"] as! NSArray
                
                let ErrorPay : NSDictionary = paygateError[1] as! NSDictionary
                error = ErrorPay["text"] as! String
                self.present(UIAlertController.alertWithTitle(title: "", message: error , buttonTitle: "OK"), animated: true, completion: nil)
            }else if dic2["ns2:SinglePaymentResponse"] != nil {
                let dic3 : NSDictionary = dic2["ns2:SinglePaymentResponse"] as! NSDictionary
                let dic4  = dic3["ns2:CardPaymentResponse"] as! NSDictionary
                if dic4["ns2:Status"] != nil {
                    let dic5  = dic4["ns2:Status"] as! NSDictionary
                    
                    //let if dic6
                    let resultDesc = dic5["ns2:ResultDescription"] as! NSDictionary
                    let resultDescValue = resultDesc["text"] as! String
                    
                    self.present(UIAlertController.alertWithTitle(title: "", message:  resultDescValue , buttonTitle: "OK"), animated: true, completion: nil)
                }else{
                   // sendEmail(mainBody: dic4 as! String)
                    self.present(UIAlertController.alertWithTitle(title: "", message: "Invalid Detail" , buttonTitle: "OK"), animated: true, completion: nil)
                }
            }else{
                //sendEmail(mainBody: dic2 as! String)
                self.present(UIAlertController.alertWithTitle(title: "", message: "Invalid Detail" , buttonTitle: "OK"), animated: true, completion: nil)
            }
            
        }
        
    }
    
    
    
    
    //MARK:------------------------Set Card For "Web Payment" ----------------------------------
    func setWebRequsetParms() -> String {
        
        do
        {
            //Account XMl String
            let paygateAccount = ["paygateId":self.merchantInfo.payGateId,"password":self.merchantInfo.encryptionKey]
            let jsonDataAcc = try JSONSerialization.data(withJSONObject:paygateAccount, options: .prettyPrinted)
            let accountXML = try decoder.decode(AccountXML.self, from: jsonDataAcc)
            
            let stringNotify = "\(paymentRedirectBaseURL)paygate/transaction/\(self.merchantInfo.orderNumber)/\(self.merchantInfo.orderID)?device=ios"
            //print(stringNotify)
            let redirect = RedirectDetails(JSON:["NotifyUrl":"https://192.168.0.12/A/PayHost/notify.php","ReturnUrl":stringNotify])
            
            
            //            let redirect = RedirectDetails(JSON:["NotifyUrl":"https://192.168.0.12/A/PayHost/notify.php","ReturnUrl":"http://redirect/now"]) //Change this to your server url
            
            
            
            
            //Redirect XMl
            
            //            let cardDetails = """
            //            <ns:CardNumber>\(self.cardInfo.cardNumber)</ns:CardNumber>
            //            <ns:CardExpiryDate>\(self.cardInfo.expireMonth)\(self.cardInfo.expireYear)</ns:CardExpiryDate>
            //            <ns:CVV>123</ns:CVV>
            //            <ns:BudgetPeriod>0</ns:BudgetPeriod>
            //            """
            
            let cardDetails = """
            <ns:CardNumber>\(self.cardNumberTextFld.text!)</ns:CardNumber>
            <ns:CardExpiryDate>\(self.cardInfo.expireDate!)</ns:CardExpiryDate>
            <ns:CVV>\(self.cardInfo.cvvNumber!)</ns:CVV>
            <ns:BudgetPeriod>0</ns:BudgetPeriod>
            """
            
            //<ns:CVV>\(self.cardInfo.cvvNumber)</ns:CVV>
            //let redirect = RedirectDetails(JSON:["NotifyUrl":"https://192.168.0.12/A/PayHost/notify.php","ReturnUrl":"ww"])
            
            //CustomerDetailXMLModel  XML String
            //            let CustomerDic:[String : Any] = ["Title":" ","FirstName":merchantInfo.buyerName,"LastName":" ","Email":merchantInfo.buyerEmail,"Country":"ZAF","account":paygateAccount]
            
            let CustomerDic:[String : Any] = ["Title":" ","FirstName":merchantInfo.buyerName,"LastName":" ","Email":merchantInfo.buyerEmail,"Country":"ZAF","account":paygateAccount]
            
//            let CustomerDic:[String : Any] = ["Title":" ","FirstName":merchantInfo.buyerName,"LastName":" ","Email":"anjana@aksinteractive.com","Country":"ZAF","account":paygateAccount]

            let jsonData = try JSONSerialization.data(withJSONObject: CustomerDic, options: .prettyPrinted)
            let customerStr = try decoder.decode(CustomerDetailXMLModel.self, from: jsonData)
            
            //Generate Reference
            let date = Date()
            let formatterReference = DateFormatter()
            //
            formatterReference.timeZone = TimeZone(abbreviation: "UTC")
            formatterReference.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
            //            //(formatterReference.string(from: date))
            let transactionDate = formatterReference.string(from: date)
            
            //needs to be done...
            
            let orderIDInString = String(self.merchantInfo.orderID)
            
            let orderData = try JSONSerialization.data(withJSONObject: ["MerchantOrderId":orderIDInString,"Currency":"ZAR","Amount":paygateTotal(),"TransactionDate":transactionDate,"billing":CustomerDic], options: .prettyPrinted)
            
            let orderDetail = try decoder.decode(OrderDetails.self, from: orderData)
            
            
            //let ConcateStr = (accountXML.soapStr)! + (customerStr.soapStr)! + (redirect?.soapStr)! + (orderDetail.soapStr)!
            let ConcateStr = (accountXML.soapStr)! + (customerStr.soapStr)! + cardDetails + (redirect?.soapStr)! + (orderDetail.soapStr)!
            
            //let makeObject = WebpaymentRequest(soapvalue:ConcateStr)
            let makeObject = CardpaymentRequest(soapvalue:ConcateStr)
            //print(makeObject.soapvalue)
            
            return makeObject.soapvalue
        }
        catch{
            return ""
            
        }
    }
    
    // MARK:- API
    fileprivate func callAPIToInitiatePaymentGateway(orderId: Int, completionHandler: @escaping (_ sucess: Bool) -> Void) {
        let isSucceed: Bool = true
        let paraDict = NSMutableDictionary()
        paraDict["order_id"] = orderId
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/orders/paymentProcessing") { (response, error , message, statusCode) in
            self.isNextButtonTapped = false
            completionHandler(true)
        }
        
    }
    
    // MARK:- ADD NEW CARD
    fileprivate func callAPIToAddNewCard(encryptedKey: String) {
 
        let paramDict = NSMutableDictionary()
        paramDict["details"] = encryptedKey
        paramDict["card_number"] = self.cardInfo.cardNumber
        indicatorViewObj.showIndicatorView(viewController: self)
        ServiceHelper.sharedInstance.callAPIWithParameter(paramDict as! [String : Any], method: .post, apiName: "buyer/cards/add") { (response, error, message, statusCode) in
            
            self.isNextButtonTapped = false
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                   
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                // add condition if comiung from dashboard
                
                if self.isFromSideMenu {
                    let JSON = response as! NSDictionary
                    guard let data = JSON["data"] as? NSDictionary else {
                        return
                    }
                    
                    let msg = data["message"] as? String
                    let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.navigationController?.popViewController(animated: false)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    self.callAPIToInitiatePaymentGateway(orderId: self.orderId, completionHandler: { (sucess) in
                        
                        self.isNextButtonTapped = false
                        
                        self.setMerchantData(orderId: self.orderId, orderNo: self.orderNo)
                        self.mainStr = self.setWebRequsetParms()
                        self.callCheckSumAPI()
                    })
                }
                
               
                
            }
        }
    }
    
    func callAPIToTransaction() {
        let paramDict = NSMutableDictionary()
        
        //        PassToDic["url"] = redirectUrl
        //        PassToDic["ref_id"] = ReferenceId
        //        PassToDic["chaeksum_id"] = cheakSumid
        //        PassToDic["payGate_id"] = paygateid
        //        PassToDic["pay_requ_id"] = pay_requ_id
        
        paramDict["order_id"] = self.merchantInfo.orderID
        paramDict["order_number"] = self.merchantInfo.orderNumber
        paramDict["transaction_id"] = PassToDic["pay_requ_id"]
        paramDict["status_code"] = 1
        //  paramDict["response"] = ["checksum_id": self.PassToDic["chaeksum_id"]]
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paramDict as! [String : Any], method: .post, apiName: "buyer/orders/paymentStatus") { (response, error, message, statusCode) in
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                //                let alertController = UIAlertController(title: "", message: data["message"] as? String, preferredStyle: .alert)
                //                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                //                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                //                    self.navigationController?.pushViewController(nVC, animated: true)
                //                }
                //                alertController.addAction(action1)
                //                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    
}

extension PaygateCardDetailsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Send Mail
    func sendEmail(mainBody: String) {
        let composer = MFMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail() {
            composer.mailComposeDelegate = self
            composer.setToRecipients(["deekshs.deep123@gmail.com"])
            composer.setSubject("Paygate Uncaught Error")
            composer.setMessageBody(mainBody, isHTML: false)
            present(composer, animated: true, completion: nil)
        }
    }
}




