//
//  AddCardViewController.swift
//  Bean2Go
//
//  Created by Anjana Aks on 19/03/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import CryptoSwift


class AddCardViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var sideMenuButton: UIButton!
    
    var merchantInfo = MerchantInfo()
    var listOfCard = [CardDetails]()
    var listArray = NSArray()
    var refreshController = UIRefreshControl()
    var orderNo = String()
    var orderId = Int()
    var isFromSideMenu: Bool = true    //change left side button icon
    
    // Mark:- View Override Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Remove After API Call
        //self.createArray()
        
//        if isFromSideMenu == true {
//            sideMenuButton.setImage(UIImage(named: "menu"), for: .normal)
//        } else {
            sideMenuButton.setImage(UIImage(named: "back_arrwo_icon"), for: .normal)
       // }
       
        topView.layer.addborder(width: 1.2, color: UIColor.gray)
        topView.layer.cornerRadius = 5
        
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.refreshControl = refreshController
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callAPIToGetListOfCard()
    }
    
    // Remove After API Call
    func createArray() {
        
        for _ in 1...5 {
            let cardObj = CardDetails()
            cardObj.cardHolderName = "Testing Account"
            cardObj.cardNumber = "4000000000000002"
            cardObj.expireMonth = "03"
            cardObj.expireYear = "2020"
            cardObj.cvvNumber = "999"
            cardObj.isSelected = false
            
            listOfCard.append(cardObj)
        }
    }
    
    //MARK:- Refresh Action
    @objc func refresh(sender:AnyObject) {
        
        self.callAPIToGetListOfCard()
        refreshController.endRefreshing()
    }
    
    // MARK:- Button Events
    @IBAction func deleteCardAPI(_ sender: UIButton) {
        

        let alertController = UIAlertController(title: "", message: "Are you sure to delete this card?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
            let cartInfo = self.listOfCard[sender.tag]
        
            self.callAPIToDelete(cartId: cartInfo.cardId, selectedIndex: sender.tag)
        }

        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func addNewCard(_ sender: UIButton) {
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PaygateCardDetailsViewController") as! PaygateCardDetailsViewController
        nVC.isNewCard = true
        nVC.isFromSideMenu = self.isFromSideMenu
        nVC.merchantInfo = self.merchantInfo
        nVC.orderId = orderId
        nVC.orderNo = orderNo
        //nVC.paygateDelegate = self as closeCurrentViewAndAddCard
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    // MARK:- Side Menu Tapped
    @IBAction func sideMenuBtnDidTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARKL:- TableView Datasource And Delegate
extension AddCardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfCard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AddCardTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddCard", for: indexPath) as! AddCardTableViewCell
        cell.borderView.layer.addborder(width: 1.2, color: UIColor.gray)
        cell.borderView.layer.cornerRadius = 5
        let cardDetailObj = listOfCard[indexPath.row]
        
        if isFromSideMenu {
            cell.deleteButton.isHidden = false
        } else {
            cell.deleteButton.isHidden = true
        }
        
        cell.deleteButton.tag = indexPath.item
        
        
        cell.cardHolderName.text = cardDetailObj.cardHolderName
        
        var cardNo: String = cardDetailObj.cardNumber!
        
        let start = cardNo.index(cardNo.startIndex, offsetBy: 4);
        let end = cardNo.index(cardNo.startIndex, offsetBy: 12);
        cardNo.replaceSubrange(start..<end, with: "XXXXXXXX")
        
        cell.cardNumber.text = cardNo
//        cell.radioButton.isSelected = cardDetailObj.isSelected
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PaygateCardDetailsViewController") as! PaygateCardDetailsViewController
            nVC.isNewCard = false
            nVC.isFromSideMenu = self.isFromSideMenu
            nVC.merchantInfo = self.merchantInfo
            nVC.orderId = orderId
            nVC.orderNo = orderNo
            nVC.cardInfo = listOfCard[indexPath.row]
            self.navigationController?.pushViewController(nVC, animated: true)
        }
}

// MARK:- CALL API TO GET CARD LIST
extension AddCardViewController {
    
    
    fileprivate func decryptFunc(encString: String) throws {
        do {
            let decryptString = try AESEncryptionClass.AESSharedObj.aesDecrypt(textToDncrypt: encString)
            
            print(decryptString)
        } catch _ {
        }
    }
    
    fileprivate func callAPIToGetListOfCard() {
        
        let paraDict = NSMutableDictionary()
        indicatorViewObj.showIndicatorView(viewController: self)
        self.listOfCard = []
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "buyer/cards/myCards") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    //self.collectionview.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                // self.collectionview.isHidden = true
                return
            } else {
                
                guard let orderList = response?["data"] as? NSArray else {
                    return
                }
                
                if orderList.count != 0 {
                    
                    self.listArray = orderList
                    for cardInfo in orderList {
                        guard let card = cardInfo as? NSDictionary else {
                            return
                        }

                        let encString: String = card["details"] as? String ?? ""
                        let newDict = NSMutableDictionary()
                        var decryptString: String = ""
                        
                        
//                        do {
//                            decryptString = try encString.aesDecrypt()
//                            
//                            print(decryptString)
//                        } catch {
//                            print("JSONSerialization error:", error)
//                        }

                        if encString != "" {
                            let decryptedDataArray = encString.components(separatedBy: ";")
                            if decryptedDataArray.count != 0 {
                                for dict in decryptedDataArray {
                                    if dict != "" {
                                        let dictArray = dict.components(separatedBy: ":")
                                        if dictArray.count != 0 {
                                            newDict["\(dictArray[0])"] = "\(dictArray[1])"
                                        }
                                    }
                                }
                            }
                            let cardDetail = CardDetails()
                            cardDetail.cardHolderName = newDict["CardHolderName"] as? String
                            cardDetail.cardNumber = newDict["CardNumber"] as? String
                            cardDetail.cvvNumber = newDict["CVV"] as? String
                            cardDetail.expireDate = newDict["ExpiryDate"] as? String
                            
                            
                            
                            let date = (cardDetail.expireDate)!.components(separatedBy: "/")
                            cardDetail.expireMonth = date[0] as? String
                            cardDetail.expireYear = date[1]
                            cardDetail.cardId = card["id"] as? String ?? ""
                            self.listOfCard.append(cardDetail)
                        }
                    }
                    self.tableView.reloadData()
                    
                } else {
                    let alertController = UIAlertController(title: "", message: "No cards found", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        //self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action1)
                    //self.present(alertController, animated: true, completion: nil)
                    //self.collectionview.isHidden = true
                }
            }
        }
    }
    
    // MARK:- Delete card
   fileprivate func callAPIToDelete(cartId: String, selectedIndex: Int) {
        
        let paraDict = NSMutableDictionary()
        paraDict["id"] = cartId
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .delete, apiName: "buyer/cards/delete") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
     
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                let msg = data["message"] as? String
                
                let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                    self.listOfCard.remove(at: selectedIndex)
//                    self.refreshMainView()
                    self.tableView.reloadData()
                    
                }
                
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
//            self.refreshMainView()
            
        }
    }
}
