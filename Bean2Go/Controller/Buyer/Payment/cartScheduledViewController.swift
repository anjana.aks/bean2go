//
//  cartScheduledViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 05/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

struct TotalAmount {
    var totalBaseAmount: String?
    var tax: String?
    var taxOfProduct: String?
    var totalPriceOfProduct: String?
    var totalPriceOfProductIncludingTax: String?
}

//MARK:- Merchant Modal Class
class MerchantInfo: NSObject{
    var payGateId = String()
    var encryptionKey = String()
    var buyerName = String()
    var buyerLastName = String()
    var buyerEmail = String()
    var orderID = Int()
    var totalAmount = String()
    var OrderName = String()
    var currency = String()
    var orderNumber = String()
    var encyptedUrlKey = String()
}

protocol openCartViewCOntrollerDelegate {
    func openCartViewCOntrollerOnFailureTransaction()
}

class cartScheduledViewController: UIViewController ,calendarDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet weak var subTotalLbl: UILabel!
//    @IBOutlet weak var taxLbl: UILabel!
//    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var rightBarBtn: UIBarButtonItem!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var billingView: UIView!
    @IBOutlet weak var totalAmountOfProductsIncludingTaxLabel: UILabel!
    @IBOutlet weak var viewDetailView: UIView!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var totalTaxLabel: UILabel!
    @IBOutlet weak var taxHeadder: UILabel!
    @IBOutlet weak var ingredientViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var superCommentStackView: UIStackView!
    @IBOutlet weak var usercommentTextview: UITextView!
    @IBOutlet weak var usercommentHeightconstraint: NSLayoutConstraint!
    @IBOutlet weak var ingredeintView: UIView!
    @IBOutlet weak var productNameLabel: UILabel!
    
//    @IBOutlet weak var ingredientPrice: UILabel!
//    @IBOutlet weak var ingredientOptionalPrice: UILabel!
//    @IBOutlet weak var ingredientTax: UILabel!
//    @IBOutlet weak var ingredientToalWithTax: UILabel!
    
    @IBOutlet weak var totalNumberOfCartLabel: UILabel!
    @IBOutlet weak var totalAmountButton: UIButton!
    
    @IBOutlet weak var confirmationPopUp: UIView!
    
    @IBOutlet weak var failurePopUp: UIView!
    
    var openCartViewControllerDelegate: openCartViewCOntrollerDelegate?
    
    var PassToDic = [String:String]()
    
//    var merchantId: String = "1026676100014"
//    var merchantEncyptKey: String = "gVZpBqf79T3DxCxdu6s0Q"

    var merchantId: String = "10011072130"
    var merchantEncyptKey: String = "test"
    
    var orderType: Int = 0
    var scheduleTime: String = ""
    var selectedTimeZone: String = ""
    var isFailureTransaction: Bool = false
    
    var refreshController = UIRefreshControl()
    
    var myCartListArray = [CartCellData]()
    var productQuantityCount = 1
    var ordertype = 1
    var ArrayData = [1,2,1]
    var storeId = -1
    var storeCloseAt: String = ""
    let minHeight: CGFloat = 80.0
    let maxHeight: CGFloat = 150.0
    
    var cardUrl = "payhost/process.trans"
    var decoder = JSONDecoder()
    var merchantInfo = MerchantInfo()
    var mainStr = String()
    var isFromScreen: String = ""
    var userLocationLat: String = ""
    var userLocationLng: String = ""
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callToGetMyCartList()
        
        self.usercommentTextview.layer.addborder(width: 2, color: UIColor.lightGray)
        self.usercommentTextview.layer.cornerRadius = 5
        self.usercommentTextview.setContentOffset(CGPoint.zero, animated: false)
        usercommentTextview.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushPaymentController(notification:)), name: NSNotification.Name(rawValue: "openPaymentsucessViewcontroller"), object: nil)
    

        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.collectionView.refreshControl = refreshController
        
        if isFailureTransaction {
            self.failurePopUp.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //bottomView.backgroundColor = UIColor(patternImage: UIImage(named: "btn_img")!)
        self.billingView.shadowApply(shadowRadius: 10)
        self.bottomView.shadow(UIColor.lightGray)
    }
    
    //MARK:- Refresh Action
    @objc func refresh(sender:AnyObject) {
        callToGetMyCartList()
        refreshController.endRefreshing()
    }
    
    @objc func pushPaymentController(notification: NSNotification) {
        
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        nVC.orderNumber = self.merchantInfo.orderNumber
        nVC.orderId = self.merchantInfo.orderID
        self.navigationController?.pushViewController(nVC, animated: true)
        
    }
    
    //MARK:- Button Action
    
    @IBAction func closeFailurePopUp(_ sender: UIButton) {
        self.failurePopUp.isHidden = true
    }
    @IBAction func detailViewHide(_ sender: UIButton) {
        viewDetailView.isHidden = true
        self.removeIngredientData()
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        let checkComingFrom: String = USER_DEFAULTS.value(forKey: iS_FROM_SCREEN_FOR_CART) as? String ?? ""
        
        if checkComingFrom == DASHBOARD {
            if isFailureTransaction {
                let controllers = self.navigationController?.viewControllers
                for vc in controllers! {
                    if vc is DashBoardViewController {
                        self.navigationController?.popToViewController(vc as! DashBoardViewController, animated: true)
                        return
                    }
                }
                return
            }
        } else if checkComingFrom == FEATURE_CONTROLLER {
            if isFailureTransaction {
                let controllers = self.navigationController?.viewControllers
                for vc in controllers! {
                    if vc is FeatureScrollingViewController {
                        self.navigationController?.popToViewController(vc as! FeatureScrollingViewController, animated: true)
                        return
                    }
                }
                return
            }
        }
            self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func rightBarBtnAction(_ sender: UIBarButtonItem) {
        if ordertype == 1 {
            let screen = self.storyboard?.instantiateViewController(withIdentifier: "CalendarViewController") as? CalendarViewController
            screen?.delegate = self
            screen?.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(screen!, animated: false, completion: nil)
        }
        else{
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "orderDetailsViewController") as! orderDetailsViewController
            self.navigationController?.pushViewController(nVC, animated: true)
        }
    }
    @IBAction func deleteButtonDidTapped(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "", message: "Are you sure to delete this item?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
            let cartInfo = self.myCartListArray[sender.tag] as CartCellData
            
            self.callAPIToDelete(cartId: cartInfo.cartId, selectedIndex: sender.tag)
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func viewDetailButtonDidTapped(_ sender: UIButton) {
        //        self.view.bringSubviewToFront(viewDetailView)
        
        self.viewDetailView.isHidden = false;
        self.setIngredientData(atIndex: sender.tag)
        
    }
    
    @IBAction func upBtnAction(_ sender: UIButton) {
        
        let cartInfo = myCartListArray[sender.tag] as CartCellData
        
        var productQuantityCount = cartInfo.productQuantity
        productQuantityCount = productQuantityCount + 1
        
        self.callAPIToUpdate(cartId: cartInfo.cartId, qunatity: productQuantityCount)
        
    }
    
    @IBAction func downBtnAction(_ sender: UIButton) {
        
        let cartInfo = myCartListArray[sender.tag] as CartCellData
        
        var productQuantityCount = cartInfo.productQuantity
        
        if productQuantityCount > 1 {
            productQuantityCount = productQuantityCount - 1
            self.callAPIToUpdate(cartId: cartInfo.cartId, qunatity: productQuantityCount)
        }
        
    }
    
    @IBAction func confirmBtnAction(_ sender: UIButton){
        
        if myCartListArray.count != 0 {
            if comparetime() {
                
                let screen = self.storyboard?.instantiateViewController(withIdentifier: "scheduleDBViewController") as? scheduleDBViewController
                screen?.delegate = self
                screen?.maxDateString = storeCloseAt
                screen?.modalPresentationStyle = .overCurrentContext
                self.navigationController?.present(screen!, animated: false, completion: nil)
            } else {
                self.present(UIAlertController.alertWithTitle(title: "", message: "oops! Store is closed" , buttonTitle: "OK"), animated: true, completion: nil)
            }
        } else {
             self.present(UIAlertController.alertWithTitle(title: "", message: "Please add cart." , buttonTitle: "OK"), animated: true, completion: nil)
        }
    }
    
    @IBAction func gobackButtonDidTapped(_ sender: UIButton) {
        self.confirmationPopUp.isHidden = true
    }
    
    @IBAction func proceedFurtherButtonDidTapped(_ sender: UIButton) {
        
       
            USER_DEFAULTS.set("", forKey: iS_FROM_SCREEN_FOR_CART)
            USER_DEFAULTS.set(isFromScreen, forKey: iS_FROM_SCREEN_FOR_CART)
            
            // MARK:- Call delegate to get user location
            LocationManagerClass.locationSharedObj.startUpdateLocation(roomNameID: 0)
            LocationManagerClass.locationSharedObj.latLngDelegate = self
            
            //self.callAPIToMakeOrder()
            self.confirmationPopUp.isHidden = true
    }
    
    internal func calendarAction(move: Bool) {
        if move == true {
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "orderDetailsViewController") as! orderDetailsViewController
            self.navigationController?.pushViewController(nVC, animated: true)
        }
    }
    
    fileprivate func comparetime() -> Bool{
        let date = Date()
        let format = DateFormatter()
        var calculateClosedTime = 0
        
        format.dateFormat = "yyyy-MM-dd H:mm:ss"
        format.timeZone = TimeZone.current
        let dateString1 = format.string(from: date)
        
        print(dateString1)
        
        var currentDateArray = dateString1.split(separator: " ")
        
        var currentHour = currentDateArray[1].split(separator: ":")
        
        print(currentHour.count)
        
        let calculateCurrentTime = (60*Int(currentHour[0])!) + Int(currentHour[1])!
        
        var closedTimeArray = (storeCloseAt ?? "").split(separator: " ")
        
        if closedTimeArray.count > 1{
            var closedTime = closedTimeArray[1].split(separator: ":")
            if closedTime.count > 2{
                calculateClosedTime = (60*Int(closedTime[0])!) + Int(closedTime[1])!
            }
        }
        
        LogInfo(calculateCurrentTime-calculateClosedTime)
        
        if calculateCurrentTime >= calculateClosedTime{
            return false
        }else{
            return true
        }
        
    }
    
    fileprivate func removeIngredientData() {
        
        let allViews = self.ingredeintView.subviews
        if allViews.count != 0 {
            for view in allViews {
                if view.isKind(of: cartItemDetailRowClass.self) {
                    view.removeFromSuperview()
                }
            }
        }
        
        superCommentStackView.isHidden = true
        usercommentTextview.layoutIfNeeded()
        
    }
    
    //MARK:- Set ingredient data on view
    fileprivate func setIngredientData(atIndex: Int){
        
        let productOptions = myCartListArray[atIndex] as CartCellData
        self.productNameLabel.text = productOptions.productName
//        self.ingredientPrice.text = "\(productOptions.productCurrency)\(productOptions.productPrice)"
//        self.ingredientOptionalPrice.text = "\(productOptions.productCurrency)\(productOptions.optionsTotal)"
//        self.ingredientTax.text = "\(productOptions.productTax)%"
//        self.ingredientToalWithTax.text = "\(productOptions.productCurrency)\(productOptions.productTotalAmountWithTax)"
        
        if productOptions.options.count != 0 {
            for i in 0...productOptions.options.count-1 {
                let ingredientViewObj = cartItemDetailRowClass()
                let ingredintInfo = productOptions.options[i] as IngredientInfo
                
                ingredientViewObj.frame = CGRect(x: 0, y: 25 * i, width: Int(self.ingredeintView.bounds.width), height: 25)
                
                ingredientViewObj.addValue(heading: "\(ingredintInfo.optionType)", value: "\(ingredintInfo.option_name)")
                self.ingredeintView.addSubview(ingredientViewObj)
            }
            
            let comment = productOptions.productComment
            if comment != "" {
                superCommentStackView.isHidden = false
                usercommentTextview.text = comment
                usercommentTextview.isEditable = false

                if usercommentTextview.contentSize.height >= maxHeight || usercommentTextview.contentSize.height > minHeight{
                    //self.usercommentTextview.isScrollEnabled = true
                    self.usercommentHeightconstraint.constant = maxHeight
                }
                else{

                    self.usercommentHeightconstraint.constant = minHeight
                }
                usercommentTextview.layoutIfNeeded()
            }
            
            // needs to be put in condition
            
            //self.ingredientViewHeightConstraint.constant = CGFloat(25*(optionDetailList.count))
            
            let ingredientViewObj = cartItemDetailRowClass()
            ingredientViewObj.frame = CGRect(x: 0, y: 25 * productOptions.options.count, width: Int(self.ingredeintView.bounds.width), height: 25)
            
            ingredientViewObj.addValue(heading: "Quantity", value: "\(productOptions.productQuantity) ")
            
            self.ingredeintView.addSubview(ingredientViewObj)
            
            self.ingredientViewHeightConstraint.constant = CGFloat(25*(productOptions.options.count + 1))
            
        }
    }
    
    // MARK:- Set and show Amount of product
    fileprivate func showTotalSumOfAllProduct() -> TotalAmount {
        
        var totalAmountStructObj = TotalAmount()
        
        var totalAmountTax: Float = 0.0
        var totalBaAmount: Float = 0.0
        // var totalTaAmount: Float = 0.0
        var tax: Int = 0
        var currency: String = "R"
        
        
        
        for cart in myCartListArray {
            let totalAmountWithTax = Float(cart.productTotalAmountWithTax)!
            totalAmountTax = (totalAmountTax + totalAmountWithTax)//.rounded()
            let totalAmountTaxAfterTwoDigit = String(format: "%.2f", totalAmountTax)
            totalAmountStructObj.totalPriceOfProductIncludingTax = "\(cart.productCurrency)\(totalAmountTaxAfterTwoDigit)"
            
            let totalBaseAmount = Float(cart.productTotalAmount)!
            totalBaAmount = totalBaAmount + totalBaseAmount
            totalAmountStructObj.totalBaseAmount = "\(cart.productCurrency)\(totalBaAmount)"
            
            tax = Int(cart.productTax)!
            currency = cart.productCurrency
        }
        
       
        //Required to sent to paygate as total amount
        
        self.merchantInfo.totalAmount = String(describing: totalAmountTax)
        self.merchantInfo.currency = currency

        let calculateTaxOnTotalPrice = totalBaAmount * Float(tax)/100
        let taxAfterTwoDigitDecimal = String(format: "%.2f", calculateTaxOnTotalPrice)
        totalAmountStructObj.tax = "Tax (\(tax)%)"
        totalAmountStructObj.taxOfProduct = "\(currency)\(taxAfterTwoDigitDecimal)"
        
        return totalAmountStructObj
        
    }
    
    //MARK:- Refresh Main View
    fileprivate func refreshMainView() {
        self.collectionView.reloadData()
        let totalAmountStructObj = self.showTotalSumOfAllProduct()
        // Update bottom price view
        self.totalNumberOfCartLabel.text = myCartListArray.count == 1 || myCartListArray.count == 0 ? "\(myCartListArray.count) item" : "\(myCartListArray.count) items"
        
        self.totalAmountButton.setTitle(totalAmountStructObj.totalPriceOfProductIncludingTax, for: .normal)
        
    }
    
    // MARK:- Set data on model class
    fileprivate func setDataOnCollection(listArray: NSArray) {
        myCartListArray = []
        for cart in listArray {
            let cartDict = cart as! NSDictionary
            
            let cartInfo = CartCellData()
            cartInfo.cartId = cartDict["cart_id"] as? Int ?? -1
            cartInfo.productName = cartDict["product_name"] as? String ?? ""
            cartInfo.productImage = cartDict["product_image"] as? String ?? ""
            cartInfo.productQuantity = cartDict["qty"] as? Int ?? 0
            cartInfo.productCurrency = cartDict["currency"] as? String ?? ""
            cartInfo.productTax = cartDict["tax"] as? String ?? ""
            cartInfo.productPrice = cartDict["product_price"] as? String ?? ""
            cartInfo.optionsTotal = cartDict["options_total"] as? String ?? ""
            cartInfo.productTotalAmount = cartDict["product_total_amount"] as? String ?? ""
            cartInfo.productTotalAmountWithTax = cartDict["product_total_amount_with_tax"] as? String ?? ""
            cartInfo.productComment = cartDict["comment"] as? String ?? ""
            
            let array = cartDict["options"] as? NSArray ?? []
            //productInfo.options = array as? [IngredientInfo] ?? []
            
            if array.count != 0 {
                for proInfo in array {
                    let dict = proInfo as? NSDictionary
                    let ingredientInfo = IngredientInfo()
                    ingredientInfo.option_name = dict?["option_name"] as? String ?? ""
                    ingredientInfo.option_price = dict?["option_price"] as? String ?? ""
                    ingredientInfo.optionType = dict?["option_type"] as? String ?? ""
                    ingredientInfo.product_option_id = dict?["product_option_id"] as? Int ?? -1
                    cartInfo.options.append(ingredientInfo)
                }
            } else {
                let alertController = UIAlertController(title: "", message: "No data found", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
            }
            self.myCartListArray.append(cartInfo)
        }
        refreshMainView()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openPaymentsucessViewcontroller"), object: nil)
        
    }
}

//MARK:- collection view datasource and delegate
extension cartScheduledViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myCartListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: cartScheduledViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cartScheduledViewCell", for: indexPath) as! cartScheduledViewCell
        
        cell.itemImgView.layer.cornerRadius = 10
        cell.itemImgView.clipsToBounds = true
        
        cell.quantityView.layer.cornerRadius = 4
        cell.quantityView.clipsToBounds = true
        cell.quantityView.layer.borderWidth = 1.5
        cell.quantityView.layer.borderColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1).cgColor
        //cell.quantityView.shadowApply(shadowRadius: 2)
        
        cell.viewDetailButton.setBackgroundImageAndCornerRadius()
        
        cell.upBtn.tag = indexPath.item
        cell.downBtn.tag = indexPath.item
        cell.deleteButton.tag = indexPath.item
        cell.viewDetailButton.tag = indexPath.item
        
        cell.shadowView.shadow(UIColor.lightGray)
        
        
        let cartInfo = myCartListArray[indexPath.row] as CartCellData
        
        //cell.viewDetailButton.tag = indexPath.item
        cell.itemNameLbl.text = cartInfo.productName
        cell.amountLbl.text = "Product Price: \(cartInfo.productCurrency) \(cartInfo.productPrice)"
        cell.quantityLbl.text = ""
        cell.quantityLbl.text = String(cartInfo.productQuantity)
        cell.itemImgView.af_setImage(withURL: URL(string: cartInfo.productImage)!)
        cell.ingredientPriceLbel.text = "Options Price: \(cartInfo.productCurrency) \(cartInfo.optionsTotal)"
        cell.totalAmountLabel.text = "Total Amount: \(cartInfo.productCurrency) \(cartInfo.productTotalAmount)"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 32, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "hedderview", for: indexPath)
            
            return headerView
            
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footerview", for: indexPath) as! CartFooterView
            
            footerView.shadowView.shadow(UIColor.lightGray)
            
            let totalAmountStructObj = self.showTotalSumOfAllProduct()
            
            footerView.percentageLabel.text = totalAmountStructObj.tax
            footerView.percentageInRupeeLbl.text = totalAmountStructObj.taxOfProduct
            footerView.totalAmountWithTaxLbl.text = totalAmountStructObj.totalPriceOfProductIncludingTax
            footerView.totalBasePriceLbl.text = totalAmountStructObj.totalBaseAmount
            return footerView
            
        default:
            
            assert(false, "Unexpected element kind")
            return UICollectionReusableView()
        }
        
    }
    
}

//MARK:- ORDER DELEGATE
extension cartScheduledViewController: ordertypeDelegateMethod {
    //0 if OTG else 1 for scheduled
    func orderTimeForSchedular(orderTime: String,type: String, timeZone: String) {
        if type == "Schedule" {
//            let alertController = UIAlertController(title: "", message: "Cannot schedule your order right now.Please try again later", preferredStyle: .alert)
//            let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
//                self.viewDetailView.isHidden = true
//                self.confirmationPopUp.isHidden = false
//            }
//            alertController.addAction(action1)
//            self.present(alertController, animated: true, completion: nil)
            
            self.orderType = 1
            self.scheduleTime = orderTime
            self.selectedTimeZone = timeZone
            
        } else {
            self.orderType = 0
            self.scheduleTime = ""
            self.selectedTimeZone = timeZone
        }
        self.viewDetailView.isHidden = true
        self.confirmationPopUp.isHidden = false
    }
}

// MARK:- Web service
extension cartScheduledViewController {
    
    // Mark:- Get cart list
    fileprivate func callToGetMyCartList(){
        
        let paraDict = NSMutableDictionary()
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "buyer/cart/getMyCart") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSArray else {
                    return
                }
                
                if data.count == 0 {
                    let alertController = UIAlertController(title: "", message: "No products in cart", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
                guard let meta = JSON["meta"] as? NSDictionary else {
                    return
                }
                
                guard let storeId = meta["store_id"] as? Int else {
                    return
                }
                
                guard let timingDict = meta["timings"] as? NSDictionary else {
                    return
                }
                
                self.storeId = storeId
                
                self.storeCloseAt = timingDict["closes_at"] as? String ?? ""
                
                self.setDataOnCollection(listArray: data)
                
            }
        }
    }
    
    // MARK:- Delete cart
    fileprivate func callAPIToDelete(cartId: Int, selectedIndex: Int) {
        
        let paraDict = NSMutableDictionary()
        paraDict["cart_id"] = cartId
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .delete, apiName: "buyer/cart/destroyMyCart") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                   /* let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                let msg = data["message"] as? String
                
                let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                    self.myCartListArray.remove(at: selectedIndex)
                    self.refreshMainView()
                    
                    if self.myCartListArray.count == 0 {
                        if let navController = self.navigationController {
                            for controller in navController.viewControllers {
                                if controller is DashBoardViewController { // Change to suit your menu view controller subclass
                                    navController.popToViewController(controller, animated:true)
                                    break
                                }
                            }
                        }
                    }
                    
                    //self.collectionView.reloadData()
                    
                }
                
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
            self.refreshMainView()
            
        }
    }
    
    // MARK:- Update cart
    fileprivate func callAPIToUpdate(cartId: Int, qunatity: Int) {
        
        let paraDict = NSMutableDictionary()
        paraDict["cart_id"] = cartId
        paraDict["qty"] = qunatity
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .put, apiName: "buyer/cart/updateMyCart") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                
                self.myCartListArray = []
                
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSArray else {
                    return
                }
                
                self.setDataOnCollection(listArray: data)
                
            }
        }
    }
    
    fileprivate func callAPIToInitiatePaymentGateway(orderId: Int, completionHandler: @escaping (_ sucess: Bool) -> Void) {
        
        let isSucceed: Bool = true
        
        
        let paraDict = NSMutableDictionary()
        paraDict["order_id"] = orderId
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/orders/paymentProcessing") { (response, error , message, statusCode) in
            
            
            completionHandler(true)
            /*if message != nil {
                if statusCode == "401" {
                    
                    let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                
                
                
            }*/
        }
        
    }
    // MARK:- Make order
    
    fileprivate func callAPIToMakeOrder(lat: String, lng: String) {
        
        let paraDict = NSMutableDictionary()
        
        // store All cart id in common array
        var cartIdArray = [Int]()
        var totalTaAmount: Float = 0.0
        var tax: String = "0"
        
        for cart in myCartListArray {
            let id = cart.cartId
            let productPrice = cart.productTotalAmount
            cartIdArray.append(id)
            paraDict["store_product_total[\(id)]"] = productPrice
            let totalAmount = Float(cart.productTotalAmountWithTax)!
            totalTaAmount = totalTaAmount + totalAmount
            tax = cart.productTax
        }
        
        paraDict["store_id"] = storeId
        paraDict["cartId"] = cartIdArray
        paraDict["order_amount"] = totalTaAmount
        paraDict["type"] = self.orderType
        paraDict["tax"] = tax
        paraDict["timezone"] = self.selectedTimeZone
        paraDict["lat"] = lat
        paraDict["lng"] = lng
        
        if self.orderType == 1 {
            paraDict["sheduled_for"] = scheduleTime
        }
        
        //print(paraDict)
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/orders/createNewOrder") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                
                let JSON = response as! NSDictionary
                //print(JSON)
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                guard let message = data["message"] as? String else {
                    return
                }
                
                guard let orderNumber = data["order_number"] as? String else {
                    return
                }
            
                
                guard let orderId = data["order_id"] as? Int else {
                    return
                }
                
                let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
                nVC.isFromSideMenu = false
                nVC.orderId = orderId
                nVC.orderNo = orderNumber
                nVC.merchantInfo = self.merchantInfo
                self.navigationController?.pushViewController(nVC, animated: true)
//                
//                let nVC = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "PaygateCardDetailsViewController") as! PaygateCardDetailsViewController
//                nVC.merchantInfo = self.merchantInfo
//                nVC.orderId = orderId
//                nVC.orderNo = orderNumber
//                self.navigationController?.pushViewController(nVC, animated: true)
//                
                
               // uncomment after check order status
                
                /*   WEB API
                self.callAPIToInitiatePaymentGateway(orderId: orderId, completionHandler: { (sucess) in
                    self.setMerchantData(orderId: orderId, orderNo: orderNumber)
                    self.mainStr = self.setWebRequsetParms()
                    self.callCheckSumAPI()
                })*/
                
                //comment this on monday
//                let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
//                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
//                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
//                    nVC.orderNumber = orderNumber
//                    nVC.orderId = orderId
//                    self.navigationController?.pushViewController(nVC, animated: true)
//                }
//                alertController.addAction(action1)
//                self.present(alertController, animated: true, completion: nil)
                
            }
        }
        
    }
    
    
}

//Mark:- PayGate Implementation
extension cartScheduledViewController: XMLParserDelegate{
    
    func setMerchantData(orderId : Int, orderNo: String){
//        self.merchantInfo.payGateId = "10011072130"
//        self.merchantInfo.encryptionKey = "test"
        
        self.merchantInfo.payGateId = self.merchantId
        self.merchantInfo.encryptionKey = self.merchantEncyptKey
        
        self.merchantInfo.buyerName = USER_DEFAULTS.value(forKey: USER_NAME) as? String ?? ""
        self.merchantInfo.buyerEmail = USER_DEFAULTS.value(forKey: EMAIL_ADDRESS) as? String ?? ""
        self.merchantInfo.buyerLastName = ""
        self.merchantInfo.orderID = orderId
        self.merchantInfo.orderNumber = orderNo
    }
    
    func paygateTotal() -> String{
        var returnString = self.merchantInfo.totalAmount
        
        let range: Range<String.Index> = returnString.range(of: ".")!
        let index: Int = returnString.distance(from: returnString.startIndex, to: range.lowerBound)
        //print(returnString.count-index)
        if returnString.count-index < 3{
            for _ in 1...(returnString.count-index - 1) {
                returnString.append("0")
            }
        }
        returnString = returnString.replacingOccurrences(of: ".", with: "")
        
        return returnString
    }
    
    func callCheckSumAPI(){
        ServiceHelper.sharedInstance.callPaygateApi(parms: mainStr, Url: cardUrl, Success: {(responseObject,ResponseString) -> () in
            DispatchQueue.main.async {
                //self.req_textview.resignFirstResponder()
                if(ResponseString.contains("Complete")){
                    self.ReadWebPaymnetMethod(Response: ResponseString, success: true)
                }else{
                    self.ReadWebPaymnetMethod(Response: ResponseString, success: false)
                    //LogInfo("Error...\(ResponseString)")
                }
            }
            
        }, Failure: {(error) -> () in
            //print(error!.description)
            self.present(UIAlertController.alertWithTitle(title: "", message: "Request Failed" , buttonTitle: "OK"), animated: true, completion: nil)
        },showLoader: true, hideLoader: true)
    }
    
    func ReadWebPaymnetMethod(Response:String, success: Bool){
        var paygateid = String()
        var pay_requ_id = String()
        var ReferenceId  = String()
        var cheakSumid  = String()
        var error = String()
        
        var xmlDictionary = try? XMLReader.dictionary(forXMLString: Response)
        let dic1 : NSDictionary = xmlDictionary!["SOAP-ENV:Envelope"] as! NSDictionary
        let dic2 : NSDictionary = dic1["SOAP-ENV:Body"] as! NSDictionary
        
        if success {
            let dic3 : NSDictionary = dic2["ns2:SinglePaymentResponse"] as! NSDictionary
            var dic4  = NSDictionary()
            
            dic4 = dic3["ns2:WebPaymentResponse"] as! NSDictionary
            let dic5  = dic4["ns2:Redirect"] as! NSDictionary
            
            if(dic5["ns2:RedirectUrl"] != nil){
                let dic6 = dic5["ns2:RedirectUrl"] as! NSDictionary
                let redirectUrl = dic6["text"] as! String
                let urlParms = dic5["ns2:UrlParams"] as! NSArray
                
                let dic1 = urlParms[0] as! NSDictionary
                let dicvalue = dic1["ns2:value"] as! NSDictionary
                paygateid = dicvalue["text"] as! String
                
                let dic2 = urlParms[1] as! NSDictionary
                let payreqVal = dic2["ns2:value"] as! NSDictionary
                pay_requ_id = payreqVal["text"] as! String
                
                let dic3 = urlParms[2] as! NSDictionary
                let ReferenceDic = dic3["ns2:value"] as! NSDictionary
                ReferenceId = ReferenceDic["text"] as! String
                
                let dic4 = urlParms[3] as! NSDictionary
                let cheakSum = dic4["ns2:value"] as! NSDictionary
                cheakSumid = cheakSum["text"] as! String
                
               
                PassToDic["url"] = redirectUrl
                PassToDic["ref_id"] = ReferenceId
                PassToDic["chaeksum_id"] = cheakSumid
                PassToDic["payGate_id"] = paygateid
                PassToDic["pay_requ_id"] = pay_requ_id

               // self.callAPIToTransaction()
                
//                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//                vc.PassToDic = PassToDic
//                //vc.orderId = self.merchantInfo.orderID
//                self.navigationController?.pushViewController(vc, animated:true)
            }
        } else {
            
            if dic2["SOAP-ENV:Fault"] != nil {
                let dic3 : NSDictionary = dic2["SOAP-ENV:Fault"] as! NSDictionary
                let dic4 : NSDictionary = dic3["detail"] as! NSDictionary
                let paygateError = dic4["payhost:error"] as! NSArray
                
                let ErrorPay : NSDictionary = paygateError[1] as! NSDictionary
                error = ErrorPay["text"] as! String
                self.present(UIAlertController.alertWithTitle(title: "", message: error , buttonTitle: "OK"), animated: true, completion: nil)
            }
            
        }
       
    }
    
    
    //MARK:------------------------Set Card For "Web Payment" ----------------------------------
    func setWebRequsetParms() -> String {
        
        do
        {
            //Account XMl String
            let paygateAccount = ["paygateId":merchantInfo.payGateId,"password":merchantInfo.encryptionKey]
            let jsonDataAcc = try JSONSerialization.data(withJSONObject:paygateAccount, options: .prettyPrinted)
            let accountXML = try decoder.decode(AccountXML.self, from: jsonDataAcc)
            
            let stringNotify = "\(paymentRedirectBaseURL)paygate/transaction/\(self.merchantInfo.orderNumber)/\(self.merchantInfo.orderID)?device=ios"

            //print(stringNotify)
            
            //Redirect XMl
            let redirect = RedirectDetails(JSON:["NotifyUrl":"","ReturnUrl":stringNotify])
            
            
            
            
             //let redirect = RedirectDetails(JSON:["NotifyUrl":"https://192.168.0.12/A/PayHost/notify.php","ReturnUrl":"ww"])
            
            //CustomerDetailXMLModel  XML String
//            let CustomerDic:[String : Any] = ["Title":" ","FirstName":merchantInfo.buyerName,"LastName":" ","Email":merchantInfo.buyerEmail,"Country":"ZAF","account":paygateAccount]

            let CustomerDic:[String : Any] = ["Title":" ","FirstName":merchantInfo.buyerName,"LastName":" ","Email":merchantInfo.buyerEmail,"Country":"ZAF","account":paygateAccount]


            let jsonData = try JSONSerialization.data(withJSONObject: CustomerDic, options: .prettyPrinted)
            let customerStr = try decoder.decode(CustomerDetailXMLModel.self, from: jsonData)
            
            //Generate Reference
            let date = Date()
            let formatterReference = DateFormatter()
//
            formatterReference.timeZone = TimeZone(abbreviation: "UTC")
            formatterReference.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
//            //(formatterReference.string(from: date))
            let transactionDate = formatterReference.string(from: date)
            
            //needs to be done...
            
            let orderIDInString = String(self.merchantInfo.orderNumber)
            
            let orderData = try JSONSerialization.data(withJSONObject: ["MerchantOrderId":orderIDInString,"Currency":"ZAR","Amount":paygateTotal(),"TransactionDate":transactionDate,"billing":CustomerDic], options: .prettyPrinted)
            
            let orderDetail = try decoder.decode(OrderDetails.self, from: orderData)
            
            
            let ConcateStr = (accountXML.soapStr)! + (customerStr.soapStr)! + (redirect?.soapStr)! + (orderDetail.soapStr)!
            
            let makeObject = WebpaymentRequest(soapvalue:ConcateStr)
            //print(makeObject.soapvalue)
            
            return makeObject.soapvalue
        }
        catch{
            return ""
            
        }
    }



    func callAPIToTransaction() {
        let paramDict = NSMutableDictionary()
        
//        PassToDic["url"] = redirectUrl
//        PassToDic["ref_id"] = ReferenceId
//        PassToDic["chaeksum_id"] = cheakSumid
//        PassToDic["payGate_id"] = paygateid
//        PassToDic["pay_requ_id"] = pay_requ_id
        
        paramDict["order_id"] = self.merchantInfo.orderID
        paramDict["order_number"] = self.merchantInfo.orderNumber
        paramDict["transaction_id"] = PassToDic["pay_requ_id"]
        paramDict["status_code"] = 1
      //  paramDict["response"] = ["checksum_id": self.PassToDic["chaeksum_id"]]
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paramDict as! [String : Any], method: .post, apiName: "buyer/orders/paymentStatus") { (response, error, message, statusCode) in
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
//                let alertController = UIAlertController(title: "", message: data["message"] as? String, preferredStyle: .alert)
//                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
//                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
//                    self.navigationController?.pushViewController(nVC, animated: true)
//                }
//                alertController.addAction(action1)
//                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}

extension cartScheduledViewController: getLatLngOfUserDelegate {
    func getUserLatLng(latitude: String, longitude: String) {
        self.callAPIToMakeOrder(lat: latitude, lng: longitude)
        LocationManagerClass.locationSharedObj.stopUserLocation()
        
    }
}
