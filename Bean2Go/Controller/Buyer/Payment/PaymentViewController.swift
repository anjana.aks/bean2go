//
//  PaymentViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 03/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import SocketIO

class PaymentViewController: UIViewController {

    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var footerView: UIView!

    var orderNumber: String = ""
    var orderId: Int = 0
    var transactionId: String = "ansh_123_paygate"
    
    //MARK:- VIEW OVERRIDE
    fileprivate func connectionEstablishWithSocket() {
        // create connection with socket
        SocketManagerClass.shared.eventsHandleInSocket(roomName: orderNumber, id: PAYMENT_COMPLETED_ID) // PaymentCompleted id
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        orderIdLbl.text = "Order No: \(orderNumber)"
        
        connectionEstablishWithSocket()
        
    }
  
    //MARK:_ BUTTON ACTION
    @IBAction func homeBtnAction(_ sender: UIButton) {
        //let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        
//        self.navigationController?.popToViewController(DashBoardViewController.self, animated: false)
        //self.navigationController?.pushViewController(nVC, animated: true)
        
      
        if let navController = self.navigationController {
            for controller in navController.viewControllers {
                if controller is DashBoardViewController { // Change to suit your menu view controller subclass
                    navController.popToViewController(controller, animated:true)
                    break
                }
            }
        }
        
    }
}

/*extension PaymentViewController: getDataArrayOfLatLng {
    func getLatLngFromSocket(latLongArray: [Any]) {
        print()
    }
    
    
}*/

//MARK:- Payment gateway
extension PaymentViewController {
    
    fileprivate func callAPIToTransactionIsCompleted() {
        let paramDict = NSMutableDictionary()
//        paramDict["order_id"] = self.orderId
//        paramDict["order_number"] = self.orderNumber
//        paramDict["transaction_id"] = self.transactionId
//        paramDict["status_code"] = 1
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paramDict as! [String : Any], method: .get, apiName: "buyer/orders/getPaymentStatus/\(self.orderNumber)/\(self.orderId)") { (response, error, message, statusCode) in
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                guard let status = data["status"] as? String else {
                    return
                }
                
                let alertController = UIAlertController(title: "", message: data["message"] as? String, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                    self.navigationController?.pushViewController(nVC, animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
