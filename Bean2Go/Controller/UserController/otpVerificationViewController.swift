//
//  otpVerificationViewController.swift
//  ProjectScreen
//
//  Created by Vivek Godiwal on 12/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import Alamofire
class otpVerificationViewController: UIViewController {
    
    var temp: String?
    @IBOutlet weak var digit1TextView: UITextField!
    @IBOutlet weak var digit2TextView: UITextField!
    @IBOutlet weak var digit3TextView: UITextField!
    @IBOutlet weak var digit4TextView: UITextField!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var resendOtpBtn: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    
    
    let toolbar = UIToolbar()
    var user_Id = Int()
    var otp: String = ""
    var one = String()
    var two = String()
    var three = String()
    var four = String()
    var timer = Timer()
    var isRememberLogin: Bool = false
    var seconds = 60
    var emailId: String = ""
    var password: String = ""
    
    var userType: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        startTimer()
        resendOtpBtn.isHidden = true
        timerLabel.isHidden = false
        
        self.digit1TextView.addToolBar()
        self.digit2TextView.addToolBar()
        self.digit3TextView.addToolBar()
        self.digit4TextView.addToolBar()
        
        self.digit1TextView.becomeFirstResponder()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        okButton.setBackgroundImageAndCornerRadius()
    }

    //MARK:- Button Action
    
    @IBAction func okButtonDidTapped(_ sender: UIButton) {
        
        one = digit1TextView.text!
        two = digit2TextView.text!
        three = digit3TextView.text!
        four = digit4TextView.text!
        
        if isAllVerified(){
            otp = "\(one)\(two)\(three)\(four)"
            
            LogInfo("\(otp)")
            callOtpVerifyAPI()
        }
       
    }
    
    @IBAction func ResendOtptnDidTapped(_ sender: UIButton) {
        digit1TextView.text = ""
        digit2TextView.text = ""
        digit3TextView.text = ""
        digit4TextView.text = ""
        
        timer.invalidate()
        callResendOtpAPI()
        
    }
    
    @IBAction func viewDidTap(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    //MARK:-  timer Functions
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(otpVerificationViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
        if seconds == 1 {
            timer.invalidate()
            seconds = 60
            resendOtpBtn.isHidden = false
            timerLabel.isHidden = true
            resendOtpBtn.setTitle("Resend", for: .normal)
        } else {
            seconds -= 1
           // resendOtpBtn.isEnabled = false
            resendOtpBtn.isHidden = true
            timerLabel.isHidden = false
            timerLabel.text = "Resend otp after \(seconds)"
        }
    }
    
    func isAllVerified() -> Bool{
        
        
        if digit1TextView.text == ""  || digit2TextView.text == "" || digit3TextView.text == "" || digit4TextView.text == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter otp", buttonTitle: "OK"), animated: true, completion: nil)
            return false
        }else{
            return true
        }
    }
    
}

extension otpVerificationViewController: UITextFieldDelegate
{
    //Needs To Be Upgraded
    func textFieldDidBeginEditing(_ textField: UITextField) {
  
        textField.text = ""
    }
    
    @IBAction func textFieldDidChanged(_ sender: UITextField) {
        
        let text = sender.text
        
        if (text?.utf16.count)! >= 1 {
            switch sender {
            case digit1TextView:
                digit2TextView.becomeFirstResponder()
                break
                
            case digit2TextView:
                digit3TextView.becomeFirstResponder()
                break
                
            case digit3TextView:
                digit4TextView.becomeFirstResponder()
                break
                
            case digit4TextView:
                digit4TextView.resignFirstResponder()
                break
            default:
                sender.resignFirstResponder()
                break
            }
        }
        
    }
  
    @objc func donedatePicker(){
        self.toolbar.removeFromSuperview()
    }
    
    @objc func cancelDatePicker(){
        self.toolbar.removeFromSuperview()
    }
    
    //MARK:- OTP verify API
    func callOtpVerifyAPI() {
        let paraDict = NSMutableDictionary()
        
        let email = self.emailId
        let password = self.password
        paraDict["user_id"] = user_Id
        paraDict["otp"] = otp
        paraDict["email"] = email
        paraDict["password"] = password
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/otp/verification") { (response, error,message, statusCode) in
            
            if message != nil{
                self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                  
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)

                return
            } else {
                let metaData = response?["meta"] as? NSDictionary
                let msg = metaData!["responseMessage"] as? String ?? ""
                
                let data = response?["data"] as? NSDictionary
                let email = data!["email"] as? String
                let otp_Status = data!["is_mobile_verified"] as? Int ?? 0
                let name = data!["name"] as? String
                let role = data!["role"] as? String
                let userId = data!["user_id"] as? Int
                let authToken = metaData!["token"] as? String ?? ""
                
                if otp_Status == 1 {
                    USER_DEFAULTS.set(name, forKey: USER_NAME)
                    USER_DEFAULTS.set(email, forKey: EMAIL_ADDRESS)
                    USER_DEFAULTS.set(role, forKey: USER_TYPE)
                    USER_DEFAULTS.set(userId, forKey: USER_ID)
                    USER_DEFAULTS.set(true, forKey: IS_LOGGEDIN)
                    USER_DEFAULTS.set(true, forKey: IS_MOBILE_NUMBER_VERIFIED)
                    USER_DEFAULTS.set(authToken, forKey: Auth_Token)
                    
                    if role == BUYER {
                        
                        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                        self.navigationController?.pushViewController(nVC, animated: true)
                    } else if role == SELLER {
                        
                        let isApproved = metaData!["is_store_approved"] as? Int == 0 ? false : true
                        USER_DEFAULTS.set(isApproved, forKey: IS_SELLER_APPROVED)
                        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "recentOrderViewController") as! recentOrderViewController
                        self.navigationController?.pushViewController(nVC, animated: true)
                    }
                }
                
                self.showToast(message: msg)
            }
        }

    }
    
    // MARK:- Resend OTP API
    func callResendOtpAPI(){
        let paraDict = NSMutableDictionary()
        
        paraDict["user_id"] = user_Id
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/otp/resend") { (response, error, message, statusCode) in
            
            if message != nil{
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)

                return
            } else {
                let msg = response?["message"] as? String ?? ""
                self.showToast(message: msg)
                self.startTimer()
            }
        }

    }
}

