//
//  ChangePasswordViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 04/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit



class ProfileViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    @IBOutlet weak var nameTxtFld: UITextField!
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var mobileNoTxtFld: UITextField!
    
    @IBOutlet weak var nameTxtLbl: UILabel!
    @IBOutlet weak var emailTxtLbl: UILabel!
    @IBOutlet weak var mobileTxtLbl: UILabel!
    
    @IBOutlet weak var nameBorderLbl: UILabel!
    @IBOutlet weak var emailBorderLbl: UILabel!
    @IBOutlet weak var mobileNoBorderLbl: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    
    @IBOutlet weak var confirmationPopUp: UIView!
    @IBOutlet weak var popUpView: UIView!
    
    
    var userDetails = UserDetails()
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Clear background of navigation bar
        clearNavigationBar(navigationBar: self.navigationBar)
        self.mobileNoTxtFld.addToolBar()
        callGetProdileApi()
        
        //Setup Initial View Layout
        //setUpInitialView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.popUpView.shadowApply(shadowRadius: 10)
    }
    
    //MARK:- HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK:- SET UP VIEW
    func setUpInitialView() {
        //        self.userDetails.userName = USER_DEFAULTS.value(forKey: USER_NAME) as? String
        //        self.userDetails.userContactNo = USER_DEFAULTS.value(forKey: MOBIL_NO) as? String
        //        self.userDetails.userEmail = USER_DEFAULTS.value(forKey: EMAIL_ADDRESS) as? String
        
        self.nameTxtFld.text = self.userDetails.userName
        self.emailTxtFld.text = self.userDetails.userEmail
        self.mobileNoTxtFld.text = self.userDetails.userContactNo
        
    }
    
    
    //MARK:- BUTTON ACTIONS
    @IBAction func submittButtonDidTapped(_ sender: UIButton) {
        
        //if isAllFieldVerified() {
        //            self.userDetails.userName = trimWhiteSpaceNew(str: nameTxtFld.text)
        //            self.userDetails.userEmail = trimWhiteSpaceNew(str: emailTxtFld.text)
        //            self.userDetails.userContactNo = trimWhiteSpaceNew(str: mobileNoTxtFld.text)
        
        if isAllFieldVerified(){
            callAPIToUpdateProfile()
        }
        
        //            USER_DEFAULTS.set(userDetails.userName, forKey: USER_NAME)
        //            USER_DEFAULTS.set(userDetails.userEmail, forKey: EMAIL_ADDRESS)
        //            USER_DEFAULTS.set(userDetails.userContactNo, forKey: MOBIL_NO)
        
        
        
        //}
    }
    
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //    @IBAction func okButtonDidTapped(_ sender: UIButton) {
    //        //self.navigationController?.popViewController(animated: true)
    //    }
    
    @IBAction func hideConfirmViewBtnTapped(_ sender: UIButton) {
        confirmationPopUp.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- TOUCH EVENT
    @IBAction func viewdidtapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // MARK:- Validations
    func isAllFieldVerified() -> Bool {
        let updatedName = trimWhiteSpaceNew(str: nameTxtFld.text)
        let updatedEmail = trimWhiteSpaceNew(str: emailTxtFld.text)
        var isFieldVerified = false
        let isEmailAddressValid = validation.validationShared.isEmailValid(email: updatedEmail ?? "")
        
        if updatedName == nil || updatedName?.count == 0  {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your name.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if updatedEmail == nil || updatedEmail?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your email ID.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if !isEmailAddressValid {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your valid email ID.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if ((updatedName == userDetails.userName) && (updatedEmail == userDetails.userEmail)){
            present(UIAlertController.alertWithTitle(title: "", message: "Please Enter Updated Details.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
        } else {
           // print("Verfication ending 405")
            isFieldVerified = true
        }
        
        return isFieldVerified
    }
}

extension ProfileViewController: UITextFieldDelegate {
    //MARK:- TextField Functions
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameTxtFld {
            nameBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            nameTxtFld.placeholder?.removeAll()
            nameTxtLbl.alpha = 1
            
        } else if textField == emailTxtFld {
            emailBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            emailTxtFld.placeholder?.removeAll()
            emailTxtLbl.alpha = 1
            
        } else if textField == mobileNoTxtFld {
            mobileNoBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            mobileNoTxtFld.placeholder?.removeAll()
            mobileTxtLbl.alpha = 1
            
            if mobileNoTxtFld.text?.count == 10 {
                textField.resignFirstResponder()
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == nameTxtFld {
            nameBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            nameTxtLbl.alpha = 0
            if nameTxtFld.text == "" {
                nameTxtFld.placeholder = "Full Name"
            }
            //self.userDetails.userName = trimWhiteSpaceNew(str: nameTxtFld.text)
            
        } else if textField == emailTxtFld {
            emailBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            emailTxtLbl.alpha = 0
            if emailTxtFld.text == "" {
                emailTxtFld.placeholder = "Email"
            }
            //self.userDetails.userEmail = trimWhiteSpaceNew(str: emailTxtFld.text)
            
        } else if textField == mobileNoTxtFld {
            mobileNoBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            mobileTxtLbl.alpha = 0
            if mobileNoTxtFld.text == "" {
                mobileNoTxtFld.placeholder = "Mobile Number"
            }
            //self.userDetails.userContactNo = trimWhiteSpaceNew(str: mobileNoTxtFld.text)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            if textField == nameTxtFld {
                emailTxtFld.becomeFirstResponder()
                
            } else {
                mobileNoTxtFld.becomeFirstResponder()
                
            }
        }
        else if textField.returnKeyType == .done {
            textField.resignFirstResponder()
            
        }
        return true
    }
}

//MARK: Web APi
extension ProfileViewController {
    // Mark:- Get cart list
    fileprivate func callGetProdileApi(){
        
        let paraDict = NSMutableDictionary()
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "user/profile/get") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                self.userDetails.userName = data["name"] as? String ?? ""
                self.userDetails.userEmail = data["email"] as? String ?? ""
                self.userDetails.userContactNo = data["mobile_number"] as? String ?? ""
                
                self.setUpInitialView()
            }
        }
    }
    
    // MARK:- Update Profile
    fileprivate func callAPIToUpdateProfile() {
        
        let paraDict = NSMutableDictionary()
        paraDict["name"] = trimWhiteSpaceNew(str: nameTxtFld.text)
        paraDict["email"] = trimWhiteSpaceNew(str: emailTxtFld.text)
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .put, apiName: "user/profile/update") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                self.userDetails.userName = data["name"] as? String ?? ""
                self.userDetails.userEmail = data["email"] as? String ?? ""
                self.userDetails.userContactNo = data["mobile_number"] as? String ?? ""
                
                USER_DEFAULTS.set(self.userDetails.userEmail, forKey: EMAIL_ADDRESS)
                USER_DEFAULTS.set(self.userDetails.userName, forKey: USER_NAME)
                USER_DEFAULTS.set(self.userDetails.userContactNo, forKey: MOBIL_NO)
//                USER_DEFAULTS.set("", forKey: USER_PASSWORD)
                
                //self.popUpMessageLabel.text = meta["message"] as? String ?? ""
                
                self.view.endEditing(true)
                self.confirmationPopUp.isHidden = false
                
                self.setUpInitialView()
                
            }
        }
    }
}
