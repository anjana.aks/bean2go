//
//  PrivacyPolicyViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 28/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: UIViewController,UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var leftNavigationItem: UIBarButtonItem!
    @IBOutlet weak var navigationBar: UINavigationBar!

    var restImage: UIImage?
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Clear background of navigation bar
        clearNavigationBar(navigationBar: self.navigationBar)
        //set Textview content start or scroll form top
        
        callGetPrivacyPolicyApi()

    }
   
  
    //MARK:- HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK:- UIBUTTON ACTIONS
    
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)

    }

}

//MARK:- Web Api
extension PrivacyPolicyViewController{
    
    fileprivate func callGetPrivacyPolicyApi(){
        
        let paraDict = NSMutableDictionary()
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "common/privacyPolicy") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                  
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                guard let content = data["content"] as? String else {
                    return
                }
                self.webView.loadHTMLString(content, baseURL: nil)
                //self.descriptionTxtView.text = content
                
            }
        }
    }
}
