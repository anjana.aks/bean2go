//
//  TermsConditionsVC.swift
//  
//
//  Created by Anjana Aks on 12/04/19.
//

import UIKit
import WebKit

class TermsConditionsVC: UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    @IBOutlet weak var webView: WKWebView!
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Clear background of navigation bar
        clearNavigationBar(navigationBar: self.navigationBar)
        //set Textview content start or scroll form top
        callGetTermsConditionApi()
        
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK:- HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

extension TermsConditionsVC {
    
    fileprivate func callGetTermsConditionApi(){
      
        self.webView.load(URLRequest.init(url: URL(string: "http://13.126.113.25/termsAndConditions")!))
        return
        
        let paraDict = NSMutableDictionary()
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "http://13.126.113.25/termsAndConditions") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    
                    let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                guard let content = data["content"] as? String else {
                    return
                }
                
                
                
                self.webView.loadHTMLString(content, baseURL: nil)
            }
        }
    }
}
