//
//  ContactUsViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 03/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var nameHeaderLbl: UILabel!
    @IBOutlet weak var nameTxtFld: UITextField!
    @IBOutlet weak var emailHeaderLbl: UILabel!
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var commentsHeaderLbl: UILabel!
    @IBOutlet weak var commentsTxtView: UITextView!
    @IBOutlet weak var nameBorderLbl: UILabel!
    @IBOutlet weak var emailBorderLbl: UILabel!
    @IBOutlet weak var commentsBorderLbl: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var dialogBoxView: UIView!
    
    @IBOutlet weak var popUpView: UIView!
    
    var userDetails = UserDetails()
    
    var restImage: UIImage?
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Initial View
        setUpInitialView()
        
        // Clear background of navigation bar
        clearNavigationBar(navigationBar: self.navigationBar)
        
        self.dialogBoxView.isHidden = true
        self.popUpView.layer.cornerRadius = 10
        
    }
    
    //MARK:- HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK:- SET UP VIEW
    func setUpInitialView() {
        self.userDetails.userName = USER_DEFAULTS.value(forKey: USER_NAME) as? String
        self.userDetails.userEmail = USER_DEFAULTS.value(forKey: EMAIL_ADDRESS) as? String
        
        self.nameTxtFld.text = self.userDetails.userName
        self.emailTxtFld.text = self.userDetails.userEmail
        
    }
    
    //MARK:- Button Action
    @IBAction func submitBtnAction(_ sender: UIButton)
    {
        self.userDetails.userName = trimWhiteSpaceNew(str: nameTxtFld.text)
        self.userDetails.userEmail = trimWhiteSpaceNew(str: emailTxtFld.text)
        self.userDetails.userComment = trimWhiteSpaceNew(str: commentsTxtView.text)
        
        if isAllFieldVerified(){
            self.view.endEditing(true)
            callAPIToContact()
        }
        
        //            USER_DEFAULTS.set(userDetails.userName, forKey: USER_NAME)
        //            USER_DEFAULTS.set(userDetails.userEmail, forKey: EMAIL_ADDRESS)
        
        //            let screen = self.storyboard?.instantiateViewController(withIdentifier: "commentSubmittedViewController") as? commentSubmittedViewController
        //            screen?.modalPresentationStyle = .overCurrentContext
        //            self.present(screen!, animated: false, completion: nil)
        
        // }
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func dialogBackgrondButtonAction(_ sender: Any) {
        self.dialogBoxView.isHidden = true
        
    }
    
    @IBAction func dialogOkbuttonAction(_ sender: Any) {
        self.dialogBoxView.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Validations
    func isAllFieldVerified() -> Bool {
        var isFieldVerified = false
        let isEmailAddressValid = validation.validationShared.isEmailValid(email: emailTxtFld.text!)
        
        if userDetails.userName == nil {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your name", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if userDetails.userEmail == nil || userDetails.userEmail?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your email ID", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if !isEmailAddressValid {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your valid email ID", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if userDetails.userComment == "Comments" || userDetails.userComment?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your comment", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }else if userDetails.userComment?.count ?? 0 < 10 {
            present(UIAlertController.alertWithTitle(title: "", message: "The comments must be at least 10 characters.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else {
            //print("Verfication ending 405")
            isFieldVerified = true
        }
        
        return isFieldVerified
    }
}

extension ContactUsViewController: UITextFieldDelegate,UITextViewDelegate {
    
    //MARK:- TextField Functions
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameTxtFld {
            nameBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            nameTxtFld.placeholder?.removeAll()
            nameHeaderLbl.alpha = 1
            
        }else if textField == emailTxtFld {
            emailBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            emailTxtFld.placeholder?.removeAll()
            emailHeaderLbl.alpha = 1
        }
    }
    
    @IBAction func viewdidtapped(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTxtFld {
            emailBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            emailHeaderLbl.alpha = 0
            
            if emailTxtFld.text == "" {
                emailTxtFld.placeholder = "Email"
            }
            
        }else if textField == nameTxtFld {
            nameBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            nameHeaderLbl.alpha = 0
            
            if nameTxtFld.text == "" {
                nameTxtFld.placeholder = "Full Name"
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField.returnKeyType == .next {
            if textField == nameTxtFld {
                emailTxtFld.becomeFirstResponder()
            }else{
                commentsTxtView.becomeFirstResponder()
            }
        }else if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK:- TextView Functions
    func textViewDidBeginEditing(_ textView: UITextView) {
        commentsBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
        commentsTxtView.text = ""
        commentsTxtView.textColor = .black
        commentsHeaderLbl.alpha = 1
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        commentsBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
        commentsHeaderLbl.alpha = 0
        
        if commentsTxtView.text == "" {
            clearCommentBox()
            //            commentsTxtView.textColor = UIColor.lightGray
            //            commentsTxtView.text = "Comments"
        }
    }
    
    //To resign first responder on done click
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (range.length==0) {
            
            if text == "\n" {
                textView.resignFirstResponder()
                return false;
            }
        }
        return true;
    }
    
    func clearCommentBox(){
        commentsTxtView.textColor = UIColor.lightGray
        commentsTxtView.text = "Comments"
        
    }
    
}

// MARK:- Web Api
extension ContactUsViewController{
    
    fileprivate func callAPIToContact() {
        
        let paraDict = NSMutableDictionary()
        paraDict["name"] = self.userDetails.userName
        paraDict["email"] = self.userDetails.userEmail
        paraDict["comments"] = self.userDetails.userComment
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "common/contactUs") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                guard let message = data["message"] as? String else {
                    return
                }
                
//                let screen = self.storyboard?.instantiateViewController(withIdentifier: "commentSubmittedViewController") as? commentSubmittedViewController
//                screen?.modalPresentationStyle = .overCurrentContext
//                self.present(screen!, animated: false, completion: nil)
                self.dialogBoxView.isHidden = false
                
                self.clearCommentBox()
                
            }
        }
    }
}
