//
//  ChangePasswordViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 04/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var oldPasswordHeaderLbl: UILabel!
    @IBOutlet weak var oldPasswordTxtFld: UITextField!
    @IBOutlet weak var newPasswordHeaderLbl: UILabel!
    @IBOutlet weak var newPasswordTxtFld: UITextField!
    @IBOutlet weak var confirmPasswordHeaderLbl: UILabel!
    @IBOutlet weak var confirmPasswordTxtFld: UITextField!
    @IBOutlet weak var oldPasswordBorderLbl: UILabel!
    @IBOutlet weak var newPasswordBorderLbl: UILabel!
    @IBOutlet weak var confirmPasswordBorderLbl: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    @IBOutlet weak var dialogBox: UIView!
    @IBOutlet weak var popUpView: UIView!
    var restImage: UIImage?
    var userDetails = UserDetails()
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        // Clear background of navigation bar
        clearNavigationBar(navigationBar: self.navigationBar)
        self.popUpView.isHidden = true
        self.dialogBox.layer.cornerRadius = 10
        
    }
  
    
    //MARK:- BUTTON CLICKED
    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: false)
        
    }
    
    //MARK:- HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
//
   
    
    //MARK:- Button Action
    
    @IBAction func submitBtnAction(_ sender: UIButton)
    {
        self.userDetails.userOldPassword = trimWhiteSpaceNew(str: oldPasswordTxtFld.text)
        self.userDetails.userNewPassword = trimWhiteSpaceNew(str: newPasswordTxtFld.text)
        self.userDetails.userConfirmPassword = trimWhiteSpaceNew(str: confirmPasswordTxtFld.text)
        if isAllFieldVerified() {
            

//            //NEEd to uncomment this
//            callAPIToChangePassword()
            
////            USER_DEFAULTS.set(userDetails.userOldPassword, forKey: "oldPassword")
////            USER_DEFAULTS.set(userDetails.userNewPassword, forKey: "newPassword")
//            USER_DEFAULTS.set(userDetails.userConfirmPassword, forKey: USER_PASSWORD)
//
//            //on successfull submission
//        //}
        }
    }
    
    
    @IBAction func popupBackgroundButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        self.logoutCurrentActiveUser()
    }
    
    // MARK:- Validations
    func isAllFieldVerified() -> Bool {
        
        var isFieldVerified = false
        let isOldPasswordValid = validation.validationShared.isValidPassword(testStr: self.userDetails.userOldPassword!)
        let isNewPasswordValid = validation.validationShared.isValidPassword(testStr: self.userDetails.userNewPassword!)
        let isConfirmPasswordValid = validation.validationShared.isValidPassword(testStr: self.userDetails.userConfirmPassword!)
        
        var currentPassword = USER_DEFAULTS.value(forKey: USER_PASSWORD) as? String
        var oldPassword = self.userDetails.userOldPassword!
        
        if oldPasswordTxtFld.text == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your old Password.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }
        else if (oldPasswordTxtFld.text?.count)! < 8 {
            present(UIAlertController.alertWithTitle(title: "", message: "Invalid old Password.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }else if isOldPasswordValid == false {
            present(UIAlertController.alertWithTitle(title: "", message: "Password Should have At least one Uppercase letter.Atleast one Lower case letter.Also,At least one numeric value.And, At least one special character.Must be more than 6 characters long", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }
        else if newPasswordTxtFld.text == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your New Password.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }
        else if (newPasswordTxtFld.text?.count)! < 8 {
            present(UIAlertController.alertWithTitle(title: "", message: "Invalid New Password.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }else if isNewPasswordValid == false {
            present(UIAlertController.alertWithTitle(title: "", message: "Password Should have At least one Uppercase letter.Atleast one Lower case letter.Also,At least one numeric value.And, At least one special character.Must be more than 6 characters long", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }
        else if confirmPasswordTxtFld.text == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter Confirm Password.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }
        else if (confirmPasswordTxtFld.text?.count)! < 8 {
            present(UIAlertController.alertWithTitle(title: "", message: "Invalid Confirm Password.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }else if isConfirmPasswordValid == false {
            present(UIAlertController.alertWithTitle(title: "", message: "Password Should have At least one Uppercase letter.Atleast one Lower case letter.Also,At least one numeric value.And, At least one special character.Must be more than 6 characters long", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }
        else if newPasswordTxtFld.text != confirmPasswordTxtFld.text {
            present(UIAlertController.alertWithTitle(title: "", message: "Confirm password does not matched.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else {
            self.view.endEditing(true)
            
            let alertController = UIAlertController(title: "", message: "Are you sure you want to change your password?", preferredStyle: .alert)
            let yes = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
                self.callAPIToChangePassword()
                
                //return isFieldVerified
            }
            let no = UIAlertAction(title: "No", style: .cancel) { (UIAlertAction) in
                //return isFieldVerified
            }

            alertController.addAction(no)
            alertController.addAction(yes)
            self.present(alertController, animated: true, completion: nil)
            
           // print("Verfication ending 405")
            isFieldVerified = true
        }
        return isFieldVerified
    }
    
    // MARK:- TOUCH EVENT
    @IBAction func viewdidtapped(_ sender: Any) {
        self.view.endEditing(true)
    }
}

extension ChangePasswordViewController: UITextFieldDelegate {

    //MARK:- TextField Functions
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == oldPasswordTxtFld {
            oldPasswordBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            oldPasswordTxtFld.placeholder?.removeAll()
            oldPasswordHeaderLbl.alpha = 1
            
        }
        else if textField == newPasswordTxtFld {
            newPasswordBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            newPasswordTxtFld.placeholder?.removeAll()
            newPasswordHeaderLbl.alpha = 1
            
        }
        else if textField == confirmPasswordTxtFld {
            confirmPasswordBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            confirmPasswordTxtFld.placeholder?.removeAll()
            confirmPasswordHeaderLbl.alpha = 1
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        
        if textField == oldPasswordTxtFld {
            
            oldPasswordBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            oldPasswordHeaderLbl.alpha = 0
            
            if oldPasswordTxtFld.text == "" {
                oldPasswordTxtFld.placeholder = "Old Password"
            }
        }
        else if textField == newPasswordTxtFld {
            newPasswordBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            newPasswordHeaderLbl.alpha = 0
            
            if newPasswordTxtFld.text == "" {
                newPasswordTxtFld.placeholder = "New Password"
            }
            
        }
        else if textField == confirmPasswordTxtFld {
            
            confirmPasswordBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            confirmPasswordHeaderLbl.alpha = 0
            
            if confirmPasswordTxtFld.text == "" {
                confirmPasswordTxtFld.placeholder = "Confirm Password"
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        if textField.returnKeyType == .next {
            if textField == oldPasswordTxtFld {
                newPasswordTxtFld.becomeFirstResponder()
            }
            else{
                confirmPasswordTxtFld.becomeFirstResponder()
            }
        }
        else if textField.returnKeyType == .done{
            textField.resignFirstResponder()
        }
        return true
    }
   
}

extension ChangePasswordViewController {
    fileprivate func logoutCurrentActiveUser() {
        //        USER_DEFAULTS.bool(forKey: IS_REMEMBER)
        
        USER_DEFAULTS.set(false, forKey: IS_REMEMBER)
        let isCleared = ConveienceClass.clearAllDefaultValue()
        if isCleared {
            if self.navigationController?.viewControllers.count ?? 0 > 0 {
                
                if ((self.navigationController?.viewControllers.last) != nil) {
                    self.navigationController?.popToRootViewController(animated: false)
                } else {
                    self.navigationController?.viewControllers.removeAll()
                }
            }
            let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
            self.navigationController?.viewControllers = [mainView]
            let obj = AppDelegate()
            obj.window?.rootViewController = self.navigationController
            obj.window?.makeKeyAndVisible()
        }
        
    }
    
    fileprivate func callAPIToChangePassword() {
        
        let paraDict = NSMutableDictionary()
        paraDict["oldPassword"] = self.userDetails.userOldPassword
        paraDict["newPassword"] = self.userDetails.userNewPassword
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/user/changePassword") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSDictionary else {
                    return
                }
                
                guard let message = data["message"] as? String else {
                    return
                }
                
                USER_DEFAULTS.set(self.newPasswordTxtFld.text, forKey: "oldPassword")
                self.popUpView.isHidden = false
                self.clearField()
               
            }
        }
    }
    
    func clearField(){
        self.oldPasswordTxtFld.text = ""
        self.newPasswordTxtFld.text = ""
        self.confirmPasswordTxtFld.text = ""
        oldPasswordTxtFld.placeholder = "Old Password"
        confirmPasswordTxtFld.placeholder = "Confirm Password"
        newPasswordTxtFld.placeholder = "New Password"
    }
}
