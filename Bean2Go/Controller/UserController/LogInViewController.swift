//
//  LogInViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 18/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Firebase
import Alamofire
import AVKit
import AVFoundation


enum loginType: String {
    case kSocialLogin = "social"
    case kNormalLogin = "Normal"
}

class LogInViewController: UIViewController ,GIDSignInUIDelegate {
    
    // MARK:- OUTLETS
    @IBOutlet weak var GmailBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var rememberMeBtn: UIButton!
    
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var emailHeaderLbl: UILabel!
    @IBOutlet weak var emailBorderLbl: UILabel!
    
    @IBOutlet weak var passowrdTxtFld: UITextField!
    @IBOutlet weak var passwordBorderLbl: UILabel!
    @IBOutlet weak var passwordHeaderLbl: UILabel!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var videoView: UIView!
    
    @IBOutlet weak var orLbl: UILabel!
    
    // MARK:- DECLARE VARIABLES
    var check_status = true
    var validEmail = false
    var userDetails = UserDetails()
    var loginFrom = loginType.kNormalLogin
    var signInInfo = SocialLoginClass()
    var socialLoginGlobalPass: String = "abcd4321@A"
    var player: AVPlayer!
   
        
    private func playBackgoundVideo() {
        if let filePath = Bundle.main.path(forResource: "demoVideo", ofType:"mp4") {
            let filePathUrl = NSURL.fileURL(withPath: filePath)
            player = AVPlayer(url: filePathUrl)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil) { (_) in
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            }
            self.videoView.layer.addSublayer(playerLayer)
            player?.play()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self as GIDSignInUIDelegate
        playBackgoundVideo()
        
        orLbl.layer.cornerRadius = 3
        
        emailTxtFld.attributedPlaceholder = NSAttributedString(string: "Email",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        
        passowrdTxtFld.attributedPlaceholder = NSAttributedString(string: "Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        
        //GIDSignIn.sharedInstance().signIn()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpInitialView()
        self.loginBtn.layer.cornerRadius = 2
        self.view.endEditing(true)
    }
    
    //MARK:- HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK:- SET UP VIEW
    func setUpInitialView() {
        let isLogedIn = USER_DEFAULTS.bool(forKey: IS_REMEMBER)
        
        if isLogedIn {
            
            self.userDetails.userEmail = USER_DEFAULTS.value(forKey: EMAIL_ADDRESS) as? String
            self.userDetails.userPassword = USER_DEFAULTS.value(forKey: USER_PASSWORD) as? String
            
            self.emailTxtFld.text = self.userDetails.userEmail
            self.passowrdTxtFld.text = self.userDetails.userPassword
            
        } else {
            self.emailTxtFld.text = ""
            self.passowrdTxtFld.text = ""
        }
        self.userDetails.isRemember = isLogedIn
        rememberMeBtn.isSelected = isLogedIn
    }
    
    // MARK:- TOUCH EVENT to hide keyboard
    @IBAction func viewdidtapped(_ sender: Any) {
        
        if emailTxtFld.text == "" {
            emailTxtFld.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
            
        }
        
        if passowrdTxtFld.text == "" {
            passowrdTxtFld.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        }
        
        self.view.endEditing(true)
    }
    
    
    
    //MARK:BUTTON ACTION
    
    //MARK:- Login Button Action
    @IBAction func LoginBtnAction(_ sender: UIButton){
        
        self.userDetails.userEmail = ""
        self.userDetails.userPassword = ""
        
        self.userDetails.userEmail = trimWhiteSpaceNew(str: emailTxtFld.text)
        self.userDetails.userPassword = trimWhiteSpaceNew(str: passowrdTxtFld.text)
        
        if isAllFieldVerified(){
            //            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            //            self.navigationController?.pushViewController(nVC, animated: true)
            callLoginAPI()
        }
        
    }
    
    // MARK:- Remember Button Action
    @IBAction func rememberMeBtnAction(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        userDetails.isRemember = sender.isSelected
        
        if sender.isSelected {
            self.rememberMeBtn.isSelected = sender.isSelected
        } else {
            self.rememberMeBtn.isSelected = sender.isSelected
        }
    }
    
    // MARK:- Forgot Password Action
    @IBAction func forgotPasswordBtnAction(_ sender: UIButton) {
        let screen = self.storyboard?.instantiateViewController(withIdentifier: "fogotpasswordDBViewController") as? fogotpasswordDBViewController
        screen?.delegate = self as delegateMethod
        screen?.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(screen!, animated: false, completion: nil)
    }
    
    // MARK:- Gmail Button Action
    @IBAction func gmailBtnAction(_ sender: UIButton) {
        loginFrom = .kSocialLogin
        GmailLoginClass.gmailLoginShared.gmailDelegate = self as GmailSucessDelegate
        GmailLoginClass.gmailLoginShared.callSignIn()
    }
    
    // MARK:- Facebook Button Action
    
    @IBAction func facebookBtnAction(_ sender: UIButton) {
        loginFrom = .kSocialLogin
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")){
                    self.getFBUserData()
                    fbLoginManager.logOut()
                        
                    }
                
            }
            
        }
        
    }
    
}
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                if let userDataDict = result as? NSDictionary {
                    
                    self.signInInfo.givenName = userDataDict["name"] as? String ?? ""
                    self.signInInfo.email = userDataDict["email"] as? String ?? ""
                    self.signInInfo.userId = userDataDict["id"] as? String ?? ""
                    
                    //if self.checkLogin() == true{
                    self.userDetails.userEmail = userDataDict["email"] as? String ?? ""
                    self.userDetails.userName = userDataDict["name"] as? String ?? ""
                    self.userDetails.userPassword = self.socialLoginGlobalPass
                    self.callLoginAPI()
                    
//                    self.userName = userDataDict["name"] as? String ?? ""
//                    let email = userDataDict["email"] as? String ?? ""
//                    self.signUpCallService(userMobile: "", device_id: "ios", fcmKey: self.deicetokn, loginType: "facebook", email: email)
                    
                }
            }
        })
        }
    }
    
    
    // MARK:-Sign Up button Action
    @IBAction func SignUpBtnAction(_ sender: UIButton){
        
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        nVC.isFromSocialLogin = false
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
}



extension LogInViewController: UITextFieldDelegate {
    // MARK:-  Text field Validation
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTxtFld {
            //emailBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            emailHeaderLbl.alpha = 1
            
        } else if textField == passowrdTxtFld {
            //passwordBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            passwordHeaderLbl.alpha = 1
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == emailTxtFld {
            //emailBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            emailHeaderLbl.alpha = 0
            
            
        } else if textField == passowrdTxtFld {
            //passwordBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            passwordHeaderLbl.alpha = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            passowrdTxtFld.becomeFirstResponder()
        }
        else if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        return true
    }
}

extension LogInViewController: delegateMethod {
    //MARK:- Delegate Method
    
    func SubmitStatus() {
        let screen = self.storyboard?.instantiateViewController(withIdentifier: "resetPasswordDBViewController") as? resetPasswordDBViewController
        
        screen?.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(screen!, animated: false, completion: nil)
    }
}

extension LogInViewController: GmailSucessDelegate {
    
    func getLoginDetails(loginInfo: SocialLoginClass) {
        var gmailIdIsExits = false
        //if gmailIdIsExits {
        // one extra parameter add type
        
        self.userDetails.userEmail = loginInfo.email
        self.userDetails.userPassword = socialLoginGlobalPass
        signInInfo = loginInfo
        GIDSignIn.sharedInstance()?.signOut()
        
        self.callLoginAPI()
      
    }
}

//MARK:- FACEBOOK AUTHENTICATION
extension LogInViewController{
    
    func signInUsingFacebook(){
        
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
               // print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            guard let accessToken = FBSDKAccessToken.current() else {
               // LogInfo("Failed to get access token")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            // Perform login by calling Firebase APIs
            Auth.auth().signInAndRetrieveData(with: credential, completion: { (user, error) in
                if let error = error {
                    
                    LogInfo("Login error: \(error.localizedDescription)")
                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                } else {
                    if let currentUser = Auth.auth().currentUser {
                        
                        self.signInInfo.givenName = currentUser.displayName
                        self.signInInfo.email = currentUser.email
                        self.signInInfo.userId = currentUser.uid
                        
                        //if self.checkLogin() == true{
                        self.userDetails.userEmail = self.signInInfo.email
                        self.userDetails.userPassword = self.socialLoginGlobalPass
                        fbLoginManager.logOut()
                        self.callLoginAPI()
                       
                        
                    }
                }
                
            })
            
        }
        
    }
    
    //MARK:- check credientials
    func checkLogin() -> Bool{
        let email = USER_DEFAULTS.value(forKey: EMAIL_ADDRESS) as? String ?? ""
        if signInInfo.email == email{
            return true
        }
        return false
    }
}

extension LogInViewController {
    
    // MARK:- Validations
    fileprivate func isAllFieldVerified() -> Bool {
        
        var isFieldVerified = false
        let isEmailAddressValid = validation.validationShared.isEmailValid(email: self.userDetails.userEmail ?? "")
        let isPasswordValid = validation.validationShared.isValidPassword(testStr: self.userDetails.userPassword!)
        
        self.view.endEditing(true)
        
        if (userDetails.userEmail)! == "" || (userDetails.userEmail?.count)! == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your Login ID", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if isEmailAddressValid == false && userDetails.userEmail?.count != 11 {
            
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your valid Login ID", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if userDetails.userPassword == nil || userDetails.userPassword?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your Password", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if isPasswordValid == false {
            present(UIAlertController.alertWithTitle(title: "", message: "Password Should have At least one Uppercase letter.Atleast one Lower case letter.Also,At least one numeric value.And, At least one special character.Must be more than 6 characters long", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }  else {
            isFieldVerified = true
        }
        
        return isFieldVerified
    }
    
    //MARK:-  Call Web API
    
    func callLoginAPIo() {
        
        let paramDict = NSMutableDictionary()
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paramDict as! [String : Any], method: .post, apiName: "seller/status/isApproved") { (response, error,message,statusCode)  in
            
            if message != nil{
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                     let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                     if self.navigationController?.viewControllers.count ?? 0 > 0 {
                     
                     if ((self.navigationController?.viewControllers.last) != nil) {
                     self.navigationController?.popToRootViewController(animated: false)
                     } else {
                     self.navigationController?.viewControllers.removeAll()
                     }
                     }
                     let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                     self.navigationController?.viewControllers = [mainView]
                     let obj = AppDelegate()
                     obj.window?.rootViewController = self.navigationController
                     obj.window?.makeKeyAndVisible()
                     }
                     alertController.addAction(action1)
                     self.present(alertController, animated: true, completion: nil)
                     */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                return
            }else {
                
            }
        }
        
    }
    
    // MARK:- Login API
    func callLoginAPI() {
        let paraDict = NSMutableDictionary()
        paraDict["email"] = self.userDetails.userEmail
        paraDict["password"] = self.userDetails.userPassword
        paraDict["type"] = self.loginFrom.rawValue
        paraDict["device_id"] = USER_DEFAULTS.value(forKey: DEVICE_TOKEN)
        paraDict["gcm_key"] = USER_DEFAULTS.value(forKey: FCM_TOKEN)
        paraDict["device_type"] = "ios"
        
        print(paraDict)
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/login") { (response, error,message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    
                } else if statusCode == "201" {
                    
                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as!SignUpViewController
                    nVC.isFromSocialLogin = true
                    nVC.socialLoginObj = self.signInInfo
                    self.navigationController?.pushViewController(nVC, animated: true)
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                
                return
            }else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                return
            }else {
                
                LogInfo("")
                
                let JSON = response as! NSDictionary
                
                let data = JSON["data"] as? NSDictionary
                let otp_Status = data!["is_mobile_verified"] as? Int ?? 0
                let name = data!["name"] as? String ?? ""
                let role = data!["role"] as? String ?? ""
                let metaData = JSON["meta"] as? NSDictionary
                let token = metaData!["token"] as? String ?? ""
                let email = data!["email"] as? String ?? self.userDetails.userEmail
                let userId = data!["user_id"] as? Int ?? self.userDetails.userID
                
                USER_DEFAULTS.set(token, forKey: Auth_Token)
                
                if otp_Status == 1 {
                    USER_DEFAULTS.set(name, forKey: USER_NAME)
                    USER_DEFAULTS.set(email, forKey: EMAIL_ADDRESS)
                    USER_DEFAULTS.set(self.userDetails.userPassword, forKey: USER_PASSWORD)
                    USER_DEFAULTS.set(role, forKey: USER_TYPE)
                    USER_DEFAULTS.set(userId, forKey: USER_ID)
                    USER_DEFAULTS.set(true, forKey: IS_LOGGEDIN)
                    USER_DEFAULTS.set(self.userDetails.isRemember, forKey: IS_REMEMBER)
                    USER_DEFAULTS.set(true, forKey: IS_MOBILE_NUMBER_VERIFIED)
                    
                    let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
                    
                    if userType == BUYER {
                        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                        SocketManagerClass.shared.connectSocket()
                        self.navigationController?.pushViewController(nVC, animated: true)
                    } else {
                        
                        let isApproved = metaData!["is_store_approved"] as? Int == 0 ? false : true
                        
                        USER_DEFAULTS.set(isApproved, forKey: IS_SELLER_APPROVED)
                        
                        if isApproved {
                            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "recentOrderViewController") as! recentOrderViewController
                            //MARK:- Coonection establish with socket
                            SocketManagerClass.shared.connectSocket()
                            
                            self.navigationController?.pushViewController(nVC, animated: true)
                        } else {
                            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "recentOrderViewController") as! recentOrderViewController
                            self.navigationController?.pushViewController(nVC, animated: true)
                            
                            //self.navigationController?.popToRootViewController(animated: false)
                        }
                    }
                    
                    
                    
                } else {
                    USER_DEFAULTS.set(true, forKey: IS_MOBILE_NUMBER_VERIFIED)
                    let data = JSON["data"] as? NSDictionary
                    let userId = data!["user_id"] as? Int ?? 0
                    let meta = JSON["meta"] as? NSDictionary
                    let alertController = UIAlertController(title: "", message: "Verify mobile number. Resend Otp?", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.callResendOtpAPI(userId: userId)
                    }
                    
                    let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    
                    alertController.addAction(action1)
                    alertController.addAction(action2)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                    
                }
                
            }
        }
    }
    
    //MARK:- Resend OTP API
    func callResendOtpAPI(userId: Int){
        let paraDict = NSMutableDictionary()
        
        paraDict["user_id"] = userId
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/otp/resend") { (response, error, message, statusCode) in
            
            if message != nil{
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                     let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                     if self.navigationController?.viewControllers.count ?? 0 > 0 {
                     
                     if ((self.navigationController?.viewControllers.last) != nil) {
                     self.navigationController?.popToRootViewController(animated: false)
                     } else {
                     self.navigationController?.viewControllers.removeAll()
                     }
                     }
                     let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                     self.navigationController?.viewControllers = [mainView]
                     let obj = AppDelegate()
                     obj.window?.rootViewController = self.navigationController
                     obj.window?.makeKeyAndVisible()
                     }
                     alertController.addAction(action1)
                     self.present(alertController, animated: true, completion: nil)
                     */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //                ConveienceClass.networkAlert(controller: self, completionHandler: { (sucess) in
                //                    if sucess ?? false {
                //                        //self.navigationController?.popViewController(animated: true)
                //                    }
                //                })
                return
            }else {
                let meta = response?["meta"] as? NSDictionary
                let otp = meta?["otp"] as? String ?? ""
                
                if otp != "" {
                    let VC = UIStoryboard(name: "Main" , bundle:nil).instantiateViewController(withIdentifier: "otpVerificationViewController") as! otpVerificationViewController
                    VC.user_Id = userId
                    VC.isRememberLogin = self.userDetails.isRemember
                    let userEmail = trimWhiteSpaceNew(str: self.emailTxtFld.text)
                    let userPassword = trimWhiteSpaceNew(str: self.passowrdTxtFld.text)
                    VC.emailId = userEmail!
                    VC.password = userPassword!
                    VC.otp = otp
                    self.navigationController?.pushViewController(VC, animated: true)
                    
                }
            }
        }
        
        //        let url = URL(string: "\(baseURL)auth/otp/resend")
        //
        //        self.indicatorViewObj.showIndicatorView(viewController: self)
        //
        //        Alamofire.request(url!, method: .post, parameters: paraDict as? [String: Any], encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json"]).responseJSON { response in
        //            print(response)
        //            self.indicatorViewObj.hideIndicatorView()
        //            guard let result = response.result.value else{
        //                return
        //            }
        //            let JSON = result as! NSDictionary
        //
        //            //to get status code
        //            if let status = response.response?.statusCode {
        //                switch(status){
        //                case 200:
        //                    let meta = JSON["meta"] as? NSDictionary
        //                    let msg = JSON["message"] as? String ?? ""
        //                    let otp = meta?["otp"] as? String ?? ""
        //
        //                    if otp != "" {
        //                        print(otp)
        //
        //                        //let characters = otp.components(separatedBy: "")
        //
        //                        let characters = Array(otp)
        //                        print(characters)
        //                        self.digit1TextView.text = String(characters[0])
        //                        self.digit2TextView.text = String(characters[1])
        //                        self.digit3TextView.text = String(characters[2])
        //                        self.digit4TextView.text = String(characters[3])
        //
        //                    }
        //
        //
        //
        //                    self.showToast(message: msg)
        //
        //                    self.startTimer()
        //
        //                case 422:
        //                    let message = JSON["message"] as? String ?? ""
        //                    self.showToast(message: message)
        //                default:
        //                    print("error with response status: \(status)")
        //                }
        //            }
        //
        //        }
        
    }
}
