//
//  myAccountViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 05/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class myAccountViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    var imageArray = [String]()

    var array: [[String: String]] = [["Image": "Account_1","Name": "MY PROFILE"],
                                     ["Image": "Account_2","Name": "PASSWORD"],
                                     ["Image": "Account_3","Name": "ABOUT US"],
                                     ["Image": "Account_4","Name": "CONTACT US"],
                                     ["Image": "Account_5","Name": "PRIVACY POLICY"]]
//                                     ["Image": "transection_icon","Name": "TRANSACTION"],
//                                     ["Image": "my_orders_icon","Name": "MY ORDERS"]]
    
   
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Clear background of navigation bar
        clearNavigationBar(navigationBar: self.navigationBar)
        
        for i in 1...5 {
            imageArray.append("Account_\(i)")
        }
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
            
        case 1:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
            
        case 2:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
            
        case 3:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
            
        case 4:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
            
       
        default:
            break
        }
    }
    // MARK:- Hide Status Bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK:- Button Action
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension myAccountViewController: UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myAccountCollectionViewCell", for: indexPath) as! myAccountCollectionViewCell
                
//        cell.contentView.layer.addborder(width: 2, color: UIColor.init(red: 230/255, green: 230/255, blue: 230/255, alpha: 1))
//        cell.contentView.layer.cornerRadius = 2
//        cell.contentView.clipsToBounds = true
        cell.cellButton.tag = indexPath.item
        cell.cellImgView.image = UIImage(named: imageArray[indexPath.row])
        //cell.cellImgView.image = UIImage(named: array[indexPath.row]["Image"]!)
        //cell.cellLbl.text = array[indexPath.row]["Name"]!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.item == 4 {
            return CGSize(width: (collectionView.frame.size.width) - 10, height: (collectionView.frame.size.width / 2 ))
        } else {
            return CGSize(width: (collectionView.frame.size.width / 2 ) - 10, height: (collectionView.frame.size.width / 2 ))
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
            
        case 1:
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
            
        default:
            break
        }
    }
    
}
