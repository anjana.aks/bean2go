//
//  signUpViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 18/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import AVFoundation
import CountryPickerView

enum selectedUserType: String{
    case kBuyer = "buyer"
    case kSeller = "seller"
}

class SignUpViewController: UIViewController ,UITextFieldDelegate {
    
    @IBOutlet weak var backgroundImgView: UIImageView!
    @IBOutlet weak var passwordBorderLbl: UILabel!
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var passwordHeaderLbl: UILabel!
    @IBOutlet weak var emailBorderLbl: UILabel!
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var emailHeaderLbl: UILabel!
    @IBOutlet weak var nameBorderLbl: UILabel!
    @IBOutlet weak var nametxtFld: UITextField!
    @IBOutlet weak var nameHeaderLbl: UILabel!
    @IBOutlet weak var phoneTxtfld: UITextField!
    @IBOutlet weak var phoneHeaderLbl: UILabel!
    @IBOutlet weak var phoneBorderLbl: UILabel!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var logoImgView: UIImageView!
     @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var userTypeHeaderLbl: UILabel!
    @IBOutlet weak var userTypeBorderLbl: UILabel!
    @IBOutlet weak var userTypeButton: UIButton!
    @IBOutlet weak var sellerButton: UIButton!
    @IBOutlet weak var buyerButton: UIButton!
    @IBOutlet weak var userTypeOptionsView: UIView!
    @IBOutlet weak var userTypeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoView: UIView!
    
    @IBOutlet weak var contryCodeBtn: UIButton!
    @IBOutlet weak var contryCodeLbl: UILabel!
    
    var selectedTypeUser: selectedUserType = .kBuyer
    var bottomstring = "By creating account, you agree to our terms,already have an account? SignIn"
    var userDetails = UserDetails()
    
    var socialLoginObj = SocialLoginClass()
    var isFromSocialLogin: Bool = false
    var player: AVPlayer!
    let countryCodePickerView = CountryPickerView()
    
    private func playBackgoundVideo() {
        if let filePath = Bundle.main.path(forResource: "demoVideo", ofType:"mp4") {
            let filePathUrl = NSURL.fileURL(withPath: filePath)
            player = AVPlayer(url: filePathUrl)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil) { (_) in
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            }
            self.videoView.layer.addSublayer(playerLayer)
            player?.play()
        }
    }
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad(){
        super.viewDidLoad()
        self.countryCodePickerView.delegate = self
        
        UIApplication.shared.statusBarStyle = .default
        
        self.signUpBtn.layer.cornerRadius = 4
        self.signUpBtn.clipsToBounds = true
        
        self.emailHeaderLbl.isHidden = true
        self.nameHeaderLbl.isHidden = true
        self.passwordHeaderLbl.isHidden = true
        self.phoneHeaderLbl.isHidden = true
        self.phoneTxtfld.addToolBar()
     
        nametxtFld.attributedPlaceholder = NSAttributedString(string: "Full Name",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        
        emailTxtFld.attributedPlaceholder = NSAttributedString(string: "Email",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        
        passwordTxtFld.attributedPlaceholder = NSAttributedString(string: "Password",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        
        phoneTxtfld.attributedPlaceholder = NSAttributedString(string: "Contact No.",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        
        playBackgoundVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.signUpBtn.setBackgroundImageAndCornerRadius()
        
        //if isFromSocialLogin {
            nametxtFld.text = socialLoginObj.givenName
            emailTxtFld.text = socialLoginObj.email
            //passwordTxtFld.text = socialLoginObj.password
            //self.passwordView.isHidden = true
        //} else {
            //self.passwordView.isHidden = false
            
        //}
    }
    
    //MARK:- HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    //MARK:- Textfield Functions
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        hideUserTypeOptionalView()
        if textField == nametxtFld {
            //nameBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            nametxtFld.placeholder?.removeAll()
            self.nameHeaderLbl.isHidden = false
            
        } else if textField == emailTxtFld {
            //emailBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            emailTxtFld.placeholder?.removeAll()
            emailHeaderLbl.isHidden = false
        }
        else if textField == passwordTxtFld {
            //passwordBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
            passwordTxtFld.placeholder?.removeAll()
            self.passwordHeaderLbl.isHidden = false
        }
        else if textField == phoneTxtfld {
            
            let password: String = passwordTxtFld.text ?? " "
            if password.starts(with: " ") || password.last == " " {
                present(UIAlertController.alertWithTitle(title: "", message: "Please don't begin or end your password with blank space.", buttonTitle: "OK"), animated: true, completion: nil)
                passwordTxtFld.becomeFirstResponder()
            } else {
                //phoneBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
                
                phoneTxtfld.placeholder?.removeAll()
                
                self.phoneHeaderLbl.isHidden = false
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == nametxtFld {
            //nameBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            self.nameHeaderLbl.isHidden = true
            if nametxtFld.text == "" {
                nametxtFld.placeholder = "Full Name"
            }
        } else if textField == emailTxtFld {
            //emailBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            emailHeaderLbl.isHidden = true
            
            if emailTxtFld.text == "" {
                emailTxtFld.placeholder = "Email"
            }
        } else if textField == passwordTxtFld {
            //passwordBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            self.passwordHeaderLbl.isHidden = true
            if passwordTxtFld.text == "" {
                passwordTxtFld.placeholder = "Password"
            }
        } else if textField == phoneTxtfld {
            
            //phoneBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
            self.phoneHeaderLbl.isHidden = true
            if phoneTxtfld.text == "" {
                phoneTxtfld.placeholder = "Contact No."
            }
        }
        
        resetTextFldPlaceHolder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            if textField == nametxtFld {
                emailTxtFld.becomeFirstResponder()
            }
            else if textField == emailTxtFld {
               // if isFromSocialLogin {
                  //  self.phoneTxtfld.becomeFirstResponder()
                //} else {
                    passwordTxtFld.becomeFirstResponder()
                //}
            } else if textField == passwordTxtFld {
                let password: String = passwordTxtFld.text ?? " "
                if password.starts(with: " ") || password.last == " " {
                    present(UIAlertController.alertWithTitle(title: "", message: "Please don't begin or end your password with blank space.", buttonTitle: "OK"), animated: true, completion: nil)
                    passwordTxtFld.becomeFirstResponder()
                }
                phoneTxtfld.becomeFirstResponder()
            }
            
        }
        else if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        
        resetTextFldPlaceHolder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTxtfld {
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            var str : NSString = (textField.text as NSString?)!
            str = str.replacingCharacters(in: range, with: string) as NSString
            if trimWhiteSpace(str: str as String).count < 8 || trimWhiteSpace(str: str as String).count > 10     {
                if trimWhiteSpace(str: str as String).count > 10 {
                    // imgView_checkPhone.image = UIImage(named: "check_green")
                    return false
                }
            }
            
            return string == numberFiltered
            
        } else if textField == emailTxtFld {
            var str : NSString = (textField.text as NSString?)!
            str = str.replacingCharacters(in: range, with: string) as NSString
            if trimWhiteSpace(str: str as String).count < 10 || trimWhiteSpace(str: str as String).count > 30     {
                if trimWhiteSpace(str: str as String).count > 30 {
                    // imgView_checkPhone.image = UIImage(named: "check_green")
                    return false
                }
            }
        } else if textField == passwordTxtFld {
            var str : NSString = (textField.text as NSString?)!
            str = str.replacingCharacters(in: range, with: string) as NSString
            if trimWhiteSpace(str: str as String).count < 6 || trimWhiteSpace(str: str as String).count > 20     {
                if trimWhiteSpace(str: str as String).count > 20 {
                    // imgView_checkPhone.image = UIImage(named: "check_green")
                    return false
                }
            }
        } else if textField == nametxtFld {
            var str : NSString = (textField.text as NSString?)!
            str = str.replacingCharacters(in: range, with: string) as NSString
            if trimWhiteSpace(str: str as String).count < 6 || trimWhiteSpace(str: str as String).count > 30     {
                if trimWhiteSpace(str: str as String).count > 30 {
                    // imgView_checkPhone.image = UIImage(named: "check_green")
                    return false
                }
            }
        }
        
        return true
        
    }
    
    //MARK:- Button Actions
    
    //Mark:- Select Country Code
    
    
    
    @IBAction func SignUpBtnAction(_ sender: UIButton) {
        self.userDetails.userName = trimWhiteSpaceNew(str: nametxtFld.text)
        self.userDetails.userEmail = trimWhiteSpaceNew(str: emailTxtFld.text)
        
        self.userDetails.userPassword = trimWhiteSpaceNew(str: passwordTxtFld.text)
        self.userDetails.userContactNo = trimWhiteSpaceNew(str: phoneTxtfld.text)
        
        //LogInfo(otp)
        
        if isAllFieldVerified() {
            RegisterAPI()
            
        }
        
        
    }
    
    fileprivate func showOptionalUserTypeView() {
        userTypeOptionsView.isHidden = false
        userTypeViewHeightConstraint.constant = 80
        userTypeOptionsView.applyShadow(shadowRadius: 5, height: userTypeViewHeightConstraint.constant,color: UIColor.black)
    }
    
    @IBAction func selectUserTypeButtonAction(_ sender: UIButton) {
        
        self.viewDidTapped(self)
        //userTypeBorderLbl.backgroundColor = BORDER_COLOR_SELECTED
        userTypeHeaderLbl.isHidden = false
        showOptionalUserTypeView()
        
    }
    
    @IBAction func sellerButtonAction(_ sender: UIButton) {
        
        selectedTypeUser = .kSeller
        //userTypeBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
        userTypeHeaderLbl.isHidden = true
        hideUserTypeOptionalView()
        
        selectedTypeUser = .kSeller
        userDetails.userType = selectedTypeUser.rawValue
        userTypeButton.setTitleColor(UIColor.white, for: .normal)
        userTypeButton.setTitle(SELLER, for: .normal)
        
    }
    
    fileprivate func hideUserTypeOptionalView() {
        self.userTypeHeaderLbl.isHidden = true
        userTypeOptionsView.isHidden = true
        userTypeViewHeightConstraint.constant = 0
        userTypeOptionsView.applyShadow(shadowRadius: 0, height: 0,color: UIColor.black)
    }
    
    @IBAction func buyerButtonAction(_ sender: UIButton) {
        
        selectedTypeUser = .kBuyer
        //userTypeBorderLbl.backgroundColor = BORDER_COLOR_UNSELECTED
        userTypeHeaderLbl.isHidden = true
        hideUserTypeOptionalView()
        selectedTypeUser = .kBuyer
        userDetails.userType = selectedTypeUser.rawValue
        //userTypeOptionsView.isHidden = true
        userTypeButton.setTitleColor(UIColor.white, for: .normal)
        userTypeButton.setTitle(BUYER, for: .normal)
        
    }
    
    
    @IBAction func termsBtnAction(_ sender: UIButton){
//        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "TermsConditionsVC") as! TermsConditionsVC
//        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @IBAction func SignInBtnAction(_ sender: UIButton){
        let VC = UIStoryboard(name:"Main" , bundle:nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    // MARK:- TOUCH EVENT
    fileprivate func resetTextFldPlaceHolder() {
        if emailTxtFld.text == "" {
            emailTxtFld.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
            
        }
        
        if nametxtFld.text == "" {
            nametxtFld.attributedPlaceholder = NSAttributedString(string: "Full Name",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        }
        
        if passwordTxtFld.text == "" {
            passwordTxtFld.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        }
        
        if phoneTxtfld.text == "" {
            phoneTxtfld.attributedPlaceholder = NSAttributedString(string: "Contact No.",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#b3b3b3")])
        }
    }
    
    @IBAction func viewDidTapped(_ sender: Any){
        self.view.endEditing(true)
        
        resetTextFldPlaceHolder()
        
        
        hideUserTypeOptionalView()
        
    }
    
    // MARK:- Validations
    func isAllFieldVerified() -> Bool {
        var isFieldVerified = false
        let isEmailAddressValid = validation.validationShared.isEmailValid(email: emailTxtFld.text!)
        let isPasswordValid = validation.validationShared.isValidPassword(testStr: self.userDetails.userPassword!)
        
        if userDetails.userName == nil || userDetails.userName?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your full name", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if userDetails.userEmail == nil || userDetails.userEmail?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your email ID", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if !isEmailAddressValid {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your valid email ID", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if userDetails.userPassword == nil || userDetails.userPassword?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your Password", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if isPasswordValid == false {
            present(UIAlertController.alertWithTitle(title: "", message: "Password Should have At least one Uppercase letter.Atleast one Lower case letter.Also,At least one numeric value.And, At least one special character.Must be more than 6 characters long", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }  else if (userDetails.userPassword?.count)! < 6 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter valid password", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if (userDetails.userContactNo?.count)! != 10 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter valid Phone No", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if userDetails.userType == nil || userDetails.userType?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please select user type.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else {
            //print("Verfication ending 405")
            isFieldVerified = true
        }
        
        return isFieldVerified
    }
    
    
    
    //MARK:- Web API
    func RegisterAPI(){
        let paraDict = NSMutableDictionary()
        
        _ = ConveienceClass.clearAllDefaultValue()
        
        
        let countryCode = "+91"
        
        paraDict["name"] = self.userDetails.userName
        paraDict["password"] = self.userDetails.userPassword
        paraDict["email"] = self.userDetails.userEmail
        paraDict["mobile_number"] = self.userDetails.userContactNo
        paraDict["device_id"] = USER_DEFAULTS.value(forKey: DEVICE_TOKEN)
        paraDict["gcm_key"] = USER_DEFAULTS.value(forKey: FCM_TOKEN)
        paraDict["device_type"] = "ios"
        paraDict["country_code"] = self.userDetails.countryCode
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/register/\(selectedTypeUser.rawValue)") { (response, error,message, statusCode) in
            
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection error", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                return
            } else {
                let JSON = response as! NSDictionary
                
                let data = JSON["data"] as? NSDictionary
                let userId = data!["user_id"] as? Int ?? 0
                let userName = data!["name"] as? String ?? ""
                let userEmail = data!["email"] as? String ?? ""
                let userType = data!["role"] as? String ?? ""
                
                let meta = JSON["meta"] as? NSDictionary
                let otp = meta!["otp"] as? String ?? ""
                
                LogInfo("\(otp)")
                LogInfo("\(userId)")
                
                let sucess = ConveienceClass.clearAllDefaultValue()
                
                if sucess {
                    USER_DEFAULTS.set("", forKey: MOBIL_NO)
                    USER_DEFAULTS.set(userName, forKey: USER_NAME)
                    USER_DEFAULTS.set(userEmail, forKey: EMAIL_ADDRESS)
                    USER_DEFAULTS.set(self.userDetails.userPassword, forKey: USER_PASSWORD)
                    USER_DEFAULTS.set(userType, forKey: USER_TYPE)
                    USER_DEFAULTS.set(userId, forKey: USER_ID)
                    USER_DEFAULTS.set(self.userDetails.countryCode, forKey: COUNTRY_CODE)
                }
                                
                let VC = UIStoryboard(name: "Main" , bundle:nil).instantiateViewController(withIdentifier: "otpVerificationViewController") as! otpVerificationViewController
                VC.user_Id = userId
                VC.otp = otp
                VC.emailId = userEmail
                VC.password = self.userDetails.userPassword ?? ""
                VC.userType = self.selectedTypeUser.rawValue
                self.navigationController?.pushViewController(VC, animated: true)
                
                LogInfo(JSON)
            }
            
        }
        //
        //
        //        let url = URL(string: "\(baseURL)auth/register/\(selectedTypeUser.rawValue)")
        //
        //        self.indicatorViewObj.showIndicatorView(viewController: self)
        //
        //        Alamofire.request(url!, method: .post, parameters: paraDict as? [String: Any], encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json"]).responseJSON { response in
        //            LogInfo(response)
        //
        //            /*
        //             Verfication ending 405
        //             SUCCESS: {
        //             data =     {
        //             email = "tester@gmail.com";
        //             "is_email_verified" = 0;
        //             "is_mobile_verified" = 0;
        //             name = tester;
        //             role = buyer;
        //             "user_id" = 24;
        //             };
        //             meta =     {
        //             otp = 4428;
        //             };
        //             }
        // */
        //            self.indicatorViewObj.hideIndicatorView()
        //            self.hideUserTypeOptionalView()
        //
        //            let error = response.error
        //
        //            if error != nil {
        //                self.present(UIAlertController.alertWithTitle(title: "", message: "Network connection problem" , buttonTitle: "OK"), animated: true, completion: nil)
        //                return
        //            } else {
        //
        //                guard let result = response.result.value else{
        //                    return
        //                }
        //                let JSON = result as! NSDictionary
        //
        //                LogInfo(JSON)
        //
        //                //to get status code
        //                if let status = response.response?.statusCode {
        //                    switch(status){
        //                    case 200:
        //                        let data = JSON["data"] as? NSDictionary
        //                        let userId = data!["user_id"] as? Int ?? 0
        //                        let userName = data!["name"] as? String ?? ""
        //                        let userEmail = data!["email"] as? String ?? ""
        //                        let userType = data!["role"] as? String ?? ""
        //
        //                        let meta = JSON["meta"] as? NSDictionary
        //                        let otp = meta!["otp"] as? String ?? ""
        //
        //                        LogInfo("\(otp)")
        //                        LogInfo("\(userId)")
        //
        //                        USER_DEFAULTS.set(userName, forKey: USER_NAME)
        //                        USER_DEFAULTS.set(userEmail, forKey: EMAIL_ADDRESS)
        //                        USER_DEFAULTS.set(self.userDetails.userPassword, forKey: USER_PASSWORD)
        //                        USER_DEFAULTS.set(userType, forKey: USER_TYPE)
        //                        USER_DEFAULTS.set(userId, forKey: USER_ID)
        //
        //                        let VC = UIStoryboard(name: "Main" , bundle:nil).instantiateViewController(withIdentifier: "otpVerificationViewController") as! otpVerificationViewController
        //                        VC.user_Id = userId
        //                        VC.otp = otp
        //                        VC.userType = self.selectedTypeUser.rawValue
        //                        self.navigationController?.pushViewController(VC, animated: true)
        //
        //                        break
        //
        //                    case 422:
        //                        self.hideUserTypeOptionalView()
        //                        let errorsDict: NSDictionary = JSON["errors"] as! NSDictionary
        //                        LogInfo(errorsDict)
        //                        for error in errorsDict {
        //                            LogInfo(error.key)
        //                            LogInfo(error.value)
        //
        //                            let errorMsgArray: NSArray = (error.value as? NSArray)!
        //
        //                            for msg in errorMsgArray {
        //                                LogInfo(msg)
        //                                self.present(UIAlertController.alertWithTitle(title: "", message: "\(msg)", buttonTitle: "OK"), animated: true, completion: nil)
        //                                return
        //                            }
        //                        }
        //                        break
        //
        //                    case 500:
        //                        self.hideUserTypeOptionalView()
        //                        break
        //                    default:
        //                        self.hideUserTypeOptionalView()
        //                        print("error with response status: \(status)")
        //                    }
        //                }
        //            }
        //
        //        }
        
    }
    
}

//MARK:- Country Code
extension SignUpViewController : CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        //self.countryCode.setTitle(country.phoneCode, for: .normal)
        self.contryCodeLbl.text = country.phoneCode
        self.userDetails.countryCode = country.phoneCode
        
    }
    
    @IBAction func selectCountryCodeBtnAction(_ sender: UIButton) {
        countryCodePickerView.showCountriesList(from: self)
    }
}
