//
//  recentOrderViewController.swift
//  Bean2Go
//
//  Created by Anjana Aks on 27/03/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import Firebase

class recentOrderViewController: UIViewController {
    
    var window: UIWindow?
    
    @IBOutlet weak var congratsSubView: UIView!
    @IBOutlet weak var congratsView: UIView!
    
    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var checkStatusButton: UIButton!
    
    @IBOutlet weak var notificationCount: UIButton!
    //  @IBOutlet var navigationBar: UINavigationItem!
    @IBOutlet weak var collectionView: UICollectionView!
    //@IBOutlet weak var navigationItemObject: UINavigationItem!
    @IBOutlet var titleBar: UINavigationItem!
    @IBOutlet var navigationLogoutButton: UIBarButtonItem!
    @IBOutlet var navigattionNotificationButton: UIBarButtonItem!
    @IBOutlet var serachBarButton: UIBarButtonItem!
    @IBOutlet var sideMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var notificationLbl: UILabel!
    var urlString: String = ""
    
    var recentOrderList = [RecentOrdersInfo]()
    
    var refreshController = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recentOrderList = []
        
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.collectionView.refreshControl = refreshController
        
        congratsSubView.layer.cornerRadius = 15
        congratsSubView.shadow(UIColor.lightGray)
        intialBar()
        
    }
    
    //MARK:- Refresh Action
    fileprivate func refreshAllData() {
        self.recentOrderList = []
        self.collectionView.reloadData()
        self.callAPIToGetRecentOrder()
    }
    
    @objc func refresh(sender:AnyObject) {
        refreshAllData()
        refreshController.endRefreshing()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showAndHideCongratsView()
    }
    
    fileprivate func intialBar() {
        titleBar.rightBarButtonItem = nil
        titleBar.leftBarButtonItem = nil
    }
    
    fileprivate func showCongratsView() {
        
        self.callAPIToCheckStatus()
        //        titleBar.rightBarButtonItem = nil
        titleBar.rightBarButtonItem = nil
        titleBar.rightBarButtonItem = navigationLogoutButton
        titleBar.rightBarButtonItem?.tintColor = .white
        congratsView.isHidden = false
        titleBar.title = ""
        titleBar.leftBarButtonItem = nil
    }
    
    fileprivate func hideCongratsView() {
        //titleBar.rightBarButtonItem = nil
        titleBar.rightBarButtonItem = serachBarButton
        titleBar.rightBarButtonItem = navigattionNotificationButton
        titleBar.rightBarButtonItem?.tintColor = .white
        titleBar.leftBarButtonItem?.tintColor = .white
        congratsView.isHidden = true
        titleBar.title = "Recent Order"
        titleBar.leftBarButtonItem = sideMenuButton
        callAPIToGetRecentOrder()
    }
    
    fileprivate func showAndHideCongratsView() {
        let isApproved = USER_DEFAULTS.bool(forKey: IS_SELLER_APPROVED)
        
        if isApproved == false {
            showCongratsView()
        } else {
            hideCongratsView()
        }
    }
    
    
    //MARK:- Button Action
    @IBAction func backBtnTapped(_ sender: UIBarButtonItem) {
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
        nVC.openSelectedDelegate = self as openSelectedViewControllerDelegate
        nVC.modalPresentationStyle = .overCurrentContext
        
        self.navigationController?.present(nVC, animated: false, completion: nil)
    }
    
    @IBAction func clickHereIsClicked(_ sender: UITapGestureRecognizer) {
        guard let text = messageLabel.attributedText?.string else {
            return
        }
        
        if text.range(of:"Click here") != nil {
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url, options: [:])
            }
        }
    }
    
    @IBAction func navigationLogoutButtondidTapped(_ sender: UIBarButtonItem) {
        logoutButtonDidTapped()
    }
    
    @IBAction func notification(_ sender: UIButton) {
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    
    //MARK:- Web API Recent order
    fileprivate func callAPIToGetRecentOrder() {
        let paraDict = NSMutableDictionary()
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "seller/orders/getMyRecentOrders") { (response, error , message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                     let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                     if self.navigationController?.viewControllers.count ?? 0 > 0 {
                     
                     if ((self.navigationController?.viewControllers.last) != nil) {
                     self.navigationController?.popToRootViewController(animated: false)
                     } else {
                     self.navigationController?.viewControllers.removeAll()
                     }
                     }
                     let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                     self.navigationController?.viewControllers = [mainView]
                     let obj = AppDelegate()
                     obj.window?.rootViewController = self.navigationController
                     obj.window?.makeKeyAndVisible()
                     }
                     alertController.addAction(action1)
                     self.present(alertController, animated: true, completion: nil)
                     */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                //self.clearAllData()
            } else {
                
                //self.clearAllData()
                let JSON = response as! NSDictionary
                guard let data = JSON["data"] as? NSArray else {
                    return
                }
                guard let meta = JSON["meta"] as? NSDictionary else {
                    return
                }
                
                self.setDataOnCollectionView(listArray: data)
                let notificationCount = meta["notificationCount"] as? Int ?? 0
                
                if notificationCount != 0 {
                    //self.notificationCount.setTitle("\(notificationCount)", for: .normal)
                    self.notificationLbl.text = "\(notificationCount)"
                }else {
                        self.notificationLbl.text = ""
                    }
            }
        }
    }
    
    fileprivate func setDataOnCollectionView(listArray: NSArray) {
        recentOrderList = []
        for recentOrder in listArray {
            let cartDict = recentOrder as! NSDictionary
            
            let cartInfo = RecentOrdersInfo()
            cartInfo.orderId = cartDict["id"] as? Int ?? -1
            cartInfo.orderNumber = cartDict["order_number"] as? String ?? ""
            cartInfo.orderAmount = cartDict["order_amount"] as? String ?? ""
            cartInfo.currency = cartDict["currency"] as? String ?? ""
            cartInfo.buyerName = cartDict["buyer_name"] as? String ?? ""
            cartInfo.totalProducts = cartDict["total_products"] as? Int ?? 1
            cartInfo.buyerContactNumber = cartDict["buyer_contact_number"] as? String ?? ""
            cartInfo.orderStatus = cartDict["status"] as? String ?? ""
            cartInfo.orderDateTime = cartDict["date_time"] as? String ?? ""
            cartInfo.orderTimeAgo = cartDict["time_ago"] as? String ?? ""
            cartInfo.timestamp = cartDict["timestamp_available_upto"] as? String ?? ""
            cartInfo.orderAmountWithCurrency = cartInfo.currency + " " + cartInfo.orderAmount
            self.recentOrderList.append(cartInfo)
        }
        self.collectionView.reloadData()
    }
    
    //MARK:- Web API check status
    fileprivate func callAPIToCheckStatus() {
        let paramDict = NSMutableDictionary()
        
        //indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paramDict as! [String : Any], method: .post, apiName: "seller/status/isApproved") { (response, error,message, statusCode) in
            
            if message != nil{
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /* let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                     let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                     if self.navigationController?.viewControllers.count ?? 0 > 0 {
                     
                     if ((self.navigationController?.viewControllers.last) != nil) {
                     self.navigationController?.popToRootViewController(animated: false)
                     } else {
                     self.navigationController?.viewControllers.removeAll()
                     }
                     }
                     let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                     self.navigationController?.viewControllers = [mainView]
                     let obj = AppDelegate()
                     obj.window?.rootViewController = self.navigationController
                     obj.window?.makeKeyAndVisible()
                     }
                     alertController.addAction(action1)
                     self.present(alertController, animated: true, completion: nil)
                     */
                    
                } else {
                    let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.callAPIToCheckStatus()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    clearData()
                }
                
                
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.callAPIToCheckStatus()
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                clearData()
                return
            } else {
                
                
                
                guard let data = response?["data"] as? NSDictionary else {
                    return
                }
                let isApproved = data["is_storeApproved"] as? Int == 0 ? false : true
                
                USER_DEFAULTS.set(isApproved, forKey: IS_SELLER_APPROVED)
                
                if isApproved == false {
                    clearData()
                    
                    self.urlString = data["url"] as? String ?? ""
                    var messageString = data["message"] as? String// "See Less"
                    if self.urlString == "" {
                        self.messageLabel.text = messageString
                        
                    } else {
                        messageString = "\(messageString ?? "") Click here"
                        
                        let messageAttributedString = NSMutableAttributedString(string:messageString ?? "")
                        
                        messageAttributedString.setAsColor(textToFind: "Click here")
                        self.messageLabel.attributedText = messageAttributedString
                    }
                    self.tittleLabel.text = data["title"] as? String
                } else {
                    self.hideCongratsView()
                }
            }
        }
        
        //Clear Pervious data
        func clearData(){
            self.messageLabel.text = ""
            self.tittleLabel.text = ""
        }
    }
    
    @IBAction func checkStatusButtonDidTapped(_ sender: UIButton) {
        
        callAPIToCheckStatus()
        
    }
    
    //    fileprivate func shadow(_ color:UIColor) {
    //        self.layer.shadowColor = color.cgColor;
    //        self.layer.shadowOpacity = 1
    //        self.layer.shadowRadius = 10
    //        self.layer.masksToBounds = false
    //        self.layer.shadowOffset = CGSize(width: 3, height: 3)
    //    }
    
}

extension recentOrderViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recentOrderList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sellerRecentOrderCVC", for: indexPath) as! sellerRecentOrderCVC
        cell.contentView.layer.addborder(width: 2, color: UIColor.init(red: 235/255, green: 235/255, blue: 235/255, alpha: 0.9))
        cell.shadow3sideApply(shadowRadius: 2)
        
        var recentOrderInformation = RecentOrdersInfo()
        recentOrderInformation = recentOrderList[indexPath.item]
        
        cell.orderNoLbl.text = recentOrderInformation.orderNumber
        cell.buyerNameLbl.text = recentOrderInformation.buyerName
        cell.statusLbl.text = recentOrderInformation.orderStatus
        cell.noOfItemLbl.text = "\(recentOrderInformation.totalProducts)"
        cell.amountLbl.text = recentOrderInformation.orderAmountWithCurrency
        
        //cell.contentView.shadowApply(shadowRadius: 5)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width ) , height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nVC = UIStoryboard(name:"Main" , bundle:nil).instantiateViewController(withIdentifier: "sellerOrderDetailVC") as! sellerOrderDetailVC
        var recentOrderInformation = RecentOrdersInfo()
        recentOrderInformation = recentOrderList[indexPath.item]
        nVC.orderNumber = recentOrderInformation.orderNumber
        nVC.orderID = recentOrderInformation.orderId
        nVC.originalTimestamp = recentOrderInformation.timestamp
        
        self.navigationController?.pushViewController(nVC, animated: true)
    }
}

extension recentOrderViewController: openSelectedViewControllerDelegate {
    func openSelectedViewCOntroller(selectedView: String) {
        switch selectedView {
        case "Profile":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "My Account":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "myAccountViewController") as! myAccountViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Orders":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "MyOrderViewController") as! MyOrderViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "My Favourite":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "MyFavouritesViewController") as! MyFavouritesViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Payment":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
            nVC.isFromSideMenu = true
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Notifications":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "About us":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Contact us":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            
            break
        case "FAQ":
            break
        case "Privacy Policy":
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(nVC, animated: true)
            break
        case "Rate the app":
            break
        case "Share":
            break
        case "Logout":
            self.logoutButtonDidTapped()
            break
        default:
            break
        }
    }
    
    func logoutButtonDidTapped() {
        
        let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            kAppDelegate.logoutAPI(viewController: self)
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
               // print ("Signout")
            } catch let signOutError as NSError {
                //print ("Error signing out: %@", signOutError)
            }
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
        /*ConveienceClass.logoutAlert(controller: self) { (sucess) in
         if sucess ?? false {
         
         if self.navigationController?.viewControllers.count ?? 0 > 0 {
         
         if ((self.navigationController?.viewControllers.last) != nil) {
         self.navigationController?.popToRootViewController(animated: false)
         } else {
         self.navigationController?.viewControllers.removeAll()
         }
         }
         let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
         self.navigationController?.viewControllers = [mainView]
         let obj = AppDelegate()
         obj.window?.rootViewController = self.navigationController
         obj.window?.makeKeyAndVisible()
         
         }
         }*/
    }
}
