//
//  sellerOrderDetailVC.swift
//  Bean2Go
//
//  Created by AKS on 15/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import GoogleMaps
import Crashlytics


struct SelleTotalAmountStruct {
    var totalBaseAmount: String?
    var tax: String?
    var totalPriceOfProductIncludingTax: String?
}

class sellerOrderDetailVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var ingredeintView: UIView!
    @IBOutlet weak var viewDetailView: UIView!
    @IBOutlet weak var superCommentStackView: UIStackView!
    @IBOutlet weak var ingredientViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewHeightConstraints: NSLayoutConstraint!
    
    //New
    @IBOutlet weak var reasontoRejectTxtView: UITextView!
    @IBOutlet weak var usercommentTextview: UITextView!
    @IBOutlet weak var rejectSuperView: UIView!
    @IBOutlet weak var rejectMainView: UIView!
    @IBOutlet weak var ButtonFooterView: UIView!
    @IBOutlet weak var usercommentHeightconstraint: NSLayoutConstraint!
    @IBOutlet weak var verifyOtpView: UIView!
    @IBOutlet weak var digit1TextView: UITextField!
    @IBOutlet weak var digit2TextView: UITextField!
    @IBOutlet weak var digit3TextView: UITextField!
    @IBOutlet weak var digit4TextView: UITextField!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var viewDetailBackBtn: UIButton!
    
    var locationManager = CLLocationManager()
    var coordinates = CLLocation()
    //End

   let toolbar = UIToolbar()
    
    var otpHeightMax: CGFloat = 65
    var timer = Timer()
    var seconds = 120
    
    var orderNumber: String?
    var orderID: Int = 0
    var notificationId = String()
    
    var productDetail = ProductSellerDetailInfo()
    var subtotal = 0
    var userType:String?
    var originalTimestamp = ""
    var currentDate = Date()
    
    let minHeight: CGFloat = 50.0
    let maxHeight: CGFloat = 150.0
    
    var remarkMaxheight:CGFloat = 100.0
    var remarkMinheight:CGFloat = 30.0
    var remarkHeight:CGFloat = 30.0
    var oneTimeAutoReject: Bool = false
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set S
        self.verifyOtpView.shadow(UIColor.lightGray)
        
        self.digit1TextView.addToolBar()
        self.digit2TextView.addToolBar()
        self.digit3TextView.addToolBar()
        self.digit4TextView.addToolBar()
        
        userType = USER_DEFAULTS.value(forKey: USER_TYPE) as? String
        
        self.usercommentTextview.layer.addborder(width: 2, color: UIColor.lightGray)
        self.usercommentTextview.layer.cornerRadius = 5
        self.usercommentTextview.setContentOffset(CGPoint.zero, animated: false)
        usercommentTextview.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        //self.usercommentTextview.isScrollEnabled = false
        
        self.reasontoRejectTxtView.sizeToFit()
        
        // New Code starts
        self.rejectSuperView.isHidden = true
        self.rejectMainView.layer.cornerRadius = 10
        self.reasontoRejectTxtView.layer.addborder(width: 2, color: UIColor.lightGray)
        self.reasontoRejectTxtView.layer.cornerRadius = 5
        
        
//        if isFrom == "myOrder" {
//            self.footerViewHeightConstraints.constant = 0
//            isTimerOn = false
//        } else {
//            self.footerViewHeightConstraints.constant = 50
//            self.ButtonFooterView.topShadow(shadowRadius: 2, color: UIColor.black)
//            //self.billingView.shadowApply(shadowRadius: 10)
//            //convertTimestamp()
//            if convertTimestamp() < 0 {
//                self.footerViewHeightConstraints.constant = 0
//                isTimerOn = false
//            } else {
//                seconds = convertTimestamp()
//                startTimer()
//            }
//        }
        //convertTimestamp()
        callAPItoGetOrderDetail()
    }
        
    // MARK:- Set up Initial Value
   
    
    func isAllVerified() -> Bool{
        
        if digit1TextView.text == ""  || digit2TextView.text == "" || digit3TextView.text == "" || digit4TextView.text == "" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter pin.", buttonTitle: "OK"), animated: true, completion: nil)
            return false
        }else{
            return true
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func enterOtpButtonDidTapped(_ sender: UIButton) {
       sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            //self.otpViewBottomConstraints.constant = 0
            self.verifyOtpView.isHidden = false
            self.digit1TextView.becomeFirstResponder()
//            let defaultValue: CGPoint = self.verifyOtpView.frame.origin
//            UIView.animate(withDuration: TimeInterval(1.2), delay: 0,
//                           usingSpringWithDamping: 0.4,
//                           initialSpringVelocity: 0.8, options: UIView.AnimationOptions.curveEaseIn, animations: {
//                            self.verifyOtpView.frame.origin = defaultValue
//            }, completion: nil)
        } else {
            self.verifyOtpView.isHidden = true
//            self.otpViewBottomConstraints.constant = self.otpHeightMax
        }
    }
    
    @IBAction func closeVerifyView(_ sender: UIButton) {
        self.verifyOtpView.isHidden = true
        
    }
    
    @IBAction func verifyOtpBtnSubmit(_ sender: UIButton) {
        
        let one = digit1TextView.text!
        let two = digit2TextView.text!
        let three = digit3TextView.text!
        let four = digit4TextView.text!
        
        if isAllVerified(){
            let otpStr = "\(one)\(two)\(three)\(four)"
            
            self.verifyOtpAPI(otpString: otpStr)
        }
        
    }
    
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        SocketManagerClass.shared.leaveRoom(key: productDetail.orderNumber ?? "0")
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func locationButtonDidTapped(_ sender: UIButton) {
        let nVC = UIStoryboard(name:"Main" , bundle:nil).instantiateViewController(withIdentifier: "SellerNavigationViewController") as! SellerNavigationViewController
        
//        nVC.destinationLat = Double(self.productDetail.storeLat!)!
//        nVC.destinationLng = Double(self.productDetail.storeLng!)!

        nVC.originLat = Double(self.productDetail.storeLat!)!
        nVC.originLng = Double(self.productDetail.storeLng!)!
        
        nVC.destinationLat = Double(self.productDetail.buyerLocationLat!)!
        nVC.destinationLng = Double(self.productDetail.buyerLocationLng!)!
        
        nVC.orderNumber = self.productDetail.orderNumber ?? ""
//        nVC.destinationName = self.productDetail.storeName ?? "Cafe coffee day."
        //Crashlytics.sharedInstance().crash()

        
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @IBAction func rejectButtonAction(_ sender: UIButton) {
        
        
        let alertController = UIAlertController(title: "", message: "Are you sure, you want reject this order?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            self.rejectSuperView.isHidden = false
        }
        let no = UIAlertAction(title: "No", style: .default) { (UIAlertAction) in
            //self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(yes)
        alertController.addAction(no)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func acceptBtnTapped(_ sender: UIButton) {
        self.footerViewHeightConstraints.constant = 0
        timer.invalidate()
        self.collectionView.reloadData()
        callToAcceptRejectOrder(approveStatus: 1)
        
     
        
    }
    
    
    //New function
    @IBAction func sendRejectReasondidTapped(_ sender: UIButton) {
        if reasontoRejectTxtView.text == "Reason" {
            present(UIAlertController.alertWithTitle(title: "", message: "Please give a reason", buttonTitle: "OK"), animated: true, completion: nil)
        } else {
            callToAcceptRejectOrder(approveStatus: 0)
        }
    }
    
    @IBAction func rejectViewBackButtonAction(_ sender: UIButton) {
        dismissRejectView()
    }
    @IBAction func verifyViewDidTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func viewdidTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.reasontoRejectTxtView.text == "" {
            self.reasontoRejectTxtView.textColor = UIColor.lightGray
            self.reasontoRejectTxtView.text = "Reason"
        }
    }
    
    @IBAction func viewDetailButtonTapped(_ sender: UIButton) {
        self.viewDetailView.isHidden = false;
        self.viewDetailBackBtn.tag = sender.tag
        setIngredientData(atIndex: sender.tag)
    }
    
    @IBAction func viewDetailBackButtonDidTapped(_ sender: UIButton) {
        self.viewDetailView.isHidden = true; 
        self.removeIngredientData(atIndex: sender.tag)
    }
    
    fileprivate func removeIngredientData(atIndex: Int) {
        
        let allViews = self.ingredeintView.subviews
        if allViews.count != 0 {
            for view in allViews {
                if view.isKind(of: cartItemDetailRowClass.self) {
                    view.removeFromSuperview()
                }
            }
        }
        
        superCommentStackView.isHidden = true
        usercommentTextview.layoutIfNeeded()
        
    }
    
    //MARK:-  timer Functions
    
    func startTimer(){
        //let currentTimestamp = currentDate.toMillis()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(sellerOrderDetailVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func convertTimestamp() -> Int {
        //        let hour =
        //        let origin = getDateFromString(dateString: originalTimestamp)
        //        let diff = origin.toMillis() - currentDate.toMillis()
       // originalTimestamp = self.productDetail.timestamp ?? ""
        let backDate = getDateFromString(dateString: originalTimestamp)
        var backMin = (backDate.minute(date: backDate))// - 30)
        let backSec = backDate.sec(date: backDate)
        let backHour = backDate.hour(date: backDate)
        
        
//        if backMin < 0 {
//            backMin = backMin + 60
//        }
        
        let deviceMin = currentDate.minute(date: currentDate)
        let deviceSec = currentDate.sec(date: currentDate)
        let deviceHour = currentDate.hour(date: currentDate)
        
        let diff = (((backHour * 60 * 60) + (backMin * 60) + backSec) - ((deviceHour * 60 * 60) + (deviceMin * 60) + deviceSec))
//        let minute = diff / 60
//        let seconds = diff % 60
//
//        let totalTime = "\(minute):\(seconds)"
        
        return diff
    }
    
    func calculateTime() -> String {
        
        let dateFormatter = DateFormatter()
        
        let userCalendar = Calendar.current
        
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") 
        let startTime = Date()
        
        //let startDateTime = startTime.addingTimeInterval(TimeInterval(5.0 * 60.0))
        
        guard let dateInString = self.productDetail.timestamp else {
            return "Invalidate"
        }
        
        if dateInString == "" {
            return "Invalidate"
        }
        let endTime = dateFormatter.date(from: dateInString)
        
        //let endDateTime = endTime!.addingTimeInterval(TimeInterval(15.5 * 60.0))
        
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime, to: endTime!)
        
        //        var diffrece = "\(timeDifference.month) Months \(timeDifference.day) Days \(timeDifference.minute) Minutes \(timeDifference.second) Seconds"
        
        // if seconds goes to negative then return 0:0
        if ((timeDifference.second)!) < 0 {
            return "0:0"
        }
        // if seconds less than 10 then add 0 before seconds
        if ((timeDifference.second)!) < 10 {
            let stringToShowInMinTime = "\((timeDifference.minute)!):0\((timeDifference.second)!)"
            return stringToShowInMinTime
        }
        
        let stringToShowInMinTime = "\((timeDifference.minute)!):\((timeDifference.second)!)"
        return stringToShowInMinTime
    }
    
    @objc func updateTimer() {
        self.collectionView.reloadData()
    }
    
    // MARK:- Auto Reject
    func invalidateTimeAndCallAutoRejectAPI() {
        timer.invalidate()
        //seconds = 0
        
        if oneTimeAutoReject == false {
            self.autoRejectAPI()
            self.oneTimeAutoReject = true
        }
        
    }
    
    //MARK:- Calculate the total amount of product with tax
    fileprivate func setTotalAmountOfProduct() -> SelleTotalAmountStruct {
        
        var totalAmount = SelleTotalAmountStruct()
        
        let orderAmountInString = self.productDetail.orderAmount as NSString?
        let orderTaxInString = self.productDetail.orderTax as NSString?
        
        let orderAmount = orderAmountInString?.floatValue
        let percentageOfProduct = orderTaxInString?.floatValue
        let percentageAmoutOfProduct = (orderAmount ?? 0) * ((percentageOfProduct ?? 1) / 100)
        
        let percentage2DigitAfterDecimal = String(format: "%.2f", percentageAmoutOfProduct)
        
        let productAmountWithTax = percentageAmoutOfProduct + (orderAmount ?? 0)
        
        totalAmount.tax = self.productDetail.currency + " \(percentage2DigitAfterDecimal)"
        
        if self.productDetail.orderAmount != nil {
            totalAmount.totalBaseAmount = self.productDetail.currency + " \(self.productDetail.orderAmount!)"
        }
        
        totalAmount.totalPriceOfProductIncludingTax = self.productDetail.currency + " \(productAmountWithTax)"
        
        return totalAmount
        
    }
    
    //MARK:- Set ingredient data on view
    fileprivate func setIngredientData(atIndex: Int){
        
        var optionDetailList = productDetail.eachProductInfo[atIndex].subProductList
        self.productNameLabel.text = productDetail.eachProductInfo[atIndex].productName
        
        for i in 0...optionDetailList.count - 1 {
            let ingredientViewObj = cartItemDetailRowClass()
            let ingredintInfo = optionDetailList[i] as IngredientInfo
            
            ingredientViewObj.frame = CGRect(x: 0, y: 25 * i, width: Int(self.ingredeintView.bounds.width), height: 25)
            
            ingredientViewObj.addValue(heading: "\(ingredintInfo.optionType)", value: "\(ingredintInfo.option_name) ")
            
            self.ingredeintView.addSubview(ingredientViewObj)
            
        }
        
        let comment = productDetail.eachProductInfo[atIndex].productComment
        if comment != "" {
            superCommentStackView.isHidden = false
            usercommentTextview.text = comment
            usercommentTextview.isEditable = false
            
            if usercommentTextview.contentSize.height >= maxHeight || usercommentTextview.contentSize.height > minHeight{
                //self.usercommentTextview.isScrollEnabled = true
                self.usercommentHeightconstraint.constant = maxHeight
            }
            else{
                
                self.usercommentHeightconstraint.constant = minHeight
            }
            usercommentTextview.layoutIfNeeded()
        }
        
        // needs to be put in condition
        
        self.ingredientViewHeightConstraint.constant = CGFloat(25*(optionDetailList.count))
        //self.ingredientViewHeightConstraint.constant = CGFloat(25*productOptions.options.count)
        
    }
    
    func dismissRejectView(){
        self.rejectSuperView.isHidden = true
        self.reasontoRejectTxtView.textColor = UIColor.lightGray
        self.reasontoRejectTxtView.text = "Reason"
    }
    
    // MARK:- leave room
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
    }
}

extension sellerOrderDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productDetail.eachProductInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seller_OrderDetailCVC", for: indexPath) as! seller_OrderDetailCVC
        cell.shadow(UIColor.lightGray)
        cell.viewDetailButton.tag = indexPath.row
        let orderInfo: SubProductsInfo = productDetail.eachProductInfo[indexPath.item]
        
        //cell.cardView.bottomShadow(shadowRadius: 10, color: UIColor.black)
        cell.cardView.layer.cornerRadius = 2
        cell.itemImgView.layer.cornerRadius = 10
        cell.itemImgView.clipsToBounds = true
        cell.itemNameLbl.text = orderInfo.productName
        cell.quantityLabel.text = "\(orderInfo.quantityOfProduct)"
        cell.amountLbl.text = productDetail.currency + orderInfo.ammount
        cell.itemImgView.af_setImage(withURL: URL(string: orderInfo.productImage)!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 20, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "hedderView", for: indexPath) as! orderDetailHeaderView
            headerView.shopName.text = productDetail.buyerName
            headerView.timeLbl.text = productDetail.storeDateTime
            headerView.statusLbl.text = productDetail.storeStatus
            headerView.amountLbl.text = productDetail.storePriceWithCurrency
            
            
            if productDetail.isSchedule == true {
                headerView.scheduleFor.isHidden = false
                headerView.scheduleForLbl.text = productDetail.scheduleForDate
            }
            
//            headerView.timmerLbl.text = "\(seconds)"
            
            let orderStatusId = productDetail.orderStatusId
            
            switch orderStatusId {
            case 1: // Order Create
                headerView.timmerView.isHidden = false
                headerView.navigationButton.isHidden = true
                self.footerViewHeightConstraints.constant = 50
                self.ButtonFooterView.topShadow(shadowRadius: 2, color: UIColor.black)
                
                let timeString = self.calculateTime()
                 if timeString == "0:0" {
                    invalidateTimeAndCallAutoRejectAPI()
                    self.footerViewHeightConstraints.constant = 0
                    headerView.timmerView.isHidden = true
                    
                 } else if timeString == "Invalidate" {
                    
                 } else {
                    headerView.timmerLbl.text = timeString
                    startTimer()
                }
                
                break
                
            case 2:// Payment Processing
                break
                
            case 3:// Payment Failed
                break
                
            case 4:// Payment Completed
                headerView.timmerView.isHidden = false
                headerView.navigationButton.isHidden = true
                self.footerViewHeightConstraints.constant = 50
                self.ButtonFooterView.topShadow(shadowRadius: 2, color: UIColor.black)
                
                let timeString = self.calculateTime()
                if timeString == "0:0" {
                    invalidateTimeAndCallAutoRejectAPI()
                    self.footerViewHeightConstraints.constant = 0
                    headerView.timmerView.isHidden = true
                    
                } else if timeString == "Invalidate" {
                    
                } else {
                    headerView.timmerLbl.text = timeString
                    startTimer()
                }
                
                break
            case 5:// Order Reject
                headerView.timmerView.isHidden = true
                self.footerViewHeightConstraints.constant = 0
                headerView.navigationButton.isHidden = true
                break
            case 6:// Order Accept
                headerView.timmerView.isHidden = true
                self.footerViewHeightConstraints.constant = 0
                headerView.navigationButton.isHidden = false
                
                headerView.enterPinButtonView.isHidden = false
                headerView.enterPinButtonView.layer.borderColor = (UIColor.white).cgColor
                headerView.enterPinButtonView.layer.borderWidth = 2
                headerView.enterPinButtonView.layer.cornerRadius = 5
                
                
                break
            case 7: // Auto Declined
                headerView.timmerView.isHidden = true
                self.footerViewHeightConstraints.constant = 0
                headerView.navigationButton.isHidden = true
                break
            case 8:// Cancelled
                headerView.timmerView.isHidden = true
                headerView.remarkStackview.isHidden = false
                headerView.remarkTxtView.text = self.productDetail.cancelReason
                
                if headerView.remarkTxtView.contentSize.height > maxHeight {
                    remarkHeight = remarkMaxheight
                    headerView.remarkTxtViewHeightConstraint.constant = remarkMaxheight
                    headerView.remarkTxtView.isUserInteractionEnabled = true
                } else {
                    remarkHeight = remarkMinheight
                    headerView.remarkTxtViewHeightConstraint.constant = remarkMinheight
                    headerView.remarkTxtView.isUserInteractionEnabled = false   // isscrollenable moves the textview content a bit toward bottom
                }
                self.collectionView.collectionViewLayout.invalidateLayout()
                self.footerViewHeightConstraints.constant = 0
                headerView.navigationButton.isHidden = true
                break
            case 9:// recieved
                headerView.timmerView.isHidden = true
                self.footerViewHeightConstraints.constant = 0
                headerView.navigationButton.isHidden = true
                break
            default:
                break
                
            }
            
            return headerView
            
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footerView", for: indexPath) as! sellerOrderDetailFooter
            
            footerView.bottomView.shadow(UIColor.lightGray)
            
//            let totalAmountStructObj = self.setTotalAmountOfProduct()
//            footerView.shadowApply(shadowRadius: 2)
            footerView.subTotalAmountLbl.isHidden = true
            footerView.taxAmountLbl.isHidden = true
//            footerView.subTotalAmountLbl.text = totalAmountStructObj.totalBaseAmount
//            footerView.taxAmountLbl.text = totalAmountStructObj.tax
//            footerView.totalAmountLbl.text = totalAmountStructObj.totalBaseAmount
            footerView.totalAmountLbl.text = self.productDetail.orderAmount
            //
            return footerView
            
        default:
            
            assert(false, "Unexpected element kind")
            return UICollectionReusableView()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        // ofSize should be the same size of the headerView's label size:
        return CGSize(width: collectionView.frame.size.width, height: 142)
    }
    
    
}

//MARK:- CALL WEB API
extension sellerOrderDetailVC {
    func callAPItoGetOrderDetail() {
        
        let paraDict = NSMutableDictionary()
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "seller/orders/details/\(self.orderNumber!)/\(self.orderID)?notiId=\(notificationId)") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    //self.collectionView.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                //self.collectionView.isHidden = true
                return
            } else {
                
                self.collectionView.isHidden = false
                
                guard let productInfo = response?["data"] as? NSDictionary else {
                    return
                }
                
                guard let metaData = response?["meta"] as? NSDictionary else {
                    return
                }
                
                let notificationCount = metaData["notificationCount"] as? String ?? "0"
                
                
                self.productDetail.buyerName = productInfo["buyer_name"] as? String ?? ""
                
                self.productDetail.currency = productInfo["currency"] as? String ?? "R"
                self.productDetail.storeDateTime = productInfo["date_time"] as? String ?? "0.0"
                self.productDetail.id = productInfo["id"] as? Int ?? 0
                self.productDetail.orderAmount = productInfo["order_amount"] as? String ?? ""
                
                self.productDetail.orderNumber = productInfo["order_number"] as? String ?? ""
                self.productDetail.storeStatus = productInfo["status"] as? String ?? "Waiting for approval."
                self.productDetail.cancelReason = productInfo["cancel_reason"] as? String ?? ""
                
                self.productDetail.orderTax = productInfo["tax"] as? String ?? ""
                self.productDetail.orderStatusId = productInfo["status_id"] as? Int ?? 0
                self.productDetail.storePriceWithCurrency = "\(self.productDetail.currency) \(self.productDetail.orderAmount ?? "0")"
                self.productDetail.timestamp = productInfo["timestamp_available_upto"] as? String ?? ""
                self.productDetail.scheduleForDate = productInfo["scheduled_for"] as? String ?? ""
                
                if self.productDetail.scheduleForDate != "" {
                    self.productDetail.isSchedule = true
                }
                
                guard let location = productInfo["location"] as? NSDictionary else {
                    return
                }
                
                guard let buyerLocation = productInfo["buyer_location"] as? NSDictionary else {
                    return
                }
                
                self.productDetail.storeLat = location["lat"] as? String ?? "0.0"
                self.productDetail.storeLng = location["lng"] as? String ?? "0.0"
                
                self.productDetail.buyerLocationLat = buyerLocation["lat"] as? String ?? "0.0"
                self.productDetail.buyerLocationLng = buyerLocation["lng"] as? String ?? "0.0"
                
                let subProductArray = productInfo["order_products"] as? NSArray ?? []
                //self.productDetail?.eachProductInfo = []
                
                // seller join socket
                if self.productDetail.orderStatusId == 4 {
                    self.joinSeller(statusID: self.productDetail.orderStatusId)

                } else if self.productDetail.orderStatusId == 6 {
//                    self.joinSeller(statusID: self.productDetail.orderStatusId)
                    SocketManagerClass.shared.eventsHandleInSocket(roomName: self.productDetail.orderNumber, id: self.productDetail.orderStatusId)
                    
                }
                
                if subProductArray.count != 0 {
                    
                    for order in subProductArray {
                        let dict = order as? NSDictionary
                        let orderDetail = SubProductsInfo()
                        
                        orderDetail.productName = dict?["product_name"] as? String ?? ""
                        orderDetail.ammount = dict?["ammount"] as? String ?? ""
                        orderDetail.quantityOfProduct = dict?["qty"] as? Int ?? 0
                        orderDetail.productComment = dict?["comment"] as? String ?? ""
                        orderDetail.productImage = dict?["product_image"] as? String ?? ""
                        let optionsArray = dict?["options"] as? NSArray ?? []
                        
                        
                        //self.productDetail?.subProductList = []
                        if optionsArray.count != 0 {
                            for option in optionsArray {
                                let optionDict = option as? NSDictionary
                                let optionDetail = IngredientInfo()
                                optionDetail.option_name = optionDict?["option_name"] as? String ?? ""
                                optionDetail.option_price = optionDict?["option_price"] as? String ?? ""
                                optionDetail.optionType = optionDict?["option_type"] as? String ?? ""
                                orderDetail.subProductList.append(optionDetail)
                            }
                        }
                        
                        self.productDetail.eachProductInfo.append(orderDetail)
                    }
                    
                  
                    self.collectionView.reloadData()
                    
                } else {
                    let alertController = UIAlertController(title: "", message: "No detail fround.", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                //self.collectionView.isHidden = true
            }
        }
    }
    
    // MARK:- Seller join Room
    fileprivate func joinSeller(statusID: Int) {
        SocketManagerClass.shared.eventsHandleInSocket(roomName: self.productDetail.orderNumber, id: statusID)
    }
    
    func verifyOtpAPI(otpString: String) {
        
        let paraDict = NSMutableDictionary()
        
        paraDict["order_id"] = self.orderID
        paraDict["order_number"] = orderNumber
        paraDict["otp"] = otpString
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "seller/orders/deliverOrder") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    self.digit1TextView.text = ""
                    self.digit2TextView.text = ""
                    self.digit3TextView.text = ""
                    self.digit4TextView.text = ""
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
               // self.collectionView.isHidden = true
                return
            } else {
                
                self.collectionView.isHidden = false
                
                guard let data = response?["data"] as? NSDictionary else {
                    return
                }
                
                self.joinSeller(statusID: RECEIVED_ID)
                
                let message = data["message"] as? String ?? ""
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.verifyOtpView.isHidden = true
                    self.navigationController?.popViewController(animated: true)
                    //self.callAPItoGetOrderDetail()
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
        
    }
    
    // MARK:- Accept and reject API
    func callToAcceptRejectOrder(approveStatus: Int) {
        
        // hide reject view
       
//        approveStatus == 1 ? self.joinSeller(statusID: ORDER_ACCEPTED_ID) : self.joinSeller(statusID: ORDER_REJECTED_ID)
//        return
        
        let paraDict = NSMutableDictionary()
        
        paraDict["order_id"] = self.orderID
        paraDict["order_number"] = orderNumber
        paraDict["approve"] = approveStatus
        paraDict["reject_reason"] = self.reasontoRejectTxtView.text
        
         self.dismissRejectView()
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "seller/orders/store/status") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    //self.collectionView.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    SocketManagerClass.shared.leaveRoom(key: self.productDetail.orderNumber!)
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                //self.collectionView.isHidden = true
                return
            } else {
                
                self.collectionView.isHidden = false
                
                guard let data = response?["data"] as? NSDictionary else {
                    return
                }
                
                let message = data["message"] as? String ?? ""
                
                guard let metaData = response?["meta"] as? NSDictionary else {
                    return
                }
                
                approveStatus == 1 ? self.joinSeller(statusID: ORDER_ACCEPTED_ID) : self.joinSeller(statusID: ORDER_REJECTED_ID)

                
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                     SocketManagerClass.shared.leaveRoom(key: self.productDetail.orderNumber!)
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                
                
                
                
                
//                self.present(UIAlertController.alertWithTitle(title: "", message: message , buttonTitle: "OK"), animated: true, completion: nil)
//
//                self.collectionView.reloadData() //untested
//                self.dismissRejectView()
//                self.footerViewHeightConstraints.constant = 0
            }
        }
        
    }
    
    // MARK:- Auto reject API
    fileprivate func autoRejectAPI() {
        
        let paraDict = NSMutableDictionary()
        paraDict["order_number"] = self.productDetail.orderNumber
        paraDict["order_id"] = self.productDetail.id
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "seller/orders/autoReject") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    self.collectionView.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                    SocketManagerClass.shared.leaveRoom(key: self.productDetail.orderNumber!)
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                //self.collectionView.isHidden = true
                return
            } else {
                
                guard let dataInfo = response?["data"] as? NSDictionary else {
                    return
                }
                
                guard let msg = dataInfo["message"] as? String else {
                    return
                }
                
                self.joinSeller(statusID: AUTO_DECLINED_ID)
                
                self.showToast(message: msg)
                
//                let alertController = UIAlertController(title: "Time Up !!", message: msg, preferredStyle: .alert)
//                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    SocketManagerClass.shared.leaveRoom(key: self.productDetail.orderNumber!)
                    self.navigationController?.popViewController(animated: true)
//                }
//                alertController.addAction(action1)
//                self.present(alertController, animated: true, completion: nil)
               
                
            }
        }
    }
}

extension sellerOrderDetailVC: UITextFieldDelegate {
    //MARK:- TextField functions
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        return true
    }
    
}


extension sellerOrderDetailVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.reasontoRejectTxtView.text = ""
        self.reasontoRejectTxtView.textColor = UIColor.black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            self.reasontoRejectTxtView.textColor = UIColor.lightGray
            self.reasontoRejectTxtView.text = "Reason"
        }
    }
    
    //Needs To Be Upgraded
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.text = ""
    }
    
    @IBAction func textFieldDidChanged(_ sender: UITextField) {
        
        let text = sender.text
        
        if (text?.utf16.count)! >= 1 {
            switch sender {
            case digit1TextView:
                digit2TextView.becomeFirstResponder()
                break
                
            case digit2TextView:
                digit3TextView.becomeFirstResponder()
                break
                
            case digit3TextView:
                digit4TextView.becomeFirstResponder()
                break
                
            case digit4TextView:
                digit4TextView.resignFirstResponder()
                break
            default:
                sender.resignFirstResponder()
                break
            }
        }
        
    }
    
    @objc func donedatePicker(){
        self.toolbar.removeFromSuperview()
    }
    
    @objc func cancelDatePicker(){
        self.toolbar.removeFromSuperview()
    }
    
}


