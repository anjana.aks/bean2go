//
//  SellerNavigationViewController.swift
//  Bean2Go
//
//  Created by Anjana Aks on 13/03/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import GoogleMaps
import Crashlytics


class SellerNavigationViewController: UIViewController, ARCarMovementDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var distanceLbl: UILabel!
    
    var moveMent: ARCarMovement!
    
    
    var didFindMyLocation = false
    var zoomLevel: Float = 14.5
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    
    var originLat: Double = 0.0
    var originLng: Double = 0.0
    
    var destinationLat: Double = 0.0
    var destinationLng: Double = 0.0
    
    var selectedRoute: Dictionary<String, AnyObject> = Dictionary()
    var overviewPolyline: Dictionary<String, AnyObject> = Dictionary()
    
    var path: GMSPath = GMSPath()
    var routePolyline: GMSPolyline = GMSPolyline()
    
    var orderNumber = String()
    
    var oldCoordinate = CLLocationCoordinate2D()
    var newCoordinate = CLLocationCoordinate2D()
    var new_driverMarker = GMSMarker()
    var iTemp:Int = 0
    var marker = GMSMarker()
    var oldPolylineArr = [GMSPolyline]()
    var arrayOfPath = [GMSPath]()
    var timer = Timer()
    
    var counter: Int = 0
    var CoordinateArr = [Float]()
    
    var isNewCoordinate: Bool = false
    var isFirstTime: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moveMent = ARCarMovement()
        moveMent.delegate = self
        isNewCoordinate = false
        
        self.bottomView.shadow(UIColor.lightGray)
        
        /*from File
         var filePath = Bundle.main.path(forResource: "coordinates", ofType: "json")
         var jsonData = NSData(contentsOfFile: filePath ?? "") as Data?
         if let jsonData = jsonData, let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [Any] {
         self.CoordinateArr = json
         }
         
         //set old coordinate
         //
         oldCoordinate = CLLocationCoordinate2DMake(originLat, originLng)
         
         */
        self.setUpMapViewInitial()
        
        let marker = GMSMarker()
        let position = CLLocationCoordinate2DMake(originLat,originLng)
        marker.position = position
        marker.map = googleMapView
        
        
        self.getDirections(origin: "\(self.originLat),\(self.originLng)", destination:
        "\(destinationLat),\(destinationLng)", waypoints: nil, travelMode: "driving" as AnyObject) { [unowned self](status, success, distance) -> Bool in
            if success! {
                
                
                self.oldCoordinate = CLLocationCoordinate2DMake(self.destinationLat,self.destinationLng)
                // Creates a marker in the center of the map.
                
                self.new_driverMarker.position = self.oldCoordinate
                self.new_driverMarker.icon = UIImage(named: "tracking_icon") //"carIcon")
                
                self.new_driverMarker.map = self.googleMapView
                
                let updatedCamera = GMSCameraUpdate.setTarget(self.new_driverMarker.position, zoom: self.zoomLevel) //self.mapView.camera.zoom
                self.googleMapView.moveCamera(updatedCamera)
                self.googleMapView.animate(with: updatedCamera)
                
                self.counter  = self.counter + 1
                
                
                self.clearRoute()
                self.drawRoute()
                return false
            }
            else {
                //isReturn = true
                return true
                // print(status!)
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SocketManagerClass.shared.eventsHandleInSocket(roomName: orderNumber, id: 6)
        SocketManagerClass.shared.socketDelegate = self as getDataArrayOfLatLng
    }
    
    
    func setUpMapViewInitial() {
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: Double(originLat), longitude: Double(originLng), zoom: zoomLevel)
        
        self.googleMapView.delegate = self
        self.googleMapView.mapType = .normal
        self.googleMapView.camera = camera
        
        self.new_driverMarker.icon = UIImage(named: "tracking_icon")
        new_driverMarker.position = self.oldCoordinate;
        self.new_driverMarker.map = googleMapView
    }
    
    // Mark:- Button Click
    @IBAction func backButtonDidTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- clearRoute()
    private func clearRoute() {
        
        if oldPolylineArr.count != 0 {
            for p in (0 ..< oldPolylineArr.count) {
                oldPolylineArr[p].map = nil
            }
        }
        
        if self.arrayOfPath.count != 0 {
            arrayOfPath = []
        }
        
    }
    
    // MARK:- Draw Route
    private func drawRoute() {
        //showToast(message: "new path found")
        
        let route = self.overviewPolyline["points"] as! String
        self.path = GMSPath(fromEncodedPath: route)!
        
        let path = GMSPath(fromEncodedPath: route)!
        let routePolyline = GMSPolyline(path: path)
        routePolyline.map = self.googleMapView
        routePolyline.strokeWidth = 5
        routePolyline.strokeColor = UIColor.darkGray
        routePolyline.geodesic = true
        self.arrayOfPath.append(path)
        self.oldPolylineArr.append(routePolyline)
    }
}

// MARK:- Get Direction API
extension SellerNavigationViewController {
    //MARK:- SET MAp Initial
    
    // MARK:- Direction API and draw route
    func getDirections(origin: String!, destination: String!, waypoints: Array<String>!, travelMode: AnyObject!, completionHandler: @escaping ((_ status: String?, _ success: Bool?, _ distance: String?) -> Bool)) {
        if origin != nil {
            if destination != nil {
                // var directionsURLString = baseURLDirections + "origin=" + origin + "&destination=" + destination
                //"&mode=walking"
                
                let directionsURLString = "\(baseURLDirections)origin=\(origin!)&destination=\(destination!)&mode=driving&sensor=false&mode=car&key=AIzaSyD71EOMcYPgpUlI6qOe7LtWm6PC3hc_Rac" //&units=metric
                
                print(directionsURLString)
                directionsURLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                
                if let directionsURL = URL(string: directionsURLString){
                    
                    if Thread.isMainThread {
                        
                        calculateDistance(directionsURL, completionHandler)
                    } else {
                        DispatchQueue.main.sync {
                            calculateDistance(directionsURL, completionHandler)
                        }
                    }
                }
                
            }
            else {
                _ = completionHandler("Destination is nil.", false, "")
            }
        }
        else {
            _ = completionHandler("Origin is nil", false,"")
        }
    }
    
    //MARK:- getDirections()
    fileprivate func calculateDistance(_ directionsURL: URL, _ completionHandler: ((String?, Bool?, String?) -> Bool)) {
        if let directionsData = NSData(contentsOf: directionsURL){
            do {
                let dictionary = try JSONSerialization.jsonObject(with: directionsData as Data, options: JSONSerialization.ReadingOptions.mutableContainers)
                let status = (dictionary as! Dictionary<String, AnyObject>)["status"] as! String
                
                if status == "OK" {
                    self.selectedRoute = ((dictionary as! Dictionary<String, AnyObject>)["routes"] as! Array<Dictionary<NSObject, AnyObject>>)[0] as! Dictionary<String, AnyObject>
                    self.overviewPolyline = (self.selectedRoute)["overview_polyline"] as! Dictionary<NSObject, AnyObject> as! Dictionary<String, AnyObject>
                    
                    let legs = (selectedRoute)["legs"] as! Array<Dictionary<String, AnyObject>>
                    
                    var totalDistanceInMeters: UInt = 0
                    var totalDurationInSeconds: UInt = 0
                    
                    
                    for leg in legs {
                        totalDistanceInMeters += (leg["distance"] as! Dictionary<String, AnyObject>)["value"] as! UInt
                        totalDurationInSeconds += (leg["duration"] as! Dictionary<String, AnyObject>)["value"] as! UInt
                    }
                    
                    let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
                    let totalDistance = "\(distanceInKilometers) Km"
                    
                    let mins = totalDurationInSeconds / 60
                    let totalDuration = "\(mins) min"
                    //                    let hours = mins / 60
                    //                    let days = hours / 24
                    //                    let remainingHours = hours % 24
                    //                    let remainingMins = mins % 60
                    //                    let remainingSecs = totalDurationInSeconds % 60
                    
                    //   totalDuration = "Duration: \(days) d, \(remainingHours) h, \(remainingMins) mins, \(remainingSecs) secs"
                    
                    
                    //                    print("totalDistance   ******\(totalDistance)")
                    //                    print("totalDistance   ******\(totalDurationInSeconds)")
                    
                    distanceLbl.text = totalDistance
                    timeLbl.text = totalDuration
                    
                    /* Anjana Changes start
                     DispatchQueue.main.async(execute: {
                     let route = self.overviewPolyline["points"] as! String
                     
                     //print("points:",route)
                     
                     self.path = GMSPath(fromEncodedPath: route)!
                     self.routePolyline = GMSPolyline(path: self.path)
                     
                     self.routePolyline.map = self.googleMapView
                     self.routePolyline.strokeWidth = 5
                     self.routePolyline.strokeColor = UIColor.darkGray
                     self.routePolyline.geodesic = true
                     })
                     */
                    
                    _ = completionHandler(status, true, totalDistance)
                }
                else {
                    _ = completionHandler(status, false, "")
                }
            }
            catch {
                print(error)
                _ = completionHandler("", false, "")
            }
        }
    }
}

// MARK:- Delegate to get the latitude and longitude
extension SellerNavigationViewController: getDataArrayOfLatLng {
    
    fileprivate func drawPolyLineOneTime(_ newCoordinate: CLLocationCoordinate2D) {
        // if !isFirstTime {
        //isFirstTime = true
        
        if GMSGeometryIsLocationOnPathTolerance(newCoordinate, self.arrayOfPath.last ?? GMSPath(), false, 4) {
            //showToast(message: "same path")
            //            KSToastView.ks_showToast("YES: you are in this polyline with newCoordinate:- \(newCoordinate)", duration: 3.0)
        } else {
            
            self.getDirections(origin: "\(newCoordinate.latitude),\(newCoordinate.longitude)", destination:
            "\(self.originLat),\(self.originLng)", waypoints: nil, travelMode: "driving" as AnyObject) { [unowned self](status, success, distance) -> Bool in
                if success! {
                    //                storeInfo.storeDistance = distance
                    //                self.placesArray[index] = storeInfo
                    //                marker.tracksInfoWindowChanges = true
                    //isReturn = false
                    self.clearRoute()
                    self.drawRoute()
                    
                    return false
                }
                else {
                    //isReturn = true
                    return true
                }
            }
        }
        
        //}
    }
    
    func getLatLngFromSocket(latLongArray: [Any]) {
        
        for location in latLongArray {
            let currentLocation = location as! NSDictionary
            
            let latitude = currentLocation["lat"] as? String ?? ""
            let longitude = currentLocation["lng"] as? String ?? ""
            
            if latitude == "" && longitude == "" {
                return
            }
            
            let MomentaryLatitude = (latitude as NSString).doubleValue
            let MomentaryLongitude = (longitude as NSString).doubleValue
            
            if self.counter == 0 {
                
                self.oldCoordinate = CLLocationCoordinate2DMake(MomentaryLatitude,MomentaryLongitude)
                // Creates a marker in the center of the map.
                
                self.new_driverMarker.position = self.oldCoordinate
                self.new_driverMarker.icon = UIImage(named: "tracking_icon") //"carIcon")
                
                self.new_driverMarker.map = self.googleMapView
                
                let updatedCamera = GMSCameraUpdate.setTarget(self.new_driverMarker.position, zoom: self.zoomLevel) //self.mapView.camera.zoom
                self.googleMapView.moveCamera(updatedCamera)
                self.googleMapView.animate(with: updatedCamera)
                
                self.counter  = self.counter + 1
            }
            else {
                
                if self.oldCoordinate.latitude == 0 && self.oldCoordinate.longitude == 0 {
                    self.oldCoordinate = CLLocationCoordinate2DMake(MomentaryLatitude,MomentaryLongitude)
                }
                
                self.newCoordinate = CLLocationCoordinate2DMake(MomentaryLatitude,MomentaryLongitude)
                /*self.drawPolyLineOneTime(newCoordinate)
                 
                 /**
                 *  You need to pass the created/updating marker, old & new coordinate, mapView and bearing value from backend
                 *  to turn properly. Here coordinates json files is used without new bearing value. So that
                 *  bearing won't work as expected.
                 */
                 
                 self.moveMent.arCarMovement(marker: self.new_driverMarker, oldCoordinate: self.oldCoordinate, newCoordinate: self.newCoordinate, mapView: self.googleMapView, bearing: 0)
                 //instead value 0, pass latest bearing value from backend
                 self.oldCoordinate = self.newCoordinate*/
                
                // Start like chetan
                let newCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(MomentaryLatitude, MomentaryLongitude)
                
                self.drawPolyLineOneTime(newCoordinate)
                let updatedCamera = GMSCameraUpdate.setTarget(newCoordinate, zoom: self.zoomLevel) //self.mapView.camera.zoom)
                self.googleMapView.moveCamera(updatedCamera)
                self.googleMapView.animate(to: GMSCameraPosition.camera(withLatitude: MomentaryLatitude, longitude:MomentaryLongitude, zoom: zoomLevel))
                
                self.moveMent.arCarMovement(self.new_driverMarker, withOldCoordinate: self.oldCoordinate, andNewCoordinate: newCoordinate, inMapview: self.googleMapView, withBearing: 0)
                
                //                 self.moveMent.arCarMovement(marker: self.new_driverMarker, oldCoordinate: self.oldCoordinate, newCoordinate: newCoordinate, mapView: self.googleMapView, bearing: 0)
                
                self.oldCoordinate = newCoordinate
                self.new_driverMarker.map = self.googleMapView
            }
            
            
            
            //=========>> Redraw polyline if location is changed(Start) // Rerouting <<===================
            
            //            if GMSGeometryContainsLocation(newCoordinate, path, false) {
            //                //            KSToastView.ks_showToast("YES: you are in this polyline with newCoordinate:- \(newCoordinate)", duration: 3.0)
            //            } else {
            //                self.getDirections(origin: "\(coordinates.coordinate.latitude),\(coordinates.coordinate.longitude)", destination:
            //                    "\(destinationLat),\(destinationLng)",
            //                    waypoints: nil, travelMode: "" as AnyObject, completionHandler: { (status, success) -> Void in
            //                        if success! {
            //                            //self.mapView.clear()
            //                            self.clearRoute()
            //                            self.drawRoute()
            //                            print("\(self.totalDistance + "\n" + self.totalDuration)")
            //                        }
            //                        else {
            //                            print(status!)
            //                            //KSToastView.ks_showToast("Message by Google : \(status!)", duration: 4.0)
            //                            if status! == "ZERO_RESULTS" {
            //                                //                                    KSToastView.ks_showToast("Google Returning ZERO RESULTS", duration: 2.0)
            //                            }
            //                            else if status! == "OVER_QUERY_LIMIT" {
            //                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            //                                    self.getDirections(origin: "\(self.coordinates.coordinate.latitude),\(self.coordinates.coordinate.longitude)", destination:
            //                                        "\(destinationLat),\(destinationLng)",
            //                                        waypoints: nil, travelMode: "" as AnyObject, completionHandler: { (status, success) -> Void in
            //                                            if success! {
            //                                                self.drawRoute()
            //                                                print("\(self.totalDistance + "\n" + self.totalDuration)")
            //                                            }
            //                                    })
            //                                }
            //                                //                                    KSToastView.ks_showToast("Message by Google : \(status!)\nThat's why path is not drawn", duration: 2.0)
            //                            }
            //                        }
            //                })
            //                //            KSToastView.ks_showToast("YES: you are out of this polyline:- \(coordinates.coordinate.latitude),\(coordinates.coordinate.longitude)", duration: 3.0)
            //            }
            
            //===============>> Redraw polyline if location is changed(End) <<===================
            
        }
    }
    
    // MARK: - ARCarMovementDelegate
    func arCarMovementMoved(_ marker: GMSMarker) {
        new_driverMarker = marker
        new_driverMarker.map = self.googleMapView
        
        
    }
}
