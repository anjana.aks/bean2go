//
//  SellerNavigationViewController.swift
//  Bean2Go
//
//  Created by Anjana Aks on 12/02/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import GoogleMaps


class SellerNavigationViewControllerBack: UIViewController {

    @IBOutlet weak var googleMapView: GMSMapView!
    
    
    var moveMent = ARCarMovement()
    var oldCoordinate = CLLocationCoordinate2D()
    var timer: Timer = Timer()
    
    var driverMarker = GMSMarker()
    var zoomLevel: Float = 14.0
    
    var coordinates = CLLocation()
    
    var destinationLat: Double = 0.0
    var destinationLng: Double = 0.0
    
    var originLat: Double = 0.0
    var originLng: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        moveMent.delegate = self
        // Get Latitude and Longitude of reciver
        self.reciveLatLongFromSocket()
        
        
        
    }
    
    // MARK:- Get latitude and longitude
    fileprivate func reciveLatLongFromSocket() {
        //Socket
//        SocketManagerClass.shared.socketDelegate = self as getDataArrayOfLatLng
//        SocketManagerClass.shared.connectSocket()
//        SocketManagerClass.shared.receiveMsg()
    }
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SellerNavigationViewControllerBack: ARCarMovementDelegate {
    func arCarMovementMoved(_ marker: GMSMarker) {
        driverMarker = marker;
        driverMarker.map = self.googleMapView;
        
        //animation to make car icon in center of the mapview
        let updatedCamera = GMSCameraUpdate.setTarget(self.driverMarker.position, zoom: self.zoomLevel) //self.mapView.camera.zoom
        self.googleMapView.animate(with: updatedCamera)
    }
}

// MARK:- Call delegate to move car on map
extension SellerNavigationViewControllerBack: getDataArrayOfLatLng {
    
    func getLatLngFromSocket(latLongArray: [Any]) {
        
        let toastMsgObj = ToastClassView.init(frame: CGRect(x: 0, y: UISCREEN_HEIGHT-200, width: UISCREEN_WIDTH, height: 44))
        toastMsgObj.showToastMsg(toastMsg: "\(latLongArray.count)",viewController: self)
        
        for location in latLongArray {
            
            let currentLocation = location as! NSDictionary
            var latitude = ""
            var longitude = ""
            var angle = ""
            
            for i in 0...currentLocation.allValues.count - 1 {
                let dict = currentLocation.allValues[i] as? NSDictionary
                
                latitude = dict?["lat"]  as? String ?? ""
                longitude = dict?["lng"]  as? String ?? ""
                angle = dict?["angle"]  as? String ?? ""
                
//                print("laitude \(latitude)")
//                print("longitude \(longitude)")
//                print("angle \(angle)")
            }
            
            if latitude == "" || longitude == "" {
                return
            }
            
            
            let MomentaryLatitude = (latitude as NSString).doubleValue
            let MomentaryLongitude = (longitude as NSString).doubleValue
            
            self.oldCoordinate = CLLocationCoordinate2DMake(MomentaryLatitude,MomentaryLongitude)
            // Creates a marker in the center of the map.
            
            self.driverMarker.position = self.oldCoordinate
            self.driverMarker.icon = UIImage(named: "car")
          
            self.driverMarker.map = self.googleMapView
            
            self.driverMarker.rotation = (angle as NSString).doubleValue
            
            let updatedCamera = GMSCameraUpdate.setTarget(self.driverMarker.position, zoom: self.zoomLevel) //self.mapView.camera.zoom
            self.googleMapView.moveCamera(updatedCamera)
            
            
            UIView.animate(withDuration: 2.0) {
                self.googleMapView.animate(with: updatedCamera)
            }
        }
    }
    
    
}

