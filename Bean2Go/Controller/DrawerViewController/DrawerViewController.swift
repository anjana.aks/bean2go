//
//  DrawerViewController.swift
//  Bean2Go
//
//  Created by AKS on 21/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import StoreKit

protocol openSelectedViewControllerDelegate {
    func openSelectedViewCOntroller(selectedView: String)
}

class DrawerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var emailAddLbl: UILabel!
    @IBOutlet weak var tableViewWidthConstaints: NSLayoutConstraint!
    
    var openSelectedDelegate: openSelectedViewControllerDelegate?
    
    var drawerList = ["Home","My Account","My Orders","My Favourite", "Payment","Rate the app","Share App","Logout"]
    
    var drawerListSeller = ["Recent Order","My Account","My Orders","","","Rate the app","Share App","Logout"]
    
    var iconArray = [String]()
    var username = String()
    var password = String()
    var userDetails = UserDetails()
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialView()
        tableView.dataSource = self as UITableViewDataSource
        tableView.delegate = self as UITableViewDelegate
        
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        iconArray = []
        if userType == SELLER {
            for i in 1...drawerList.count {
                iconArray.append("s_\(i)")
            }
        } else {
            for i in 1...drawerListSeller.count {
                iconArray.append("s_\(i)")
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableViewWidthConstaints.constant = self.view.bounds.width * -100
        self.tableViewWidthConstaints.constant = self.view.bounds.width * 0.75
        //self.view!.layoutIfNeeded()
        self.view.animateViewFromLeftToRight()
    }
    
    override func viewDidLayoutSubviews(){
        
        //tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        
        //tableView.reloadData()
    }
    
    //MARK:- SET UP VIEW
    func setUpInitialView() {
        
        self.userDetails.userName = USER_DEFAULTS.value(forKey: USER_NAME) as? String     
        //self.userDetails.userPassword = USER_DEFAULTS.value(forKey: USER_PASSWORD) as? String
        //self.userDetails.userType = USER_DEFAULTS.value(forKey: USER_TYPE) as? String
        let name: String = USER_DEFAULTS.value(forKey: USER_NAME) as! String
        self.userNameLbl.text = name.uppercased()
        //self.emailAddLbl.text = self.userDetails.userPassword
        
    }
    
    
    //MARK:- HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK:- BUTTON ACTIONS
    
    fileprivate func closeCurrentViewController() {
        //Animate View From Right To Left
        self.tableViewWidthConstaints.constant = self.view.bounds.width * -100
        self.view.animateViewFromRightLeft()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // change 2 to
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func closeViewController(_ sender: UIButton) {
        closeCurrentViewController()
        
    }
    
}

extension DrawerViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        
        if userType == SELLER {
            return drawerListSeller.count
        } else {
            return drawerList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "drawerTVC", for: indexPath) as! drawerTVC
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        if userType == SELLER {
            cell.titleLbl.text = drawerListSeller[indexPath.row]
            cell.iconImgView.image = UIImage(named: iconArray[indexPath.row])
        } else {
            cell.titleLbl.text = drawerList[indexPath.row]
            cell.iconImgView.image = UIImage(named: iconArray[indexPath.row])
        }
        
        //        cell.titleLbl.text = drawerList[indexPath.row]
        //        cell.iconImgView.image = UIImage(named: iconArray[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.closeCurrentViewController()
            
            break
            //        case 1:
            //            self.dismiss(animated: false, completion: nil)
            //            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "Profile")
            //
            //            break
            
        case 1:
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "My Account")
            
            break
        case 2:
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "Orders")
            
            break;
        case 13:
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "Notifications")
            break
        case 3:
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "My Favourite")
            break
        case 4:
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "Payment")
            
            break
            
        case 15:
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "About us")
            
            
            break
        case 16:
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "Contact us")
            
            break
            //        case 8:
            //            self.closeCurrentViewController()
            ////            self.dismiss(animated: false, completion: nil)
            ////            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "About us")
        //            break
        case 17:
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "Privacy Policy")
            
            break
        case 5:
            //  Share APP
            self.closeCurrentViewController()
            
            if #available( iOS 10.3,*){
                SKStoreReviewController.requestReview()
            } else {
                let url = URL(string: APP_LINK)
                SHARED_APP.open(url!, options: [:], completionHandler: nil)
            }
            
            break
            
        case 6:
            // Share APP
            ImageHelper.shareImage(image: UIImage(named: "AppIcon")!, controller: self)
            break
            
        case 7:
            //            let firebaseAuth = Auth.auth()
            //            do {
            //                try firebaseAuth.signOut()
            //                print ("Signout")
            //            } catch let signOutError as NSError {
            //                print ("Error signing out: %@", signOutError)
            //            }
            self.dismiss(animated: false, completion: nil)
            self.openSelectedDelegate?.openSelectedViewCOntroller(selectedView: "Logout")
            break
            
        case 11:
            
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        if userType == SELLER {
            if drawerListSeller[indexPath.row] != "" {
                return 55
                
            }
            return 0
        } else {
            if drawerList[indexPath.row] != "" {
                return 55
            }
            return 0
        }
    }
}


