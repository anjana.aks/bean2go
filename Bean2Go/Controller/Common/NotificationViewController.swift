//
//  NotificationViewController.swift
//  Bean2Go
//
//  Created by Anjana Aks on 10/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var collectionview: UICollectionView!
    
    var currentPageCount: Int = 1
    var footerView:CustomFooterView?
    var isLoading:Bool = false
    let footerViewReuseIdentifier = "RefreshFooterView"
    var notificationList = [notificationInfo]()
    var refreshController = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationList = []
        
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.collectionview.refreshControl = refreshController
        
        self.collectionview.register(UINib(nibName: "CustomFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
        
        callAPItoGetNotificationList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionview.reloadData()
        
    }
    
    //MARK:- Refresh Action
    @objc func refresh(sender:AnyObject) {
        currentPageCount = 1
        notificationList = []
        self.collectionview.reloadData()
        callAPItoGetNotificationList()
        refreshController.endRefreshing()
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func backButtonDidTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
}

//MARK:- CollectionView Datasource and Delegate

extension NotificationViewController: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationList.count
        
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(0.5)
        )
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: NotificationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        let data = notificationList[indexPath.row]
        cell.timeLabel.text = data.notificationTimeAgo
        cell.nameOfRestaurant.text = data.notificationOrderNumber
        cell.notificationStatus.text = data.notificationMsg
        if data.notificationIsRead {
            cell.notiBackView.backgroundColor = UIColorFromRGB(rgbValue: 0xE0E0E0)
        } else {
            cell.notiBackView.backgroundColor = UIColor.white
        }
        cell.shadow3sideApply(shadowRadius: 5)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        
        let noti = notificationList[indexPath.row]
        noti.notificationIsRead = true
        notificationList[indexPath.row] = noti
        
        if userType == BUYER {
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "orderDetailsViewController") as! orderDetailsViewController
            nVC.orderNumber = noti.notificationOrderNumber
            nVC.orderID = noti.notificationOrderId
            nVC.notificationId = noti.notificationId
            self.navigationController?.pushViewController(nVC, animated: true)
            
        } else {
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "sellerOrderDetailVC") as! sellerOrderDetailVC
            nVC.orderNumber = noti.notificationOrderNumber
            nVC.orderID = noti.notificationOrderId
            nVC.notificationId = noti.notificationId
            self.navigationController?.pushViewController(nVC, animated: true)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width - 20, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if isLoading {
            return CGSize.zero
        }
        return CGSize(width: collectionView.bounds.size.width, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
            self.footerView = aFooterView
            self.footerView?.backgroundColor = UIColor.clear
            return aFooterView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
    }
    
    //compute the scroll value and play witht the threshold to get desired effect
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let threshold   = 100.0 ;
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold  = Float((diffHeight - frameHeight))/Float(threshold);
        triggerThreshold   =  min(triggerThreshold, 0.0)
        let pullRatio  = min(abs(triggerThreshold),1.0);
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        if pullRatio >= 1 {
            self.footerView?.animateFinal()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                self.footerView?.stopAnimate()
            }
        } else {
            self.footerView?.stopAnimate()
            self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(0.0))
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.footerView?.stopAnimate()
        }
        
        //print("pullRation:\(pullRatio)")
    }
    
    //compute the offset and call the load method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = abs(diffHeight - frameHeight);
       // print("pullHeight:\(pullHeight)");
        if pullHeight == 0.0
        {
            if !(self.footerView?.isAnimatingFinal)! {
               // print("load more trigger")
                self.isLoading = true
                self.footerView?.startAnimate()
               // Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                    self.callAPItoGetNotificationList()
                    self.collectionview.reloadData()
                    self.isLoading = false
               // })
            }
        } else {
            self.footerView?.stopAnimate()
            self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(0.0))
        }
    }
    
    
}

//MARK:- Web Api
extension NotificationViewController {
    
    
    func callAPItoGetNotificationList() {
        
        let paraDict = NSMutableDictionary()
        
        //paraDict["filter"] = filterId
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "user/getMyNotifications?page=\(self.currentPageCount)") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    self.collectionview.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection error", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                self.collectionview.isHidden = true
                return
            } else {
                guard let orderList = response?["data"] as? NSArray else {
                    return
                }
                
                guard let meta = response?["meta"] as? NSDictionary else {
                    return
                }
                
                guard let currentPage = meta["current_page"] as? Int else {
                    return
                }
                
                guard let lastPage = meta["last_page"] as? Int else {
                    return
                }
                
                if currentPage <= lastPage {
                    self.currentPageCount = self.currentPageCount + 1
                    
                } else {
                    self.currentPageCount = lastPage+1
                    return
                }
                
                if orderList.count != 0 {
                    
                    for order in orderList {
                        let dict = order as? NSDictionary
                        let notiDetail = notificationInfo()
                        notiDetail.notificationId = dict?["notification_id"] as? String ?? ""
                        notiDetail.notificationOrderNumber = dict?["order_number"] as? String ?? ""
                        notiDetail.notificationOrderId = dict?["order_id"] as? Int ?? 0
                        notiDetail.notificationMsg = dict?["message"] as? String ?? "R"
                        notiDetail.notiUserType = dict?["type"] as? String ?? ""
                        notiDetail.notificationTimeAgo = dict?["time_ago"] as? String ?? "Unknown"
                        notiDetail.notificationIsRead = dict?["isRead"] as! Int != 0 ? true : false
                        self.notificationList.append(notiDetail)
                    }
                    self.collectionview.reloadData()
                    
                } else {
                    let alertController = UIAlertController(title: "", message: "No data found.", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        //self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    self.collectionview.isHidden = true
                }
                
                
            }
        }
    }
    
}
