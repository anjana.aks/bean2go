//
//  MyOrderViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 03/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class MyOrderViewController: UIViewController {
    
    var filter_status = false
    var filterId = 0
    var userType = ""
    var localUrl = ""
    var currentPageCount: Int = 1
    var footerView:CustomFooterView?
    var isLoading:Bool = false
    let footerViewReuseIdentifier = "RefreshFooterView"
    var refreshController = UIRefreshControl()
    
    @IBOutlet weak var backBarBtn: UIBarButtonItem!
    @IBOutlet weak var filterBarBtn: UIBarButtonItem!
    @IBOutlet weak var searchBarBtn: UIBarButtonItem!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var notificationLbl: UILabel!
    
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    var orderArrayList = [orderInfo]()
    var filterList = [filterInfo]()

        //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.collectionview.refreshControl = refreshController
        
        orderArrayList = []
        
        if filter_status == false{
            self.filterView.isHidden = true
        }
        
        self.collectionview.register(UINib(nibName: "CustomFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
        
        // MARK:- call API to fetch list
        userType = (USER_DEFAULTS.value(forKey: USER_TYPE) as? String)!
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushOrderDetailPage(notification:)), name: NSNotification.Name(rawValue: "openOrderDetailPage"), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.refreshAllData()
        
    }
    
    @objc func pushOrderDetailPage(notification: NSNotification) {
        
        if let data = notification.userInfo as! NSMutableDictionary?
        {
            for (orderId, orderNumber) in data
            {
              //  print("\(orderId) scored \(orderNumber) points!")
                
                if userType == BUYER {
                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "orderDetailsViewController") as! orderDetailsViewController
                    nVC.orderNumber = orderNumber as? String
                    var orderId = orderId as? String
                   // nVC.orderID = Int(orderId)
                    self.navigationController?.pushViewController(nVC, animated: true)
                } else {
                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "sellerOrderDetailVC") as! sellerOrderDetailVC
                    nVC.orderNumber = orderNumber as? String
                   // nVC.orderID = Int(orderId as? String)
                    self.navigationController?.pushViewController(nVC, animated: true)
                }
                
            }
        }
        
//        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
//        nVC.orderNumber = self.merchantInfo.orderNumber
//        nVC.orderId = self.merchantInfo.orderID
//        self.navigationController?.pushViewController(nVC, animated: true)
        
    }
    
    //MARK:- Refresh Action
    fileprivate func refreshAllData() {
        currentPageCount = 1
        orderArrayList = []
        self.collectionview.reloadData()
        
        userType == BUYER ? self.callAPItoGetBuyerOrderList() : self.callAPItoGetSellerOrderList()
    }
    
    @objc func refresh(sender:AnyObject) {
        refreshAllData()
        refreshController.endRefreshing()
    }
    
    //MARK:- Button Actions
    @IBAction func backBarBtnAction(_ sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterBarBtnAction(_ sender: UIBarButtonItem) {

      //  if orderArrayList.count != 0 {
            if filter_status == false {
                self.filterView.isHidden = false
                filter_status = true
            } else {
                self.filterView.isHidden = true
                filter_status = false
            }
      //  }
//        else {
//            self.filterView.isHidden = true
//            filter_status = false
//        }
    }
    
    @IBAction func notificationBtnAction(_ sender: UIButton) {
        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(nVC, animated: true)
    }
    
    @IBAction func filterBackBtnAction(_ sender: Any) {
        self.filterView.isHidden = true
        filter_status = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "openOrderDetailPage"), object: nil)
        
    }
}

extension MyOrderViewController: UICollectionViewDelegate ,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return orderArrayList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myorderCollectionViewCell", for: indexPath) as! myorderCollectionViewCell
        
        let orderInfo = orderArrayList[indexPath.row]
        
        cell.shadow3sideApply(shadowRadius: 2)
        cell.amountLbl.text = orderInfo.storePriceWithCurrency
        cell.storeNameLbl.text = orderInfo.orderNumber
        cell.statusLbl.text = "\(orderInfo.storeStatus!)"
        cell.dateLbl.text = orderInfo.storeDateTime
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 25, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        
        if userType == BUYER {
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "orderDetailsViewController") as! orderDetailsViewController
            let orderI = orderArrayList[indexPath.row]
            nVC.orderNumber = orderI.orderNumber
            nVC.orderID = orderI.id
            self.navigationController?.pushViewController(nVC, animated: true)
        } else {
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "sellerOrderDetailVC") as! sellerOrderDetailVC
            let orderI = orderArrayList[indexPath.row]
            nVC.orderNumber = orderI.orderNumber
            nVC.orderID = orderI.id
            // Connect Seller with selected order number
            
            self.navigationController?.pushViewController(nVC, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if isLoading {
            return CGSize.zero
        }
        return CGSize(width: collectionView.bounds.size.width, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
            self.footerView = aFooterView
            self.footerView?.backgroundColor = UIColor.clear
            return aFooterView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
    }
    
    //compute the scroll value and play witht the threshold to get desired effect
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let threshold   = 100.0 ;
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold  = Float((diffHeight - frameHeight))/Float(threshold);
        triggerThreshold   =  min(triggerThreshold, 0.0)
        let pullRatio  = min(abs(triggerThreshold),1.0);
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        if pullRatio >= 1 {
            self.footerView?.animateFinal()
           
        } else {
            self.footerView?.stopAnimate()
            self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(0.0))
        }
        
        
        //print("pullRation:\(pullRatio)")
    }
    
    //compute the offset and call the load method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = abs(diffHeight - frameHeight);
       // print("pullHeight:\(pullHeight)");
        if pullHeight == 0.0
        {
            if !(self.footerView?.isAnimatingFinal)! {
               // print("load more trigger")
                self.isLoading = true
                self.footerView?.startAnimate()
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                    self.callAPItoGetBuyerOrderList()
                    self.collectionview.reloadData()
                    self.isLoading = false
                })
            }
        } else {
            //self.footerView?.animateFinal()
            self.footerView?.stopAnimate()
            self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(0.0))
        }
    }
    
    
}

extension MyOrderViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterTVC", for: indexPath) as! filterTVC
        cell.filterNameLabel.text = filterList[indexPath.row].filterName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        filterId = filterList[indexPath.row].filterId
        self.filterView.isHidden = true
        filter_status = false
        self.currentPageCount = 1
        orderArrayList = []
        if userType == BUYER {
            self.callAPItoGetBuyerOrderList()
        } else {
            self.callAPItoGetSellerOrderList()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func reloadTable() {
        
        self.tableView.frame = CGRect(x: Int(UISCREEN_WIDTH - 200), y: 0, width: 200, height: (filterList.count) * 40)
        self.tableView.reloadData()
    }
    
}

// MARK:- WEB API
extension MyOrderViewController {
    func callAPItoGetBuyerOrderList() {
        
        let paraDict = NSMutableDictionary()
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        //self.orderArrayList = []
       
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "buyer/orders/getMyOrders/\(filterId)?page=\(self.currentPageCount)") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    //self.collectionview.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
               // self.collectionview.isHidden = true
                return
            } else {
                
                guard let orderList = response?["data"] as? NSArray else {
                    return
                }
                
                guard let meta = response?["meta"] as? NSDictionary else {
                    return
                }
                
                guard let currentPage = meta["current_page"] as? Int else {
                    return
                }
                
                guard let lastPage = meta["last_page"] as? Int else {
                    return
                }
                
                 let notificationCount = meta["notificationCount"] as? Int ?? 0
                
                if notificationCount != 0 {
                    self.notificationLbl.text = "\(notificationCount)"
                } else {
                    self.notificationLbl.text = ""
                }
                
                if currentPage <= lastPage {
                    self.currentPageCount += self.currentPageCount
                } else {
                    self.currentPageCount = lastPage+1
                    return
                }
                
                let filterArray = meta["filters"] as? NSArray
                
                if filterArray?.count != 0 {
                    self.filterList = []
                    for item in filterArray!{
                        let dataDict = item as? NSDictionary
                        let filterData = filterInfo()
                        filterData.filterId = dataDict!["id"] as? Int ?? 0
                        filterData.filterName = dataDict!["filter"] as? String ?? ""
                        self.filterList.append(filterData)
                    }
                }
               
                if orderList.count != 0 {
                    
                    for order in orderList {
                        let dict = order as? NSDictionary
                        let orderDetail = orderInfo()
                        orderDetail.id = dict?["id"] as? Int ?? 0
                        orderDetail.orderNumber = dict?["order_number"] as? String ?? ""
                        orderDetail.orderAmount = dict?["order_amount"] as? String ?? ""
                        orderDetail.currency = dict?["currency"] as? String ?? "R"
                        orderDetail.storeName = dict?["store_name"] as? String ?? ""
                        orderDetail.storeStatus = dict?["status"] as? String ?? "Waiting for approval."
                        orderDetail.storeDateTime = dict?["date_time"] as? String ?? ""
                        orderDetail.storePriceWithCurrency = "\(orderDetail.currency) \(orderDetail.orderAmount!)"
                        self.orderArrayList.append(orderDetail)
                    }
                    self.collectionview.isHidden = false
                    self.collectionview.reloadData()
                    self.reloadTable()
                    
                } else {
                    let alertController = UIAlertController(title: "", message: "You have no orders till now.", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        //self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    //self.collectionview.isHidden = true
                }
            }
        }
        
        //self.filterId = 0
    }
    
    
    func callAPItoGetSellerOrderList() {
        
        let paraDict = NSMutableDictionary()
        
        //paraDict["filter"] = filterId
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        //self.orderArrayList = []
        self.filterList = []
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .get, apiName: "seller/orders/getMyOrders/\(filterId)?page=\(self.currentPageCount)") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                    self.collectionview.isHidden = true
                }
                
                return
                
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                self.collectionview.isHidden = true
                return
            } else {
                
                guard let orderList = response?["data"] as? NSArray else {
                    return
                }
                
                guard let meta = response?["meta"] as? NSDictionary else {
                    return
                }
                
                guard let currentPage = meta["current_page"] as? Int else {
                    return
                }
                
                guard let lastPage = meta["last_page"] as? Int else {
                    return
                }
                
                let notificationCount = meta["notificationCount"] as? Int ?? 0
                
                if notificationCount != 0 {
                    self.notificationLbl.text = "\(notificationCount)"
                } 
                
                if currentPage <= lastPage {
                    self.currentPageCount += self.currentPageCount
                } else {
                    self.currentPageCount = lastPage+1
                    return
                }
                
                let filterArray = meta["filters"] as? NSArray
                
                for item in filterArray! {
                    let dataDict = item as? NSDictionary
                    let filterData = filterInfo()
                    filterData.filterId = dataDict!["id"] as? Int ?? 0
                    filterData.filterName = dataDict!["filter"] as? String ?? ""
                    
                    self.filterList.append(filterData)
                }
                
                if orderList.count != 0 {
                    
                    for order in orderList {
                        let dict = order as? NSDictionary
                        let orderDetail = orderInfo()
                        orderDetail.id = dict?["id"] as? Int ?? 0
                        orderDetail.orderNumber = dict?["order_number"] as? String ?? ""
                        orderDetail.orderAmount = dict?["order_amount"] as? String ?? ""
                        orderDetail.currency = dict?["currency"] as? String ?? "R"
                        orderDetail.storeName = dict?["buyer_name"] as? String ?? ""
                        orderDetail.storeStatus = dict?["status"] as? String ?? "Waiting for approval."
                        orderDetail.storeDateTime = dict?["date_time"] as? String ?? ""
                        orderDetail.storePriceWithCurrency = "\(orderDetail.currency) \(orderDetail.orderAmount!)"
                        self.orderArrayList.append(orderDetail)
                    }
                    self.collectionview.isHidden = false
                    self.collectionview.reloadData()
                    self.reloadTable()
                    
                } else {
                    let alertController = UIAlertController(title: "", message: "You have no orders till now.", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        //self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    self.collectionview.isHidden = true
                }
                
                
                
            }
        }
        
        //self.filterId = 0
    }
    
    
}
