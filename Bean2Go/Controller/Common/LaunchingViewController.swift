//
//  LaunchingViewController.swift
//  Bean2Go
//
//  Created by Anjana Aks on 10/04/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import SwiftyGif

class LaunchingViewController: UIViewController {

    @IBOutlet weak var gifImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            let gif = try UIImage(gifName: "logo_splash.gif", levelOfIntegrity:0.5)
            self.gifImageView.setGifImage(gif)
        } catch _ {
        }
       
        
        
        // Do any additional setup after loading the view.
    }
    

  

}
