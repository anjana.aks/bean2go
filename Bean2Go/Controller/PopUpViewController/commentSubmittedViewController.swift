//
//  commentSubmittedViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 03/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class commentSubmittedViewController: UIViewController {

    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var popUpView: UIView!
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.okBtn.layer.cornerRadius = 2
        self.okBtn.clipsToBounds = true
        self.popUpView.shadowApply(shadowRadius: 5)
        self.popUpView.layer.cornerRadius = 4
    }
    
    @IBAction func okBtnAction(_ sender: UIButton) {
    
    }
    
    @IBAction func BackBtnAction(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
}
