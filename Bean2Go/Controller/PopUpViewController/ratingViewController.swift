//
//  ratingViewController.swift
//  Bean2Go
//
//  Created by AKS on 14/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit

class ratingViewController: UIViewController {
    @IBOutlet weak var dialogBoxView: UIView!
    @IBOutlet weak var resturantNameLabel: UILabel!
    @IBOutlet weak var qualityRateView: CosmosView!
    @IBOutlet weak var valueRateView: CosmosView!
    @IBOutlet weak var conviRateView: CosmosView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var commentTextView: UITextView!
    
    var orderId: Int = 0
    var storeId: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dialogBoxView.layer.cornerRadius = 10
        commentTextView.layer.cornerRadius = 5
        //commentTextView.becomeFirstResponder()
        commentTextView.layer.addborder(width: 1, color: UIColor.gray)
        self.dialogBoxView.isHidden = false
        
        

    }
    
    @IBAction func skipButtonAction(_ sender: UIButton) {
        self.skipRateAPI()
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.callAPIToGiveRate()
        self.dialogBoxView.isHidden = true
    }
    
    
    @IBAction func viewDidTapped(_ sender: UIButton) {
        view.endEditing(true)
    }
    
    // MARK:- get string after zero digit
    private func formatValue(_ value: Double) -> String {
        return String(format: "%.f", value)
    }
    
    // MARK:- get string after one digit
    private func formatValueAfterOneDigit(_ value: Double) -> String {
        return String(format: "%.1f", value)
    }
    
}

extension ratingViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        commentTextView.text = ""
        commentTextView.textColor = .black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if commentTextView.text == "" {
            clearCommentBox()
        }
    }
    
    //To resign first responder on done click
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (range.length==0) {
            
            if text == "\n" {
                textView.resignFirstResponder()
                return false;
            }
        }
        return true;
    }
    
    func clearCommentBox(){
        commentTextView.textColor = UIColor.lightGray
        commentTextView.text = "Leave a Comment..."
        
    }
}

// MARK:- CAll API
extension ratingViewController {
    
    // MARK:- Submit Rate and comment
    fileprivate func callAPIToGiveRate() {
        
        let paraDict = NSMutableDictionary()
        
        let valueRate = valueRateView.rating
        let qualityRate = qualityRateView.rating
        let convenienceRate = conviRateView.rating
        
        var avgRating: Double = 0.0
        
        var keyValueArray:[[String: Any]] = [["key": "value","value": valueRate],
                                                ["key": "quality","value": qualityRate],
                                                ["key": "convenience","value": convenienceRate]
                                                ]
        
        var avgRatingArray: [Double] = []
        
        for i in 0...keyValueArray.count-1 {
            var dict = [String: Any]()
            dict = keyValueArray[i]
            let ratingValue = dict["value"] as? Double
            if ratingValue != 0.0 {
                avgRatingArray.append(ratingValue!)
                paraDict["ratings[\(dict["key"]!)]"] = formatValue(ratingValue!)
            }
        }
        
        if avgRatingArray.count == 0 {
            self.skipRateAPI()
           
        } else {
            var total: Double = 0.0
            for value in avgRatingArray {
                total = total + value
            }
            
            if total != 0.0 {
                avgRating = total/Double(avgRatingArray.count)
            }
            
            //lat="28.528232" lon="77.275734"
            paraDict["store_id"] = self.storeId //28.528232
            paraDict["rateAvg"] = formatValueAfterOneDigit(avgRating) //77.275734
            
            var commentText = trimWhiteSpace(str: commentTextView.text)
            
            if commentText == "Leave a Comment..." {
                commentText = ""
            }
            
            paraDict["comments"] = commentText //28.528232
            paraDict["order_id"] = self.orderId //77.275734
            
           // print(paraDict)
            
            //indicatorViewObj.showIndicatorView(viewController: self)
            
            ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/stores/rateStore") { (response, error,message, statusCode) in
                
                if message != nil {
                    if statusCode == "401" {
                        kAppDelegate.logoutAPI(viewController: self)
                        /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                        let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                            if self.navigationController?.viewControllers.count ?? 0 > 0 {
                                
                                if ((self.navigationController?.viewControllers.last) != nil) {
                                    self.navigationController?.popToRootViewController(animated: false)
                                } else {
                                    self.navigationController?.viewControllers.removeAll()
                                }
                            }
                            let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                            self.navigationController?.viewControllers = [mainView]
                            let obj = AppDelegate()
                            obj.window?.rootViewController = self.navigationController
                            obj.window?.makeKeyAndVisible()
                        }
                        alertController.addAction(action1)
                        self.present(alertController, animated: true, completion: nil)
                        */
                        
                    } else {
                        
                        let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                        let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(action1)
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    return
                } else if error != nil || response == nil {
                    let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                }  else {
                    guard let data = response?["data"] as? NSDictionary else {
                        return
                    }
                    
                    let msg = data["message"] as? String
                    
                    let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK:- Skip Rate API
    fileprivate func skipRateAPI() {
        
        let paraDict = NSMutableDictionary()
        
        
        //lat="28.528232" lon="77.275734"
        paraDict["store_id"] = self.storeId //28.528232
        paraDict["order_id"] = self.orderId //77.275734
        
       // print(paraDict)
        
        //indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/stores/rateStore/skip") { (response, error,message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            } else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                return
            }  else {
                guard let data = response?["data"] as? NSDictionary else {
                    return
                }
                
                let msg = data["message"] as? String
                
                let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
