//
//  dialogBoxViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 18/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class accountCreateddialogBoxViewController: UIViewController {

    @IBOutlet weak var dialogBoxView: UIView!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dialogBoxView.layer.cornerRadius = 20
        dialogBoxView.clipsToBounds = true
        
        self.okBtn.layer.cornerRadius = self.okBtn.frame.size.height / 2
        self.okBtn.clipsToBounds = true
        
        self.loginBtn.layer.cornerRadius = self.loginBtn.frame.size.height / 2
        self.loginBtn.clipsToBounds = true
    }

    //MARK:- Button Actions
    
    @IBAction func backBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func okBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
    }

}
