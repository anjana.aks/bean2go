//
//  orderDBViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 18/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

//protocol  ordertypeDelegateMethod {
//    func orderTimeForSchedular(orderTime:String)
//}
class orderDBViewController: UIViewController {

    let datePickerView:UIDatePicker = UIDatePicker()
    let toolbar = UIToolbar()
    
    //MARK:- outlets
    @IBOutlet weak var dialogBoxView: UIView!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var onTheGoBtn: UIButton!
    @IBOutlet weak var scheduleBtn: UIButton!
    
    //var delegate : ordertypeDelegateMethod!
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()

//        dialogBoxView.layer.cornerRadius = 10
//        dialogBoxView.clipsToBounds = true
//        dialogBoxView.shadowApply(shadowRadius: 10)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.dialogBoxView.shadowApply(shadowRadius: 10)
    }

    //MARK:- Button Action
    @IBAction func backgroundViewDidTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onTheGoBtnAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        //self.delegate?.orderTimeForSchedular(orderTime: "")
        
    }
    @IBAction func scheduleBtnAction(_ sender: UIButton)
    {

        showDatePicker()
        
    }
    
    func showDatePicker(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm a"
        let dateString = formatter.string(from: date)
        let minDate = formatter.date(from: dateString)
        
        //print(minDate as Any)
        
        onTheGoBtn.isEnabled = false
        scheduleBtn.isEnabled = false
        dialogBoxView.alpha = 0.95
        
        datePickerView.datePickerMode = UIDatePicker.Mode.time
        datePickerView.minimumDate = minDate
        datePickerView.frame = CGRect(x: 0.0, y: (self.view.frame.height - 150.0), width: self.view.frame.width, height: 150.0)
        datePickerView.backgroundColor = UIColor.init(white: 233, alpha: 1.0)
        //specialPriceFromTextField.inputView = datePickerView
        
        
        toolbar.sizeToFit()
        toolbar.frame = CGRect(x: 0.0, y: (self.view.frame.height - 190.0), width: self.view.frame.width, height: 40.0)
        toolbar.backgroundColor = .white
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        self.view.addSubview(toolbar)
        
        //datePickerView.addTarget(self, action: #selector(datePickerFromValueChanged), for: UIControlEvents.valueChanged)
        self.view.addSubview(datePickerView)
        
    }
    
    func getCurrentTime(){
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        LogInfo(hour)
    }
    
    @objc func donedatePicker(){
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm a"
        let dateString = formatter.string(from: date)
        let minDate = formatter.date(from: dateString)
        
       // print(minDate as Any)
        //let formatter = DateFormatter()
        formatter.timeStyle = .short
        //formatter.dateFormat = "dd/MM/yyyy"
        let txtDatePicker = formatter.string(from: datePickerView.date)
        //self.view.endEditing(true)
        self.datePickerView.removeFromSuperview()
        self.toolbar.removeFromSuperview()
        
        self.dismiss(animated: false, completion: nil)
        //self.delegate?.orderTimeForSchedular(orderTime: txtDatePicker)
    }
    
    @objc func cancelDatePicker(){
       
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        LogInfo(hour)
        self.datePickerView.removeFromSuperview()
        self.toolbar.removeFromSuperview()
        
        onTheGoBtn.isEnabled = true
        scheduleBtn.isEnabled = true
        dialogBoxView.alpha = 1.0
    }
   
}
