//
//  fogotpasswordDBViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 19/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import Alamofire

protocol delegateMethod {
    func SubmitStatus()
}

class fogotpasswordDBViewController: UIViewController {
    
    @IBOutlet weak var resendBtn: UIButton!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var mobileNumberTextFld: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var dialogViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var otpTxtFld: UITextField!
    
    
    var delegate: delegateMethod!
    var userDetails = UserDetails()
    var isShowOTP: Bool = false
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.mobileNumberTextFld.layer.addborder(width: 2, color: UIColor.lightGray)
        self.mobileNumberTextFld.layer.cornerRadius = 2
        self.mobileNumberTextFld.setLeftPadding(value: 10)
        self.mobileNumberTextFld.addToolBar()
        
        self.otpTxtFld.layer.addborder(width: 2, color: UIColor.lightGray)
        self.otpTxtFld.layer.cornerRadius = 2
        self.otpTxtFld.setLeftPadding(value: 10)
        self.otpTxtFld.addToolBar()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.dialogView.shadowApply(shadowRadius: 10)
        
        self.submitBtn.setBackgroundImageAndCornerRadius()
    }
//
//    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0 {
//                self.view.frame.origin.y -= keyboardSize.height
//            }
//        }
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification) {
//        if self.view.frame.origin.y != 0 {
//            self.view.frame.origin.y = 0
//        }
//    }
    
    //MARK:- Button Actions
    @IBAction func submitBtAction(_ sender: UIButton) {
        if !isShowOTP {
            if isAllFieldVerified() {
                self.callForgotPasswordAndGetOtpAPI()
            }
        } else {
            if checkOTPVerified() {
                self.callVerifyAPI()
            }
        }
    }
    @IBAction func resendOtpBtnDidTapped(_ sender: UIButton) {
        self.callForgotPasswordAndGetOtpAPI()
    }
    
    @IBAction func canceBtnAction(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func BackBtnAction(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- TOUCH EVENT
    @IBAction func viewDidTapped(_ sender: UIButton){
        self.view.endEditing(true)
    }
    
    func checkOTPVerified() -> Bool {
        var isVerified = false
        let otpText = trimWhiteSpaceNew(str: otpTxtFld.text)
        self.userDetails.otp = otpText
        if otpText == "" {
            
            let alertController = UIAlertController(title: "", message: "Please enter the OTP.", preferredStyle: .alert)
            let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                self.otpTxtFld.text = ""
                self.userDetails.otp = ""
                self.otpTxtFld.resignFirstResponder()
            }
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
            
           
            return isVerified
        } else if otpText != userDetails.otp {
            
            let alertController = UIAlertController(title: "", message: "Please enter valid OTP.", preferredStyle: .alert)
            let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                self.otpTxtFld.text = ""
                self.userDetails.otp = ""
                self.otpTxtFld.resignFirstResponder()
            }
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
            
            return isVerified
            
        } else {
            isVerified = true
        }
        
        
        return isVerified
    }
    
    /// MARK:- Validations
    func isAllFieldVerified() -> Bool {
        var isFieldVerified = false
        userDetails.userContactNo = trimWhiteSpaceNew(str: mobileNumberTextFld.text)
            
        if userDetails.userContactNo == nil || userDetails.userContactNo?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter your phone number.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if (userDetails.userContactNo?.count)! != 10 {
            
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter valid phone number.", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }  else {
            isFieldVerified = true
        }
        
        return isFieldVerified
    }
 
}

extension fogotpasswordDBViewController {
    
    // MARK:- VERIFY API
    fileprivate func callVerifyAPI() {
        
        let paraDict = NSMutableDictionary()
        paraDict["contact_number"] = userDetails.userContactNo
        paraDict["otp"] = userDetails.otp
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/user/forgotPassword/verify") { (response, error,message,statusCode)  in
            
            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                   
                } else {
                    
                    let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.otpTxtFld.text = ""
                        self.userDetails.otp = ""
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                return
            }  else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                return
            } else {
                let JSON = response as! NSDictionary
                let msg = JSON["message"] as? String ?? ""
                
                USER_DEFAULTS.set(self.userDetails.userContactNo, forKey: MOBIL_NO)
                
                // show toast msg
                let toastMsgObj = ToastClassView.init(frame: CGRect(x: 0, y: UISCREEN_HEIGHT-200, width: UISCREEN_WIDTH, height: 44))
                toastMsgObj.showToastMsg(toastMsg: msg,viewController: self)
                
                // clear all values after 1 min
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    self.mobileNumberTextFld.text = ""
                    self.dismiss(animated: false, completion: nil)
                    self.delegate?.SubmitStatus()
                }
                
            }

        }
    }
    
    // MARK:- FORGOTAPI AND GET OTP
    
    fileprivate func callForgotPasswordAndGetOtpAPI() {
        let paraDict = NSMutableDictionary()
        
        paraDict["contact_number"] = userDetails.userContactNo
        
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/user/forgotPassword") { (response, error,message, statusCode) in

            if message != nil {
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                  
                    
                } else {
                    
                    let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.resendBtn.isHidden = true
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    
                    //self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)

                }
                
                return
            }else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)

                return
            } else {
                let JSON = response as! NSDictionary
                let msg = JSON["message"] as? String ?? ""
                
                let meta = JSON["meta"] as? NSDictionary
                let otp = meta!["otp"] as? String ?? ""
                
                self.mobileNumberTextFld.resignFirstResponder()
                self.mobileNumberTextFld.isUserInteractionEnabled = false
                
                //self.userDetails.otp = otp
                self.dialogViewHeightConstraint.constant = self.dialogViewHeightConstraint.constant + 30
                self.otpTxtFld.isHidden = false
                self.otpTxtFld.text = ""//self.userDetails.otp
                self.isShowOTP = true
                
                let toastMsgObj = ToastClassView.init(frame: CGRect(x: 0, y: UISCREEN_HEIGHT-200, width: UISCREEN_WIDTH, height: 44))
                toastMsgObj.showToastMsg(toastMsg: msg,viewController: self)
            }
        }

    }
}

extension fogotpasswordDBViewController: UITextFieldDelegate {
    
    
    //MARK:- TextField functions
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField.returnKeyType == .done{
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextFld {
            var str : NSString = (textField.text as NSString?)!
            str = str.replacingCharacters(in: range, with: string) as NSString
            if trimWhiteSpace(str: str as String).count < 8 || trimWhiteSpace(str: str as String).count > 10     {
                if trimWhiteSpace(str: str as String).count > 10 {
                    // imgView_checkPhone.image = UIImage(named: "check_green")
                    return false
                }
            }
        } else if textField == otpTxtFld {
            var str : NSString = (textField.text as NSString?)!
            str = str.replacingCharacters(in: range, with: string) as NSString
            if trimWhiteSpace(str: str as String).count < 2 || trimWhiteSpace(str: str as String).count > 4     {
                if trimWhiteSpace(str: str as String).count > 4 {
                    // imgView_checkPhone.image = UIImage(named: "check_green")
                    return false
                }
            }
        }
        
        return true
        
    }
    
    
}


