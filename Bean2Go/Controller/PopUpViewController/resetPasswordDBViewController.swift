//
//  resetPasswordDBViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 19/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import Alamofire

class resetPasswordDBViewController: UIViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var dialogview: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var newpasswordTxtFld: UITextField!
    @IBOutlet weak var confirmPasswordTxtFld: UITextField!
    @IBOutlet weak var resetPasswordBtn: UIButton!
    
     var userDetails = UserDetails()
    
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup Initial View Layout
        
        self.newpasswordTxtFld.layer.addborder(width: 2, color: UIColor.lightGray)
        self.confirmPasswordTxtFld.layer.addborder(width: 2, color: UIColor.lightGray)
        
        self.newpasswordTxtFld.setLeftPadding(value: 10)
        self.confirmPasswordTxtFld.setLeftPadding(value: 10)
        
        self.newpasswordTxtFld.layer.cornerRadius = 2
        cancelBtn.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetPasswordBtn.setBackgroundImageAndCornerRadius()

    }
   
    //MARK:- Textfield Functions
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next{
            confirmPasswordTxtFld.becomeFirstResponder()
        }else if textField.returnKeyType == .done{
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    //MARK:- TOUCH EVENT
    @IBAction func viewDidTapped(_ sender: Any){
        self.view.endEditing(true)
    }
    
    //MARK:- Button Action
    @IBAction func cancelBtnAction(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backgroundBackBtn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetPasswordBtnAction(_ sender: UIButton){
        
        self.userDetails.userNewPassword = trimWhiteSpaceNew(str: newpasswordTxtFld.text)
        self.userDetails.userConfirmPassword = trimWhiteSpaceNew(str: confirmPasswordTxtFld.text)
        
        if isAllFieldVerified() {
            self.callResetPasswordApi()
            
        }
    }
    
    // MARK:- Validations
    func isAllFieldVerified() -> Bool {
        var isFieldVerified = false
        let newPassword = trimWhiteSpaceNew(str: newpasswordTxtFld.text)
        let confirmPassword = trimWhiteSpaceNew(str: confirmPasswordTxtFld.text)
        
        let isNewPasswordValid = validation.validationShared.isValidPassword(testStr: self.userDetails.userNewPassword!)
        let isconfirmPasswordValid = validation.validationShared.isValidPassword(testStr: self.userDetails.userConfirmPassword!)
        
        if newPassword == nil || newPassword?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter new password", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if (newPassword?.count)! < 6 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter valid password", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }else if isNewPasswordValid == false {
            present(UIAlertController.alertWithTitle(title: "", message: "Password Should have At least one Uppercase letter.Atleast one Lower case letter.Also,At least one numeric value.And, At least one special character.Must be more than 6 characters long", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if confirmPassword == nil || confirmPassword?.count == 0 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter confirm passoword", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if (confirmPassword?.count)! < 6 {
            present(UIAlertController.alertWithTitle(title: "", message: "Please enter valid confirm password", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        }else if isconfirmPasswordValid == false {
            present(UIAlertController.alertWithTitle(title: "", message: "Password Should have At least one Uppercase letter.Atleast one Lower case letter.Also,At least one numeric value.And, At least one special character.Must be more than 6 characters long", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else if newPassword != confirmPassword {
            present(UIAlertController.alertWithTitle(title: "", message: "Passwords do not match", buttonTitle: "OK"), animated: true, completion: nil)
            return isFieldVerified
            
        } else {
            //print("Verfication ending 405")
            isFieldVerified = true
        }
        
        return isFieldVerified
    }
    
    //MARK: Reset API
    
    func callResetPasswordApi() {
        let paraDict = NSMutableDictionary()
        
        userDetails.userContactNo = USER_DEFAULTS.value(forKey: MOBIL_NO) as? String
        
        paraDict["contact_number"] = userDetails.userContactNo
        paraDict["new_password"] = userDetails.userNewPassword
        paraDict["new_password_confirmation"] = userDetails.userConfirmPassword
        
        //print(paraDict)
        
        indicatorViewObj.showIndicatorView(viewController: self)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/user/resetPassword") { (response, error,message,statusCode) in
            
            if message != nil{
                if statusCode == "401" {
                    kAppDelegate.logoutAPI(viewController: self)
                    /*let alertController = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.navigationController?.viewControllers.count ?? 0 > 0 {
                            
                            if ((self.navigationController?.viewControllers.last) != nil) {
                                self.navigationController?.popToRootViewController(animated: false)
                            } else {
                                self.navigationController?.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.navigationController?.viewControllers = [mainView]
                        let obj = AppDelegate()
                        obj.window?.rootViewController = self.navigationController
                        obj.window?.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    self.present(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    self.present(UIAlertController.alertWithTitle(title: "", message: message! , buttonTitle: "OK"), animated: true, completion: nil)
                }
                return
            }else if error != nil || response == nil{
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
//                ConveienceClass.networkAlert(controller: self, completionHandler: { (sucess) in
//                    if sucess ?? false {
//                        //self.navigationController?.popViewController(animated: true)
//                    }
//                })
                return
            } else {
                let JSON = response as! NSDictionary
                let msg = JSON["message"] as? String ?? ""
                
                let toastMsgObj = ToastClassView.init(frame: CGRect(x: 0, y: UISCREEN_HEIGHT-200, width: UISCREEN_WIDTH, height: 44))
                toastMsgObj.showToastMsg(toastMsg: msg,viewController: self)
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    self.dismiss(animated: false, completion: nil)
                    
                }
            }
        }
        
    }
    
}
