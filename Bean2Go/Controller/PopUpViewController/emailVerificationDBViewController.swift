//
//  emailVerificationDBViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 18/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class emailVerificationDBViewController: UIViewController {

    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var dialogBoxView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    
    //MARK:-VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let gradientlayer = CAGradientLayer()
//        gradientlayer.colors = [GRADIENT_FIRST_COLOR,GRADIENT_SECOND_COLOR]
//        
//        gradientlayer.startPoint = CGPoint(x: 0, y: 0)
//        gradientlayer.endPoint = CGPoint(x: 1, y: 1)
//        gradientlayer.frame = self.view.bounds
//         self.view.layer.addSublayer(gradientlayer)
        
        dialogBoxView.layer.cornerRadius = 20
        dialogBoxView.clipsToBounds = true
        
        self.cancelBtn.layer.cornerRadius = (self.cancelBtn.frame.size.height / 2) - 2
        self.cancelBtn.clipsToBounds = true
        
        self.sendBtn.layer.cornerRadius = (self.sendBtn.frame.size.height / 2) - 2
        self.sendBtn.clipsToBounds = true
        
        self.emailTxtFld.layer.cornerRadius = (self.emailTxtFld.frame.size.height / 2 ) - 2
        self.emailTxtFld.clipsToBounds = true
        self.emailTxtFld.layer.addborder(width: 2, color: UIColor.lightGray)
        self.emailTxtFld.setLeftPadding(value: 10)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.sendBtn.setBackgroundImageAndCornerRadius()
    }
    
    //MARK:- Button Action
    @IBAction func backgroundBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func CancelBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func sendBtnAction(_ sender: UIButton) {
    }

}
