//
//  scheduleDBViewController.swift
//  Bean2Go
//
//  Created by AKS on 02/11/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
protocol  ordertypeDelegateMethod {
    func orderTimeForSchedular(orderTime:String,type: String, timeZone: String)
}

class scheduleDBViewController: UIViewController {
    
    @IBOutlet weak var dialogBoxView: UIView!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var scheduledButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var toolBar: UIToolbar!
    
    let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
    
    var delegate : ordertypeDelegateMethod!
    var maxDateString = String()
    //let date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        let calendar = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        //comps.year = 30
        let maxDate = calendar.date(byAdding: comps, to: Date())
        //comps.year = -30
        
        //let minDatee = calendar.da
        let minDate = calendar.date(byAdding: comps, to: Date())
        //datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate

        
        //datePicker.getDatePicker().setMinDate(system.currentTimeMillis())
        
        //datePicker.minimumDate = getPickerDate(datePickerDate: date as Date) as Date//getstartDate() as Date
        datePicker.maximumDate = getPickerDate(datePickerDate: UTCToLocal(date: maxDateString)) as Date
        
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        datePicker.backgroundColor = UIColorFromRGB(rgbValue: 0xC9994F)
        datePicker.isHidden = true
        toolBar.isHidden = true
        
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    
    override func viewDidAppear(_ animated: Bool) {
        self.dialogBoxView.shadowApply(shadowRadius: 10)
    }
    
    @IBAction func backButtonDidTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func cancelBtnDidTapped(_ sender: Any) {
        self.datePicker.isHidden = true
        toolBar.isHidden = true
        self.goButton.isEnabled = true
        self.scheduledButton.isEnabled = true
    }
    
    @IBAction func doneBtnDidTapped(_ sender: UIBarButtonItem) {
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormater.timeZone = TimeZone(abbreviation: "UTC")
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        let txtDatePicker = formatter.string(from: datePicker.date)
        let pickedDate = dateFormater.string(from: datePicker.date)
        //print(pickedDate)
        self.goButton.isEnabled = true
        self.scheduledButton.isEnabled = true
        self.datePicker.isHidden = true
        toolBar.isHidden = true
        self.dismiss(animated: false, completion: nil)
        
        let timeZone: String = TimeZone.current.identifier
        
        self.delegate?.orderTimeForSchedular(orderTime: pickedDate,type: "Schedule", timeZone: timeZone)
    }
    @IBAction func scheduleBtnDidTapped(_ sender: UIButton) {
        
        self.dialogBoxView.alpha = 0.95
        self.goButton.isEnabled = false
        self.scheduledButton.isEnabled = false
        self.datePicker.isHidden = false
        toolBar.isHidden = false
    }
    
    @IBAction func goBtnDidTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        let timeZone: String = TimeZone.current.identifier
        self.delegate?.orderTimeForSchedular(orderTime: "",type: "OnTheGo", timeZone: timeZone)
    }
    
    
}
