//
//  profileUpdatedViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 04/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class profileUpdatedViewController: UIViewController {

    @IBOutlet weak var okBtn: UIButton!
    
    //MARK:- VIEW OVERRIDE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.okBtn.layer.cornerRadius = 2
        self.okBtn.clipsToBounds = true
        
        //self.popUpView.layer.cornerRadius = 4
        //self.popUpView.shadowApply(shadowRadius: 10)
    }
    
    //MARK:- Button Action
    @IBAction func backButtonDidTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okButton_Action(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
