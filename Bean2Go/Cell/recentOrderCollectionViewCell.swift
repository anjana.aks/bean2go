//
//  recentOrderCollectionViewCell.swift
//  Bean2Go
//
//  Created by AKS on 30/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class recentOrderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var coffeeNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
}
