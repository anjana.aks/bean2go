//
//  orderDetailHeaderView.swift
//  Bean2Go
//
//  Created by AKS on 31/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class orderDetailHeaderView: UICollectionReusableView {
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var timmerView: UIView!
    @IBOutlet weak var scheduleFor: UIStackView!
    @IBOutlet weak var timmerLbl: UILabel!
    
    @IBOutlet weak var scheduleForLbl: UILabel!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var otpLbl: UILabel!
    @IBOutlet weak var enterPinButtonView: UIView!
    
    @IBOutlet weak var navigationButton: UIButton!
    
    // needed in order detail buyer section
    @IBOutlet weak var remarkStackview: UIStackView!
    @IBOutlet weak var remarkTxtView: UITextView!
    @IBOutlet weak var remarkTxtViewHeightConstraint: NSLayoutConstraint!
}

class sellerOrderDetailFooter: UICollectionReusableView {
    @IBOutlet weak var subTotalAmountLbl: UILabel!
    @IBOutlet weak var taxAmountLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var paymentStatusLbl: UILabel!
    @IBOutlet weak var paymentMode: UILabel!
    @IBOutlet weak var otalAmountPaidLbl: UILabel!
}

