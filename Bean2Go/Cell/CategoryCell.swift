//
//  CategoryCell.swift
//  Expandable Table View
//
//  Created by Aks on 15/12/18.
//  Copyright © 2018 Aks. All rights reserved.
//

import UIKit

protocol SubCatagoryCellSelected {
    func selectedCellPriceCalculate()
}

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var subCatagoryCollectionView: UICollectionView!
    
    var selectedCellDelegate: SubCatagoryCellSelected?
    var productInfoArray = [IngredientInfo]()
    var isSelectedCell: Int = -1
    var isDeSelectCell: Int = -1
    var selectedCell = [IndexPath]()
    
    @IBOutlet weak var arrowViewConstraints: NSLayoutConstraint!
}

extension CategoryCell: UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productInfoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SubCatagoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCellId", for: indexPath) as! SubCatagoryCell
 
        cell.tag = indexPath.item
        let subProductInfo = productInfoArray[indexPath.item]
        
        cell.imageOfProduct.image = UIImage(named: "uncheck_square")
        subProductInfo.isSelected = false
        
        if isSelectedCell == indexPath.item {
            cell.imageOfProduct.image = UIImage(named: "checkSquare")
            subProductInfo.isSelected = true
        }
        
        cell.nameOfProduct.text = subProductInfo.option_name
        
        productInfoArray[indexPath.item] = subProductInfo
 

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        isSelectedCell = indexPath.item
        subCatagoryCollectionView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.005) {
            self.selectedCellDelegate?.selectedCellPriceCalculate()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
}

class SubCatagoryCell: UICollectionViewCell {
    @IBOutlet weak var nameOfProduct: UILabel!
    @IBOutlet weak var imageOfProduct: UIImageView!
    
   // var isCellSelected: Bool = false
    
    
    /*override var isSelected: Bool {
        didSet {
            if self.isSelected {
                //isCellSelected = true
                //self.tag =
                
                //self.imageOfProduct.image = UIImage(named: "checkSquare")
            } else {
                //isCellSelected = false
               // self.imageOfProduct.image = UIImage(named: "uncheck_square")
            }
        }
    }*/
    
}
