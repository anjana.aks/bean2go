//
//  orderDetailsTableViewCell.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 05/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class orderDetailsCell: UICollectionViewCell {

    @IBOutlet weak var itemImgView: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var viewDetailButton: UIButton!
  

}
