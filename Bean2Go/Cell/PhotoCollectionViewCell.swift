//
//  PhotoCollectionViewCell.swift
//  Bean2Go
//
//  Created by AKS on 24/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImgView: UIImageView!
    @IBOutlet weak var viewAllView: UIView!
}

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuImgView: UIImageView!
    @IBOutlet weak var viewAllView: UIView!
}

