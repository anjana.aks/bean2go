//
//  drawerTVC.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 17/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class drawerTVC: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iconImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
