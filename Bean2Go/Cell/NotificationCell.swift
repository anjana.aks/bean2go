//
//  NotificationCell.swift
//  Bean2Go
//
//  Created by Anjana Aks on 10/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit

class NotificationCell: UICollectionViewCell {
   
    @IBOutlet weak var notiBackView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameOfRestaurant: UILabel!
    @IBOutlet weak var notificationStatus: UILabel!
}
