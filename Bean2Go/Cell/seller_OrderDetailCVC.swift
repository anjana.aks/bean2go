//
//  seller|OrderDetailCVC.swift
//  Bean2Go
//
//  Created by AKS on 15/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit

class seller_OrderDetailCVC: UICollectionViewCell {
    @IBOutlet weak var itemImgView: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var commentStackView: UIStackView!
    @IBOutlet weak var commentLbl: UILabel!
}
