//
//  ReviewTableViewCell.swift
//  Bean2Go
//
//  Created by AKS on 24/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet var reviewView: CosmosView!
    @IBOutlet weak var cellBgView: UIView!
    @IBOutlet weak var bookMarkButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        reviewView = CosmosView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
//    func setReview(rating: String) {
//        let ratingInDouble: String = storeReviewClass.reviewRating ?? "3.5"
//        // cell.reviewView.rating = Double(ratingInDouble) ?? 3.5
//        
//        cell.reviewView.rating = 3.5
//    }

}
