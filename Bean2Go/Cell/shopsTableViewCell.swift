//
//  shopsTableViewCell.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 20/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class coffeeCell: UICollectionViewCell {

    @IBOutlet weak var sepratorLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var itemCostLbl: UILabel!
    @IBOutlet weak var itemTypeLbl: UILabel!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemImgView: UIImageView!
    @IBOutlet weak var upBtn: UIButton!
    @IBOutlet weak var downBtn: UIButton!
    @IBOutlet weak var quantityPickerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sepratorLbl.layer.shadowColor = (UIColor.lightGray).cgColor
        sepratorLbl.layer.shadowOffset = CGSize(width: 0, height: 0)
        sepratorLbl.layer.shadowRadius = 3
        sepratorLbl.layer.shadowOpacity = 6
        
        
        itemImgView.layer.cornerRadius = 10
        itemImgView.clipsToBounds = true
        
        quantityPickerView.layer.cornerRadius = 4
        quantityPickerView.shadowApply(shadowRadius: 2)
        
    }

}
