//
//  myAccountCollectionViewCell.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 05/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class myAccountCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImgView: UIImageView!
    @IBOutlet weak var cellLbl: UILabel!
    @IBOutlet weak var cellButton: UIButton!
}
