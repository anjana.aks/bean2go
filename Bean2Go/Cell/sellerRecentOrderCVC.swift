//
//  sellerRecentOrderCVC.swift
//  Bean2Go
//
//  Created by AKS on 02/02/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit

class sellerRecentOrderCVC: UICollectionViewCell {
    
    @IBOutlet weak var orderNoLbl: UILabel!
    @IBOutlet weak var buyerNameLbl: UILabel!
    @IBOutlet weak var noOfItemLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
}
