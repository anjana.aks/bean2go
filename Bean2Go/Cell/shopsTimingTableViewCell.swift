//
//  shopstimingTableViewCell.swift
//  Bean2Go
//
//  Created by Aks on 25/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class shopsTimingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class AddCardTableViewCell: UITableViewCell {
    @IBOutlet weak var cardHolderName: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
