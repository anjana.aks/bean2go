//
//  cartScheduledViewCell.swift
//  Bean2Go
//
//  Created by AKS on 16/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class cartScheduledViewCell: UICollectionViewCell {
    
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var itemImgView: UIImageView!
    @IBOutlet weak var upBtn: UIButton!
    @IBOutlet weak var downBtn: UIButton!
    //@IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var viewDetailButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var ingredientPriceLbel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
}
