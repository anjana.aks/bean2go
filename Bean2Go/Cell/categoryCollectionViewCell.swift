//
//  categoryCollectionViewCell.swift
//  Bean2Go
//
//  Created by AKS on 30/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class categoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coffeeImgView: UIImageView!
    @IBOutlet weak var coffeeNameLbl: UILabel!
    
}
