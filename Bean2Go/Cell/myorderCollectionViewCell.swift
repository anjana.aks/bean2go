//
//  myorderCollectionViewCell.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 03/10/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class myorderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
}
