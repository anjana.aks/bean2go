//
//  MarkerView.swift
//  Bean2Go
//
//  Created by AKS on 25/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class MarkerView: UIView {

    @IBOutlet weak var closedLbl: UILabel!
    @IBOutlet var markerView: UIView!
    @IBOutlet var userCurrentLocationView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var restauNameLabel: UILabel!
    @IBOutlet weak var restauAddressLabel: UILabel!
    
    var viewHeight: CGFloat = 80
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        viewHeight = frame.height
        
        viewHeight == 80 ? initiateCurrentUserView() : initiateView()
        //initiateView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        viewHeight == 80 ? initiateCurrentUserView() : initiateView()
        
        fatalError("init(coder:) has not been implemented")
    }
    
    func initiateView() {
        Bundle.main.loadNibNamed("MarkerView", owner: self, options: nil)
        addSubview(markerView)
        markerView.frame = self.bounds
        markerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
       
    }
    
    func initiateCurrentUserView() {
        Bundle.main.loadNibNamed("MarkerView", owner: self, options: nil)
        addSubview(userCurrentLocationView)
        userCurrentLocationView.frame = self.bounds
        userCurrentLocationView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }
    
    
}
