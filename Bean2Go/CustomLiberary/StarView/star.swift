//
//  star.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 24/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

@IBDesignable class star: UIView {
    
    var checkedButton = 0
    @IBOutlet var customView: UIView!
    @IBOutlet weak var star1: UIButton!
    @IBOutlet weak var star2: UIButton!
    @IBOutlet weak var star3: UIButton!
    @IBOutlet weak var star4: UIButton!
    @IBOutlet weak var star5: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    
    func setUp(){
        Bundle.main.loadNibNamed("star", owner: self, options: nil)
        addSubview(customView)
        customView.frame = self.bounds
        customView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        star1.imageView?.contentMode = .scaleAspectFit
        star2.imageView?.contentMode = .scaleAspectFit
        star3.imageView?.contentMode = .scaleAspectFit
        star4.imageView?.contentMode = .scaleAspectFit
        star5.imageView?.contentMode = .scaleAspectFit
    }
    
    @IBInspectable var Selectedstar:Int {
        get {
            return 0
        }
        set(number) {
            switch number {
            case 0:
                star1.isSelected = false
                star2.isSelected = false
                star3.isSelected = false
                star4.isSelected = false
                star5.isSelected = false
                break
                
            case 1:
                star1.isSelected = true
                star2.isSelected = false
                star3.isSelected = false
                star4.isSelected = false
                star5.isSelected = false
                break
                
            case 2:
                star1.isSelected = true
                star2.isSelected = true
                star3.isSelected = false
                star4.isSelected = false
                star5.isSelected = false
                break
                
            case 3:
                star1.isSelected = true
                star2.isSelected = true
                star3.isSelected = true
                star4.isSelected = false
                star5.isSelected = false
                break
                
            case 4:
                star1.isSelected = true
                star2.isSelected = true
                star3.isSelected = true
                star4.isSelected = true
                star5.isSelected = false
                break
                
            case 5:
                star1.isSelected = true
                star2.isSelected = true
                star3.isSelected = true
                star4.isSelected = true
                star5.isSelected = true
                break
                
            default:
                break
            }
        }
    }
    
    @IBAction func starButton_Action(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        
        star2.isSelected = false
        star3.isSelected = false
        star4.isSelected = false
        star5.isSelected = false
        
    }
    @IBAction func star2button_Action(_ sender: UIButton)
    {
        
        sender.isSelected = !sender.isSelected
        
        star1.isSelected = true
        star3.isSelected = false
        star4.isSelected = false
        star5.isSelected = false
//        print(checkedButton)
//        if checkedButton > 1
//        {
//            star2.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            star3.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            star4.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            star5.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            checkedButton = 1
//        }else{
//            star1.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star2.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            checkedButton = 2
//        }
    }
    
    @IBAction func star3Button_Action(_ sender: UIButton)
    {
        
        sender.isSelected = !sender.isSelected
        
        star2.isSelected = true
        star1.isSelected = true
        star4.isSelected = false
        star5.isSelected = false
//        if checkedButton > 2
//        {
//            star3.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            star4.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            star5.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            checkedButton = 2
//        }else{
//            star1.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star2.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star3.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            checkedButton = 3
//        }
    }
    @IBAction func star4Button_Action(_ sender: UIButton)
    {
        
        sender.isSelected = !sender.isSelected
        
        star1.isSelected = true
        star2.isSelected = true
        star3.isSelected = true
        star5.isSelected = false
//        if checkedButton > 3
//        {
//            star4.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            star5.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            checkedButton = 3
//        }else
//        {
//            star1.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star2.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star3.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star4.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            checkedButton = 4
//        }
    }
    @IBAction func star5button_Action(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        
        star2.isSelected = true
        star3.isSelected = true
        star4.isSelected = true
        star1.isSelected = true
        
//        if checkedButton > 4
//        {
//            star5.setImage(#imageLiteral(resourceName: "empty_star"), for: .normal)
//            checkedButton = 4
//        }else{
//            star1.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star2.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star3.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star4.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            star5.setImage(#imageLiteral(resourceName: "filled_star"), for: .normal)
//            checkedButton = 5
//        }
    }
    
}
