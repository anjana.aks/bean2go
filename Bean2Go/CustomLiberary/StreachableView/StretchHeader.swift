//
//  StretchHeader.swift
//  StretchHeaderDemo
//
//  Created by yamaguchi on 2016/03/24.
//  Copyright © 2016年 h.yamaguchi. All rights reserved.
//

import UIKit

@IBDesignable class StretchHeader: UIView {
    
    @IBOutlet var streachableHeadderView: UIView!
    @IBOutlet weak var restaurantImgView: UIImageView!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var restaurantName: UILabel!
    @IBOutlet weak var bookMarkButton: UIButton!
    @IBOutlet weak var starCountingLbl: UILabel!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var reveiewParentView: UIView!
    
    fileprivate var contentSize = CGSize.zero
    fileprivate var topInset : CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initiateView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initiateView()
    }
    
    func initiateView() {
        Bundle.main.loadNibNamed("StretchHeader", owner: self, options: nil)
        addSubview(streachableHeadderView)
        //bookMarkButton.showsTouchWhenHighlighted = true
        streachableHeadderView.frame = self.bounds
        streachableHeadderView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        stretchHeaderSize(image: UIImage())
        //        restaurantImgView = UIImageView()
        //        restaurantImgView.image = UIImage(named: "restaurants1")
    }
    
    func setData(resturantName: String,rating: String, imageUrl: String) {
        self.restaurantName.text = resturantName
        
        if rating == "0" {
            reveiewParentView.isHidden = true
        } else {
            reveiewParentView.isHidden = false
        }
        
        self.starView.rating = Double(rating) ?? 3.7
        self.starCountingLbl.text = rating
        
        if imageUrl != "" {
            restaurantImgView.af_setImage(withURL: URL(string: imageUrl)!)
        } else {
            restaurantImgView.image = UIImage(named: "restaurants1")
        }
        

        //// UIImage(named: "restaurants1")
    }
    
    //MARK:- Button Action
    @IBAction func bookMarkAction_Button(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        //MARK:- ADD to fav and remove
        
        addToFavAPICall(storeId: sender.tag)
        
        if sender.isSelected {
            self.bookMarkButton.isSelected = sender.isSelected
        } else {
            self.bookMarkButton.isSelected = sender.isSelected
        }
    }
    
    // MARK: Public
    func stretchHeaderSize(image: UIImage) {
        contentSize = self.bounds.size
    }
    
    open func updateScrollViewOffset(_ scrollView: UIScrollView) {
        
        if restaurantImgView == nil { return }
        let scrollOffset : CGFloat = scrollView.contentOffset.y
        //print(scrollOffset)
        
        
        if scrollOffset < 0 {
            restaurantImgView.frame = CGRect(x: scrollOffset ,y: scrollOffset, width: self.frame.size.width - (scrollOffset * 2) , height: contentSize.height - scrollOffset);
            
        } else {
            restaurantImgView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: contentSize.height);
        }
    }
    
    
    func showHideView(isHidden: Bool, backImage: String) {
        blackView.alpha = 0
        restaurantImgView.image = UIImage(named:backImage)
    }
    
    func addToFavAPICall(storeId: Int) {
        
        let paraDict = NSMutableDictionary()
        
        paraDict["store_id"] = storeId
        
        //print(paraDict)
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "buyer/favStores/addToFav") { (response, error,message,statusCode ) in
            
            if message != nil {
                
                return
                
            } else if error != nil || response == nil {}else {}
        }
        
        
        
        //        let url = URL(string: "\(baseURL)getData/getStoreDetailsForBuyer")
        //
        //        //self.indicatorViewObj.showIndicatorView(viewController: self)
        //
        //        Alamofire.request(url ?? "", method: .post, parameters: paraDict as? [String: Any], encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json"]).responseJSON { response in
        //            print(response)
        //
        //            // self.indicatorViewObj.hideIndicatorView()
        //
        //
        //            guard let result = response.result.value else{
        //                return
        //            }
        //            let JSON = result as! NSDictionary
        //            //to get status code
        //            if let status = response.response?.statusCode {
        //                switch(status){
        //                case 200:
        //                    //let data = JSON["data"] as? NSMutableArray
        //
        //                    guard let data = JSON["data"] as? NSDictionary else {
        //                        return
        //                    }
        //                    self.shopeDetail = ShopDetailClass()
        //                    self.shopeDetail?.storeId = data["store_id"] as? Int
        //                    self.shopeDetail?.storeContactInfo = data["store_contact_number"] as? String
        //                    self.shopeDetail?.storeAddress = data["store_address"] as? String
        //                    self.shopeDetail?.vehicleNumber = data["vehicle_number"] as? String
        //                    self.shopeDetail?.storeDescription = data["description"] as? String
        //                    self.shopeDetail?.parkingAvailable = data["parking_available"] as? Int == 1 ? true : false
        //                    self.shopeDetail?.storeOpenAt = data["opens_at"] as? String
        //                    self.shopeDetail?.storeCloseAt = data["closes_at"] as? String
        //                    self.shopeDetail?.storeRating = data["ratings"] as? String
        //                    self.shopeDetail?.storeShopName = data["store_name"] as? String
        //
        //                    let storeImagesArray = data["store_images"] as? NSArray
        //                    for storeImageDetail in storeImagesArray! {
        //
        //                        var storeImageDict = Dictionary<String, AnyObject>()
        //                        storeImageDict = storeImageDetail as! Dictionary<String, AnyObject>
        //                        let storeImageObj = StoreImagesClass()
        //                        storeImageObj.storeImageUrl = storeImageDict["thumbnail_url"] as? String
        //                        storeImageObj.storeThumbImageUrl = storeImageDict["thumbnail_url"] as? String
        //                        self.shopeDetail?.storeImagesArray.append(storeImageObj)
        //
        //                    }
        //
        //                    let storeReviewArray = data["reviews"] as? NSArray
        //
        //                    for storeReviewDetail in storeReviewArray! {
        //
        //                        var storeReviewDict = Dictionary<String, AnyObject>()
        //                        storeReviewDict = storeReviewDetail as! Dictionary<String, AnyObject>
        //                        let storeReviewObj = StoreReviewClass()
        //                        storeReviewObj.reviewUserName = storeReviewDict["user_name"] as? String
        //                        storeReviewObj.reviewRating = storeReviewDict["ratings"] as? String
        //                        storeReviewObj.reviewComment = storeReviewDict["comment"] as? String
        //                        storeReviewObj.reviewUserImageUrl = storeReviewDict["user_thumbnail"] as? String
        //                        self.shopeDetail?.storeReviewArray.append(storeReviewObj)
        //
        //                    }
        //
        ////
        ////                    for skuDetail in skudetailsArray! {
        ////                        var skuDict = Dictionary<String, AnyObject>()
        ////                        skuDict = skuDetail as! Dictionary<String, AnyObject>
        ////                        let skuInfo = MASKUInfo()
        ////                        skuInfo.skuID = skuDict["skuId"] as? String  ?? ""
        ////                        skuInfo.skuQty = skuDict["skuQty"] as? String  ?? ""
        ////                        skuInfo.skuName = skuDict["skuName"] as? String  ?? ""
        ////                        skuInfo.skuAvailQty = skuDict["skuAvailability"] as? String  ?? ""
        ////                        skuInfo.packingType = skuDict["packingType"] as? String  ?? ""
        ////                        orderInfo.skuDetails.add(skuInfo)
        ////                    }
        //
        //
        //                    print(self.shopeDetail)
        //                    self.setDataOnView()
        //                    self.mainTableView.reloadData()
        //
        //                case 422:
        //                    let message = JSON["message"] as? String ?? ""
        //                    self.present(UIAlertController.alertWithTitle(title: "", message: message , buttonTitle: "OK"), animated: true, completion: nil)
        //                default:
        //                    print("error with response status: \(status)")
        //                }
        //            }
        //
        //        }
        
        
    }
}
