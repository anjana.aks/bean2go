//
//  SkuDetailViewClass.swift
//  Expandable Table View
//
//  Created by Aks on 27/11/18.
//  Copyright © 2018 Aks. All rights reserved.
//

import UIKit

class cartItemDetailRowClass: UIView {
    
    @IBOutlet var modifiedView: UIView!
    @IBOutlet weak var originalQqatityLbl: UILabel!
    @IBOutlet weak var avaiaQuantityLbl: UILabel!
    
    let nibName = "SkuDetailViewClass"
    var contentView: UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
    }
    
    func addValue( heading: String, value: String) {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(modifiedView)
        modifiedView.frame = self.bounds
        modifiedView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        originalQqatityLbl.text = heading
        avaiaQuantityLbl.text = value
    }
    
    func removeAllValue() {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(modifiedView)
        modifiedView.frame = self.bounds
        modifiedView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        originalQqatityLbl.text = ""
        avaiaQuantityLbl.text = ""
    }
    
}
