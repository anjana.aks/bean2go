//
//  loaderVC.swift
//  User
//
//  Created by AKS on 05/12/18.
//  Copyright © 2018 AKS. All rights reserved.
//

import UIKit

class loaderVC: UIView {

    @IBOutlet weak var activityIndicator: UIView!
    @IBOutlet var LoaderView: UIView!
    
    let nibName = "loaderVC"
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        
        addSubview(LoaderView)
        LoaderView.frame = self.bounds
        LoaderView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func showIndicatorView(viewController: UIViewController) {
        viewController.view.addSubview(self)
        activityIndicator.applyShadow(shadowRadius: 10, height: 50,color: UIColor.white)
    }
    
    func hideIndicatorView()  {
        self.removeFromSuperview()
    }
    
}
