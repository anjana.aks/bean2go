//
//  ToastClassView.swift
//  Bean2Go
//
//  Created by Aks on 30/11/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

class ToastClassView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet var toastView: UIView!
    @IBOutlet weak var msgLabel: UILabel!
    
    let nibName = "ToastClassView"
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func showToastMsg(toastMsg: String, viewController: UIViewController) {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(toastView)
        toastView.frame = self.bounds
        toastView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        msgLabel.text = toastMsg
        
        viewController.view.addSubview(self)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.removeFromSuperview()
        }
        
    }
}
