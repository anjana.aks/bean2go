//
//  LocationManager.swift
//  Bean2Go
//
//  Created by Anjana Aks on 07/03/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import CoreLocation

func RADIANS_TO_DEGREES(radians: Double) -> Double { return radians * 180.0 / .pi }

protocol getLatLngOfUserDelegate {
    func getUserLatLng(latitude: String, longitude: String)
}

class LocationManagerClass: NSObject, CLLocationManagerDelegate {

    static let locationSharedObj = LocationManagerClass()
    var locationManager = CLLocationManager()
    var emitKeyId: Int?
    var latLngDelegate: getLatLngOfUserDelegate?
    
    // MARK:- Update User Location
    func startUpdateLocation(roomNameID: Int?) {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self as CLLocationManagerDelegate
        emitKeyId = roomNameID != 0 ? roomNameID : nil
    }
    
    // MARK:- Stop User Location
    func stopUserLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.last else {
            return
        }
        
        // Calculate Angle start
        /*var oldLocation: CLLocation?

        if locations.count != 0 {
            if locations.count > 1 {
                oldLocation = locations[locations.count - 2]
            } else {
                oldLocation = currentLocation
            }
        }
        
        let angleInRadian = angleFromCoordinate(first: oldLocation!, second: currentLocation)
        let angle = String(RADIANS_TO_DEGREES(radians: Double(angleInRadian)))*/
        //// Calculate Angle end
        let lat:String = String(format:"%f", currentLocation.coordinate.latitude)
        let lng:String = String(format:"%f", currentLocation.coordinate.longitude)
        
        latLngDelegate?.getUserLatLng(latitude: lat, longitude: lng)
        
        //print("Location Manger Class \("angle") \(lat) \(lng)")
        
        if emitKeyId != nil {
            SocketManagerClass.shared.emitTheUserLocation(latitude: lat, longitude: lng, angle: "angle", keyID: emitKeyId!)
        }

    }
    
    fileprivate func angleFromCoordinate(first: CLLocation, second: CLLocation) -> Float {
        let deltaLongitude: Float = Float(second.coordinate.longitude - first.coordinate.longitude)
        let deltaLatitude: Float = Float(second.coordinate.latitude - first.coordinate.latitude)
        let angle: Float = Float((.pi * 0.5)) - atan(deltaLatitude / deltaLongitude)
        
        if deltaLongitude > 0 {
            return angle
        } else if deltaLongitude < 0 {
            return angle + .pi
        } else if deltaLatitude < 0 {
            return .pi
        }
       
        return 0.0;
        
    }
    
}
