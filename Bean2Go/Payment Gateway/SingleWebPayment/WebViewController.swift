//
//  WebViewVC.swift
//  Bean2Go
//
//  Created by AKS on 08/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit
import KRProgressHUD

class WebViewController: UIViewController {

    @IBOutlet weak var webview: UIWebView!
    var PassToDic = [String:String]()
    var orderId = String()
    var webStr = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setWebView()
        
        // Do any additional setup after loading the view.
    }
    //MARK:----------------------------Set WebView With parms ------------------------
    func setWebView(){
        
        
        var request = URLRequest(url: URL(string: PassToDic["url"]!)!)
        request.httpMethod = "POST"
        let body = "PAYGATE_ID=\(PassToDic["payGate_id"]!)&REFERENCE=\(PassToDic["ref_id"]!)&CHECKSUM=\(PassToDic["chaeksum_id"]!)&PAY_REQUEST_ID=\(PassToDic["pay_requ_id"]!)"
        request.httpBody = body.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data : Data?, response : URLResponse?, error : Error?) in
            if data != nil
            {
                
                if String(data: data!, encoding: .utf8) != nil
                {
                    let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    DispatchQueue.main.sync {
                        self.webview.loadRequest(request as URLRequest)
                        
                    }
                }
            }
        }
        task.resume()
        
    }
    
    
}

extension WebViewController:UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        //print(error)
//        let alertController = UIAlertController(title: "", message: "Unable to load", preferredStyle: .alert)
//        let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
//            self.navigationController?.popViewController(animated: true)
//            KRProgressHUD.dismiss()
//        }
//        alertController.addAction(action1)
//        self.present(alertController, animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
//        
        KRProgressHUD.dismiss()
        //print(error)
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        KRProgressHUD.show()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let resp: CachedURLResponse? = URLCache.shared.cachedResponse(for: webview.request!)
        
        let url = webview.request?.url
        //print("url**********\(url)")
    
                
        if let aFields = (resp?.response as? HTTPURLResponse)?.allHeaderFields {
           // print("\(aFields)")
//            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
//            nVC.orderNumber = orderId
//            self.navigationController?.pushViewController(nVC, animated: true)
        }
        KRProgressHUD.dismiss()
    }
    
    
    fileprivate func createPaymentStatus() {
        
    }
}
