//
//  SingleWebPaymentVC.swift
//  Bean2Go
//
//  Created by AKS on 08/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import UIKit

class SingleWebPayment: UIViewController ,XMLParserDelegate,UITextViewDelegate {
    
    var viewType : String = "web"
    var cardUrl = "payhost/process.trans"
    var decoder = JSONDecoder()
    var merchantInfo = MerchantInfo()
    var mainStr = String()

    @IBOutlet weak var cardExpdate: UITextField!
    @IBOutlet weak var cardNumber: UITextField!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setMerchantData()

        if(viewType == "web"){
            self.title = "Web Payment"
            mainStr = self.setWebRequsetParms()
        }
        
    }
    
    func setMerchantData(){
        self.merchantInfo.payGateId = "10011072130"
        self.merchantInfo.encryptionKey = "test"
        self.merchantInfo.buyerName = USER_DEFAULTS.value(forKey: USER_NAME) as? String ?? ""
        self.merchantInfo.buyerEmail = USER_DEFAULTS.value(forKey: EMAIL_ADDRESS) as? String ?? ""
        self.merchantInfo.buyerLastName = ""
    }
    
    
    //MARK:------------------------Request Button Action/API call-----------------------------------
    @IBAction func SubmitButtonAction(_ sender: Any) {
        
//        //self.req_textview.text = ""
//        if(viewType == "card"){
//            mainStr = self.SetCard()
//        }
//        else if(viewType == "token"){
//            //mainStr = self.setPaymentForToken()
//        }
        
        
        ServiceHelper.sharedInstance.callPaygateApi(parms: mainStr, Url: cardUrl, Success: {(responseObject,ResponseString) -> () in
            DispatchQueue.main.async {
                //self.req_textview.resignFirstResponder()
                if(ResponseString.contains("Complete")){
                    self.ReadWebPaymnetMethod(Response: ResponseString)
                }else{
                    LogInfo("Error...")
                }
            }
            
        }, Failure: {(error) -> () in
            //print(error!.description)
        },showLoader: true, hideLoader: true)
       
    }
//
//    //MARK:------------------------Set Card For "Card Payment" ----------------------------------
//    func SetCard() -> String{
//        do
//        {
//            //Account XML
//            //LogInfo(merchantInfo.payGateId)
//            let paygateAccount = ["paygateId":merchantInfo.payGateId,"password":merchantInfo.encryptionKey]
//            let jsonDataAcc = try JSONSerialization.data(withJSONObject:paygateAccount, options: .prettyPrinted)
//            let accountXML = try decoder.decode(AccountXML.self, from: jsonDataAcc)
//            //<ns:CardNumber>\(cardNumber.text!)</ns:CardNumber>
////            let cardDetails = """
////            <ns:CardNumber>\(cardNumber.text!)</ns:CardNumber>
////            <ns:CardExpiryDate>\(cardExpdate.text ?? "202019")</ns:CardExpiryDate>
////            <ns:CVV>124</ns:CVV>
////            <ns:BudgetPeriod>0</ns:BudgetPeriod>
////            """
//            //Redirect XMl
////            let redirect = RedirectDetails(JSON:["NotifyUrl":"https://192.168.0.12/A/PayHost/notify.php","ReturnUrl":"https://192.168.0.12/A/PayHost/result.php"]) //Change this to your server url
////
//            let redirect = RedirectDetails(JSON:["NotifyUrl":"https://192.168.0.12/A/PayHost/notify.php","ReturnUrl":"http://192.168.0.12/A/PayHost/result.php"]) //Change this to your server url
//
//            //CustomerDetailXMLModel   XML String
////            let CustomerDic:[String : Any] = ["Title":"Mr","FirstName":"PayGate","LastName":"Test","Email":"itsupport@paygate.co.za","Country":"ZAF","account":paygateAccount]
//
//            let CustomerDic:[String : Any] = ["Title":"","FirstName":merchantInfo.buyerName,"LastName":"","Email":merchantInfo.buyerEmail,"Country":"ZAF","account":paygateAccount]
//            let jsonData = try JSONSerialization.data(withJSONObject: CustomerDic, options: .prettyPrinted)
//            let customerStr = try decoder.decode(CustomerDetailXMLModel.self, from: jsonData)
//
//            //Generate Reference
//            let date = Date()
//            let formatterReference = DateFormatter()
//            formatterReference.dateFormat = "YmdHis"
//            let orderReference = "pgtest_ios_" + formatterReference.string(from: date)
//
//            //Generate TransactionDate
//            let formatterTransactionDateYear = DateFormatter()
//            formatterTransactionDateYear.dateFormat = "yyyy-MM-dd"
//            let formatterTransactionDateTime = DateFormatter()
//            formatterTransactionDateTime.dateFormat = "HH:mm:ss"
//            let TransactionDate = formatterTransactionDateYear.string(from: date) + "T" + formatterTransactionDateTime.string(from: date)
//
//            //Merchant Detail
//            let productDIc = ["ProductCode":"ABC123","Currency":"ZAR","ProductDescription":"Misc Product","ProductCategory":"misc","ProductRisk":"XX","OrderQuantity":"1","UnitPrice":"120"]
//            let orderData = try JSONSerialization.data(withJSONObject: ["MerchantOrderId":orderReference,"Currency":"ZAR","Amount":"100","TransactionDate":TransactionDate,"orderitem":productDIc], options: .prettyPrinted)
//            let orderDetail = try decoder.decode(OrderItemDetails.self, from: orderData)
//
//
////            let ConcateStr = (accountXML.soapStr)! + (customerStr.soapStr)! + cardDetails + (redirect?.soapStr)! + (orderDetail.soapStr)!
//            let ConcateStr = (accountXML.soapStr)! + (customerStr.soapStr)! + (redirect?.soapStr)! + (orderDetail.soapStr)!
//
//            let makeObject = CardpaymentRequest(soapvalue:ConcateStr)
//            print(makeObject.soapvalue)
//            return makeObject.soapvalue
//
//        }
//        catch{
//            return ""
//        }
//    }
    
    func callCheckSumAPi(){
        ServiceHelper.sharedInstance.callPaygateApi(parms: mainStr, Url: cardUrl, Success: {(responseObject,ResponseString) -> () in
            DispatchQueue.main.async {
                //self.req_textview.resignFirstResponder()
                if(ResponseString.contains("Complete")){
                    self.ReadWebPaymnetMethod(Response: ResponseString)
                }else{
                    LogInfo("Error...")
                }
            }
            
        }, Failure: {(error) -> () in
            //print(error!.description)
        },showLoader: true, hideLoader: true)
    }
    //MARK:------------------------Set Card For "Web Payment" ----------------------------------
    func setWebRequsetParms() -> String{
        
        do
        {
            //Account XMl String
            let paygateAccount = ["paygateId":merchantInfo.payGateId,"password":merchantInfo.encryptionKey]
            let jsonDataAcc = try JSONSerialization.data(withJSONObject:paygateAccount, options: .prettyPrinted)
            let accountXML = try decoder.decode(AccountXML.self, from: jsonDataAcc)
            
            //Redirect XMl
            let redirect = RedirectDetails(JSON:["NotifyUrl":"https://192.168.0.12/A/PayHost/notify.php","ReturnUrl":"https://192.168.0.12/A/PayHost/result.php"])
            
            //CustomerDetailXMLModel  XML String
            let CustomerDic:[String : Any] = ["Title":" ","FirstName":merchantInfo.buyerName,"LastName":" ","Email":merchantInfo.buyerEmail,"Country":"ZAF","account":paygateAccount]
            let jsonData = try JSONSerialization.data(withJSONObject: CustomerDic, options: .prettyPrinted)
            let customerStr = try decoder.decode(CustomerDetailXMLModel.self, from: jsonData)
            
            //Generate Reference
            let date = Date()
            let formatterReference = DateFormatter()
            formatterReference.dateFormat = "YmdHis"
            let orderReference = "pgtest_ios_" + formatterReference.string(from: date)
            
            //Merchant Detail
            let orderData = try JSONSerialization.data(withJSONObject: ["MerchantOrderId":orderReference,"Currency":"ZAR","Amount":"100","TransactionDate":"2018-06-30T05:51:35","billing":CustomerDic], options: .prettyPrinted)
            let orderDetail = try decoder.decode(OrderDetails.self, from: orderData)
            
            
            let ConcateStr = (accountXML.soapStr)! + (customerStr.soapStr)! + (redirect?.soapStr)! + (orderDetail.soapStr)!
            
            let makeObject = WebpaymentRequest(soapvalue:ConcateStr)
           // print(makeObject.soapvalue)
            
            return makeObject.soapvalue
        }
        catch{
            return ""
            
        }
        
    }
    //MARK:------------------------ Read XMl Data------------------------
    func ReadWebPaymnetMethod(Response:String){
        var paygateid = String()
        var pay_requ_id = String()
        var ReferenceId  = String()
        var cheakSumid  = String()
        
        //DispatchQueue.main.sync {
        //DispatchQueue.main.async {
            //do{
                var xmlDictionary = try? XMLReader.dictionary(forXMLString: Response)
                
                let dic1 : NSDictionary = xmlDictionary!["SOAP-ENV:Envelope"] as! NSDictionary
                let dic2 : NSDictionary = dic1["SOAP-ENV:Body"] as! NSDictionary
                let dic3 : NSDictionary = dic2["ns2:SinglePaymentResponse"] as! NSDictionary
                var dic4  = NSDictionary()
                if(self.viewType == "web"){
                    dic4 = dic3["ns2:WebPaymentResponse"] as! NSDictionary
                    let dic5  = dic4["ns2:Redirect"] as! NSDictionary
                    
                    if(dic5["ns2:RedirectUrl"] != nil){
                        let dic6 = dic5["ns2:RedirectUrl"] as! NSDictionary
                        let redirectUrl = dic6["text"] as! String
                        let urlParms = dic5["ns2:UrlParams"] as! NSArray
                        
                        let dic1 = urlParms[0] as! NSDictionary
                        let dicvalue = dic1["ns2:value"] as! NSDictionary
                        paygateid = dicvalue["text"] as! String
                        
                        let dic2 = urlParms[1] as! NSDictionary
                        let payreqVal = dic2["ns2:value"] as! NSDictionary
                        pay_requ_id = payreqVal["text"] as! String
                        
                        let dic3 = urlParms[2] as! NSDictionary
                        let ReferenceDic = dic3["ns2:value"] as! NSDictionary
                        ReferenceId = ReferenceDic["text"] as! String
                        
                        let dic4 = urlParms[3] as! NSDictionary
                        let cheakSum = dic4["ns2:value"] as! NSDictionary
                        cheakSumid = cheakSum["text"] as! String
                        
                        var PassToDic = [String:String]()
                        PassToDic["url"] = redirectUrl
                        PassToDic["ref_id"] = ReferenceId
                        PassToDic["chaeksum_id"] = cheakSumid
                        PassToDic["payGate_id"] = paygateid
                        PassToDic["pay_requ_id"] = pay_requ_id
                        
                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                        vc.PassToDic = PassToDic
                        self.navigationController?.pushViewController(vc, animated:true)
                    }
                    
                }
//            }
//            catch{
//                
//            }
        //}
        
    }
}

