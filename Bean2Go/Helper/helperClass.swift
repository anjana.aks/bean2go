//
//  helperClass.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 17/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation
import UIKit

let UISCREEN_HEIGHT = UIScreen.main.bounds.height
let UISCREEN_WIDTH = UIScreen.main.bounds.width

let APP_DELEGATE    = UIApplication.shared.delegate as! AppDelegate
let USER_DEFAULTS   = UserDefaults.standard
let SHARED_APP      = UIApplication.shared
let ADD_CARD_VC = "ADD_Card_VC"
let NOTIFICATION_COUNT = "Notification_Count"

let STATUS_BAR_COLOR = UIColor(red: 186.0/255.0, green: 136.0/255.0, blue: 65.0/255.0, alpha: 1)
//let STATUS_BAR_COLOR =  UIColorFromRGB(0xba8841)

let GRADIENT_FIRST_COLOR = UIColor.init(red: 115/255, green: 67/255, blue: 41/255, alpha: 1)
let GRADIENT_SECOND_COLOR = UIColor.init(red: 198/255, green: 153/255, blue: 79/255, alpha: 1)

var BORDER_COLOR_SELECTED = UIColor.init(red: 115/255, green: 67/255, blue: 41/255, alpha: 1)
var BORDER_COLOR_UNSELECTED = UIColor.init(red: 146/255, green: 147/255, blue: 149/255, alpha: 1)

let USER_NAME = "NAME"
let EMAIL_ADDRESS = "email"
let MOBIL_NO = "Mobile"
let USER_PASSWORD = "Password"
let USER_TYPE = "userType"
let Auth_Token = "Auth_Token"
let IS_REMEMBER = "isRemember"
let IS_MOBILE_NUMBER_VERIFIED = "isMobileNumberVerified"
let USER_ID = "0"
let IS_LOGGEDIN = "LOGGED_IN"
let IS_SELLER_APPROVED = "Seller_Approved"
let BUYER = "buyer"
let SELLER = "seller"
let IS_OTP_VERIFIED = "isOtpVerified"
let FEATURE_CONTROLLER = "FeatureController"
let DASHBOARD = "Dashboard"
let iS_FROM_SCREEN_FOR_CART = "IsFromScreen"
let COUNTRY_CODE = "countryCode"


var ALBUM_NAME      = "Bean2Go"

let APP_LINK        = "https://itunes.apple.com/us/app/Bean2Go/id1461099346?ls=1&mt=8"//"itms-apps://itunes.apple.com/app/id\(8Y4CAK8LQL)"

let indicatorViewObj = loaderVC.init(frame: CGRect(x: 0, y: 0, width: UISCREEN_WIDTH, height: UISCREEN_HEIGHT))



class ImageHelper {
    
    class func shareImage(image:UIImage, controller:UIViewController) {
        
        let activity = UIActivityViewController(activityItems: [image, "#\(ALBUM_NAME) - \(APP_LINK)"], applicationActivities: nil)
        activity.popoverPresentationController?.sourceView = controller.view
        controller.present(activity, animated: true, completion: nil)
        
    }
    
    class func shareRestaurentdetail(image:UIImage, controller:UIViewController, storeId: Int, description: String) {
        
        let activity = UIActivityViewController(activityItems: [image, "#\(ALBUM_NAME) - \(description)\ncom.bean2go://ShopViewController/\(storeId)"], applicationActivities: nil)
        
        //        let activity = UIActivityViewController(activityItems: ["#\(ALBUM_NAME)", "com.bean2go://ShopViewController/\(storeId)"], applicationActivities: nil)
        
        activity.popoverPresentationController?.sourceView = controller.view
        controller.present(activity, animated: true, completion: nil)
        
    }
}

//Convert date to current tiemzone and format ..
func UTCToLocal(date:String) -> Date {
    let defaultDate = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
    let stringDate = dateFormatter.string(from: dt ?? defaultDate)
    dateFormatter.timeZone = TimeZone.current
    let requiredDate = dateFormatter.date(from: stringDate)
    return requiredDate!
}

func getDateFromString(dateString: String) -> Date {
    let defaultDate = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    return(dateFormatter.date(from: dateString) ?? defaultDate)
}

func getPickerDate(datePickerDate: Date) -> NSDate{
    let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
    let components: NSDateComponents = gregorian.components(([.day, .month, .year]), from: datePickerDate as Date) as NSDateComponents
    LogInfo("Date \(datePickerDate)")
    components.hour = datePickerDate.hour(date: datePickerDate)
    components.minute = datePickerDate.minute(date: datePickerDate)
    let startDate: NSDate = gregorian.date(from: components as DateComponents)! as NSDate
    return startDate
    
}

//MARK: - Date Conversion
func convertDateToDisplay( dateAsString : String) -> String{
    
    var dateString = dateAsString.split(separator: " ")
    //LogInfo(dateString[1])
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    
    if dateString.count < 2{
        return " "
    }
    guard let date = dateFormatter.date(from: String(dateString[1])) else{
        return " "
    }
    dateFormatter.dateFormat = "hh:mm a"
    let Date12 = dateFormatter.string(from: date)
    //LogInfo("12 hour formatted Date: \(Date12)")
    
    return Date12
    //return Date12
    
}

//MARK:- Print

func LogInfo(_ str:Any){
    print(str)
}

//MARK:- Trim
func trimWhiteSpace(str: String) -> String {
    let trimmedString = str.trimmingCharacters(in: NSCharacterSet.whitespaces)
    return trimmedString
}

func trimWhiteSpaceNew(str: String?) -> String? {
    let trimmedString = str?.trimmingCharacters(in: NSCharacterSet.whitespaces)
    return trimmedString
}


// MARK:- HIDE NAVIGATION BAR BACKGROUND COLOR
func clearNavigationBar(navigationBar: UINavigationBar) {
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.shadowImage = UIImage()
}

//MARK:- HIDE THE STATAUS BAR

func hideAndShowHideStatusBar(isShow: Bool) {
    SHARED_APP.isStatusBarHidden = isShow
}



// MARK:- SET GLOBALLY NAVIGATION BAR TITLE SIZE AND COLOR

func setNavigationBarTitleFontAndSize() {
    let attrs = [
        NSAttributedString.Key.foregroundColor: UIColor.white,
        NSAttributedString.Key.font: UIFont(name: "BalooBhai-Regular", size: 18)!
    ]
    
    UINavigationBar.appearance().titleTextAttributes = attrs
}


extension UIAlertController {
    static func alertWithTitle(title: String, message: String, buttonTitle: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
        alertController.addAction(action)
        return alertController
    }
}



// MARK:- VIEW EXTENSION
extension UIView {
    func topShadow(shadowRadius: CGFloat,color: UIColor){
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0 - (shadowRadius))
        self.layer.shadowOpacity = 0.3
        //self.layer.shadowPath = shadowPath.cgPath
        //self.layer.shouldRasterize = true
        self.layer.shadowRadius = shadowRadius
    }
    func bottomShadow(shadowRadius: CGFloat,color: UIColor){
        let shadowPath = UIBezierPath(rect: CGRect(x: 0 + (shadowRadius/2),
                                                   y: self.frame.size.height,
                                                   width: self.frame.size.width - shadowRadius,
                                                   height: shadowRadius))
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = shadowPath.cgPath
        //self.layer.shouldRasterize = true
        self.layer.shadowRadius = shadowRadius
    }
    
    func shadow(_ color:UIColor) {
        self.layer.shadowColor = color.cgColor;
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
    }
    
    func applyShadow(shadowRadius: CGFloat,height: CGFloat,color: UIColor ){
        let shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                   y: 0,
                                                   width: self.frame.size.width,
                                                   height: height))
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = shadowPath.cgPath
        //self.layer.shouldRasterize = true
        self.layer.shadowRadius = shadowRadius
    }
    
    func applyShadow(shadowRadius: CGFloat,height: CGFloat,width: CGFloat,color: UIColor ){
        let shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                   y: 0,
                                                   width: width - 10,
                                                   height: height))
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = shadowPath.cgPath
        //self.layer.shouldRasterize = true
        self.layer.shadowRadius = shadowRadius
    }
    
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x:0.5 , y:1)
        gradient.endPoint = CGPoint(x:1 , y:1)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    //Animate View From Left To Right
    func animateView()  {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeIn)
        self.window!.layer.add(transition, forKey: kCATransition)
    }
    
    //Animate View From Right To Left
    func animateViewFromRTL()  {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeIn)
        self.window!.layer.add(transition, forKey: kCATransition)
    }
    
    func animateViewFromLeftToRight() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .transitionFlipFromLeft, animations: {
            self.layoutIfNeeded()
        }, completion: nil);
    }
    
    func animateViewFromRightLeft() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .transitionFlipFromRight, animations: {
            self.layoutIfNeeded()
        }, completion: nil);
    }
    
    func animateViewFromBottomTop() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .transitionFlipFromBottom, animations: {
            self.layoutIfNeeded()
        }, completion: nil);
    }
    
    func shadowApplyNew(shadowSize: CGFloat)  {
        
        self.layer.shadowColor = UIColor(white: 0.0, alpha: 0.5).cgColor
        self.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 2.0
        //creating a trapezoidal path for shadow
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: -shadowSize, y: -shadowSize))
        
        // Create a line between the starting point and the bottom-left side of the view.
        path.addLine(to: CGPoint(x: -shadowSize, y: self.bounds.height + shadowSize))
        
        // Create the bottom line (bottom-left to bottom-right).
        path.addLine(to: CGPoint(x: self.bounds.height + shadowSize, y: self.bounds.width + shadowSize))
        
        // Create the vertical line from the bottom-right to the top-right side.
        path.addLine(to: CGPoint(x: self.bounds.width + shadowSize, y: -shadowSize))
        
        // Close the path. This will create the last line automatically.
        path.close()
        
        self.layer.shadowPath = path.cgPath
    }
    
    /*func shadowApply(shadowSize: CGFloat){
     let layer = CALayer()
     
     layer.frame = self.frame
     //layer.backgroundColor = UIColor.black.cgColor
     layer.shadowColor = UIColor.black.cgColor
     layer.shadowRadius = 5
     layer.shadowOpacity = 1
     
     let contactShadowSize: CGFloat = 20
     let shadowPath = CGPath(ellipseIn: CGRect(x: -contactShadowSize,
     y: -contactShadowSize * 0.5,
     width: layer.bounds.width + contactShadowSize * 2,
     height: contactShadowSize),
     transform: nil)
     
     self.layer.shadowPath = shadowPath
     }*/
    
    
    func shadowApply(shadowRadius: CGFloat){
        //let shadowSize : CGFloat = 2.0
        
        let shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                   y: 0,
                                                   width: self.frame.size.width,
                                                   height: self.frame.size.height))
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowRadius = shadowRadius
    }
    
    
    func shadowApplyFourSides(shadowRadius: CGFloat){
        //let shadowSize : CGFloat = 2.0
        
        let shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                   y: 0,
                                                   width: self.frame.size.width+8,
                                                   height: self.frame.size.height))
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowRadius = shadowRadius
    }
    
    func shadow3sideApply(shadowRadius: CGFloat){
        //let shadowSize : CGFloat = 2.0
        
        let shadowPath = UIBezierPath(rect: CGRect(x: 0 ,
                                                   y: 0 + shadowRadius,
                                                   width: self.frame.size.width,
                                                   height: self.frame.size.height))
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowRadius = shadowRadius
    }
    
    func createRectangle(shadowSize: CGFloat) -> UIBezierPath {
        // Initialize the path.
        let path = UIBezierPath()
        
        // Specify the point that the path should start get drawn.
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        
        // Create a line between the starting point and the bottom-left side of the view.
        path.addLine(to: CGPoint(x: 0.0, y: self.frame.size.height))
        
        // Create the bottom line (bottom-left to bottom-right).
        path.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height))
        
        // Create the vertical line from the bottom-right to the top-right side.
        path.addLine(to: CGPoint(x: self.frame.size.width, y: 0.0))
        
        // Close the path. This will create the last line automatically.
        path.close()
        
        return path
    }
}

extension UITextField {
    
}

//MARK:- CALAYER
extension CALayer {
    func addborder(width: CGFloat , color: UIColor){
        borderColor = color.cgColor
        borderWidth = width
        
    }
    
}

//MARK:- TEXTFIELD
extension UITextField{
    func setLeftPadding( value : CGFloat){
        let paddingVIEW = UIView(frame: CGRect(x:0 , y:0 , width: value , height: frame.height))
        self.leftView = paddingVIEW
        self.leftViewMode = .always
    }
    func setrightpadding(value: CGFloat) {
        let paddingVIEW = UIView(frame: CGRect(x:0 , y:0 , width: value , height: frame.height))
        self.rightView = paddingVIEW
        self.rightViewMode = .always
    }
    
    func addToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.tintColor = UIColor.black
        toolBar.isTranslucent = true
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        self.inputAccessoryView = toolBar
    }
    
    @objc func donePressed(){
        self.endEditing(true)
    }
    
    @objc func cancelPressed(){
        self.text = ""
        self.endEditing(true)
    }
}


//MARK:- ATTRIBUTED STRING

extension NSMutableAttributedString {
    
    func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttributes([NSAttributedString.Key.link: URL(string: linkURL)!], range: foundRange)
            return true
        }
        return false
    }
    
    func setAsColor(textToFind:String) {
        
        var mutableString: NSAttributedString?
        
        let foundRange = self.mutableString.range(of: textToFind)
        
        let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor : UIColor.init(red: 55.0/255.0, green: 123.0/255.0, blue: 181.0/255.0, alpha: 1)]
        
        
        if foundRange.location != NSNotFound {
            
            mutableString = NSMutableAttributedString(string:textToFind, attributes:attrs1)
            
            self.replaceCharacters(in: foundRange, with: mutableString!)
        }
        
        // return mutableString
    }
    
}

class ConveienceClass: NSObject {
    
    @objc class func showSimpleAlert(title: String, message: String, controller: UIViewController, onCancel:@escaping ((UIAlertAction) -> Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: onCancel))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    class func networkAlert(controller: UIViewController, completionHandler:@escaping (Bool?) -> Void) {
        let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
            completionHandler(true)
        }
        alertController.addAction(action1)
        alertController.present(alertController, animated: true, completion: nil)
    }
    
    
    
    class func logoutAlert(controller: UIViewController, completionHandler:@escaping (Bool?) -> Void) {
        
        let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            
            if ConveienceClass.clearAllDefaultValue() {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
            // UserDefaults.standard.synchronize()
            
            
            
        }
        
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    class func clearAllDefaultValue() -> Bool {
        var isCleared: Bool = false
        
        let isRemember = USER_DEFAULTS.bool(forKey: IS_REMEMBER)
        
        if isRemember == false {
            
            USER_DEFAULTS.set("", forKey: EMAIL_ADDRESS)
            USER_DEFAULTS.set("", forKey: USER_PASSWORD)
            USER_DEFAULTS.set(false, forKey: IS_REMEMBER)
            USER_DEFAULTS.set("", forKey: MOBIL_NO)
            USER_DEFAULTS.set("", forKey: USER_TYPE)
            USER_DEFAULTS.set("", forKey: USER_NAME)
            USER_DEFAULTS.set("", forKey: COUNTRY_CODE)
        }
        USER_DEFAULTS.set("", forKey: IS_MOBILE_NUMBER_VERIFIED)
        USER_DEFAULTS.set(false, forKey: IS_LOGGEDIN)
        USER_DEFAULTS.set("", forKey: SESS_TOKEN)
        USER_DEFAULTS.set(0, forKey: ORDER_STATUS_ID)
        USER_DEFAULTS.set("", forKey: ORDER_NUMBER)
        USER_DEFAULTS.set(false, forKey: IS_SELLER_APPROVED)
        USER_DEFAULTS.set(0, forKey: NOTIFICATION_COUNT)
//        USER_DEFAULTS.set("", forKey: DEVICE_TOKEN)
//        USER_DEFAULTS.set("", forKey: FCM_TOKEN)
        
      
        //USER_DEFAULTS.set("", forKey: ORDER_STATUS_ID)
        
        isCleared = true
        
        
        return isCleared
    }
    
}

extension UIButton {
    func setBackgroundImageAndCornerRadius()  {
        self.layer.cornerRadius = 2
    }
}

extension Date
{
    
    func hour(date: Date) -> Int
    {
        //Get Hour
        //let currentDateTime = Date()
        // get the user's calendar
        let userCalendar = Calendar.current
        let hour = userCalendar.component(.hour, from: date)
        
        
        //Return Hour
        return hour
    }
    
    
    func minute(date: Date) -> Int
    {
        //let currentDateTime = Date()
        //Get Minute
        let userCalendar = Calendar.current
        let min = userCalendar.component(.minute, from: date)
        
        //Return Minute
        return min
    }
    
    func sec(date: Date) -> Int
    {
        //let currentDateTime = Date()
        //Get Minute
        let userCalendar = Calendar.current
        let sec = userCalendar.component(.second, from: date)
        
        //Return Minute
        return sec
    }
    
    func toShortTimeString() -> String
    {
        let currentDateTime = Date()
        //Get Short Time String
        let formatter = DateFormatter()
        formatter.timeStyle = .long
        let timeString = formatter.string(from: currentDateTime)
        
        //Return Short Time String
        return timeString
    }
    
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970)
    }
}

extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 20 , height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 8.0)
        toastLabel.adjustsFontForContentSizeCategory = true
        toastLabel.minimumScaleFactor = 0.5
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

