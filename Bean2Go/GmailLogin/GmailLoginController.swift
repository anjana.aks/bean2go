//
//  GmailLoginController.swift
//  Bean2Go
//
//  Created by Anjana Aks on 07/01/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//



import Foundation
import UIKit
import GoogleSignIn

class SocialLoginClass: NSObject {
    var userId: String? = ""
    var idToken: String? = ""
    var fullName: String? = ""
    var givenName: String? = ""
    var familyName: String? = ""
    var email: String? = ""
    var password: String = "abcd4321@A"
}

protocol GmailSucessDelegate {
    func getLoginDetails(loginInfo: SocialLoginClass)
}

class GmailLoginClass: NSObject, GIDSignInDelegate {
    
    static var gmailLoginShared = GmailLoginClass.init()
    var loginDetails = SocialLoginClass()
    var isAllGetGmailInfo: Bool = false
    var gmailDelegate: GmailSucessDelegate?
    
    override init() {
        GIDSignIn.sharedInstance().clientID = "100483755699-r9f2gpphiu8d2ng24vc5jqsnk0til4e2.apps.googleusercontent.com"
    }
    
    
    func callSignIn() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            //print("\(error.localizedDescription)")
            isAllGetGmailInfo = false
        } else {
            // Perform any operations on signed in user here.
            loginDetails.userId = user.userID
            loginDetails.idToken = user.authentication.idToken
            loginDetails.fullName = user.profile.name
            loginDetails.givenName = user.profile.givenName
            loginDetails.familyName = user.profile.familyName
            loginDetails.email = user.profile.email
            
            gmailDelegate?.getLoginDetails(loginInfo: loginDetails)
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
}

