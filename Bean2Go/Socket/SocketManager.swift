//
//  SocketManager.swift
//  Socket
//
//  Created by Anjana Aks on 05/02/19.
//  Copyright © 2019 Anjana Aks. All rights reserved.
//

import Foundation
import SocketIO

protocol getDataArrayOfLatLng {
    func getLatLngFromSocket(latLongArray: [Any])
}

protocol refreshOrderDetailScreen {
    func refreshOrderDetailScreenSocket()
}




struct obj1 {
    var name: String?
    var id: String?
}

//let ORDER_A = ["name": "order_created", "id": "1"] as [String : String]

let ORDER_CREATED = "order_created"
let PAYMENT_PROCESSING = "payment_processing"
let PAYMENT_FAILED = "payment_failed"
let PAYMENT_COMPLETED = "payment_completed"
let ORDER_REJECTED = "order_rejected"
let ORDER_ACCEPTED = "order_accepted"
let AUTO_DECLINED = "auto_declined"
let CANCELLED = "cancelled"
let RECEIVED = "received"
let ORDER_STATUS_ID = "OrderStatusId"
let ORDER_NUMBER = "OrderNumber"

let ORDER_CREATED_ID = 1
let PAYMENT_PROCESSING_ID = 2
let PAYMENT_FAILED_ID = 3
let PAYMENT_COMPLETED_ID = 4
let ORDER_REJECTED_ID = 5
let ORDER_ACCEPTED_ID = 6
let AUTO_DECLINED_ID = 7
let CANCELLED_ID = 8
let RECEIVED_ID = 9

let socketLiveUrl = "http://13.126.113.25:3000"  // Live Socket URL
let socketTestUrl = "http://10.0.0.169:3000"  // Test Socket URL

class SocketManagerClass: NSObject {
        
    static let shared = SocketManagerClass()
    
    var socket: SocketIOClient!
    
    var socketDelegate: getDataArrayOfLatLng?
    var refreshOrderDetailScreenDelegate: refreshOrderDetailScreen?
    
    // defaultNamespaceSocket and swiftSocket both share a single connection to the server
    
    let manager = SocketManager(socketURL: URL(string: socketLiveUrl)!, config: [.log(true), .compress, .forcePolling(false)])
    
    
    
    //    let manager = SocketManager(socketURL: URL(string: "http://13.126.113.25:3000")!, config: [.log(true), .compress])
    
    override init() {
        super.init()
        
        socket = manager.defaultSocket
        socket = manager.socket(forNamespace: "/orders")
        //socket.joinNamespace()
        //socket = manager.socket(forNamespace: "/games")
    }
    
    func connectSocket() {
        
        self.manager.config = SocketIOClientConfiguration()
        // let socketL = manager.socket(forNamespace: "/games")
        socket.connect()
        
//        socket.connect(timeoutAfter: 0) {
//            self.socket.connect()
//        }
    }
    
    func connectSocket1()-> Bool {
        var isConnected: Bool = false
        
        self.manager.config = SocketIOClientConfiguration()
        // let socketL = manager.socket(forNamespace: "/games")
        socket.connect(timeoutAfter: 0) {
            if self.socket.status == .connected {
                return isConnected = true
            } else {
                self.connectSocket1()
            }
        }
        
        return isConnected
    }
    
    func disconnectSocket() {
        socket.disconnect()
    }
    
   
    //MARK:- Join Room
    func joinRoom(key: String) {
        let socketL = manager.socket(forNamespace: "/orders")
        //socket.emit(message,data)
        socketL.emit("joinRoom", with: [key])
        
    }
    
    // MARK:- Leave Room
    func leaveRoom(key: String) {
        let socketL = manager.socket(forNamespace: "/orders")
        //socket.emit(message,data)
        socketL.emit("leaveRoom", with: [key])
    }
    
    // MARK:- Buyer Receive
    fileprivate func leaveAndDisconnectSocket(room: String) {
        self.leaveRoom(key: room)
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        if userType == BUYER {
            self.socket.disconnect()
            LocationManagerClass.locationSharedObj.stopUserLocation()
            return
        }
        
        print("disconeect")
        //self.socket.disconnect()
    }
    
    fileprivate func recursiveLoop() {
        var isConnected: Bool = false
        let connectionIsEstablish = connectSocket1()
        
        if connectionIsEstablish == false {
            _ = connectSocket1()
        } else {
            isConnected = true
        }
        
    }
    
    func eventsHandleInSocket(roomName: String?, id: Int) {
        //let socketL = manager.socket(forNamespace: "/games")
        
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        
       // print("Socket Status ********      \(socket.status.description)")
        self.connectSocket()
        let isConnected = connectSocket1()
        
        let toastMsgObj = ToastClassView.init(frame: CGRect(x: 0, y: UISCREEN_HEIGHT-200, width: UISCREEN_WIDTH, height: 44))
        //toastMsgObj.showToastMsg(toastMsg: "Socket is \(isConnected)",viewController: viewContrl)
        
        switch id {
        case 1:
            // ORDER_CREATED
            USER_DEFAULTS.set(1, forKey: ORDER_STATUS_ID)
            break
        case 2:
            // PAYMENT_PROCESSING
            USER_DEFAULTS.set(2, forKey: ORDER_STATUS_ID)
            break
        case 3:
            // PAYMENT_FAILED
            USER_DEFAULTS.set(3, forKey: ORDER_STATUS_ID)
            break
        case 4:
            // PAYMENT_COMPLETED
            USER_DEFAULTS.set(4, forKey: ORDER_STATUS_ID)
            if let room = roomName {
                self.joinRoom(key: room)
                USER_DEFAULTS.set(room, forKey: ORDER_NUMBER)
            }
            break
        case 5:
            // ORDER_REJECTED
            USER_DEFAULTS.set(5, forKey: ORDER_STATUS_ID)
            if userType == SELLER {
                if let room = roomName {
                    self.joinRoom(key: room)
//                    self.emitOrderStatus(name: room, id: ORDER_REJECTED_ID)
//                    self.emitOrderStatus(name: room, id: ORDER_REJECTED_ID, orderStatus: ORDER_REJECTED)

                }
            }
            
//            if userType == BUYER {
//                _ = self.receiveOrderStatus(orderStatus: CANCELLED)
//            }
            
            self.leaveAndDisconnectSocket(room: ORDER_REJECTED)
            break
        case 6:
            // ORDER_ACCEPTED
            USER_DEFAULTS.set(6, forKey: ORDER_STATUS_ID)
            if userType == BUYER {
                //self.connectSocket()
                if let room = roomName {
                    self.joinRoom(key: room)
                    USER_DEFAULTS.set(room, forKey: ORDER_NUMBER)
                }
                
//                _ = self.receiveOrderStatus(orderStatus: ORDER_ACCEPTED)
                
                self.trackBuyer(key: ORDER_ACCEPTED)
                
                LocationManagerClass.locationSharedObj.startUpdateLocation(roomNameID: ORDER_ACCEPTED_ID)
            } else if userType == SELLER {
                //self.connectSocket()
                if let room = roomName {
                    self.joinRoom(key: room)
//                    self.emitOrderStatus(name: room, id: ORDER_ACCEPTED_ID)
//                    self.emitOrderStatus(name: room, id: ORDER_ACCEPTED_ID, orderStatus: ORDER_ACCEPTED)

                    USER_DEFAULTS.set(room, forKey: ORDER_NUMBER)
                }
                
                self.trackBuyer(key: ORDER_ACCEPTED)
            }
            break
        case 7:
            // AUTO_DECLINED
            USER_DEFAULTS.set(7, forKey: ORDER_STATUS_ID)
            if userType == SELLER {
                if let room = roomName {
                    self.joinRoom(key: room)
//                    self.emitOrderStatus(name: room, id: AUTO_DECLINED_ID)
                    self.emitOrderStatus(name: room, id: AUTO_DECLINED_ID, orderStatus: AUTO_DECLINED)

                }
            }
            
            if userType == BUYER {
                _ = self.receiveOrderStatus(orderStatus: CANCELLED)
            }
            
            self.leaveAndDisconnectSocket(room: roomName!)
            break
        case 8:
            // CANCELLED
            USER_DEFAULTS.set(8, forKey: ORDER_STATUS_ID)
            if userType == BUYER {
                if let room = roomName {
                    self.joinRoom(key: room)
                    self.emitOrderStatus(name: room, id: CANCELLED_ID, orderStatus: CANCELLED)
                    //self.emitOrderStatus(name: room, id: CANCELLED_ID)
                }
            }
            
//            if userType == SELLER {
//               _ = self.receiveOrderStatus(orderStatus: CANCELLED)
//            }
            
            self.leaveAndDisconnectSocket(room: roomName!)
            break
        case 9:
            // RECEIVED
            USER_DEFAULTS.set(9, forKey: ORDER_STATUS_ID)
            if userType == SELLER {
                if let room = roomName {
                    self.emitOrderStatus(name: room, id: RECEIVED_ID, orderStatus: RECEIVED)
                }

            }
            
            _ = self.receiveOrderStatus(orderStatus: RECEIVED)
            self.leaveAndDisconnectSocket(room: roomName!)

            
            break
        default:
            break
        }
        
    }
    
    func emitOrderStatus(name: String, id: Int, orderStatus: String) {
        //let socketL = manager.socket(forNamespace: "/games")
        let dictionary = [
            "status_id": id,
            "name": orderStatus,
            "orderNumber": name
            ] as [String : Any]
        socket.emit("orderStatus", with: [dictionary])
 
    }
    
    func emitOrderStatus(name: String, id: Int) {
        //let socketL = manager.socket(forNamespace: "/games")
        let dictionary = [
            "status_id": id,
            "name": "pinVerified",
            "orderNumber": name
            ] as [String : Any]
        socket.emit("orderStatus", with: [dictionary])
        
//        if id == 6 {
//            let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
//
//            if userType == BUYER {
//                eventsHandleInSocket(roomName: name, id: id)
//            }
//        }
        
//        if id == RECEIVED_ID {
//            let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
//            
//            self.trackBuyer(key: RECEIVED)
////
////            self.refreshOrderDetailScreenDelegate?.refreshOrderDetailScreenSocket()
////
//            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "refreshScreen"),object: nil))
//        }
    }
    
    fileprivate func receiveOrderStatus(orderStatus: String) -> UUID {
        return socket.on(orderStatus) { (dataArray, ack) in
            
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "refreshScreen"),object: nil))
           
            print("Receive Msg Connected***   \(dataArray)")
        }
    }
    
    func trackBuyer(key: String) {
        //let socketL = manager.socket(forNamespace: "/games")
        socket.on("trackBuyer") { (dataArray, ack) in
           // print("Receive Msg Connected***   \(dataArray.count)")
            self.socketDelegate?.getLatLngFromSocket(latLongArray: dataArray)
            //print(dataArray.count)
        }
        
    }
    
    func emitTheUserLocation(latitude: String, longitude: String, angle: String, keyID: Int?) {
        
        //let socketL = manager.socket(forNamespace: "/games")
        
        if keyID != nil {
            let dictionary = [
                "lat": latitude,
                "lng": longitude,
                //"angle": angle,
                "status_id": keyID!
                ] as [String : Any]
            socket.emit("tracking", with: [dictionary])
        }
        
       // eventsHandleInSocket(roomName: "", id: keyID)
    }
    
    
//    func eventsHandleInSocketback(key: String, roomName: String?) {
//        //let socketL = manager.socket(forNamespace: "/games")
//
//
//        socket.on(key) { (dataArray, ack) in
//            switch key {
//            case ORDER_CREATED:
//                if USER_TYPE == BUYER {
//                    self.connectSocket()
//                    if roomName != "" {
//                        self.joinRoom(key: roomName!)
//                    }
//                }
//
//                break
//            case PAYMENT_PROCESSING:
//                break
//            case PAYMENT_FAILED:
//                break
//            case PAYMENT_COMPLETED:
//                break
//            case ORDER_REJECTED:
//                self.leaveAndDisconnectSocket(room: key)
//                break
//            case ORDER_ACCEPTED:
//                if USER_TYPE == BUYER {
//                    LocationManagerClass.locationSharedObj.startUpdateLocation(roomName: ORDER_ACCEPTED)
//                } else if USER_TYPE == SELLER {
//                    if let room = roomName {
//                        self.joinRoom(key: room)
//                    }
//
//                    self.trackBuyer(key: ORDER_ACCEPTED)
//                }
//                break
//            case AUTO_DECLINED:
//                self.leaveAndDisconnectSocket(room: key)
//
//                break
//            case CANCELLED:
//                self.leaveAndDisconnectSocket(room: key)
//
//                break
//            case RECEIVED:
//                self.leaveAndDisconnectSocket(room: key)
//                break
//            default:
//                break
//
//            }
//
//            //            print("Receive Msg Connected***   \(dataArray.count)")
//            //            self.socketDelegate?.getLatLngFromSocket(latLongArray: dataArray)
//            //print(dataArray.count)
//        }
//    }
    
    
//    func emitOrderStatusL(obj: obj1) {
//
//        //let socketL = manager.socket(forNamespace: "/games")
//        let dictionary = [
//            "status_id": obj.id,
//            "name": obj.name,
//            ] as [String : Any]
//        socket.emit("orderStatus", with: [dictionary])
//
//    }
    
//    func reciveMsgFromRoom() {
//        let socketL = manager.socket(forNamespace: "/orders")
//        socketL.on("joinRoom") { (dataArray, ack) in
//            print("Joining Room...: ")
//
//        }
//    }
//
//
//    func receiveTheUserLocation(key: String) {
//
//        socket.on(key) { (dataArray, ack) in
//            print("Receive Msg Connected***   \(dataArray.count)")
//            //self.socketDelegate?.getLatLngFromSocket(latLongArray: dataArray)
//            //print(dataArray.count)
//        }
//    }
}



// MARK: UIApplication extensions

extension UIApplication {
    
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
