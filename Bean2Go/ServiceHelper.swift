//
//  ServiceHelper.swift
//  Bean2Go
//
//  Created by Aks on 11/12/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import KRProgressHUD

enum MethodType: CGFloat {
    case get  = 0
    case post  = 1
    case put  = 2
    case delete  = 3
}

// Test Url
//let baseURL = "http://10.0.0.169:8000/api/"        // Local Test URL
//let paymentRedirectBaseURL = "http://10.0.0.169:8000/"  // Notify Url Test
//let socketTestUrl = "http://10.0.0.169:3000"  // Test Socket URL

// Staging Url

//let baseURL = "http://13.126.113.25/api/"          // Staging Test Url
//let paymentRedirectBaseURL = "http://13.126.113.25/"  // Notify Url Staging

// Live Url
let paygateBaseURL = "https://secure.paygate.co.za/"
let paymentRedirectBaseURL = "http://www.bean2go.co.za/" //Notify Url Live
let baseURL = "http://www.bean2go.co.za/api/"  // Live URL
//let socketLiveUrl = "http://13.126.113.25:3000"  // Live Socket URL

let headers = [
    "Content-Type": "text/xml",
    "Cache-Control": "no-cache"
]

var SESS_TOKEN = ""

class ServiceHelper: NSObject {
    
    class var sharedInstance: ServiceHelper {
        struct Static {
            static let instance: ServiceHelper = ServiceHelper()
        }
        return Static.instance
    }
    
    func
        callAPIWithParameter(_ paraDict: [String: Any], method:HTTPMethod, apiName :String, completionBlock: @escaping (AnyObject?, NSError?,String?,String?) -> Void) {
        
                
        if (isReachable() == false) {
            indicatorViewObj.hideIndicatorView()
            // _ = AlertController.alert("", message: "Internet connection seems offline. Please check your internet connection.")
            let error = NSError()
            
            completionBlock(nil,error,nil, nil)
            return
        }
        
        SESS_TOKEN = USER_DEFAULTS.value(forKey: Auth_Token) as? String ?? ""
       
        //let url = URL(string: "\(baseURL)\(apiName)?token=\(SESS_TOKEN)")
        let url = URL(string: "\(baseURL)\(apiName)")
        //let url = URL(string: "\(baseURL)\(apiName)")
        //print(url)
        //        Alamofire.request(url ?? "", method: method, parameters: paraDict, encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json", "Authorization" : "Bearer \(SESS_TOKEN)"]).responseJSON { response in
        
        Alamofire.request(url ?? "", method: method, parameters: paraDict, encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json","Authorization" : "Bearer  \(SESS_TOKEN)"]).responseJSON { response in
            
            indicatorViewObj.hideIndicatorView()
            
            LogInfo("paraDict    \(paraDict)")
            LogInfo("response    \(response)")
            LogInfo("response.response?.statusCode as Any     \(response.response?.statusCode as Any)")
            let error = response.error
            
            if error != nil {
                completionBlock(nil,error as NSError?,nil, nil)
                return
            } else {
                
                guard let result = response.result.value else{
                    return
                }
                //to get status code
                if let status = response.response?.statusCode {
                    switch(status) {
                    case 200:
                        
                        
                        completionBlock(result as AnyObject?,nil,nil, nil)
                        break
                        
                    case 422:
                        
                        let JSON = result as! NSDictionary
                        let errorsDict: NSDictionary = JSON["errors"] as! NSDictionary
                        //LogInfo(errorsDict)
                        for error in errorsDict {
//                            LogInfo(error.key)
//                            LogInfo(error.value)
                            
                            let errorMsgArray: NSArray = (error.value as? NSArray)!
                            
                            for msg in errorMsgArray {
                                //LogInfo(msg)
                                completionBlock(nil,nil,msg as? String ?? nil, nil)    // valid reason (user mistake)
                                break
                            }
                            break
                        }
                        
                        //completionBlock(nil,nil)
                        break
                        
                        //                    case 405:
                        //                        completionBlock(nil,error as NSError?,nil) // kuldeep sir problem
                        //                        break
                    //
                    case 401:
                        let JSON = result as! NSDictionary
                        let msg: String = JSON["message"] as! String
                        _ = ConveienceClass.clearAllDefaultValue()
                        completionBlock(nil,nil,msg, "401") // unauthentication
                        break
                        //
                        //                    case 500:
                        //                        completionBlock(nil,error as NSError?,nil) // Anshuman prob
                        //                        break
                        //
                    //
                    case 201:
                        let JSON = result as! NSDictionary
                        let msg: String = JSON["message"] as! String
                        _ = ConveienceClass.clearAllDefaultValue()
                        completionBlock(nil,nil,msg, "201") // unauthoried
                        break
                        //
                        //                    case 500:
                        //                        completionBlock(nil,error as NSError?,nil) // Anshuman prob
                        //                        break
                        //
                    //
                    default:
                        //LogInfo(status)
                        completionBlock(nil,error as NSError?,nil, nil)
                        
                        break
                    }
                    
                }
            }
        }
        
    }
    
    
    func callAPIWithExtraParameter(_ paraDict: [String: Any], method:HTTPMethod, apiName :String, completionBlock: @escaping (AnyObject?, NSError?,String?,String?) -> Void) {
        
        
        //indicatorViewObj.hideIndicatorView()
        
        if (isReachable() == false) {
            indicatorViewObj.hideIndicatorView()
            // _ = AlertController.alert("", message: "Internet connection seems offline. Please check your internet connection.")
            let error = NSError()
            
            completionBlock(nil,error,nil, nil)
            return
        }
        
        SESS_TOKEN = USER_DEFAULTS.value(forKey: Auth_Token) as? String ?? ""
        
        // SESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC93d3cuYmVhbjJnby5ha3NpbnRlcmFjdGl2ZS5jb21cL2FwaVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1NDQ3MDYzMzQsImV4cCI6MTU0NDg4NjMzNCwibmJmIjoxNTQ0NzA2MzM0LCJqdGkiOiJPano4YWtiajJrdkxSbVF6Iiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.OiHdXuMILaschV9cvOMlYHIteTZfso52S5EUE44Ggw"
        
        
        
        let url = URL(string: "\(baseURL)\(apiName)?token=\(SESS_TOKEN)")
        //let url = URL(string: "\(baseURL)\(apiName)")
        
        //        Alamofire.request(url ?? "", method: method, parameters: paraDict, encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json", "Authorization" : "Bearer \(SESS_TOKEN)"]).responseJSON { response in
        
        Alamofire.request(url ?? "", method: method, parameters: paraDict, encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json", "Content-Type": "application/x-www-form-urlencoded"]).responseJSON { response in
            
            indicatorViewObj.hideIndicatorView()
            
            LogInfo("paraDict    \(paraDict)")
            LogInfo("response    \(response)")
            LogInfo("response.response?.statusCode as Any     \(response.response?.statusCode as Any)")
            let error = response.error
            
            if error != nil {
                completionBlock(nil,error as NSError?,nil, nil)
                return
            } else {
                
                guard let result = response.result.value else{
                    return
                }
                //to get status code
                if let status = response.response?.statusCode {
                    switch(status) {
                    case 200:
                        
                        
                        completionBlock(result as AnyObject?,nil,nil, nil)
                        break
                        
                    case 422:
                        let JSON = result as! NSDictionary
                        let errorsDict: NSDictionary = JSON["errors"] as! NSDictionary
                        LogInfo(errorsDict)
                        for error in errorsDict {
                            LogInfo(error.key)
                            LogInfo(error.value)
                            
                            let errorMsgArray: NSArray = (error.value as? NSArray)!
                            
                            for msg in errorMsgArray {
                                LogInfo(msg)
                                completionBlock(nil,nil,msg as? String ?? nil, nil)    // valid reason (user mistake)
                                break
                            }
                            break
                        }
                        
                        //completionBlock(nil,nil)
                        break
                        
                        //                    case 405:
                        //                        completionBlock(nil,error as NSError?,nil) // kuldeep sir problem
                        //                        break
                    //
                    case 401:
                        let JSON = result as! NSDictionary
                        let msg: String = JSON["message"] as! String
                        
                        completionBlock(nil,nil,msg, "401") // unauthoried
                        break
                        //
                        //                    case 500:
                        //                        completionBlock(nil,error as NSError?,nil) // Anshuman prob
                        //                        break
                        //
                    //
                    default:
                        LogInfo(status)
                        completionBlock(nil,error as NSError?,nil, nil)
                        
                        break
                    }
                    
                }
            }
        }
        
    }
    
}


func isReachable() -> Bool {
    let networkReachability = Reachability.forInternetConnection()
    let networkStatus = networkReachability?.currentReachabilityStatus()
    if networkStatus == NotReachable {
        return false
    }
    else {
        return true
    }
    
}

// MARK:- Paygate Methods
extension ServiceHelper{
    
    func callPaygateApi(parms:String,Url:String,Success:@escaping (_ responseObject:AnyObject?,_ ResponseString:String) -> (),Failure: @escaping (_ error:NSError?) -> (),showLoader isShowDefaultLoader:Bool, hideLoader isHideDefaultLoader:Bool){
        
        
        
        
        
        
        KRProgressHUD.show()
        
        
        let postData: Data? = parms.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        let request = NSMutableURLRequest(url: NSURL(string: self.getFullUrl(str: Url))! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
                
        request.setValue(postData?.count.description, forHTTPHeaderField: "Content-Length")
        //request.setValue("999", forHTTPHeaderField: "Content-Length")

        //print("URL = \(self.getFullUrl(str: Url))")
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            KRProgressHUD.dismiss()
            if (error != nil) {
                Failure(error! as NSError)
                //print(error ?? "Error fetching data")
            } else {
                let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                Success(response,strData! as String)
                //print("Body: \(strData ?? "")")
            }
        })
        dataTask.resume()
    }
    
    func getFullUrl(str:String) -> String{
        return paygateBaseURL + str
    }
}


