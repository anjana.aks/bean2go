//
//  AES Encryption.swift
//  Bean2Go
//
//  Created by Anjana Aks on 27/03/19.
//  Copyright © 2019 AKSInteractive. All rights reserved.
//

import Foundation
import CommonCrypto
import CryptoSwift

extension String {
    
    func aesEncrypt() throws -> String {
        let encrypted = try AES(key: "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw", iv: "8119745113154120", padding: .pkcs7).encrypt([UInt8](self.data(using: .utf8)!))
        return Data(encrypted).base64EncodedString()
    }
    
    func aesDecrypt() throws -> String {
        guard let data = Data(base64Encoded: self) else { return "" }
        let decrypted = try AES(key: "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw", iv: "8119745113154120", padding: .pkcs7).decrypt([UInt8](data))
        return String(bytes: Data(decrypted).bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    


    
    /*func aesEncrypt() throws -> String {
        
        var result = ""
        
        do {
            
            let aes = try! AES.init(key: AESEncryptionClass.AESSharedObj.secretKeyInstance, iv: AESEncryptionClass.AESSharedObj.initializationVector, padding: .pkcs5) // AES128 .ECB pkcs7
            let encrypted = try aes.encrypt(Array(self.utf8))
            
            result = encrypted.toBase64()!
            
            print("AES Encryption Result: \(result)")
            
        } catch {
            
            print("Error: \(error)")
        }
        
        return result
    }
    
    func aesDecrypt() throws -> String {
        
        var result = ""
        
        do {
            
            let encrypted = self
            
            let aes = try! AES.init(key: AESEncryptionClass.AESSharedObj.secretKeyInstance, iv: AESEncryptionClass.AESSharedObj.initializationVector, padding: .pkcs5) // AES128 .ECB pkcs7
            let decrypted = try aes.decrypt(Array(base64: encrypted))
            
            result = String(data: Data(decrypted), encoding: .utf8) ?? ""
            
            print("AES Decryption Result: \(result)")
            
        } catch {
            
            print("Error: \(error)")
        }
        
        return result
    }*/
}

/*struct CBCModeWorker: BlockModeWorker {
    let cipherOperation: CipherOperationOnBlock
    var blockSize: Int
    let additionalBufferSize: Int = 0
    private let iv: ArraySlice<UInt8>
    private var prev: ArraySlice<UInt8>?
    
    
    
    var pswdIterations: Int = 10
    var keySize: Int = 128
    var cypherInstance: String = "AES/CBC/PKCS5Padding"
    var secretKeyInstance: String = "PBKDF2WithHmacSHA1"
    var plainText: String = "sampleText"
    var AESSalt: String = "exampleSalt"
    var initializationVector: String = "8119745113154120"
    
    init(blockSize: Int, iv: ArraySlice<UInt8>, cipherOperation: @escaping CipherOperationOnBlock) {
        self.blockSize = blockSize
        self.iv = iv
        self.cipherOperation = cipherOperation
    }
    
//    mutating func encrypt(block plaintext: ArraySlice<UInt8>) -> Array<UInt8> {
//        guard let ciphertext = cipherOperation(xor(prev ?? iv, plaintext)) else {
//            return Array(plaintext)
//        }
//        prev = ciphertext.slice
//        return ciphertext
//    }
    
//    mutating func decrypt(block ciphertext: ArraySlice<UInt8>) -> Array<UInt8> {
//        guard let plaintext = cipherOperation(ciphertext) else {
//            return Array(ciphertext)
//        }
//        let result: Array<UInt8> = xor(prev ?? iv, plaintext)
//        prev = ciphertext
//        return result
//    }
}*/


class AESEncryptionClass: NSObject {
    
    static var AESSharedObj = AESEncryptionClass()
    
    var pswdIterations: Int = 10
    var keySize: Int = 128
    var cypherInstance: String = "AES/CBC/PKCS5Padding"
    var secretKeyInstance: String = "PBKDF2WithHmacSHA1"
    var plainText: String = "sampleText"
    var AESSalt: String = "exampleSalt"
    var initializationVector: String = "8119745113154120"
    
    
    

    
    
//    func aesEncrypt(textToEncrypt: String) throws -> String {
//        let data = textToEncrypt.data(using: String.Encoding.utf8)
//        //self.dataUsingEncoding(NSUTF8StringEncoding)
//        let encrypted = try AES(key: self.secretKeyInstance, iv: initializationVector, padding: .pkcs7).encrypt((data?.bytes)!)
//        let encData = Data(bytes: encrypted, count: encrypted.count)
//        let base64str = encData.base64EncodedString(options: .init(rawValue: 0))
//        let result = String(base64str)
//        return result
//    }
//
//    func aesDecrypt(textToDncrypt: String) throws -> String {
//
//        if let data = Data(base64Encoded: textToDncrypt) {
//            let decrypted = try! AES(key: self.secretKeyInstance, iv: initializationVector, padding: .pkcs7).decrypt([UInt8](data))
//            let decryptedData = Data(decrypted)
//            return Strng(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
//        }
//        return "Could not decrypt"
//    }
    
    
    func aesEncrypt(textToEncrypt: String) throws -> String{
        let data = textToEncrypt.data(using: String.Encoding.utf8)
        let enc = try AES(key: secretKeyInstance, iv: initializationVector, padding: .pkcs5).encrypt((data?.bytes)!)
        let encData = NSData(bytes: enc, length: Int(enc.count))
        let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0));
        let result = String(base64String)
        return result
    }
    
    func aesDecrypt(textToDncrypt: String) throws -> String {
        let data = NSData(base64Encoded: textToDncrypt, options: NSData.Base64DecodingOptions(rawValue: 0))
        
//        let dataq = NSData(base64Encoded: textToDncrypt, options: NSData.Base64DecodingOptions(rawValue: 0))
        let dec = try AES(key: secretKeyInstance, iv: initializationVector, padding: .pkcs5).decrypt([UInt8](data! as Data))
        let decData = NSData(bytes: dec, length: Int(dec.count))
        let result = NSString(data: decData as Data, encoding: String.Encoding.utf8.rawValue)
        return String(result!)
    }
    
}




protocol Randomizer {
    static func randomIv() -> Data
    static func randomSalt() -> Data
    static func randomData(length: Int) -> Data
}

protocol Crypter {
    func encrypt(_ digest: Data) throws -> Data
    func decrypt(_ encrypted: Data) throws -> Data
}

struct AES256Crypter {
    
    private var key: Data
    private var iv: Data
    
    public init(key: Data, iv: Data) throws {
        guard key.count == kCCKeySizeAES256 else {
            throw Error.badKeyLength
        }
        guard iv.count == kCCBlockSizeAES128 else {
            throw Error.badInputVectorLength
        }
        self.key = key
        self.iv = iv
    }
    
    enum Error: Swift.Error {
        case keyGeneration(status: Int)
        case cryptoFailed(status: CCCryptorStatus)
        case badKeyLength
        case badInputVectorLength
    }
    
    private func crypt(input: Data, operation: CCOperation) throws -> Data {
        var outLength = Int(0)
        var outBytes = [UInt8](repeating: 0, count: input.count + kCCBlockSizeAES128)
        var status: CCCryptorStatus = CCCryptorStatus(kCCSuccess)
        input.withUnsafeBytes { (encryptedBytes: UnsafePointer<UInt8>!) -> () in
            iv.withUnsafeBytes { (ivBytes: UnsafePointer<UInt8>!) in
                key.withUnsafeBytes { (keyBytes: UnsafePointer<UInt8>!) -> () in
                    status = CCCrypt(operation,
                                     CCAlgorithm(kCCAlgorithmAES128),            // algorithm
                        CCOptions(kCCOptionPKCS7Padding),           // options
                        keyBytes,                                   // key
                        key.count,                                  // keylength
                        ivBytes,                                    // iv
                        encryptedBytes,                             // dataIn
                        input.count,                                // dataInLength
                        &outBytes,                                  // dataOut
                        outBytes.count,                             // dataOutAvailable
                        &outLength)                                 // dataOutMoved
                }
            }
        }
        guard status == kCCSuccess else {
            throw Error.cryptoFailed(status: status)
        }
        return Data(bytes: UnsafePointer<UInt8>(outBytes), count: outLength)
    }
    
    static func createKey(password: Data, salt: Data) throws -> Data {
        let length = kCCKeySizeAES256
        var status = Int32(0)
        var derivedBytes = [UInt8](repeating: 0, count: length)
        password.withUnsafeBytes { (passwordBytes: UnsafePointer<Int8>!) in
            salt.withUnsafeBytes { (saltBytes: UnsafePointer<UInt8>!) in
                status = CCKeyDerivationPBKDF(CCPBKDFAlgorithm(kCCPBKDF2),                  // algorithm
                    passwordBytes,                                // password
                    password.count,                               // passwordLen
                    saltBytes,                                    // salt
                    salt.count,                                   // saltLen
                    CCPseudoRandomAlgorithm(kCCPRFHmacAlgSHA1),   // prf
                    10000,                                        // rounds
                    &derivedBytes,                                // derivedKey
                    length)                                       // derivedKeyLen
            }
        }
        guard status == 0 else {
            throw Error.keyGeneration(status: Int(status))
        }
        return Data(bytes: UnsafePointer<UInt8>(derivedBytes), count: length)
    }
    
}

extension AES256Crypter: Crypter {
    
    func encrypt(_ digest: Data) throws -> Data {
        return try crypt(input: digest, operation: CCOperation(kCCEncrypt))
    }
    
    func decrypt(_ encrypted: Data) throws -> Data {
        return try crypt(input: encrypted, operation: CCOperation(kCCDecrypt))
    }
    
}

extension AES256Crypter: Randomizer {
    
    static func randomIv() -> Data {
        return randomData(length: kCCBlockSizeAES128)
    }
    
    static func randomSalt() -> Data {
        return randomData(length: 8)
    }
    
    static func randomData(length: Int) -> Data {
        var data = Data(count: length)
        let status = data.withUnsafeMutableBytes { mutableBytes in
            SecRandomCopyBytes(kSecRandomDefault, length, mutableBytes)
        }
        assert(status == Int32(0))
        return data
    }
    
}






