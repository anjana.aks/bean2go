//
//  calendarHelper.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 26/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import Foundation

var dayArray = ["Sun","Mon","Tue","Wed","Thur","Fri","Sat"]

var monthListArray = ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec"]

var date = Date()
let calendar = Calendar.current

var day = calendar.component(.day, from: date)
var weekday = calendar.component(.weekday, from: date)
var month = calendar.component(.month, from: date) - 1
var year = calendar.component(.year, from: date)

func getDateInString(selectedDate: Date) -> String {
    var selectedDateInString = ""
    let day = calendar.component(.day, from: selectedDate)
    let weekday = calendar.component(.weekday, from: selectedDate)
    let month = calendar.component(.month, from: selectedDate) - 1
    selectedDateInString = dayArray[weekday-1] + ", " + monthListArray[month] + " \(day)"
    return selectedDateInString
}

func getSelectedDay(date: Date) -> Int {
    var day: Int = 0
    day = calendar.component(.day, from: date)
    return day
}

func getSelectedWeekDay(date: Date) -> Int {
    
    var weekDay: Int = 0
    weekDay = calendar.component(.weekday, from: date)
    return weekDay
}

func getSelectedMonth(date: Date) -> Int {
    
    var month: Int = 0
    month = calendar.component(.month, from: date)
    return month
}

func getSelectedYear(date: Date) -> Int {
    
    var year: Int = 0
    year = calendar.component(.year, from: date)
    return year
}
