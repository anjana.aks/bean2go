//
//  CalendarViewController.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 26/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit

protocol  calendarDelegate {
    func calendarAction(move: Bool)
}
class CalendarViewController: UIViewController ,JBDatePickerViewDelegate{
    
    
    var isFromSegment = Int()
    var selectedDate = Date()
    var selectedSegment = Int()
    var dateToSelect: Date!
    
    var delegate: calendarDelegate!
    var dayArray = ["Sun","Mon","Tue","Wed","Thur","Fri","Sat"]

    var monthListArray = ["Jan",
                          "Feb",
                          "Mar",
                          "Apr",
                          "May",
                          "June",
                          "July",
                          "Aug",
                          "Sept",
                          "Oct",
                          "Nov",
                          "Dec"]
    
    //MARK:- Outlets
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomStackView: UIStackView!
    
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var leftArrowButton: UIButton!
    @IBOutlet weak var rightArrowButton: UIButton!
    @IBOutlet weak var DatePickercalendarView: JBDatePickerView!    //Calendar
    @IBOutlet weak var calendarView: UIView!                    //Calendar dialogBox SuperView
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var placeOrderButton: UIButton!
    
    

    //MARK:- Defaults
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.calendarView.layer.cornerRadius = 4
        self.bottomView.layer.cornerRadius = 2
        self.bottomStackView.layer.cornerRadius = 2
        self.cancelButton.layer.cornerRadius = 2
        self.placeOrderButton.layer.cornerRadius = 2
        
       // DatePickercalendarView.week
        
        self.yearLabel.text = String(year)
        self.dayLabel.text =  dayArray[weekday-1] + ", " + monthListArray[month-1] + " \(day)"
        DatePickercalendarView.delegate = self
        monthLabel.text = DatePickercalendarView.presentedMonthView?.monthDescription
        
    }

    override func viewDidAppear(_ animated: Bool) {
        self.calendarView.shadowApply(shadowRadius: 10)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        monthLabel.text = DatePickercalendarView.presentedMonthView.monthDescription
        
    }
    
    func didSelectDay(_ dayView: JBDatePickerDayView) {
        
        selectedDate = DatePickercalendarView.selectedDateView.date!
        self.dayLabel.text =  dayArray[weekday-1] + ", " + monthListArray[month] + " \(day)"
    }
    
    //MARK:- Button Action
    
    @IBAction func backButtonDidTapped(_ sender: UIButton)
    {
        DatePickercalendarView.loadPreviousView()
    }
    @IBAction func nextButtonDidTapped(_ sender: UIButton)
    {
        DatePickercalendarView.loadNextView()
    }
    
    @IBAction func cancelButtonDidTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        self.delegate?.calendarAction(move: false)
    }
    
    @IBAction func placeOrderButtonDidTapped(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
        self.delegate?.calendarAction(move: true)

    }
    
    @IBAction func backgroundButtonDidTapped(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
}
