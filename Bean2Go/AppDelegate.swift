//
//  AppDelegate.swift
//  Bean2Go
//
//  Created by Vivek Godiwal on 17/09/18.
//  Copyright © 2018 AKSInteractive. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import Firebase
import FBSDKCoreKit
import GoogleSignIn
import Alamofire
import UserNotifications
import SocketIO
import IQKeyboardManagerSwift
//import Fabric
//import Crashlytics

var FCM_TOKEN = "Fcm_Token"
var DEVICE_TOKEN = "DeviceToken"

let kAppDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    static var appdelegateObj = AppDelegate()
    var window: UIWindow?
    var nav = UINavigationController()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if (launchOptions?[UIApplication.LaunchOptionsKey.url] as? URL) != nil {
            // some
        }
//        USER_DEFAULTS.set("0dcce5d89f1a10dc2205a4a4660c2c085b2bc4d95a24cf80c3f348b04ec07554", forKey: DEVICE_TOKEN)
        
        // Hide navigation bar
        nav.isNavigationBarHidden = true
        // Check user is already logged in or not
        
        let isLogedIn = USER_DEFAULTS.bool(forKey: IS_LOGGEDIN)
        if isLogedIn {
            callLoginApi()
        }
        
        // Fabric.with([Crashlytics.self])
        
        // Integrate Push notification
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        _ = application.isRegisteredForRemoteNotifications
        //print(isRemoteNotificationRegistered)
        
        // Firefabase configuration
        FirebaseApp.configure()
        
        // Gmail Login register cleint id
        _ = GmailLoginClass.init()
        
        // GMS Service
        GMSServices.provideAPIKey("AIzaSyD71EOMcYPgpUlI6qOe7LtWm6PC3hc_Rac")
        
        // FB Sdk
        FBSDKApplicationDelegate.sharedInstance().application(application,didFinishLaunchingWithOptions: launchOptions)
        
        // Set Status Bar Color
        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusbar.backgroundColor = STATUS_BAR_COLOR
        }
        
        self.connectToFCM()
        
        // Set Navigation Bar Font Name ANd Size
        setNavigationBarTitleFontAndSize()
        
        IQKeyboardManager.shared.enable = true
        
        //GIDSignIn.sharedInstance().clientID = "100483755699-r9f2gpphiu8d2ng24vc5jqsnk0til4e2.apps.googleusercontent.com"
        //GIDSignIn.sharedInstance().delegate = self as! GIDSignInDelegate
        //GMSPlacesClient.provideAPIKey("AIzaSyBdVl-cTICSwYKrZ95SuvNw7dbMuDt1KG0")
        //Override point for customization after application launch.
        
        return true
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        
        USER_DEFAULTS.setValue(token, forKey: DEVICE_TOKEN)
        
        Messaging.messaging().apnsToken = deviceToken as Data
        
        self.connectToFCM()
        
        //        if let refreshedToken = InstanceID.instanceID().token() {
        //            USER_DEFAULTS.setValue(token, forKey: FCM_TOKEN)
        //            print("InstanceID token: \(refreshedToken)")
        //        }
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        //print(remoteMessage.appData)
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
       // print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
        
       // print("deviceToken:\(deviceToken)")
    }
    
    fileprivate func connectToFCM() {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                USER_DEFAULTS.setValue(result.token, forKey: FCM_TOKEN)
            }
        }
    }
    
    //    //MARK:- Gmail Integration
    //    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
    //        // ...
    //        if let error = error {
    //            // ...
    //            return
    //        }
    //        print(user.profile.name)
    //        guard let authentication = user.authentication else { return }
    //        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
    //                                                       accessToken: authentication.accessToken)
    //        // ...
    //    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        // Add any custom logic here.
        return handled
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = false
        connectSocket()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFCM()
        connectSocket()
    }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        connectToFCM()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound, .badge])
        
//        let notiCount: Int = USER_DEFAULTS.value(forKey: NOTIFICATION_COUNT) as? Int ?? 0
//        
//        UIApplication.shared.applicationIconBadgeNumber = notiCount
//        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "com.DouglasDevlops.BadgeWasUpdated"), object: nil)
        
        
        let userInfo = notification.request.content.userInfo
                
        // Print full message.
        print(userInfo)
        
//        return
        
//        if let topVC = UIApplication.getTopViewController() {
//            if topVC.isKind(of: orderDetailsViewController.self) {
//                let obj = orderDetailsViewController()
//                obj.callAPItoGetOrderDetail()
//            }
//        }
        
        guard let isRefresh = userInfo["isRefresh"] as? String else {
            return
        }
        
        guard let orderId = userInfo[AnyHashable("order_id")] as? String else {
            return
        }
        
        guard let orderNumber = userInfo[AnyHashable("order_number")] as? String else {
            return
        }
        
        guard let role = userInfo[AnyHashable("role")] as? String else {
            return
        }
        
        guard let notiId = userInfo[AnyHashable("notiId")] as? String else {
            return
        }
        
        if isRefresh == "1" {
            if role == BUYER {
                //                let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "orderDetailsViewController") as! orderDetailsViewController
                
                var dict = [String: Any]()
                dict["orderNumber"] = orderNumber
                dict["orderID"] = Int(orderId) ?? 0
                dict["notiId"] = notiId
                
                print(dict)
                
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "refreshBuyerOrderDetail"),object: dict))
               
            } else {
//                let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "sellerOrderDetailVC") as! sellerOrderDetailVC
//                nVC.orderNumber = orderNumber
//                nVC.orderID = Int(orderId) ?? 0
//                nav.pushViewController(nVC, animated: true)
            }
        }
    }
    
    // And When user tap on notification Following method is called...
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        //print(userInfo)
        
        //        guard let userType = userInfo[AnyHashable("role")] as? String else {
        //            return
        //        }
        
        guard let orderId = userInfo[AnyHashable("order_id")] as? String else {
            return
        }
        
        guard let orderNumber = userInfo[AnyHashable("order_number")] as? String else {
            return
        }
        
        guard let role = userInfo[AnyHashable("role")] as? String else {
            return
        }
        
        //        let orderDict = NSMutableDictionary()
        //        orderDict["orderId"] = orderId as? String
        //        orderDict["orderNumber"] = orderNumber as? String
        //        orderDict["role"] = role
        
        if role == BUYER {
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "orderDetailsViewController") as! orderDetailsViewController
            nVC.orderNumber = orderNumber
            nVC.orderID = Int(orderId) ?? 0
            nav.pushViewController(nVC, animated: true)
        } else {
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "sellerOrderDetailVC") as! sellerOrderDetailVC
            nVC.orderNumber = orderNumber
            nVC.orderID = Int(orderId) ?? 0
            nav.pushViewController(nVC, animated: true)
        }
        
        
        
        //        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openOrderDetailPage"), object: nil, userInfo: orderDict as? [AnyHashable : Any])
        //        let aps = userInfo[AnyHashable("aps")] as? NSDictionary
        //        print(aps!)
        //        let alert = aps!["alert"] as? NSDictionary
        //        print(alert!)
        //        let body = alert![AnyHashable("body")] as? String
        //        let title = alert!["title"] as? String
        //        print(title!)
        //        print(body!)
        //        let jsonData = body?.data(using: .utf8)!
        //        let json = try! JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! NSDictionary
        //        print(json)
        //        let notificationId = json["notificationId"] as! String
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
        -> Bool {
            
            //print(url)
            
            if let scheme = url.scheme, scheme.localizedCaseInsensitiveCompare("com.bean2go") == .orderedSame {
                let view = url.host
                //                url.relativeString
                //
                
                url.lastPathComponent
                if let lastcomponent: String = url.lastPathComponent {
                    if view == "ShopViewController" {
                        var dict = [String: Int]()
                        dict["storeId"] = Int(lastcomponent)
                        
                        
                        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ShopViewController") as! ShopViewController
                        nVC.storeId = Int(lastcomponent)
                        nav.pushViewController(nVC, animated: false)
                        
                        
                        //NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "openShopViewController"),object: dict))
                    }
                }
                
                //let allPathComponenetsAfterHostName = url.pathComponents
                
                if view == "PaymentViewController" {
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "openPaymentsucessViewcontroller"),object: nil))
                    
                } else if view == "cartScheduledViewController" {
                    
                    let comingFrom: String = USER_DEFAULTS.value(forKey: iS_FROM_SCREEN_FOR_CART) as? String ?? ""
                    
                    if comingFrom != DASHBOARD {
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "openMyCartViewController"),object: nil))

                    } else {
                         NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "openMyCartViewControllerFromDash"),object: nil))
                    }
                }
            }
            
            /*if let scheme = url.scheme, scheme.localizedCaseInsensitiveCompare("http://www.example.com") == .orderedSame {
                let view = url.host
                
                if let lastcomponent: String = url.lastPathComponent {
                    if view == "store_detail" {
                        var dict = [String: Int]()
                        dict["storeId"] = Int(lastcomponent)
                        
                        
                        let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "ShopViewController") as! ShopViewController
                        nVC.storeId = Int(lastcomponent)
                        nav.pushViewController(nVC, animated: false)
                    }
                }
            }*/
            
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: [:])
    }
    
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Bean2Go")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK:- Socket connectivity
    
    fileprivate func connectSocket() {
        
        let orderStatusId: Int = USER_DEFAULTS.integer(forKey: ORDER_STATUS_ID)
        let orderNumber: String = USER_DEFAULTS.string(forKey: ORDER_NUMBER) ?? ""
        if orderNumber != "" {
            SocketManagerClass.shared.eventsHandleInSocket(roomName: orderNumber, id: orderStatusId)
            
        }
    }
}

//MARK:- Logout API
extension AppDelegate {
    func logoutAPI(viewController: UIViewController) {
        let paraDict = NSMutableDictionary()
        paraDict["device_id"] = USER_DEFAULTS.value(forKey: DEVICE_TOKEN)
        paraDict["gcm_key"] = USER_DEFAULTS.value(forKey: FCM_TOKEN)
        
        //print(paraDict)
        
        ServiceHelper.sharedInstance.callAPIWithParameter(paraDict as! [String : Any], method: .post, apiName: "auth/logout") { (response, error,message, statusCode) in
            
            if message != nil {
                if statusCode == "401" {
                    
                    let alertController = UIAlertController(title: "", message: "Session expired!!!", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        if self.nav.viewControllers.count > 0 {
                            
                            if ((self.nav.viewControllers.last) != nil) {
                                self.nav.popToRootViewController(animated: false)
                            } else {
                                self.nav.viewControllers.removeAll()
                            }
                        }
                        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                        self.nav.viewControllers = [mainView]
                        
                        self.window!.rootViewController = self.nav
                        self.window!.makeKeyAndVisible()
                    }
                    alertController.addAction(action1)
                    viewController.present(alertController, animated: true, completion: nil)
                    
                    
                } else if statusCode == "201" {
                    
                    let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as!SignUpViewController
                    self.nav.pushViewController(nVC, animated: true)
                    
                }
                
                return
            }else if error != nil || response == nil {
                let alertController = UIAlertController(title: "", message: "Network connection problem", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    //self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action1)
                viewController.present(alertController, animated: true, completion: nil)
                return
            } else {
                let sucess = ConveienceClass.clearAllDefaultValue()
                SocketManagerClass.shared.disconnectSocket()
                
                
                if sucess == true {
                    UIApplication.shared.applicationIconBadgeNumber = 0

                    if self.nav.viewControllers.count > 0 {
                        
                        if ((self.nav.viewControllers.last) != nil) {
                            self.nav.popToRootViewController(animated: false)
                        } else {
                            self.nav.viewControllers.removeAll()
                        }
                    }
                    let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                    self.nav.viewControllers = [mainView]
                    self.window!.rootViewController = self.nav
                    self.window!.makeKeyAndVisible()
                    
                }
            }
        }
    }
}


// MARK:- Login API
extension AppDelegate {
    func callLoginApi() {
        let userType: String = USER_DEFAULTS.value(forKey: USER_TYPE) as! String
        
        if userType == BUYER {
            let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            // connect socket
            self.connectSocket()
            
            nav.viewControllers = [nVC]
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = nav
            
        } else {
            
            let isApproved = USER_DEFAULTS.bool(forKey: IS_SELLER_APPROVED)
            
            if isApproved == false {
                let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "recentOrderViewController") as! recentOrderViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                // connect socket
                self.connectSocket()
                
                nav.viewControllers = [nVC]
                appDelegate.window?.rootViewController = nav
            } else {
                let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "recentOrderViewController") as! recentOrderViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                // connect socket
                self.connectSocket()
                
                nav.viewControllers = [nVC]
                appDelegate.window?.rootViewController = nav
            }
        }
        
        /*let email = USER_DEFAULTS.value(forKey: EMAIL_ADDRESS)
         let password = USER_DEFAULTS.value(forKey: USER_PASSWORD)
         
         let paraDict = NSMutableDictionary()
         
         paraDict["email"] = email
         paraDict["password"] = password
         
         let url = URL(string: "\(baseURL)login")
         
         Alamofire.request(url!, method: .post, parameters: paraDict as? [String: Any], encoding: URLEncoding.httpBody, headers: ["Accept" : "application/json"]).responseJSON { response in
         print(response)
         guard let result = response.result.value else{
         return
         }
         let JSON = result as! NSDictionary
         //to get status code
         if let status = response.response?.statusCode {
         switch(status){
         case 200:
         // let data = JSON["data"] as? NSDictionary
         let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         appDelegate.window?.rootViewController = nVC
         
         case 422:
         let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! LogInViewController
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         appDelegate.window?.rootViewController = nVC
         _ = JSON["message"] as? String ?? ""
         // self.present(UIAlertController.alertWithTitle(title: "", message: message , buttonTitle: "OK"), animated: true, completion: nil)
         default:
         let nVC = UIStoryboard(name:"Main" ,bundle: nil).instantiateViewController(withIdentifier: "DashBoardViewController") as! LogInViewController
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         appDelegate.window?.rootViewController = nVC
         print("error with response status: \(status)")
         }
         }
         
         }*/
        
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

//    static func updateRootVC(){
//
//        let status = UserDefaults.standard.bool(forKey: "status")
//        var rootVC : UIViewController?
//
//        print(status)
//
//
//        if(status == true){
//            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarvc") as! DashBoardViewController
//        }else{
//            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginvc") as! LogInViewController
//        }
//
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = rootVC
//
//    }
